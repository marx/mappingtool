package de.kommone.xta3.webservice.konten;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class XtaIdentifierliste {

    @XmlElement(name = "identifier")
    private List<XtaIdentifier> identifierliste = new ArrayList<>();

    public List<XtaIdentifier> getIdentifierliste() {
        return identifierliste;
    }

    public void setIdentifierliste(List<XtaIdentifier> identifierliste) {
        this.identifierliste = identifierliste;
    }

}
