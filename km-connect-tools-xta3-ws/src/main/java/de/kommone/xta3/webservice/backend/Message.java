package de.kommone.xta3.webservice.backend;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Message {

    private Map<String, String> messageAttributes = new HashMap<>();

    private String content;
    
    private String messageId;

    private List<Attachement> attachements;

    public String getAttributeByName(String attrname) {
        return messageAttributes.get(attrname);
    }

    public void putAttributeByName(String attrname, String value) {
        messageAttributes.put(attrname, value);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Map<String, String> getMessageAttributes() {
        return messageAttributes;
    }

    public void setMessageAttributes(Map<String, String> messageAttributes) {
        this.messageAttributes = messageAttributes;
    }

    public List<Attachement> getAttachements() {
        return attachements;
    }

    public void setAttachements(List<Attachement> attachements) {
        this.attachements = attachements;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

}
