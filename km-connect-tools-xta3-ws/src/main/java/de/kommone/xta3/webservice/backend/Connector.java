package de.kommone.xta3.webservice.backend;

import de.kommone.xta3.webservice.konten.XtaKonto;

public interface Connector {

    public Message getMessageFromPostfach(XtaKonto konto);

    public Message getMessageFromPostfachById(XtaKonto konto, String messageId);

    public void sendMessage(XtaKonto konto, Message message);
}
