package de.kommone.xta3.webservice;

import java.security.cert.X509Certificate;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.preauth.x509.X509AuthenticationFilter;

public class APIKeyFilter extends X509AuthenticationFilter {

    private CertificateHolder holder;

    public APIKeyFilter(CertificateHolder holder) {
        this.holder = holder;
        setAuthenticationManager(new AuthenticationManager() {
            @Override
            public Authentication authenticate(Authentication authentication) {
                return authentication;
            }
        });
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        X509Certificate[] certificates = (X509Certificate[]) request.getAttribute(
                "javax.servlet.request.X509Certificate");
        if (certificates != null && certificates.length > 0) {
            X509Certificate cert = certificates[0];

            if (holder.certBelongsToValidAccount(cert)) {
                String alias = holder.getAlias(cert);
                request.setAttribute("konto-id", alias);
            }
            return certificates[0].getSubjectDN().getName();
        }
        return super.getPreAuthenticatedCredentials(request);
    }
}
