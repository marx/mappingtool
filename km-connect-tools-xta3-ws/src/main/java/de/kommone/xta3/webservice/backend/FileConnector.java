package de.kommone.xta3.webservice.backend;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import de.kommone.xta3.webservice.konten.XtaKonto;

@Service
@Qualifier("file")
public class FileConnector implements Connector {

    private static final String FILE = "file:";

    @Override
    public Message getMessageFromPostfach(XtaKonto konto) {
        String directoryName = getDirectoryName(konto.getQueueGetMessage());
        List<File> files = new ArrayList<>();
        try (Stream<Path> paths = Files.walk(Paths.get(directoryName))) {
            paths.filter(Files::isRegularFile).forEach(f -> files.add(f.toFile()));
        } catch (IOException e) {
            // nichts tun
        }
        File nachricht = files.stream().filter(f -> f.getAbsolutePath().endsWith(".xml")).findFirst().orElse(null);
        List<File> anhaenge = files.stream().filter(f -> !f.getAbsolutePath().endsWith(".xml")).collect(Collectors
                .toList());
        if (nachricht == null) {
            return null;
        }
        String content = readStringFromFile(nachricht.getAbsolutePath());
        Message message = new Message();
        message.setContent(content);
        message.setMessageId("1");
        message.setAttachements(getAttachementsFromAnhaenge(anhaenge));
        return message;
    }

    private List<Attachement> getAttachementsFromAnhaenge(List<File> anhaenge) {
        List<Attachement> attachements = new ArrayList<>();
        for (File file : anhaenge) {
            Attachement att = new Attachement();
            try {
                att.setContent(Files.readAllBytes(file.toPath()));
            } catch (IOException e) {
                // nichts tun
            }
            att.setFilename(file.getName());
            att.setType(getType(file.getName()));
            attachements.add(att);
        }
        return attachements;
    }

    private String getType(String name) {
        if(name.lastIndexOf(".") != -1) {
            return name.substring(name.lastIndexOf(".") + 1);
        }
        return name;
    }

    private String getDirectoryName(String queueGetMessage) {
        if (queueGetMessage == null) {
            return null;
        }
        return queueGetMessage.startsWith(FILE) ? queueGetMessage.substring(FILE.length()) : null;
    }

    @Override
    public Message getMessageFromPostfachById(XtaKonto konto, String messageId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void sendMessage(XtaKonto konto, Message message) {
        // TODO Auto-generated method stub

    }

    private String readStringFromFile(String input) {
        try (BufferedReader br = new BufferedReader(new FileReader(input))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
            return sb.toString();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
