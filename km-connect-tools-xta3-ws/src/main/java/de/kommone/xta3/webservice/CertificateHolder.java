package de.kommone.xta3.webservice;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;

import de.kommone.xta3.webservice.konten.XtaIdentifier;
import de.kommone.xta3.webservice.konten.XtaIdentifierliste;
import de.kommone.xta3.webservice.konten.XtaKonten;
import de.kommone.xta3.webservice.konten.XtaKonto;

@Component
public class CertificateHolder {

    private Map<String, String> aliase = new ConcurrentHashMap<>();
    private Map<String, XtaKonto> postfaecher = new ConcurrentHashMap<>();

    private static final String CLASSPATH = "classpath";

    @Value("${server.ssl.trust-store}")
    private String keystoreURL;

    @PostConstruct
    private void postConstruct() {

        XtaKonten xtaKonten = getXtaKontenFromConfig("dzbw.konten.xml");
        KeyStore ks;
        try {
            ks = KeyStore.getInstance("JKS");
            InputStream resourceAsStream = null;
            if (keystoreURL.startsWith(CLASSPATH)) {
                String resourceLocation = keystoreURL.substring(CLASSPATH.length() + 1);
                resourceAsStream = getClass().getClassLoader().getResourceAsStream(resourceLocation);
            }
            ks.load(resourceAsStream, "truststore".toCharArray());
            Enumeration<String> enumeration = ks.aliases();
            while (enumeration.hasMoreElements()) {
                String alias = enumeration.nextElement();
                Certificate cert = ks.getCertificate(alias);
                if (cert instanceof X509Certificate) {
                    aliase.put(new CertUtils().getPublicKeyAsPEM((X509Certificate) cert), alias);
                    XtaKonto xtaKonto = getKontoFromAlias(alias, xtaKonten);
                    if (xtaKonto != null) {
                        postfaecher.put(alias, xtaKonto);
                    } else {
                        // TODO : Log, Konto konnte nicht verarbeitet werden
                    }
                }

            }
        } catch (Exception e) {
            // nichts zu tun, eventuell Fehlerlogging
        }

    }

    private XtaKonto getKontoFromAlias(String alias, XtaKonten xtaKonten) {
        Optional<XtaKonto> filter = xtaKonten.getKonten().stream().filter(konto -> konto.getXtazertifikat().get(0)
                .endsWith(alias)).findFirst();
        return filter.orElse(null);
    }

    private XtaKonten getXtaKontenFromConfig(String kontenDatei) {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(
                // all the classes the context needs to know about
                XtaKonten.class, XtaKonto.class, XtaIdentifier.class, XtaIdentifierliste.class);
        InputStream xml = getClass().getClassLoader().getResourceAsStream(kontenDatei);
        return (XtaKonten) marshaller.unmarshal(new StreamSource(xml));
    }

    public XtaKonto getPostfachInfo(String alias) {
        return postfaecher.get(alias);
    }

    public boolean certBelongsToValidAccount(X509Certificate cert) {
        String publicKey = new CertUtils().getPublicKeyAsPEM(cert);
        return isValidAccount(publicKey);
    }

    private boolean isValidAccount(String key) {
        return aliase.containsKey(key);
    }

    public String getAlias(X509Certificate cert) {
        String publicKey = new CertUtils().getPublicKeyAsPEM(cert);
        return aliase.get(publicKey);
    }

}
