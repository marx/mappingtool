package de.kommone.xta3.webservice.konten;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "xtakonten")
@XmlAccessorType(XmlAccessType.FIELD)
public class XtaKonten {

    @XmlElement(name = "xtakonto")
    private List<XtaKonto> konten = new ArrayList<>();

    public List<XtaKonto> getKonten() {
        return konten;
    }

    public void setKonten(List<XtaKonto> konten) {
        this.konten = konten;
    }

}
