package de.kommone.xta3.webservice.konten;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class XtaKonto {

    @XmlAttribute
    private String id;

    @XmlElement(name = "gesperrt")
    private boolean gesperrt = false;

    @XmlElement(name = "xtazertifikat")
    private List<String> xtazertifikat;

    @XmlElement(name = "identifierliste")
    private XtaIdentifierliste identifierliste;

    @XmlElement(name = "transporterId")
    private String transporterId;

    @XmlElement(name = "queueSendMessage")
    private String queueSendMessage;

    @XmlElement(name = "queueGetMessage")
    private String queueGetMessage;

    @XmlElement(name = "mtomEnabled")
    private boolean mtomEnabled = true;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isGesperrt() {
        return gesperrt;
    }

    public void setGesperrt(boolean gesperrt) {
        this.gesperrt = gesperrt;
    }

    public List<String> getXtazertifikat() {
        return xtazertifikat;
    }

    public void setXtazertifikat(List<String> xtazertifikat) {
        this.xtazertifikat = xtazertifikat;
    }

    public XtaIdentifierliste getIdentifierliste() {
        return identifierliste;
    }

    public void setIdentifierliste(XtaIdentifierliste identifierliste) {
        this.identifierliste = identifierliste;
    }

    public String getTransporterId() {
        return transporterId;
    }

    public void setTransporterId(String transporterId) {
        this.transporterId = transporterId;
    }

    public String getQueueSendMessage() {
        return queueSendMessage;
    }

    public void setQueueSendMessage(String queueSendMessage) {
        this.queueSendMessage = queueSendMessage;
    }

    public String getQueueGetMessage() {
        return queueGetMessage;
    }

    public void setQueueGetMessage(String queueGetMessage) {
        this.queueGetMessage = queueGetMessage;
    }

    public boolean isMtomEnabled() {
        return mtomEnabled;
    }

    public void setMtomEnabled(boolean mtomEnabled) {
        this.mtomEnabled = mtomEnabled;
    }

}
