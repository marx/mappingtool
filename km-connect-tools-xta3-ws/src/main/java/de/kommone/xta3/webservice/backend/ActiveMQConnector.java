package de.kommone.xta3.webservice.backend;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;

import org.apache.activemq.command.ActiveMQTextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.xml.transform.StringSource;

import de.kommone.xta3.envelope.Attachments;
import de.kommone.xta3.envelope.CamelMessageEnvelop;
import de.kommone.xta3.envelope.Document;
import de.kommone.xta3.webservice.konten.XtaKonto;

@Service
@Qualifier("AMQ")
public class ActiveMQConnector implements Connector {

    private static final String AMQ = "AMQ:queue:";

    @Autowired
    private ConnectionFactory connectionFactory;

    @Override
    public Message getMessageFromPostfach(XtaKonto konto) {

        try (Connection connection = connectionFactory.createConnection("admin", "admin")) {
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue(getQueueNameFromKonto(konto));
            QueueBrowser queueBrowser = session.createBrowser(queue);
            Enumeration<?> msgs = queueBrowser.getEnumeration();
            while (msgs.hasMoreElements()) {
                Object nachrichtAsObject = msgs.nextElement();
                if (nachrichtAsObject instanceof ActiveMQTextMessage) {
                    ActiveMQTextMessage nachricht = (ActiveMQTextMessage) nachrichtAsObject;
                    return getMessageFromActiveMQ(nachricht);
                }
            }
        } catch (JMSException e) {
            // TODO : Log Errors
        }

        return null;
    }

    private Message getMessageFromActiveMQ(ActiveMQTextMessage nachricht) {
        try {
            String body = nachricht.getText();
            Message message = new Message();
            if (body.contains("camel.message")) {
                CamelMessageEnvelop envelope = getCamelMessageEnvelop(body);
                Document document = envelope.getBody();
                message.setContent(new String(document.getContent(), StandardCharsets.UTF_8));
                // Attachements
                List<Attachement> atts = createAttachements(envelope.getAttachments());
                message.setAttachements(atts);
            } else {
                message.setContent(body);
            }
            return message;
        } catch (JMSException e) {
            // TODO : error-log
        }
        return null;
    }

    private List<Attachement> createAttachements(Attachments attachments) {
        if (attachments == null) {
            return new ArrayList<>();
        }

        List<Attachement> attachements = new ArrayList<>();
        List<Document> documents = attachments.getAttachment();
        if (documents != null) {
            for (Document doc : documents) {
                Attachement att = new Attachement();
                att.setContent(doc.getContent());
                attachements.add(att);
            }
        }
        return attachements;
    }

    private CamelMessageEnvelop getCamelMessageEnvelop(String xml) {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(
                // all the classes the context needs to know about
                CamelMessageEnvelop.class);
        return (CamelMessageEnvelop) marshaller.unmarshal(new StringSource(xml));
    }

    private String getQueueNameFromKonto(XtaKonto konto) {
        if (konto.getQueueGetMessage() == null) {
            return null;
        }
        String queueGetMessage = konto.getQueueGetMessage();
        return queueGetMessage.startsWith(AMQ) ? queueGetMessage.substring(AMQ.length()) : null;
    }

    @Override
    public Message getMessageFromPostfachById(XtaKonto konto, String messageId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void sendMessage(XtaKonto konto, Message message) {
        // TODO Auto-generated method stub

    }

}
