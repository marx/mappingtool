package de.kommone.xta3.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.jws.HandlerChain;
import javax.xml.bind.JAXBElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.addressing.server.annotation.Action;

import de.kommone.xta3.webservice.backend.Attachement;
import de.kommone.xta3.webservice.backend.Connector;
import de.kommone.xta3.webservice.backend.Message;
import de.kommone.xta3.webservice.konten.XtaKonto;
import de.kommone.xta3.ws.ContentType;
import de.kommone.xta3.ws.GenericContentContainer;
import de.kommone.xta3.ws.GenericContentContainer.ContentContainer;
import de.kommone.xta3.ws.MsgBoxCloseRequestType;
import de.kommone.xta3.ws.MsgBoxFetchRequest;

@Endpoint
@HandlerChain(file = "../../../../../resources/handlers.xml")
public class Xta3Endpoint {

    @Autowired
    @Qualifier(value = "AMQ")
    private Connector activeMQConnector;

    @Autowired
    @Qualifier(value = "file")
    private Connector fileConnector;

    @Autowired
    private Jaxb2Marshaller marshaller;

    @Autowired
    private CertificateHolder holder;

    @Action("http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxFetchRequest")
    @PayloadRoot(localPart = "MsgBoxFetchRequest", namespace = "http://www.osci.eu/ws/2008/05/transport")
    @ResponsePayload
    public GenericContentContainer getMessage(@RequestPayload MsgBoxFetchRequest request) {
        String alias = getAliasFromRequestContext();
        if (alias == null) {
            throw new IllegalArgumentException("Zum Zertifikat konnte kein passendes Konto gefunden werden!");
        }
        XtaKonto xtaKonto = holder.getPostfachInfo(alias);
        if (xtaKonto == null) {
            throw new IllegalArgumentException("Zum Alias " + alias
                    + " sind noch keine Konfigurationsoptionen hinterlegt!");
        }
        marshaller.setMtomEnabled(xtaKonto.isMtomEnabled());
        Message message = getConnectorBySource(xtaKonto.getQueueGetMessage(), xtaKonto.getId()).getMessageFromPostfach(
                xtaKonto);
        GenericContentContainer response = new GenericContentContainer();
        ContentContainer container = new ContentContainer();
        if (message != null) {
            container.setMessage(createContentType(message));
            container.getAttachment().addAll(getAttachementsFromMessage(message));
        }
        response.setContentContainer(container);
        return response;
    }

    private Connector getConnectorBySource(String source, String id) {
        if (source == null) {
            throw new IllegalStateException(id + "Kein Connector übergeben!");
        } else if (source.startsWith("file")) {
            return fileConnector;
        } else if (source.startsWith("AMQ")) {
            return activeMQConnector;
        }
        if (source.contains(":")) {
            throw new IllegalStateException(id + ": Es gibt noch keinen Connector für " + source.split(
                    ":")[0] + "!");
        } else {
            throw new IllegalStateException("Für das Konto " + id
                    + " konnte kein Connector ermittelt werden (in der Konfiguration sollte bspw. file: oder AMQ: stehen)!");
        }
    }

    private List<ContentType> getAttachementsFromMessage(Message message) {
        if (message.getAttachements() != null) {
            List<ContentType> types = new ArrayList<>();
            for (Attachement att : message.getAttachements()) {
                ContentType type = createContentType(att);
                types.add(type);
            }
            return types;
        }
        return new ArrayList<>();
    }

    private ContentType createContentType(Attachement att) {
        ContentType type = new ContentType();
        type.setValue(att.getContent());
        type.setContentType(att.getType());
        type.setFilename(att.getFilename());
        return type;
    }

    private String getAliasFromRequestContext() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        Object alias = requestAttributes.getAttribute("konto-id", RequestAttributes.SCOPE_REQUEST);
        return alias != null ? alias.toString() : null;
    }

    @Action("http://www.xta.de/XTA/SendMessageSync")
    @PayloadRoot(localPart = "GenericContentContainer", namespace = "http://xoev.de/transport/xta/211")
    @ResponsePayload
    public GenericContentContainer sendMessageSync(@RequestPayload GenericContentContainer request) {
        request.getContentContainer().getMessage().setValue("Helloly".getBytes());
        return request;
    }

    private ContentType createContentType(Message message) {
        ContentType type = new ContentType();
        type.setContentDescription(message.getMessageId());
        type.setValue(message.getContent().getBytes());
        return type;
    }
    
    @Action("http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxCloseRequest")
    @PayloadRoot(localPart = "MsgBoxCloseRequest", namespace = "http://www.osci.eu/ws/2008/05/transport")
    public void close(@RequestPayload JAXBElement<MsgBoxCloseRequestType> request) {
    }

}
