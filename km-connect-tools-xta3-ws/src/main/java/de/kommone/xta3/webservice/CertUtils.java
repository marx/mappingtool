package de.kommone.xta3.webservice;

import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Base64;

public class CertUtils {

    private static final String BEGIN_CERT = "-----BEGIN CERTIFICATE-----";
    private static final String END_CERT = "-----END CERTIFICATE-----";
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    public String getPublicKeyAsPEM(X509Certificate cert) {

        final Base64.Encoder encoder = Base64.getMimeEncoder(64, LINE_SEPARATOR.getBytes());

        byte[] rawCrtText;
        try {
            rawCrtText = cert.getEncoded();
            final String encodedCertText = new String(encoder.encode(rawCrtText));
            return BEGIN_CERT + LINE_SEPARATOR + encodedCertText + LINE_SEPARATOR + END_CERT;
        } catch (CertificateEncodingException e) {
            // nothing to do
        }
        return null;
    }

}
