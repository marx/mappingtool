package de.kommone.webservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

import de.kommone.webservice.sources.InvalidMessageIdException;
import de.kommone.webservice.sources.MessageIdListType;
import de.kommone.webservice.sources.NachrichtType;
import de.kommone.webservice.sources.PermissionDeniedException;
import de.kommone.webservice.sources.XTAInterface;
import de.kommone.webservice.sources.XTAWSTechnicalProblemException;
import de.kommone.webservice.sources.XtaAccountIdentifikation;
import de.kommone.webservice.sources.Xtaws110;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Main extends Application {

    private static final String TL_SV1_2 = "TLSv1.2";
    private static final String KEYSTORE_TYPE_JKS = "JKS";
    private static final String KEYSTORE_TYPE_PKCS12 = "PKCS12";

    private Properties properties = null;

    private static boolean TRUST_ALL = false;

    private Stage myStage;

    private static File keystore = null;

    @FXML
    public Menu newMessageMenu;

    @FXML
    private TextField url;

    @FXML
    private TextArea textArea;

    @FXML
    private Label keystoreLoaded;

    public static void main(String[] args) throws Exception, XTAWSTechnicalProblemException {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException, ClassNotFoundException, URISyntaxException {

        final BorderPane application = (BorderPane) new FXMLLoader(getClass().getClassLoader().getResource(
                "XTA1Client.fxml")).load();

        final Scene scene = new Scene(application, 1550, 900);
        scene.setFill(Color.LIGHTGRAY);

        myStage = stage;
        myStage.setScene(scene);
        myStage.setTitle("XTA 1 Client");
        myStage.show();
    }

    @FXML
    public void initialize() throws ClassNotFoundException, IOException, URISyntaxException, InterruptedException,
            NoSuchFieldException, SecurityException {

        keystoreLoaded.setText("Loaded Keystore: " + "laif-test.jks");
        textArea.setWrapText(true);
        url.setText(getURLAsString());
    }

    @FXML
    private void fetchMessage() {

        // TODO : Keystore + URL müssen dynamisch auswählbar sein!
        String nachricht = null;
        try {
            String urlAsText = this.url.getText() != null && !url.getText().isEmpty() ? url.getText() : null;
            nachricht = xtaCall(urlAsText);
        } catch (Exception e) {
            showException(e);
        }
        textArea.setText(nachricht);
    }

    public String getURLAsString() {
        Main main = new Main();
        XTAInterface port = main.getPort(null);
        return ((BindingProvider) port).getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY).toString();
    }

    @FXML
    private void loadKeystore() {

        final Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("File Chooser");
        FileChooser fil_chooser = new FileChooser();
        File file = fil_chooser.showOpenDialog(stage);
        if (file == null) {
            return;
        }
        keystore = file;
        keystoreLoaded.setText("Loaded Keystore: " + file.getAbsolutePath());
    }

    private void showException(Exception ex) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Exception Dialog");
        alert.setHeaderText("Ein Fehler ist aufgetreten");
        alert.setContentText(ex.getMessage());
        alert.showAndWait();

    }

    public String xtaCall(String url) throws PermissionDeniedException, XTAWSTechnicalProblemException,
            InvalidMessageIdException {

        Main main = new Main();
        XTAInterface port = main.getPort(url);

        MessageIdListType messageIdList = port.getMessageIdList(null, null);
        List<String> idList = messageIdList.getMessageId();
        for (String id : idList) {
            System.out.println(id);
            System.out.println("Nachricht: ");
            NachrichtType message = port.getMessage(id);
            byte[] value = message.getValue();
            String nachricht = new String(value);
            return nachricht;
        }

        return null;
    }

    public XTAInterface getPort(String url) {

        URL in = Main.class.getClassLoader().getResource("xta110.wsdl");
        Xtaws110 service = new Xtaws110(in);
        XTAInterface port = service.getXTAPort();
        Binding binding = ((BindingProvider) port).getBinding();
        @SuppressWarnings("rawtypes")
        List<Handler> handlerChain = binding.getHandlerChain();

        this.properties = new Properties();
        try {
            this.properties.load(Main.class.getClassLoader().getResourceAsStream("ws.properties"));
        } catch (IOException e) {
            return null;
        }

        // folgende Zeile zum Loggen von Requests / Responses
        // handlerChain.add(new SOAPLoggingHandler(false));

        // credentials müssen spätestens für die sendMessage-Methode übergeben
        // werden, aktuell nicht notwendig, muss aber gesetzt sein
        handlerChain.add(new SOAPXtaAccountHandler(createXtaAccountidentifikation("", "")));
        
        binding.setHandlerChain(handlerChain);
        SSLContext sslContext = initSSL();
        ((BindingProvider) port).getRequestContext().put(
                "com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory", sslContext.getSocketFactory());
        if (url != null) {
            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
        }
        return port;
    }

    private XtaAccountIdentifikation createXtaAccountidentifikation(String absenderkennung, String absenderpraefix) {
        XtaAccountIdentifikation identifikation = new XtaAccountIdentifikation();
        identifikation.setAbsenderKennung(absenderkennung);
        identifikation.setAbsenderPraefix(absenderpraefix);
        return identifikation;
    }

    private SSLContext initSSL() {

        SSLContext sslContext = null;

        try {
            sslContext = SSLContext.getInstance(TL_SV1_2);
            sslContext.init(getKeyManagers(), getTrustManagers(), null);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
        HttpsURLConnection.setDefaultHostnameVerifier(new WsHostnameVerificationDisabler());
        return sslContext;

    }

    private TrustManager[] getTrustManagers() throws NoSuchAlgorithmException, KeyStoreException, IOException,
            java.security.cert.CertificateException {
        return loadCertificateIntoTrustStore();
    }

    private KeyManager[] getKeyManagers() throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException,
            IOException, java.security.cert.CertificateException {
        return loadCertificateIntoKeyStore();
    }

    private KeyManager[] loadCertificateIntoKeyStore() throws NoSuchAlgorithmException, KeyStoreException, IOException,
            UnrecoverableKeyException, java.security.cert.CertificateException {
        KeyManagerFactory fac = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        KeyStore store = loadCertificateIntoStore(properties.getProperty("keystoreName"), properties.getProperty(
                "keystorePass"));
        fac.init(store, properties.getProperty("keystorePass").toCharArray());
        return fac.getKeyManagers();
    }

    private TrustManager[] loadCertificateIntoTrustStore() throws NoSuchAlgorithmException, KeyStoreException,
            IOException,
            java.security.cert.CertificateException {
        TrustManagerFactory fac = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        KeyStore store = loadCertificateIntoStore(properties.getProperty("truststoreName"), properties.getProperty(
                "truststorePass"));
        fac.init(store);
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                return;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                return;
            }
        } };
        return TRUST_ALL ? trustAllCerts : fac.getTrustManagers();
    }

    private KeyStore loadCertificateIntoStore(String storeName, String storePass) throws KeyStoreException, IOException,
            NoSuchAlgorithmException, java.security.cert.CertificateException {
        KeyStore store = KeyStore.getInstance(storeName.endsWith("p12") ? KEYSTORE_TYPE_PKCS12 : KEYSTORE_TYPE_JKS);
        if (keystore != null) {
            FileInputStream fis = new FileInputStream(keystore);
            String pass = keystore.getName().substring(0, keystore.getName().lastIndexOf('.'));
            store.load(fis, pass.toCharArray());
        } // default
        else {
            InputStream is = getClass().getClassLoader().getResourceAsStream(storeName);
            if (is == null) {
                throw new IOException("Datei " + storeName + " im Classpath nicht gefunden!");
            }
            store.load(is, storePass.toCharArray());
        }
        return store;
    }
}
