package de.kommone.webservice;

import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPHeader;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import de.kommone.webservice.sources.ObjectFactory;
import de.kommone.webservice.sources.XtaAccountIdentifikation;

public class SOAPXtaAccountHandler implements SOAPHandler<SOAPMessageContext> {

    private XtaAccountIdentifikation identifikation = null;

    public SOAPXtaAccountHandler(XtaAccountIdentifikation identifikation) {
        this.identifikation = identifikation;
    }

    @Override
    public void close(MessageContext context) {
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return false;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {

        final ObjectFactory objectFactory = new ObjectFactory();

        final JAXBElement<XtaAccountIdentifikation> requesterCredentials = objectFactory.createXtaAccountIdentifikation(
                identifikation);
        try {
            // checking whether handled message is outbound one as per Martin
            // Strauss answer
            final Boolean outbound = (Boolean) context.get("javax.xml.ws.handler.message.outbound");
            if (outbound != null && outbound) {
                // obtaining marshaller which should marshal instance to xml
                final Marshaller marshaller = JAXBContext.newInstance(XtaAccountIdentifikation.class)
                        .createMarshaller();
                // adding header because otherwise it's null
                final SOAPHeader soapHeader = context.getMessage().getSOAPPart().getEnvelope().getHeader();
                // marshalling instance (appending) to SOAP header's xml node
                marshaller.marshal(requesterCredentials, soapHeader);
            }
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    @Override
    public Set<QName> getHeaders() {
        // TODO Auto-generated method stub
        return null;
    }

}
