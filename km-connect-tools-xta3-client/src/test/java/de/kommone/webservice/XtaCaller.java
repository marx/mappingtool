package de.kommone.webservice;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import de.kommone.webservice.utils.XtaHelper;

public class XtaCaller implements Runnable {

    private XtaHelper xta;

    private int calls;

    private int threadNumber;

    private String url;

    private List<String> errors = new ArrayList<>();

    private List<String> messages = new ArrayList<>();

    public XtaCaller(Properties props, File file, int calls, int threadNumber, String url) {
        this.xta = new XtaHelper(props, file);
        this.calls = calls;
        this.threadNumber = threadNumber;
        this.url = url;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public void callXTA() {
        callXTAWS();
    }

    public List<String> getMessageIDs() {
        return this.xta.getLoggedGetMessageIds();
    }

    public void run() {
        callXTAWS();
    }

    public String callXTAWS() {
        for (int i = 0; i < this.calls; i++) {
            try {
                String call = xta.xtaCall(this.url, null, new ArrayList<>());
                if (call != null && !call.isEmpty()) {
                    String message = "Thread: " + this.threadNumber + ", hole " + (i + 1) + " te Nachricht";
                    messages.add(message);
                } else {
                    errors.add("Thread: " + this.threadNumber + " konnte keine Nachricht abholen");
                }
            } catch (Exception e) {
                errors.add(e.getMessage() + " von Thread " + this.threadNumber);
            }
        }
        return "Complete!";
    }

}
