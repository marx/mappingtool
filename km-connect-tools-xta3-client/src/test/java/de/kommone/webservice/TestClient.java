package de.kommone.webservice;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import de.kommone.webservice.sources.InvalidMessageIDException;
import de.kommone.webservice.sources.PermissionDeniedException;
import de.kommone.webservice.sources.XTAWSTechnicalProblemException;
import de.kommone.webservice.utils.XtaHelper;

/**
 * Testklasse, um gegen einen lokal laufenden XTA3-Webservice zu testen.
 * Getestet wird das Abholen einer Nachricht (getMessage()) und das
 * anschließende Quittieren (close()). Die Tests stehen auf Ignore und sind auch
 * eher als integrative Tests zu verstehen, die das parallele Abholen von
 * Nachrichten prüfen.
 * 
 * @author Erdmanns
 *
 */
public class TestClient {

    @Test
    @Ignore
    public void test() throws InvalidMessageIDException, PermissionDeniedException, XTAWSTechnicalProblemException {

        Properties props = loadProperties();
        if (props == null) {
            Assert.fail("Porperties konnten nicht geladen werden");
            return;
        }
        XtaHelper xta = new XtaHelper(props, new File("src/test/resources/geobuero-test.jks"));

        // 10 Nachrichten abholen
        for (int i = 0; i < 10; i++) {
            String call = xta.xtaCall("https://localhost:8443/xta3", null, new ArrayList<>());
            Assert.assertNotNull(call);
        }

    }

    /**
     * In diesem Test muss die Anzahl der Nachrichten übergeben werden. Getestet
     * wird mit der Queue für Alva. Hier sollte die entsprechende Anzahl an
     * Nachrichten eingespielt sein (Queue-Name: de.kommone.fahrverbot.alva)
     * 
     * @throws InvalidMessageIDException
     * @throws PermissionDeniedException
     * @throws XTAWSTechnicalProblemException
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @Test
    @Ignore
    public void testParallel() throws InvalidMessageIDException, PermissionDeniedException,
            XTAWSTechnicalProblemException, InterruptedException, ExecutionException {

        int nachrichten = 100;
        String zertifikatPfad = "src/test/resources/alva-test.jks";
        String url = "https://kmcxta-dev.komm-one.net:8443/xta3";
        // String url = "https://localhost:8443/xta3";

        Properties props = loadProperties();
        if (props == null) {
            Assert.fail("Porperties konnten nicht geladen werden");
            return;
        }

        XtaCaller caller1 = new XtaCaller(props, new File(zertifikatPfad), nachrichten / 5, 1, url);
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> caller1.callXTAWS());
        XtaCaller caller2 = new XtaCaller(props, new File(zertifikatPfad), nachrichten / 5, 2, url);
        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> caller2.callXTAWS());
        XtaCaller caller3 = new XtaCaller(props, new File(zertifikatPfad), nachrichten / 5, 3, url);
        CompletableFuture<String> future3 = CompletableFuture.supplyAsync(() -> caller3.callXTAWS());
        XtaCaller caller4 = new XtaCaller(props, new File(zertifikatPfad), nachrichten / 5, 4, url);
        CompletableFuture<String> future4 = CompletableFuture.supplyAsync(() -> caller4.callXTAWS());
        XtaCaller caller5 = new XtaCaller(props, new File(zertifikatPfad), nachrichten / 5, 5, url);
        CompletableFuture<String> future5 = CompletableFuture.supplyAsync(() -> caller5.callXTAWS());

        CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(future1, future2, future3, future4, future5);

        // ...

        combinedFuture.get();

        Assert.assertTrue(future1.isDone());
        Assert.assertTrue(future2.isDone());
        Assert.assertTrue(future3.isDone());
        Assert.assertTrue(future4.isDone());
        Assert.assertTrue(future5.isDone());

        Assert.assertTrue(caller1.getErrors().isEmpty());
        Assert.assertTrue(caller2.getErrors().isEmpty());
        Assert.assertTrue(caller3.getErrors().isEmpty());
        Assert.assertTrue(caller4.getErrors().isEmpty());
        Assert.assertTrue(caller5.getErrors().isEmpty());

        Assert.assertEquals(nachrichten / 5, caller1.getMessages().size());
        Assert.assertEquals(nachrichten / 5, caller2.getMessages().size());
        Assert.assertEquals(nachrichten / 5, caller3.getMessages().size());
        Assert.assertEquals(nachrichten / 5, caller4.getMessages().size());
        Assert.assertEquals(nachrichten / 5, caller5.getMessages().size());

        List<String> duplicatedFetchedMessages = new ArrayList<>();
        List<String> catchedMessages = new ArrayList<>();
        List<String> messageIDs = caller1.getMessageIDs();
        duplicatedFetchedMessages.addAll(addMessagesAndCheckForDuplicates(catchedMessages, messageIDs, 1));
        messageIDs = caller2.getMessageIDs();
        duplicatedFetchedMessages.addAll(addMessagesAndCheckForDuplicates(catchedMessages, messageIDs, 2));
        messageIDs = caller3.getMessageIDs();
        duplicatedFetchedMessages.addAll(addMessagesAndCheckForDuplicates(catchedMessages, messageIDs, 3));
        messageIDs = caller4.getMessageIDs();
        duplicatedFetchedMessages.addAll(addMessagesAndCheckForDuplicates(catchedMessages, messageIDs, 4));
        messageIDs = caller5.getMessageIDs();
        duplicatedFetchedMessages.addAll(addMessagesAndCheckForDuplicates(catchedMessages, messageIDs, 5));
        Assert.assertEquals(nachrichten, catchedMessages.size());

        Assert.assertTrue(getErrorMessageFromDuplicatedMessages(duplicatedFetchedMessages), duplicatedFetchedMessages
                .isEmpty());
    }

    private String getErrorMessageFromDuplicatedMessages(List<String> duplicatedFetchedMessages) {
        String result = "";
        for (String messageId : duplicatedFetchedMessages) {
            String[] idThread = messageId.split("/");
            result += "\n " + "Thread: " + idThread[1] + ", Message mit ID " + idThread[0] + " wurde doppelt abgeholt!";
        }
        return result;
    }

    private List<String> addMessagesAndCheckForDuplicates(List<String> catchedMessages, List<String> messageIDs,
            int threadId) {
        List<String> duplicatedMessages = new ArrayList<>();
        for (String messageId : messageIDs) {
            // doppelte Message
            if (catchedMessages.contains(messageId)) {
                duplicatedMessages.add(messageId + "/" + threadId);
            }
            catchedMessages.add(messageId);
        }
        return duplicatedMessages;
    }

    @Test
    @Ignore
    public void close() throws InvalidMessageIDException, PermissionDeniedException, XTAWSTechnicalProblemException {

        String zertifikatPfad = "src/test/resources/alva-test.jks";
        String url = "https://kmcxta-dev.komm-one.net:8443/xta3";

        Properties props = loadProperties();
        if (props == null) {
            Assert.fail("Porperties konnten nicht geladen werden");
            return;
        }

        XtaHelper helper = new XtaHelper(props, new File(zertifikatPfad));
        try {
            helper.closeMessage(url, "ID:0f732d6d9c4f-39237-1635044853490-4:103:1:1:2");
        } catch (Exception e) {
            Assert.fail("Exception geworfen: " + e.getMessage());
        }

    }

    private Properties loadProperties() {
        Properties properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("wstest.properties"));
        } catch (IOException e) {
            // nichts tun
        }
        return properties;
    }

}
