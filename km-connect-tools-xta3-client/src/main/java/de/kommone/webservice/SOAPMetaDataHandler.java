package de.kommone.webservice;

import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import de.kommone.webservice.sources.Header;
import de.kommone.webservice.sources.MessageMetaData;

public class SOAPMetaDataHandler implements SOAPHandler<SOAPMessageContext> {

	private MessageMetaData messageMeta = null;

	public SOAPMetaDataHandler(MessageMetaData messageMeta) {
		this.messageMeta = messageMeta;
	}

    @Override
    public void close(MessageContext context) {
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return false;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {

		try {
			// checking whether handled message is outbound one as per Martin
			// Strauss answer
			final Boolean outbound = (Boolean) context.get("javax.xml.ws.handler.message.outbound");
			if (outbound != null && outbound) {
				// obtaining marshaller which should marshal instance to xml
				final Marshaller marshaller = JAXBContext.newInstance(Header.class)
						.createMarshaller();
				// adding header because otherwise it's null
				final SOAPHeader soapHeader = context.getMessage().getSOAPPart().getEnvelope().getHeader();
				// marshalling instance (appending) to SOAP header's xml node
				marshaller.marshal(messageMeta, soapHeader);

				SOAPMessage message = context.getMessage();
				message.saveChanges();
			}
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
        return true;
    }

    @Override
    public Set<QName> getHeaders() {
        // TODO Auto-generated method stub
        return null;
    }

}
