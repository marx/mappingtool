package de.kommone.webservice.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import de.kommone.ver.Dateianhang;
import de.kommone.ver.Folgenachricht;
import de.kommone.ver.FolgenachrichtVersenden;
import de.kommone.ver.LiegenschaftsKatasterAuszugBeantragen;
import de.kommone.ver.Status;

public class VerHelper {

    public FolgenachrichtVersenden transform(LiegenschaftsKatasterAuszugBeantragen antrag) {

        FolgenachrichtVersenden result = new FolgenachrichtVersenden();
        result.setFolgenachricht(createFolgeNachricht(antrag));
        return result;
    }

    private Folgenachricht createFolgeNachricht(LiegenschaftsKatasterAuszugBeantragen antrag) {
        Folgenachricht nachricht = new Folgenachricht();
        nachricht.setAktuelleMitteilung("Hallo");
        // claim-check setzen
        nachricht.setProzessId(antrag.getLiegenschaftsKatasterAuszug().getProzessId());
	nachricht.setBearbeitungsstatus(Status.SACHBEARBEITER_RUECKFRAGE);
	nachricht.getAnhaenge().add(createDateianhang());	
	nachricht.setBetreff("Das ist ein Testbetreff");
        return nachricht;
    }

    private Dateianhang createDateianhang() {
        Dateianhang anhang = new Dateianhang();
        File file = new File("src/main/resources/Bebauungsplan.pdf");
        try {
            anhang.setDatei(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            // nichts tun
        }
        anhang.setDateiname("dasIstEineTestPDF");
        return anhang;
    }

}
