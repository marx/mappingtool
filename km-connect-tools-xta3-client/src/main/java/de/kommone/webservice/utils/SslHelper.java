package de.kommone.webservice.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import de.kommone.webservice.WsHostnameVerificationDisabler;

public class SslHelper {

    private static final String TL_SV1_2 = "TLSv1.2";
    private static boolean TRUST_ALL = true;
    private static final String KEYSTORE_TYPE_JKS = "JKS";
    private static final String KEYSTORE_TYPE_PKCS12 = "PKCS12";

    private Properties properties;
    private File keystore;

    public SslHelper(Properties properties) {
        this.properties = properties;
    }

    public SSLContext initSSL() {

        SSLContext sslContext = null;

        try {
            sslContext = SSLContext.getInstance(TL_SV1_2);
            sslContext.init(getKeyManagers(), getTrustManagers(), null);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
        HttpsURLConnection.setDefaultHostnameVerifier(new WsHostnameVerificationDisabler());
        return sslContext;

    }

    private TrustManager[] getTrustManagers() throws NoSuchAlgorithmException, KeyStoreException, IOException,
            java.security.cert.CertificateException {
        return loadCertificateIntoTrustStore();
    }

    private KeyManager[] getKeyManagers() throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException,
            IOException, java.security.cert.CertificateException {
        return loadCertificateIntoKeyStore();
    }

    private KeyManager[] loadCertificateIntoKeyStore() throws NoSuchAlgorithmException, KeyStoreException, IOException,
            UnrecoverableKeyException, java.security.cert.CertificateException {
        KeyManagerFactory fac = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        KeyStore store = loadCertificateIntoStore(properties.getProperty("keystoreName"), properties.getProperty(
                "keystorePass"), false);
        String pw = null;
        if (keystore == null) {
            pw = properties.getProperty("keystorePass");
        } else {
            pw = keystore.getName().substring(0, keystore.getName().lastIndexOf('.'));
        }
        fac.init(store, pw.toCharArray());
        return fac.getKeyManagers();
    }

    private TrustManager[] loadCertificateIntoTrustStore() throws NoSuchAlgorithmException, KeyStoreException,
            IOException, java.security.cert.CertificateException {
        TrustManagerFactory fac = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        KeyStore store = loadCertificateIntoStore(properties.getProperty("truststoreName"), properties.getProperty(
                "truststorePass"), true);
        fac.init(store);
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                return;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                return;
            }
        } };
        return TRUST_ALL ? trustAllCerts : fac.getTrustManagers();
    }

    private KeyStore loadCertificateIntoStore(String storeName, String storePass, boolean loadTruststore)
            throws KeyStoreException, IOException, NoSuchAlgorithmException, java.security.cert.CertificateException {
        KeyStore store = KeyStore.getInstance(storeName.endsWith("p12") ? KEYSTORE_TYPE_PKCS12 : KEYSTORE_TYPE_JKS);
        if (keystore != null && !loadTruststore) {
            FileInputStream fis = new FileInputStream(keystore);
            String pass = keystore.getName().substring(0, keystore.getName().lastIndexOf('.'));
            store.load(fis, pass.toCharArray());
        } // default
        else {
            InputStream is = getClass().getClassLoader().getResourceAsStream(storeName);
            if (is == null) {
                throw new IOException("Datei " + storeName + " im Classpath nicht gefunden!");
            }
            store.load(is, storePass.toCharArray());
        }
        return store;
    }

    public File getKeystore() {
        return keystore;
    }

    public void setKeystore(File keystore) {
        this.keystore = keystore;
    }

}
