package de.kommone.webservice.utils;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.SSLContext;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.AddressingFeature;
import javax.xml.ws.soap.MTOMFeature;
import javax.xml.ws.soap.SOAPBinding;

import de.kommone.webservice.Main;
import de.kommone.webservice.MsgLogger;
import de.kommone.webservice.SOAPLoggingHandler;
import de.kommone.webservice.SOAPMetaDataHandler;
import de.kommone.webservice.sources.AttributedURIType;
import de.kommone.webservice.sources.CodeFehlernummer;
import de.kommone.webservice.sources.ContentType;
import de.kommone.webservice.sources.DeliveryAttributesType;
import de.kommone.webservice.sources.DestinationsType;
import de.kommone.webservice.sources.GenericContentContainer;
import de.kommone.webservice.sources.GenericContentContainer.ContentContainer;
import de.kommone.webservice.sources.InvalidMessageIDException;
import de.kommone.webservice.sources.InvalidMessageIDExceptionType;
import de.kommone.webservice.sources.KeyCodeType;
import de.kommone.webservice.sources.MessageMetaData;
import de.kommone.webservice.sources.MessageProperties;
import de.kommone.webservice.sources.MessageSchemaViolationException;
import de.kommone.webservice.sources.MessageVirusDetectionException;
import de.kommone.webservice.sources.MsgBoxCloseRequestType;
import de.kommone.webservice.sources.MsgBoxFetchRequest;
import de.kommone.webservice.sources.MsgBoxPortType;
import de.kommone.webservice.sources.MsgIdentificationType;
import de.kommone.webservice.sources.MsgSelector;
import de.kommone.webservice.sources.OriginatorsType;
import de.kommone.webservice.sources.ParameterIsNotValidException;
import de.kommone.webservice.sources.PartyIdentifierType;
import de.kommone.webservice.sources.PartyType;
import de.kommone.webservice.sources.PermissionDeniedException;
import de.kommone.webservice.sources.PropertyType;
import de.kommone.webservice.sources.QualifierType;
import de.kommone.webservice.sources.QualifierType.BusinessScenario;
import de.kommone.webservice.sources.QualifierType.MessageType;
import de.kommone.webservice.sources.SendPortType;
import de.kommone.webservice.sources.SyncAsyncException;
import de.kommone.webservice.sources.XTAService;
import de.kommone.webservice.sources.XTAWSTechnicalProblemException;

public class XtaHelper {

    private File keyStore;

    private Properties properties;

    private List<String> loggedGetMessageIds = new ArrayList<>();

    public XtaHelper(Properties properties, File keyStore) {
        this.keyStore = keyStore;
        this.properties = properties;
    }

    public File getKeyStore() {
        return keyStore;
    }

    public void setKeyStore(File keyStore) {
        this.keyStore = keyStore;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public List<String> getLoggedGetMessageIds() {
        return loggedGetMessageIds;
    }

    public void setLoggedGetMessageIds(List<String> loggedGetMessageIds) {
        this.loggedGetMessageIds = loggedGetMessageIds;
    }

    public String getURLAsString() {
        MsgBoxPortType port = getMessageBoxPort(null);
        return ((BindingProvider) port).getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY).toString();
    }

    public String xtaSendSync(String url, byte[] text) throws InvalidMessageIDException, PermissionDeniedException,
            XTAWSTechnicalProblemException, MessageSchemaViolationException, MessageVirusDetectionException,
            ParameterIsNotValidException, SyncAsyncException {

        SendPortType port = getSendPort(url);
        Holder<GenericContentContainer> holder = createHolder(createContentContainer(text));
        port.sendMessageSync(holder);

        GenericContentContainer container = holder.value;

        ContentType contentType = container.getContentContainer().getMessage();

        byte[] value = contentType.getValue();
        String nachricht = new String(value);
        return nachricht;
    }

    private Holder<GenericContentContainer> createHolder(GenericContentContainer createContentContainer) {
        Holder<GenericContentContainer> holder = new Holder<GenericContentContainer>(createContentContainer);
        return holder;
    }

    public String xtaSend(String url, String text) throws InvalidMessageIDException, PermissionDeniedException,
            XTAWSTechnicalProblemException, MessageSchemaViolationException, MessageVirusDetectionException,
            ParameterIsNotValidException, SyncAsyncException {

        SendPortType port = getSendPort(url);

        port.sendMessage(createContentContainer(text));
        return null;
    }

    private GenericContentContainer createContentContainer(byte[] bytes) {
        GenericContentContainer container = new GenericContentContainer();
        container.setContentContainer(createContent(bytes));
        return container;
    }

    private ContentContainer createContent(byte[] bytes) {
        ContentContainer container = new ContentContainer();
        container.setMessage(createContentType(bytes));
        return container;
    }

    private ContentType createContentType(byte[] bytes) {
        ContentType contentType = new ContentType();
        contentType.setValue(bytes);
        return contentType;
    }

    private GenericContentContainer createContentContainer(String text) {
        GenericContentContainer container = new GenericContentContainer();
        container.setContentContainer(createContent(text));
        return container;
    }

    private ContentContainer createContent(String text) {
        ContentContainer container = new ContentContainer();
        container.setMessage(createContentType(text));
        return container;
    }

    private ContentType createContentType(String text) {
        ContentType contentType = new ContentType();
        contentType.setValue(text.getBytes());
        return contentType;
    }

    public String xtaCall(String url, String id, List<Anhang> anhaenge) throws InvalidMessageIDException,
            PermissionDeniedException,
            XTAWSTechnicalProblemException {

        MsgBoxPortType port = getMessageBoxPort(url);

        GenericContentContainer container = port.getMessage(createMsgBoxFetchRequest(id));
        

        String messageID = null;
        if (container != null && container.getContentContainer() != null && container.getContentContainer()
                .getMessage() != null) {
            ContentType contentType = container.getContentContainer().getMessage();
            messageID = contentType.getContentDescription();
            loggedGetMessageIds.add(messageID);

            if (container.getContentContainer().getAttachment() != null) {
                for (ContentType attachement : container.getContentContainer().getAttachment()) {
                    anhaenge.add(createAnhangFromContentType(attachement));
                }
            }

            // close Operation aufrufen
            if (messageID != null) {
                try {
                    port = getMessageBoxPortForCloseOperation(url);
                    port.close(createMsgBoxCloseRequest(messageID));
                } catch (WebServiceException e) {
                    // protokollieren mit System err, sonst ignorieren
                    System.err.println(e.getMessage());
                }
            } else {
                CodeFehlernummer fehlernummer = new CodeFehlernummer();
                fehlernummer.setCode("2");
                InvalidMessageIDExceptionType exc = new InvalidMessageIDExceptionType();
                exc.setErrorCode(fehlernummer);
                throw new InvalidMessageIDException(messageID, exc);
            }

            byte[] value = contentType.getValue();
            String nachricht = new String(value);
            return nachricht;
        }

        return null;
    }

    private Anhang createAnhangFromContentType(ContentType attachement) {
        Anhang anhang = new Anhang();
        anhang.setContent(attachement.getValue());
        anhang.setName(attachement.getFilename());
        anhang.setType(attachement.getContentType());
        return anhang;
    }

    public void closeMessage(String url, String id) throws InvalidMessageIDException, PermissionDeniedException,
            XTAWSTechnicalProblemException {
        MsgBoxPortType port = getMessageBoxPortForCloseOperation(url);
        port.close(createMsgBoxCloseRequest(id));
    }

    private MsgBoxCloseRequestType createMsgBoxCloseRequest(String messageID) {
        MsgBoxCloseRequestType mcr = new MsgBoxCloseRequestType();
        mcr.getLastMsgReceived().add(createAttributedURIType(messageID));
        return mcr;
    }

    private AttributedURIType createAttributedURIType(String messageID) {
        AttributedURIType aut = new AttributedURIType();
        aut.setValue(messageID);
        return aut;
    }

    private MsgBoxFetchRequest createMsgBoxFetchRequest(String id) {
        MsgBoxFetchRequest request = new MsgBoxFetchRequest();
        request.setMsgPart("Envelope");
        request.setMsgSelector(createMessageSelector(id));
        return request;
    }

    private MsgSelector createMessageSelector(String id) {
        MsgSelector ms = new MsgSelector();
        ms.setNewEntry(true);
        ms.getMessageID().add(createAttributedURIType(id));
        return ms;
    }

    public SendPortType getSendPort(String url) {

        URL in = Main.class.getClassLoader().getResource("XTA.WSDL");
        XTAService service = new XTAService(in);
        SendPortType port = service.getSendXtaPort(new AddressingFeature(true, false), new MTOMFeature(true));
        Binding binding = ((BindingProvider) port).getBinding();
        @SuppressWarnings("rawtypes")
        List<Handler> handlerChain = binding.getHandlerChain();

        this.properties = new Properties();
        try {
            this.properties.load(Main.class.getClassLoader().getResourceAsStream("ws.properties"));
        } catch (IOException e) {
            return null;
        }

        // folgende Zeile zum Loggen von Requests / Responses
        handlerChain.add(new SOAPMetaDataHandler(createMessageMeta()));
        // handlerChain.add(new SOAPLoggingHandler(false));

        // credentials müssen spätestens für die sendMessage-Methode übergeben
        // werden, aktuell nicht notwendig, muss aber gesetzt sein
        // TODO : nice to have handlerChain.add(new
        // SOAPXtaAccountHandler(createXtaAccountidentifikation("", "")));
        binding.setHandlerChain(handlerChain);
        // MTOMFeature
        SOAPBinding soapbinding = (SOAPBinding) binding;
        soapbinding.setMTOMEnabled(true);
        SslHelper ssl = new SslHelper(properties);
        ssl.setKeystore(this.keyStore);
        SSLContext sslContext = ssl.initSSL();
        ((BindingProvider) port).getRequestContext().put(
                "com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory", sslContext.getSocketFactory());
        if (url != null) {
            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
        }
        return port;
    }

    private MessageMetaData createMessageMeta() {
        MessageMetaData metaData = new MessageMetaData();
        metaData.setDeliveryAttributes(createDeliveryAttributes());
        metaData.setOriginators(createOriginators());
        metaData.setDestinations(createDestinations());
        metaData.setMsgIdentification(createMessageIdentificationType());
        metaData.setQualifier(createQualifier());
        metaData.setMsgSize(BigInteger.valueOf(139352));
        metaData.setMessageProperties(createMessageProperties());
        return metaData;
    }

    private MessageProperties createMessageProperties() {
        MessageProperties mp = new MessageProperties();
        mp.getProperty().add(createProperty("deleteMessage", "false"));
        mp.getProperty().add(createProperty("storeMessage", "true"));
        mp.getProperty().add(createProperty("xoev_empfaenger", "Herr Müller"));
        return mp;
    }

    private PropertyType createProperty(String key, String value) {
        PropertyType pt = new PropertyType();
        pt.setKey(createKeyCodeType("urn:de:bos_bremen:gov:gateway", "1.0", key));
        pt.setValue(value);
        return pt;
    }

    private QualifierType createQualifier() {
        QualifierType qt = new QualifierType();
        qt.setService("http://www.osci.de/xmeld244/xmeld244Fortschreibung.wsdl");
        qt.setBusinessScenario(createBusinessScenario());
        qt.setMessageType(createMessageType());
        return qt;
    }

    private MessageType createMessageType() {
        MessageType mt = new MessageType();
        mt.setPayloadSchema("http://www.osci.de/xmeld241");
        mt.setListURI("urn:de:bos_bremen:gov:gateway:messageTypes");
        mt.setListVersionID("1.0");
        mt.setCode("1600");
        mt.setName("kirche.bestandslieferung.1600");
        return mt;
    }

    private BusinessScenario createBusinessScenario() {
        BusinessScenario bs = new BusinessScenario();
        bs.setDefined(createKeyCodeType("urn:de:xta:codeliste:business.scenario", "2", "XMELD_DATA"));
        return bs;
    }

    private KeyCodeType createKeyCodeType(String uri, String versionId, String code) {
        KeyCodeType kct = new KeyCodeType();
        kct.setListURI(uri);
        kct.setListVersionID(versionId);
        kct.setCode(code);
        return kct;
    }

    private DestinationsType createDestinations() {
        DestinationsType dt = new DestinationsType();
        dt.setReader(createPartyType("rel:000000000005"));
        return dt;
    }

    private OriginatorsType createOriginators() {
        OriginatorsType ot = new OriginatorsType();
        ot.setAuthor(createPartyType("ags:08999101"));
        return ot;
    }

    private PartyType createPartyType(String type) {
        PartyType partyType = new PartyType();
        partyType.setIdentifier(createPartyIdentifier(type));
        return partyType;
    }

    private PartyIdentifierType createPartyIdentifier(String type) {
        PartyIdentifierType pt = new PartyIdentifierType();
        pt.setType("xoev");
        pt.setValue(type);
        return pt;
    }

    private DeliveryAttributesType createDeliveryAttributes() {
        DeliveryAttributesType delAttr = new DeliveryAttributesType();
        delAttr.setOrigin(localDateTimeToXMLGregorianCalendar(LocalDateTime.now()));
        delAttr.setInitialSend(localDateTimeToXMLGregorianCalendar(LocalDateTime.now()));
        delAttr.setServiceQuality("meldewesen");
        return delAttr;
    }

    private static XMLGregorianCalendar localDateTimeToXMLGregorianCalendar(LocalDateTime date) {
        ZoneId zoneId = ZoneId.of("Europe/Berlin");
        ZonedDateTime zdt = date.atZone(zoneId);
        GregorianCalendar gcal = GregorianCalendar.from(zdt);
        XMLGregorianCalendar xcal;
        try {
            xcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);
            xcal.setTime(date.getHour(), date.getMinute(), date.getSecond());
            return xcal;
        } catch (DatatypeConfigurationException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private MsgIdentificationType createMessageIdentificationType() {
        MsgIdentificationType type = new MsgIdentificationType();
        type.setMessageID(createMessageId());
        return type;
    }

    private AttributedURIType createMessageId() {
        AttributedURIType type = new AttributedURIType();
        type.setValue("urn:de:xta:messageid:merkel.dzbw.de_xta3_e2ab6395-a3d8-49a3-aaa4-2b1ceadadb00");
        return type;
    }

    public MsgBoxPortType getMessageBoxPort(String url) {

        URL in = Main.class.getClassLoader().getResource("XTA.WSDL");
        XTAService service = new XTAService(in);
        MsgBoxPortType port = service.getMsgBoxPort(new AddressingFeature(true, false));
        Binding binding = ((BindingProvider) port).getBinding();
        @SuppressWarnings("rawtypes")
        List<Handler> handlerChain = binding.getHandlerChain();

        // folgende Zeile zum Loggen von Requests / Responses
        // handlerChain.add(new SOAPLoggingHandler(false));
        handlerChain.add(new MsgLogger());
        // handlerChain.add(new SOAPMetaDataHandler(createMessageMeta()));

        binding.setHandlerChain(handlerChain);
        SslHelper ssl = new SslHelper(properties);
        ssl.setKeystore(this.keyStore);
        SSLContext sslContext = ssl.initSSL();
        ((BindingProvider) port).getRequestContext().put(
                "com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory", sslContext.getSocketFactory());
        ((BindingProvider) port).getRequestContext().put("set-jaxb-validation-event-handler", "false");
        if (url != null) {
            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
        }
        return port;
    }

    private MsgBoxPortType getMessageBoxPortForCloseOperation(String url) {

        URL in = Main.class.getClassLoader().getResource("XTA.WSDL");
        XTAService service = new XTAService(in);
        MsgBoxPortType port = service.getMsgBoxPort(new AddressingFeature(true, false));
        Binding binding = ((BindingProvider) port).getBinding();
        @SuppressWarnings("rawtypes")
        List<Handler> handlerChain = binding.getHandlerChain();

        // folgende Zeile zum Loggen von Requests / Responses
        // handlerChain.add(new SOAPLoggingHandler(false));

        // credentials müssen spätestens für die sendMessage-Methode übergeben
        // werden, aktuell nicht notwendig, muss aber gesetzt sein
        // handlerChain.add(new SOAPMetaDataHandler(null));

        binding.setHandlerChain(handlerChain);
        SslHelper ssl = new SslHelper(properties);
        ssl.setKeystore(this.keyStore);
        SSLContext sslContext = ssl.initSSL();
        ((BindingProvider) port).getRequestContext().put(
                "com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory", sslContext.getSocketFactory());
        ((BindingProvider) port).getRequestContext().put("set-jaxb-validation-event-handler", "false");
        if (url != null) {
            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
        }
        return port;
    }

}
