package de.kommone.webservice.utils;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import de.kommone.xfallcontainer.ApplicationTransport0300001;
import de.kommone.xfallcontainer.CodeAuthentNiveau;
import de.kommone.xfallcontainer.CodeCountry;
import de.kommone.xfallcontainer.CodeCountryState;
import de.kommone.xfallcontainer.CodePartnerRole;
import de.kommone.xfallcontainer.CodePartnerType;
import de.kommone.xfallcontainer.CodeProcessStatus;
import de.kommone.xfallcontainer.Document;
import de.kommone.xfallcontainer.DocumentRepresentation;
import de.kommone.xfallcontainer.Header;
import de.kommone.xfallcontainer.MainApplication;
import de.kommone.xfallcontainer.PartialApplication;
import de.kommone.xfallcontainer.Partner;
import de.kommone.xfallcontainer.PartnerFunction;
import de.kommone.xfallcontainer.Status;

public class XFallHelper {

    public String extractXML(String applicationTransport) {
        Object message = null;
        try {
            message = unmarshall(applicationTransport, null);
        } catch (Exception e) {
            return null;
        }
        ApplicationTransport0300001 transport = null;
        if (message instanceof ApplicationTransport0300001) {
            transport = (ApplicationTransport0300001) message;
            DocumentRepresentation representation = transport.getPartialApplication().get(0).getDocument().get(0)
                    .getDocumentRepresentation().get(0);
            return new String(representation.getContent(), StandardCharsets.UTF_8);
        }
        return null;
    }

    private Object unmarshall(String message, String pathToScheme) {

        try {
            // JAXB- Kontext aus dem Schema
            JAXBContext context = JAXBContext.newInstance("de.kommone.xfallcontainer:de.kommone.xfalldaten");

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = null;
            if (pathToScheme != null) {
                schema = sf.newSchema(new File(pathToScheme));
            }

            // den Unmarshaller aus dem Kontext
            Unmarshaller unsmarshaller = context.createUnmarshaller();
            unsmarshaller.setSchema(schema);

            // und zurückgeben
            return unsmarshaller.unmarshal(new StringReader(message));
        } catch (JAXBException | SAXException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String putToApplicationTransport(String xml) {
        return marshall(createApplicationTransport(xml, "test-niveau", "test-usecase", "test-request-id"), null);
    }

    private ApplicationTransport0300001 createApplicationTransport(String xml,
            String authentNiveau, String usecase, String requestID) {
        ApplicationTransport0300001 transport = new ApplicationTransport0300001();
        transport.setHeader(createHeader(requestID));
        transport.getPartner().add(createPartner(authentNiveau));
        transport.setMainApplication(createMainApplication("Test", requestID));
        transport.getPartialApplication().add(createPartialApplication(usecase, xml));
        return transport;
    }

    private Header createHeader(String messageID) {
        Header header = new Header();
        header.setMessageID(messageID);
        return header;
    }

    private String marshall(Object object, String pathToScheme) {

        try {
            // JAXB- Kontext aus dem Schema
            JAXBContext context = JAXBContext.newInstance(
                    "de.kommone.xfallcontainer:de.kommone.xfalldaten");

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = null;
            if (pathToScheme != null) {
                schema = sf.newSchema(new File(pathToScheme));
            }

            // den Marshaller aus dem Kontext
            Marshaller marshaller = context.createMarshaller();
            marshaller.setSchema(schema);
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

            // den Output in einen String-Writer schreiben
            StringWriter writer = new StringWriter();
            marshaller.marshal(object, writer);

            // und zurückgeben
            return writer.toString();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private PartialApplication createPartialApplication(String title, String xml) {
        PartialApplication partialApp = new PartialApplication();
        partialApp.setTitle(title);
        partialApp.setPartialApplicationID("testID.xml");
        partialApp.getPartnerFunction().add(createPartnerFunction());
        // 2 (!!!) x partner- functions übergeben
        partialApp.getPartnerFunction().add(createPartnerFunction());
        partialApp.getDocument().add(createDocument(xml));
        partialApp.setStatus(createStatus());
        partialApp.setDeadline(null);
        return partialApp;
    }

    private Status createStatus() {
        Status status = new Status();
        CodeProcessStatus processstatus = new CodeProcessStatus();
        processstatus.setCode("1");
        status.setCurrentStatus(processstatus);
        return status;
    }

    private CodePartnerRole createPartnerRole(String rolle) {
        CodePartnerRole partnerRole = new CodePartnerRole();
        partnerRole.setCode(rolle);
        return partnerRole;
    }

    private PartnerFunction createPartnerFunction() {
        PartnerFunction function = new PartnerFunction();
        function.setPartnerID("PartnerID");
        function.setPartnerRole(createPartnerRole("AP"));
        return function;
    }

    private Document createDocument(String snapshot) {
        Document document = new Document();
        document.setName("Test-Fachvefahren");
        document.setDocumentID("IDtest");
        document.getDocumentRepresentation().add(createDocumentRepresentation("Test-Fachvefahren",
                "text/xml", snapshot.getBytes(StandardCharsets.UTF_8)));
        return document;
    }

    private DocumentRepresentation createDocumentRepresentation(String filename, String contentType, byte[] content) {
        DocumentRepresentation rep = new DocumentRepresentation();
        rep.setFilename(filename);
        rep.setContentType(contentType);
        rep.setContent(content);
        rep.setDocumentRepresentationID("test-doc-id");
        return rep;
    }

    private MainApplication createMainApplication(String name, String requestID) {
        MainApplication main = new MainApplication();
        main.setTitle(name);
        main.setApplicationNamespace(createCountryCodeState());
        main.setCaseID("");
        main.setMainApplicationID(requestID);
        return main;
    }

    private Partner createPartner(String authentNiveau) {

        Partner partner = new Partner();

        // Bürgerkonto
        partner.setPartnerID("test-partner-id");
        partner.setAuthentNiveau(createCodeAuthentNiveau(authentNiveau));

        // required
        partner.setPostalCode("test-postal-code");

        // required
        partner.setCity("test-city");

        // required
        partner.setIdentityNamespace(createCountryCodeState());
        // required
        partner.setPartnerType(createPartnerType());
        // required
        partner.setCountry(createCountryCode());
        // required
        partner.setNationality(createCountryCode());

        return partner;
    }

    private CodeAuthentNiveau createCodeAuthentNiveau(String authentNiveau) {
        CodeAuthentNiveau authNiveau = new CodeAuthentNiveau();
        authNiveau.setCode(authentNiveau);
        return authNiveau;
    }

    private CodePartnerType createPartnerType() {
        CodePartnerType partnerType = new CodePartnerType();
        partnerType.setCode("1");
        return partnerType;
    }

    private CodeCountryState createCountryCodeState() {
        CodeCountryState countryCodeState = new CodeCountryState();
        countryCodeState.setCode("DE-BW");
        return countryCodeState;
    }

    private CodeCountry createCountryCode() {
        CodeCountry countryCode = new CodeCountry();
        countryCode.setCode("DE");
        countryCode.setListVersionID("2019-04-01");
        return countryCode;
    }

}
