package de.kommone.webservice.utils;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import de.kommone.ver.FolgenachrichtVersenden;
import de.kommone.ver.LiegenschaftsKatasterAuszugBeantragen;

public class MessageHelper {

    private static final String SUPPORTED_PACKAGES = "de.kommone.xfallcontainer:de.kommone.ver";

    public String transform(String xml) {

        Object unmarshalledObject = null;
        try {
            unmarshalledObject = unmarshall(xml, null);
        } catch (Exception e) {
            return null;
        }

        // prüfen, um welches Objekt es sich handelt
        if (unmarshalledObject instanceof LiegenschaftsKatasterAuszugBeantragen) {
            LiegenschaftsKatasterAuszugBeantragen antrag = (LiegenschaftsKatasterAuszugBeantragen) unmarshalledObject;
            VerHelper helper = new VerHelper();
            FolgenachrichtVersenden transformed = helper.transform(antrag);
            return marshall(transformed, null);
        }

        return null;
    }

    private String marshall(Object object, String pathToScheme) {

        try {
            // JAXB- Kontext aus dem Schema
            JAXBContext context = JAXBContext.newInstance(SUPPORTED_PACKAGES);

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = null;
            if (pathToScheme != null) {
                schema = sf.newSchema(new File(pathToScheme));
            }

            // den Marshaller aus dem Kontext
            Marshaller marshaller = context.createMarshaller();
            marshaller.setSchema(schema);
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

            // den Output in einen String-Writer schreiben
            StringWriter writer = new StringWriter();
            marshaller.marshal(object, writer);

            // und zurückgeben
            return writer.toString();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Object unmarshall(String message, String pathToScheme) {

        try {
            // JAXB- Kontext aus dem Schema
            JAXBContext context = JAXBContext.newInstance(SUPPORTED_PACKAGES);

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = null;
            if (pathToScheme != null) {
                schema = sf.newSchema(new File(pathToScheme));
            }

            // den Unmarshaller aus dem Kontext
            Unmarshaller unsmarshaller = context.createUnmarshaller();
            unsmarshaller.setSchema(schema);

            // und zurückgeben
            return unsmarshaller.unmarshal(new StringReader(message));
        } catch (JAXBException | SAXException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
