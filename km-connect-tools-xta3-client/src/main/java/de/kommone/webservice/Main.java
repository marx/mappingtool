package de.kommone.webservice;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import de.kommone.webservice.utils.Anhang;
import de.kommone.webservice.utils.MessageHelper;
import de.kommone.webservice.utils.XFallHelper;
import de.kommone.webservice.utils.XtaHelper;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Main extends Application {

    private Properties properties = null;

    private static boolean TRUST_ALL = false;

    private Stage myStage;

    private static File keystore = null;

    @FXML
    public Menu newMessageMenu;

    @FXML
    private TextField url;

    @FXML
    private TextField identifier;

    @FXML
    private TextArea textArea;

    @FXML
    private Label keystoreLoaded;

    @FXML
    private CheckBox trustAll;

    @FXML
    private VBox downloads;

    public static void main(String[] args) {
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dumpTreshold", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "99999");
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "99999");
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException, ClassNotFoundException, URISyntaxException {

        final BorderPane application = (BorderPane) new FXMLLoader(getClass().getClassLoader().getResource(
                "XTA3Client.fxml")).load();

        final Scene scene = new Scene(application, 1550, 900);
        scene.setFill(Color.LIGHTGRAY);

        prepareFileToSaveProgress();

        myStage = stage;
        myStage.setScene(scene);
        myStage.setTitle("XTA 3 Client");
        myStage.show();
    }

    private String getResource() {
        String resource = System.getProperty("user.home");
        resource += File.separator + ".xta3client";
        return resource;
    }

    private File prepareFileToSaveProgress() {

        String resource = getResource();
        File savedFiles = new File(resource + "/" + "init.txt");

        if (savedFiles.exists()) {
            // existiert bereits -> nichts zu tun
        } else if (savedFiles.getParentFile().mkdirs()) {
            // erst Parent erzeugen, dann File
            createNewFile(savedFiles);
        } else {
            createNewFile(savedFiles);
        }
        return savedFiles;
    }

    private void createNewFile(File savedFiles, String... errormessage) {
        try {
            savedFiles.createNewFile();
        } catch (IOException e) {
            // wenn etwas schief geht, Fehlermeldung
            showException(e);
        }
    }

    @FXML
    public void initialize() throws ClassNotFoundException, IOException, URISyntaxException, InterruptedException,
            NoSuchFieldException, SecurityException {

        this.properties = new Properties();
        try {
            this.properties.load(Main.class.getClassLoader().getResourceAsStream("ws.properties"));
        } catch (IOException e) {
            showAlert("Problem occured", "Error", "Properties-File konnte nicht geladen werden", AlertType.ERROR);
        }
        keystoreLoaded.setText("Loaded Keystore: " + "km-sta.jks");
        textArea.setWrapText(true);
        url.setText(new XtaHelper(properties, keystore).getURLAsString());
        trustAll.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue != null && newValue) {
                    TRUST_ALL = true;
                } else {
                    TRUST_ALL = false;
                }
            }
        });

    }

    @FXML
    public void fetchMessage() {
        String nachricht = null;
        try {
            String urlAsText = this.url.getText() != null && !url.getText().isEmpty() ? url.getText() : null;
            List<Anhang> anhaenge = new ArrayList<>();
            nachricht = new XtaHelper(properties, keystore).xtaCall(urlAsText, identifier.getText(), anhaenge);
            addDownloads(anhaenge);
        } catch (Exception e) {
            showException(e);
            this.downloads.getChildren().clear();
        }
        textArea.setText(nachricht);
    }

    private void addDownloads(List<Anhang> anhaenge) {
        this.downloads.getChildren().clear();
        int i = 1;
        for (Anhang anhang : anhaenge) {
            Hyperlink link = new Hyperlink();
            String fileName = anhang.getName() + "." + getType(anhang.getType());
            link.setText(i++ + ". " + fileName);
            File file = new File(getResource() + "/" + fileName);
            writeFileToPath(file, anhang.getContent());
            link.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent t) {
                    openDocument(fileName);
                }

                private void openDocument(String text) {
                    try {
                        File file = new File(getResource() + "/" + text);
                        if (!Desktop.isDesktopSupported())// check if Desktop is
                                                          // supported by
                                                          // Platform or not
                        {
                            System.out.println("not supported");
                            return;
                        }
                        Desktop desktop = Desktop.getDesktop();
                        if (file.exists()) // checks file exists or not
                            desktop.open(file); // opens the specified file
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            });
            this.downloads.getChildren().add(link);
        }
    }

    private void writeFileToPath(File file, byte[] data) {
        try {
            Files.write(file.toPath(), data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getType(String type) {
        if (type.indexOf("/") != -1) {
            type = type.substring(type.lastIndexOf("/") + 1);
        }
        return type;
    }

    @FXML
    public void sendMessage() {
        String nachricht = null;
        try {
            String urlAsText = this.url.getText() != null && !url.getText().isEmpty() ? url.getText() : null;
            nachricht = new XtaHelper(properties, keystore).xtaSend(urlAsText, textArea.getText());
        } catch (Exception e) {
            showException(e);
            return;
        }
        showAlert("Nachrichtenübermittlung erfolgreich", "Gratulation", nachricht != null ? nachricht.substring(0, 10)
                + "... übermittelt" : "leere Nachricht übermittelt", AlertType.INFORMATION);
        textArea.setText(nachricht);
    }

    @FXML
    private void loadKeystore() {

        final Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("File Chooser");
        FileChooser fil_chooser = new FileChooser();
        File file = fil_chooser.showOpenDialog(stage);
        if (file == null) {
            return;
        }
        keystore = file;
        keystoreLoaded.setText("Loaded Keystore: " + file.getAbsolutePath());
    }

    @FXML
    private void fromAT() {
        String text = textArea.getText();
        if (text == null || text.isEmpty()) {
            showAlert("Keine Nachricht!", "Geben Sie bitte eine Nachricht ein!", null, AlertType.ERROR);
            return;
        }
        XFallHelper helper = new XFallHelper();
        String xml = helper.extractXML(text);
        // es handelt sich nicht um einen Application Transport
        if (xml == null) {
            showAlert("Achtung! Kein Application-Transport", "Überspringen Sie bitte diesen Schritt!", null,
                    AlertType.WARNING);
        } else {
            textArea.setText(xml);
        }

    }

    @FXML
    private void toAT() {
        String text = textArea.getText();
        if (text == null || text.isEmpty()) {
            showAlert("Keine Nachricht!", "Geben Sie bitte eine Nachricht ein!", null, AlertType.ERROR);
            return;
        }
        XFallHelper helper = new XFallHelper();
        String xml = helper.putToApplicationTransport(text);
        textArea.setText(xml);
    }

    @FXML
    private void transform() {
        MessageHelper helper = new MessageHelper();
        String transform = helper.transform(textArea.getText());
        if (transform != null) {
            textArea.setText(transform);
        } else {
            showAlert("Fehler", "Transformieren gescheitert", null, AlertType.ERROR);
        }
    }

    private void showException(Exception ex) {
        showAlert("Exception Dialog", "Ein Fehler ist aufgetreten", ex.getMessage(), AlertType.ERROR);
    }

    private void showAlert(String title, String headerText, String message, AlertType typeOfAert) {
        Alert alert = new Alert(typeOfAert);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(message);
        alert.showAndWait();
    }

}
