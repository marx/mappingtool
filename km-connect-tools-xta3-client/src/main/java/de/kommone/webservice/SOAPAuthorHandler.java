package de.kommone.webservice;

import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import de.kommone.webservice.sources.MessageMetaData;
import de.kommone.webservice.sources.OriginatorsType;

public class SOAPAuthorHandler implements SOAPHandler<SOAPMessageContext> {

	private OriginatorsType originator = null;

	public SOAPAuthorHandler(OriginatorsType originator) {
		this.originator = originator;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {

		try {
			// checking whether handled message is outbound one as per Martin
			// Strauss answer
			final Boolean outbound = (Boolean) context.get("javax.xml.ws.handler.message.outbound");
			if (outbound != null && outbound) {
				// obtaining marshaller which should marshal instance to xml
				final Marshaller marshaller = JAXBContext.newInstance(MessageMetaData.class).createMarshaller();
				// adding header because otherwise it's null
				final SOAPHeader soapHeader = context.getMessage().getSOAPPart().getEnvelope().getHeader();
				// marshalling instance (appending) to SOAP header's xml node
				MessageMetaData mmd = new MessageMetaData();
				mmd.setOriginators(originator);
				marshaller.marshal(mmd, soapHeader);

				SOAPMessage message = context.getMessage();
				message.saveChanges();
			}
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
		return true;
	}

	@Override
	public void close(MessageContext arg0) {

	}

	@Override
	public boolean handleFault(SOAPMessageContext arg0) {
		return false;
	}

	@Override
	public Set<QName> getHeaders() {
		return null;
	}

}
