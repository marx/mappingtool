package de.kommone.webservice;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class WsHostnameVerificationDisabler implements HostnameVerifier {

    @Override
    public boolean verify(String hostname, SSLSession session) {
        return true;
    }

}

