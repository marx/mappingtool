
package eu.osci.ws._2008._05.transport;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r X509TokenContainerType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="X509TokenContainerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element ref="{http://www.osci.eu/ws/2008/05/transport}X509TokenInfo"/>
 *       &lt;/sequence>
 *       &lt;attribute name="validateCompleted" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X509TokenContainerType", propOrder = {
    "x509TokenInfo"
})
public class X509TokenContainerType {

    @XmlElement(name = "X509TokenInfo", required = true)
    protected List<X509TokenInfo> x509TokenInfo;
    @XmlAttribute(name = "validateCompleted")
    protected Boolean validateCompleted;

    /**
     * Gets the value of the x509TokenInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the x509TokenInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getX509TokenInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link X509TokenInfo }
     * 
     * 
     */
    public List<X509TokenInfo> getX509TokenInfo() {
        if (x509TokenInfo == null) {
            x509TokenInfo = new ArrayList<X509TokenInfo>();
        }
        return this.x509TokenInfo;
    }

    /**
     * Ruft den Wert der validateCompleted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isValidateCompleted() {
        if (validateCompleted == null) {
            return false;
        } else {
            return validateCompleted;
        }
    }

    /**
     * Legt den Wert der validateCompleted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setValidateCompleted(Boolean value) {
        this.validateCompleted = value;
    }

}
