
package eu.osci.ws._2008._05.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r MsgTimeStampsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MsgTimeStampsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObsoleteAfter" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="Delivery" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InitialFetch" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Reception" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MsgTimeStampsType", propOrder = {
    "obsoleteAfter",
    "delivery",
    "initialFetch",
    "reception"
})
public class MsgTimeStampsType {

    @XmlElement(name = "ObsoleteAfter")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar obsoleteAfter;
    @XmlElement(name = "Delivery")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar delivery;
    @XmlElement(name = "InitialFetch")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar initialFetch;
    @XmlElement(name = "Reception")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reception;

    /**
     * Ruft den Wert der obsoleteAfter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getObsoleteAfter() {
        return obsoleteAfter;
    }

    /**
     * Legt den Wert der obsoleteAfter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setObsoleteAfter(XMLGregorianCalendar value) {
        this.obsoleteAfter = value;
    }

    /**
     * Ruft den Wert der delivery-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDelivery() {
        return delivery;
    }

    /**
     * Legt den Wert der delivery-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDelivery(XMLGregorianCalendar value) {
        this.delivery = value;
    }

    /**
     * Ruft den Wert der initialFetch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInitialFetch() {
        return initialFetch;
    }

    /**
     * Legt den Wert der initialFetch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInitialFetch(XMLGregorianCalendar value) {
        this.initialFetch = value;
    }

    /**
     * Ruft den Wert der reception-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReception() {
        return reception;
    }

    /**
     * Legt den Wert der reception-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReception(XMLGregorianCalendar value) {
        this.reception = value;
    }

}
