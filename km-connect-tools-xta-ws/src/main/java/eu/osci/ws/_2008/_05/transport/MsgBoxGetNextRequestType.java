
package eu.osci.ws._2008._05.transport;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.w3._2005._08.addressing.AttributedURIType;


/**
 * <p>Java-Klasse f�r MsgBoxGetNextRequestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MsgBoxGetNextRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="LastMsgReceived" type="{http://www.w3.org/2005/08/addressing}AttributedURIType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="MsgBoxRequestID" use="required" type="{http://www.w3.org/2001/XMLSchema}anyURI" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MsgBoxGetNextRequestType", propOrder = {
    "lastMsgReceived"
})
public class MsgBoxGetNextRequestType {

    @XmlElement(name = "LastMsgReceived")
    protected List<AttributedURIType> lastMsgReceived;
    @XmlAttribute(name = "MsgBoxRequestID", required = true)
    @XmlSchemaType(name = "anyURI")
    protected String msgBoxRequestID;

    /**
     * Gets the value of the lastMsgReceived property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lastMsgReceived property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLastMsgReceived().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributedURIType }
     * 
     * 
     */
    public List<AttributedURIType> getLastMsgReceived() {
        if (lastMsgReceived == null) {
            lastMsgReceived = new ArrayList<AttributedURIType>();
        }
        return this.lastMsgReceived;
    }

    /**
     * Ruft den Wert der msgBoxRequestID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgBoxRequestID() {
        return msgBoxRequestID;
    }

    /**
     * Legt den Wert der msgBoxRequestID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgBoxRequestID(String value) {
        this.msgBoxRequestID = value;
    }

}
