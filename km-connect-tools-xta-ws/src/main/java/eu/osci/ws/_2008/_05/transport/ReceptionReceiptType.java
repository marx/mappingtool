
package eu.osci.ws._2008._05.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3._2000._09.xmldsig_.SignatureType;


/**
 * <p>Java-Klasse f�r ReceptionReceiptType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReceptionReceiptType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.osci.eu/ws/2008/05/transport}ReceiptInfo"/>
 *         &lt;element ref="{http://www.w3.org/2000/09/xmldsig#}Signature"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceptionReceiptType", propOrder = {
    "receiptInfo",
    "signature"
})
public class ReceptionReceiptType {

    @XmlElement(name = "ReceiptInfo", required = true)
    protected ReceiptInfoType receiptInfo;
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected SignatureType signature;

    /**
     * Ruft den Wert der receiptInfo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReceiptInfoType }
     *     
     */
    public ReceiptInfoType getReceiptInfo() {
        return receiptInfo;
    }

    /**
     * Legt den Wert der receiptInfo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceiptInfoType }
     *     
     */
    public void setReceiptInfo(ReceiptInfoType value) {
        this.receiptInfo = value;
    }

    /**
     * Ruft den Wert der signature-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SignatureType }
     *     
     */
    public SignatureType getSignature() {
        return signature;
    }

    /**
     * Legt den Wert der signature-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SignatureType }
     *     
     */
    public void setSignature(SignatureType value) {
        this.signature = value;
    }

}
