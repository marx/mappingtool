
package eu.osci.ws._2008._05.transport;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.w3._2005._08.addressing.AttributedURIType;
import org.w3._2005._08.addressing.RelatesToType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}MessageID" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}RelatesTo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MsgBoxEntryTimeFrom" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="MsgBoxEntryTimeTo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Extension" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="newEntry" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "messageID",
    "relatesTo",
    "msgBoxEntryTimeFrom",
    "msgBoxEntryTimeTo",
    "extension"
})
@XmlRootElement(name = "MsgSelector")
public class MsgSelector {

    @XmlElement(name = "MessageID", namespace = "http://www.w3.org/2005/08/addressing")
    protected List<AttributedURIType> messageID;
    @XmlElement(name = "RelatesTo", namespace = "http://www.w3.org/2005/08/addressing")
    protected List<RelatesToType> relatesTo;
    @XmlElement(name = "MsgBoxEntryTimeFrom")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar msgBoxEntryTimeFrom;
    @XmlElement(name = "MsgBoxEntryTimeTo")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar msgBoxEntryTimeTo;
    @XmlElement(name = "Extension")
    protected Object extension;
    @XmlAttribute(name = "newEntry")
    protected Boolean newEntry;

    /**
     * Gets the value of the messageID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messageID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessageID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributedURIType }
     * 
     * 
     */
    public List<AttributedURIType> getMessageID() {
        if (messageID == null) {
            messageID = new ArrayList<AttributedURIType>();
        }
        return this.messageID;
    }

    /**
     * Gets the value of the relatesTo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relatesTo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelatesTo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RelatesToType }
     * 
     * 
     */
    public List<RelatesToType> getRelatesTo() {
        if (relatesTo == null) {
            relatesTo = new ArrayList<RelatesToType>();
        }
        return this.relatesTo;
    }

    /**
     * Ruft den Wert der msgBoxEntryTimeFrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMsgBoxEntryTimeFrom() {
        return msgBoxEntryTimeFrom;
    }

    /**
     * Legt den Wert der msgBoxEntryTimeFrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMsgBoxEntryTimeFrom(XMLGregorianCalendar value) {
        this.msgBoxEntryTimeFrom = value;
    }

    /**
     * Ruft den Wert der msgBoxEntryTimeTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMsgBoxEntryTimeTo() {
        return msgBoxEntryTimeTo;
    }

    /**
     * Legt den Wert der msgBoxEntryTimeTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMsgBoxEntryTimeTo(XMLGregorianCalendar value) {
        this.msgBoxEntryTimeTo = value;
    }

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setExtension(Object value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der newEntry-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNewEntry() {
        return newEntry;
    }

    /**
     * Legt den Wert der newEntry-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNewEntry(Boolean value) {
        this.newEntry = value;
    }

}
