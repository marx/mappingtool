
package eu.osci.ws._2008._05.transport;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.osci.ws._2008._05.transport package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FetchedNotification_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "FetchedNotification");
    private final static QName _X509TokenContainer_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "X509TokenContainer");
    private final static QName _ReceptionReceipt_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "ReceptionReceipt");
    private final static QName _MsgBoxGetNextRequest_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "MsgBoxGetNextRequest");
    private final static QName _SubmissionReceipt_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "SubmissionReceipt");
    private final static QName _RelayReceipt_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "RelayReceipt");
    private final static QName _MsgBoxCloseRequest_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "MsgBoxCloseRequest");
    private final static QName _FetchedNotificationDemand_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "FetchedNotificationDemand");
    private final static QName _MsgRetainDays_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "MsgRetainDays");
    private final static QName _MsgBoxStatusListRequest_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "MsgBoxStatusListRequest");
    private final static QName _TypeOfBusinessScenario_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "TypeOfBusinessScenario");
    private final static QName _DeliveryReceipt_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "DeliveryReceipt");
    private final static QName _MsgBoxResponse_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "MsgBoxResponse");
    private final static QName _ReceiptInfo_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "ReceiptInfo");
    private final static QName _ReceptionReceiptDemand_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "ReceptionReceiptDemand");
    private final static QName _MsgStatusList_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "MsgStatusList");
    private final static QName _DeliveryReceiptDemand_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "DeliveryReceiptDemand");
    private final static QName _MsgTimeStamps_QNAME = new QName("http://www.osci.eu/ws/2008/05/transport", "MsgTimeStamps");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.osci.ws._2008._05.transport
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link X509TokenInfo }
     * 
     */
    public X509TokenInfo createX509TokenInfo() {
        return new X509TokenInfo();
    }

    /**
     * Create an instance of {@link MsgBoxResponseType }
     * 
     */
    public MsgBoxResponseType createMsgBoxResponseType() {
        return new MsgBoxResponseType();
    }

    /**
     * Create an instance of {@link ObsoleteAfterAssertion }
     * 
     */
    public ObsoleteAfterAssertion createObsoleteAfterAssertion() {
        return new ObsoleteAfterAssertion();
    }

    /**
     * Create an instance of {@link FetchedNotificationDemandType }
     * 
     */
    public FetchedNotificationDemandType createFetchedNotificationDemandType() {
        return new FetchedNotificationDemandType();
    }

    /**
     * Create an instance of {@link ReceptionReceiptDemandType }
     * 
     */
    public ReceptionReceiptDemandType createReceptionReceiptDemandType() {
        return new ReceptionReceiptDemandType();
    }

    /**
     * Create an instance of {@link MsgSelector }
     * 
     */
    public MsgSelector createMsgSelector() {
        return new MsgSelector();
    }

    /**
     * Create an instance of {@link DeliveryReceiptType }
     * 
     */
    public DeliveryReceiptType createDeliveryReceiptType() {
        return new DeliveryReceiptType();
    }

    /**
     * Create an instance of {@link AcceptedMsgLimits }
     * 
     */
    public AcceptedMsgLimits createAcceptedMsgLimits() {
        return new AcceptedMsgLimits();
    }

    /**
     * Create an instance of {@link X509TokenContainerType }
     * 
     */
    public X509TokenContainerType createX509TokenContainerType() {
        return new X509TokenContainerType();
    }

    /**
     * Create an instance of {@link FetchedNotificationType }
     * 
     */
    public FetchedNotificationType createFetchedNotificationType() {
        return new FetchedNotificationType();
    }

    /**
     * Create an instance of {@link MsgStatusListType }
     * 
     */
    public MsgStatusListType createMsgStatusListType() {
        return new MsgStatusListType();
    }

    /**
     * Create an instance of {@link MsgBoxStatusListRequestType }
     * 
     */
    public MsgBoxStatusListRequestType createMsgBoxStatusListRequestType() {
        return new MsgBoxStatusListRequestType();
    }

    /**
     * Create an instance of {@link ReceptionReceiptType }
     * 
     */
    public ReceptionReceiptType createReceptionReceiptType() {
        return new ReceptionReceiptType();
    }

    /**
     * Create an instance of {@link X509TokenInfo.TokenApplication }
     * 
     */
    public X509TokenInfo.TokenApplication createX509TokenInfoTokenApplication() {
        return new X509TokenInfo.TokenApplication();
    }

    /**
     * Create an instance of {@link MsgBoxFetchRequest }
     * 
     */
    public MsgBoxFetchRequest createMsgBoxFetchRequest() {
        return new MsgBoxFetchRequest();
    }

    /**
     * Create an instance of {@link MsgBoxRequestType }
     * 
     */
    public MsgBoxRequestType createMsgBoxRequestType() {
        return new MsgBoxRequestType();
    }

    /**
     * Create an instance of {@link DeliveryReceiptDemandType }
     * 
     */
    public DeliveryReceiptDemandType createDeliveryReceiptDemandType() {
        return new DeliveryReceiptDemandType();
    }

    /**
     * Create an instance of {@link QualTspAssertion }
     * 
     */
    public QualTspAssertion createQualTspAssertion() {
        return new QualTspAssertion();
    }

    /**
     * Create an instance of {@link TypeOfBusinessScenarioType }
     * 
     */
    public TypeOfBusinessScenarioType createTypeOfBusinessScenarioType() {
        return new TypeOfBusinessScenarioType();
    }

    /**
     * Create an instance of {@link MsgTimeStampsType }
     * 
     */
    public MsgTimeStampsType createMsgTimeStampsType() {
        return new MsgTimeStampsType();
    }

    /**
     * Create an instance of {@link MsgBoxCloseRequestType }
     * 
     */
    public MsgBoxCloseRequestType createMsgBoxCloseRequestType() {
        return new MsgBoxCloseRequestType();
    }

    /**
     * Create an instance of {@link X509CertificateAssertion }
     * 
     */
    public X509CertificateAssertion createX509CertificateAssertion() {
        return new X509CertificateAssertion();
    }

    /**
     * Create an instance of {@link MsgBoxGetNextRequestType }
     * 
     */
    public MsgBoxGetNextRequestType createMsgBoxGetNextRequestType() {
        return new MsgBoxGetNextRequestType();
    }

    /**
     * Create an instance of {@link ReceiptInfoType }
     * 
     */
    public ReceiptInfoType createReceiptInfoType() {
        return new ReceiptInfoType();
    }

    /**
     * Create an instance of {@link MsgAttributeListType }
     * 
     */
    public MsgAttributeListType createMsgAttributeListType() {
        return new MsgAttributeListType();
    }

    /**
     * Create an instance of {@link ReceiptDemandType }
     * 
     */
    public ReceiptDemandType createReceiptDemandType() {
        return new ReceiptDemandType();
    }

    /**
     * Create an instance of {@link MessageBody }
     * 
     */
    public MessageBody createMessageBody() {
        return new MessageBody();
    }

    /**
     * Create an instance of {@link MsgBoxResponseType.NoMessageAvailable }
     * 
     */
    public MsgBoxResponseType.NoMessageAvailable createMsgBoxResponseTypeNoMessageAvailable() {
        return new MsgBoxResponseType.NoMessageAvailable();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FetchedNotificationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "FetchedNotification")
    public JAXBElement<FetchedNotificationType> createFetchedNotification(FetchedNotificationType value) {
        return new JAXBElement<FetchedNotificationType>(_FetchedNotification_QNAME, FetchedNotificationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link X509TokenContainerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "X509TokenContainer")
    public JAXBElement<X509TokenContainerType> createX509TokenContainer(X509TokenContainerType value) {
        return new JAXBElement<X509TokenContainerType>(_X509TokenContainer_QNAME, X509TokenContainerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceptionReceiptType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "ReceptionReceipt")
    public JAXBElement<ReceptionReceiptType> createReceptionReceipt(ReceptionReceiptType value) {
        return new JAXBElement<ReceptionReceiptType>(_ReceptionReceipt_QNAME, ReceptionReceiptType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsgBoxGetNextRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "MsgBoxGetNextRequest")
    public JAXBElement<MsgBoxGetNextRequestType> createMsgBoxGetNextRequest(MsgBoxGetNextRequestType value) {
        return new JAXBElement<MsgBoxGetNextRequestType>(_MsgBoxGetNextRequest_QNAME, MsgBoxGetNextRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeliveryReceiptType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "SubmissionReceipt")
    public JAXBElement<DeliveryReceiptType> createSubmissionReceipt(DeliveryReceiptType value) {
        return new JAXBElement<DeliveryReceiptType>(_SubmissionReceipt_QNAME, DeliveryReceiptType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeliveryReceiptType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "RelayReceipt")
    public JAXBElement<DeliveryReceiptType> createRelayReceipt(DeliveryReceiptType value) {
        return new JAXBElement<DeliveryReceiptType>(_RelayReceipt_QNAME, DeliveryReceiptType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsgBoxCloseRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "MsgBoxCloseRequest")
    public JAXBElement<MsgBoxCloseRequestType> createMsgBoxCloseRequest(MsgBoxCloseRequestType value) {
        return new JAXBElement<MsgBoxCloseRequestType>(_MsgBoxCloseRequest_QNAME, MsgBoxCloseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FetchedNotificationDemandType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "FetchedNotificationDemand")
    public JAXBElement<FetchedNotificationDemandType> createFetchedNotificationDemand(FetchedNotificationDemandType value) {
        return new JAXBElement<FetchedNotificationDemandType>(_FetchedNotificationDemand_QNAME, FetchedNotificationDemandType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "MsgRetainDays")
    public JAXBElement<BigInteger> createMsgRetainDays(BigInteger value) {
        return new JAXBElement<BigInteger>(_MsgRetainDays_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsgBoxStatusListRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "MsgBoxStatusListRequest")
    public JAXBElement<MsgBoxStatusListRequestType> createMsgBoxStatusListRequest(MsgBoxStatusListRequestType value) {
        return new JAXBElement<MsgBoxStatusListRequestType>(_MsgBoxStatusListRequest_QNAME, MsgBoxStatusListRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeOfBusinessScenarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "TypeOfBusinessScenario")
    public JAXBElement<TypeOfBusinessScenarioType> createTypeOfBusinessScenario(TypeOfBusinessScenarioType value) {
        return new JAXBElement<TypeOfBusinessScenarioType>(_TypeOfBusinessScenario_QNAME, TypeOfBusinessScenarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeliveryReceiptType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "DeliveryReceipt")
    public JAXBElement<DeliveryReceiptType> createDeliveryReceipt(DeliveryReceiptType value) {
        return new JAXBElement<DeliveryReceiptType>(_DeliveryReceipt_QNAME, DeliveryReceiptType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsgBoxResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "MsgBoxResponse")
    public JAXBElement<MsgBoxResponseType> createMsgBoxResponse(MsgBoxResponseType value) {
        return new JAXBElement<MsgBoxResponseType>(_MsgBoxResponse_QNAME, MsgBoxResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiptInfoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "ReceiptInfo")
    public JAXBElement<ReceiptInfoType> createReceiptInfo(ReceiptInfoType value) {
        return new JAXBElement<ReceiptInfoType>(_ReceiptInfo_QNAME, ReceiptInfoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceptionReceiptDemandType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "ReceptionReceiptDemand")
    public JAXBElement<ReceptionReceiptDemandType> createReceptionReceiptDemand(ReceptionReceiptDemandType value) {
        return new JAXBElement<ReceptionReceiptDemandType>(_ReceptionReceiptDemand_QNAME, ReceptionReceiptDemandType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsgStatusListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "MsgStatusList")
    public JAXBElement<MsgStatusListType> createMsgStatusList(MsgStatusListType value) {
        return new JAXBElement<MsgStatusListType>(_MsgStatusList_QNAME, MsgStatusListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeliveryReceiptDemandType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "DeliveryReceiptDemand")
    public JAXBElement<DeliveryReceiptDemandType> createDeliveryReceiptDemand(DeliveryReceiptDemandType value) {
        return new JAXBElement<DeliveryReceiptDemandType>(_DeliveryReceiptDemand_QNAME, DeliveryReceiptDemandType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsgTimeStampsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2008/05/transport", name = "MsgTimeStamps")
    public JAXBElement<MsgTimeStampsType> createMsgTimeStamps(MsgTimeStampsType value) {
        return new JAXBElement<MsgTimeStampsType>(_MsgTimeStamps_QNAME, MsgTimeStampsType.class, null, value);
    }

}
