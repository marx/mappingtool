
package eu.osci.ws._2008._05.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.wsaddressing.W3CEndpointReference;
import org.w3._2005._08.addressing.AttributedURIType;


/**
 * <p>Java-Klasse f�r FetchedNotificationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FetchedNotificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FetchedTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}MessageID"/>
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}To"/>
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}From"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FetchedNotificationType", propOrder = {
    "fetchedTime",
    "messageID",
    "to",
    "from"
})
public class FetchedNotificationType {

    @XmlElement(name = "FetchedTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fetchedTime;
    @XmlElement(name = "MessageID", namespace = "http://www.w3.org/2005/08/addressing", required = true)
    protected AttributedURIType messageID;
    @XmlElement(name = "To", namespace = "http://www.w3.org/2005/08/addressing", required = true)
    protected AttributedURIType to;
    @XmlElement(name = "From", namespace = "http://www.w3.org/2005/08/addressing", required = true)
    protected W3CEndpointReference from;

    /**
     * Ruft den Wert der fetchedTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFetchedTime() {
        return fetchedTime;
    }

    /**
     * Legt den Wert der fetchedTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFetchedTime(XMLGregorianCalendar value) {
        this.fetchedTime = value;
    }

    /**
     * Ruft den Wert der messageID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributedURIType }
     *     
     */
    public AttributedURIType getMessageID() {
        return messageID;
    }

    /**
     * Legt den Wert der messageID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributedURIType }
     *     
     */
    public void setMessageID(AttributedURIType value) {
        this.messageID = value;
    }

    /**
     * Ruft den Wert der to-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributedURIType }
     *     
     */
    public AttributedURIType getTo() {
        return to;
    }

    /**
     * Legt den Wert der to-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributedURIType }
     *     
     */
    public void setTo(AttributedURIType value) {
        this.to = value;
    }

    /**
     * Ruft den Wert der from-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link W3CEndpointReference }
     *     
     */
    public W3CEndpointReference getFrom() {
        return from;
    }

    /**
     * Legt den Wert der from-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link W3CEndpointReference }
     *     
     */
    public void setFrom(W3CEndpointReference value) {
        this.from = value;
    }

}
