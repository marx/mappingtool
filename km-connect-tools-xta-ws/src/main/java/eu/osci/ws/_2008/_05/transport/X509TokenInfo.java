
package eu.osci.ws._2008._05.transport;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import org.w3._2000._09.xmldsig_.X509DataType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.w3.org/2000/09/xmldsig#}X509Data"/>
 *         &lt;element name="TokenApplication" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TimeInstant" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                   &lt;element name="MsgItemRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="validateResultRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *                 &lt;attribute name="ocspNoCache" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="validated" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="Id" use="required" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "x509Data",
    "tokenApplication"
})
@XmlRootElement(name = "X509TokenInfo")
public class X509TokenInfo {

    @XmlElement(name = "X509Data", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected X509DataType x509Data;
    @XmlElement(name = "TokenApplication", required = true)
    protected List<X509TokenInfo.TokenApplication> tokenApplication;
    @XmlAttribute(name = "validated")
    protected Boolean validated;
    @XmlAttribute(name = "Id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Ruft den Wert der x509Data-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link X509DataType }
     *     
     */
    public X509DataType getX509Data() {
        return x509Data;
    }

    /**
     * Legt den Wert der x509Data-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link X509DataType }
     *     
     */
    public void setX509Data(X509DataType value) {
        this.x509Data = value;
    }

    /**
     * Gets the value of the tokenApplication property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tokenApplication property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTokenApplication().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link X509TokenInfo.TokenApplication }
     * 
     * 
     */
    public List<X509TokenInfo.TokenApplication> getTokenApplication() {
        if (tokenApplication == null) {
            tokenApplication = new ArrayList<X509TokenInfo.TokenApplication>();
        }
        return this.tokenApplication;
    }

    /**
     * Ruft den Wert der validated-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isValidated() {
        if (validated == null) {
            return false;
        } else {
            return validated;
        }
    }

    /**
     * Legt den Wert der validated-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setValidated(Boolean value) {
        this.validated = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TimeInstant" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *         &lt;element name="MsgItemRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="validateResultRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
     *       &lt;attribute name="ocspNoCache" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "timeInstant",
        "msgItemRef"
    })
    public static class TokenApplication {

        @XmlElement(name = "TimeInstant", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar timeInstant;
        @XmlElement(name = "MsgItemRef")
        @XmlIDREF
        @XmlSchemaType(name = "IDREF")
        protected Object msgItemRef;
        @XmlAttribute(name = "validateResultRef")
        @XmlIDREF
        @XmlSchemaType(name = "IDREF")
        protected Object validateResultRef;
        @XmlAttribute(name = "ocspNoCache")
        protected Boolean ocspNoCache;

        /**
         * Ruft den Wert der timeInstant-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getTimeInstant() {
            return timeInstant;
        }

        /**
         * Legt den Wert der timeInstant-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setTimeInstant(XMLGregorianCalendar value) {
            this.timeInstant = value;
        }

        /**
         * Ruft den Wert der msgItemRef-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getMsgItemRef() {
            return msgItemRef;
        }

        /**
         * Legt den Wert der msgItemRef-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setMsgItemRef(Object value) {
            this.msgItemRef = value;
        }

        /**
         * Ruft den Wert der validateResultRef-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getValidateResultRef() {
            return validateResultRef;
        }

        /**
         * Legt den Wert der validateResultRef-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setValidateResultRef(Object value) {
            this.validateResultRef = value;
        }

        /**
         * Ruft den Wert der ocspNoCache-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isOcspNoCache() {
            return ocspNoCache;
        }

        /**
         * Legt den Wert der ocspNoCache-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setOcspNoCache(Boolean value) {
            this.ocspNoCache = value;
        }

    }

}
