
package eu.osci.ws._2008._05.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r MsgBoxRequestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MsgBoxRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.osci.eu/ws/2008/05/transport}MsgSelector" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MsgBoxRequestType", propOrder = {
    "msgSelector"
})
@XmlSeeAlso({
    MsgBoxStatusListRequestType.class,
    MsgBoxFetchRequest.class
})
public class MsgBoxRequestType {

    @XmlElement(name = "MsgSelector")
    protected MsgSelector msgSelector;

    /**
     * Ruft den Wert der msgSelector-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MsgSelector }
     *     
     */
    public MsgSelector getMsgSelector() {
        return msgSelector;
    }

    /**
     * Legt den Wert der msgSelector-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MsgSelector }
     *     
     */
    public void setMsgSelector(MsgSelector value) {
        this.msgSelector = value;
    }

}
