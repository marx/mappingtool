
package eu.osci.ws._2008._05.transport;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import eu.osci.ws._2014._10.transport.MessageMetaData;


/**
 * <p>Java-Klasse f�r MsgStatusListType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MsgStatusListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MsgAttributes" type="{http://www.osci.eu/ws/2008/05/transport}MsgAttributeListType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}MessageMetaData" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MsgStatusListType", propOrder = {
    "msgAttributes",
    "messageMetaData"
})
public class MsgStatusListType {

    @XmlElement(name = "MsgAttributes")
    protected List<MsgAttributeListType> msgAttributes;
    @XmlElement(name = "MessageMetaData", namespace = "http://www.osci.eu/ws/2014/10/transport")
    protected List<MessageMetaData> messageMetaData;

    /**
     * Gets the value of the msgAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the msgAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMsgAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MsgAttributeListType }
     * 
     * 
     */
    public List<MsgAttributeListType> getMsgAttributes() {
        if (msgAttributes == null) {
            msgAttributes = new ArrayList<MsgAttributeListType>();
        }
        return this.msgAttributes;
    }

    /**
     * Gets the value of the messageMetaData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messageMetaData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessageMetaData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MessageMetaData }
     * 
     * 
     */
    public List<MessageMetaData> getMessageMetaData() {
        if (messageMetaData == null) {
            messageMetaData = new ArrayList<MessageMetaData>();
        }
        return this.messageMetaData;
    }

}
