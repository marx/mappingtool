
package eu.osci.ws._2008._05.transport;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.wsaddressing.W3CEndpointReference;
import org.w3._2005._08.addressing.AttributedURIType;
import org.w3._2005._08.addressing.RelatesToType;


/**
 * <p>Java-Klasse f�r MsgAttributeListType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MsgAttributeListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}MessageID"/>
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}RelatesTo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}From" minOccurs="0"/>
 *         &lt;element ref="{http://www.osci.eu/ws/2008/05/transport}TypeOfBusinessScenario"/>
 *         &lt;element name="MsgSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ObsoleteAfterDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="DeliveryTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="InitialFetchedTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MsgAttributeListType", propOrder = {
    "messageID",
    "relatesTo",
    "from",
    "typeOfBusinessScenario",
    "msgSize",
    "obsoleteAfterDate",
    "deliveryTime",
    "initialFetchedTime"
})
public class MsgAttributeListType {

    @XmlElement(name = "MessageID", namespace = "http://www.w3.org/2005/08/addressing", required = true)
    protected AttributedURIType messageID;
    @XmlElement(name = "RelatesTo", namespace = "http://www.w3.org/2005/08/addressing")
    protected List<RelatesToType> relatesTo;
    @XmlElement(name = "From", namespace = "http://www.w3.org/2005/08/addressing")
    protected W3CEndpointReference from;
    @XmlElement(name = "TypeOfBusinessScenario", required = true)
    protected TypeOfBusinessScenarioType typeOfBusinessScenario;
    @XmlElement(name = "MsgSize")
    protected int msgSize;
    @XmlElement(name = "ObsoleteAfterDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar obsoleteAfterDate;
    @XmlElement(name = "DeliveryTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar deliveryTime;
    @XmlElement(name = "InitialFetchedTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar initialFetchedTime;

    /**
     * Ruft den Wert der messageID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributedURIType }
     *     
     */
    public AttributedURIType getMessageID() {
        return messageID;
    }

    /**
     * Legt den Wert der messageID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributedURIType }
     *     
     */
    public void setMessageID(AttributedURIType value) {
        this.messageID = value;
    }

    /**
     * Gets the value of the relatesTo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relatesTo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelatesTo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RelatesToType }
     * 
     * 
     */
    public List<RelatesToType> getRelatesTo() {
        if (relatesTo == null) {
            relatesTo = new ArrayList<RelatesToType>();
        }
        return this.relatesTo;
    }

    /**
     * Ruft den Wert der from-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link W3CEndpointReference }
     *     
     */
    public W3CEndpointReference getFrom() {
        return from;
    }

    /**
     * Legt den Wert der from-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link W3CEndpointReference }
     *     
     */
    public void setFrom(W3CEndpointReference value) {
        this.from = value;
    }

    /**
     * Ruft den Wert der typeOfBusinessScenario-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TypeOfBusinessScenarioType }
     *     
     */
    public TypeOfBusinessScenarioType getTypeOfBusinessScenario() {
        return typeOfBusinessScenario;
    }

    /**
     * Legt den Wert der typeOfBusinessScenario-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeOfBusinessScenarioType }
     *     
     */
    public void setTypeOfBusinessScenario(TypeOfBusinessScenarioType value) {
        this.typeOfBusinessScenario = value;
    }

    /**
     * Ruft den Wert der msgSize-Eigenschaft ab.
     * 
     */
    public int getMsgSize() {
        return msgSize;
    }

    /**
     * Legt den Wert der msgSize-Eigenschaft fest.
     * 
     */
    public void setMsgSize(int value) {
        this.msgSize = value;
    }

    /**
     * Ruft den Wert der obsoleteAfterDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getObsoleteAfterDate() {
        return obsoleteAfterDate;
    }

    /**
     * Legt den Wert der obsoleteAfterDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setObsoleteAfterDate(XMLGregorianCalendar value) {
        this.obsoleteAfterDate = value;
    }

    /**
     * Ruft den Wert der deliveryTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeliveryTime() {
        return deliveryTime;
    }

    /**
     * Legt den Wert der deliveryTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeliveryTime(XMLGregorianCalendar value) {
        this.deliveryTime = value;
    }

    /**
     * Ruft den Wert der initialFetchedTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInitialFetchedTime() {
        return initialFetchedTime;
    }

    /**
     * Legt den Wert der initialFetchedTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInitialFetchedTime(XMLGregorianCalendar value) {
        this.initialFetchedTime = value;
    }

}
