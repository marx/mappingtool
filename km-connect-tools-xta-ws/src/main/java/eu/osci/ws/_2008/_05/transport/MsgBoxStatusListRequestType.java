
package eu.osci.ws._2008._05.transport;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java-Klasse f�r MsgBoxStatusListRequestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MsgBoxStatusListRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.osci.eu/ws/2008/05/transport}MsgBoxRequestType">
 *       &lt;attribute name="maxListItems" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" />
 *       &lt;attribute name="ListForm">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *             &lt;enumeration value="MsgAtrributes"/>
 *             &lt;enumeration value="MessageMetaData"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MsgBoxStatusListRequestType")
public class MsgBoxStatusListRequestType
    extends MsgBoxRequestType
{

    @XmlAttribute(name = "maxListItems")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger maxListItems;
    @XmlAttribute(name = "ListForm")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String listForm;

    /**
     * Ruft den Wert der maxListItems-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxListItems() {
        return maxListItems;
    }

    /**
     * Legt den Wert der maxListItems-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxListItems(BigInteger value) {
        this.maxListItems = value;
    }

    /**
     * Ruft den Wert der listForm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListForm() {
        return listForm;
    }

    /**
     * Legt den Wert der listForm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListForm(String value) {
        this.listForm = value;
    }

}
