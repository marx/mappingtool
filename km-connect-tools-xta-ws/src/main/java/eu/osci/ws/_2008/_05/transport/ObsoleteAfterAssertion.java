
package eu.osci.ws._2008._05.transport;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MsgRetainDays" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/>
 *         &lt;element name="WarningBeforeMsgObsolete" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="PolicyRef" type="{http://www.w3.org/2001/XMLSchema}anyURI" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "msgRetainDays",
    "warningBeforeMsgObsolete"
})
@XmlRootElement(name = "ObsoleteAfterAssertion")
public class ObsoleteAfterAssertion {

    @XmlElement(name = "MsgRetainDays", required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger msgRetainDays;
    @XmlElement(name = "WarningBeforeMsgObsolete")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger warningBeforeMsgObsolete;
    @XmlAttribute(name = "PolicyRef")
    @XmlSchemaType(name = "anyURI")
    protected String policyRef;

    /**
     * Ruft den Wert der msgRetainDays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMsgRetainDays() {
        return msgRetainDays;
    }

    /**
     * Legt den Wert der msgRetainDays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMsgRetainDays(BigInteger value) {
        this.msgRetainDays = value;
    }

    /**
     * Ruft den Wert der warningBeforeMsgObsolete-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getWarningBeforeMsgObsolete() {
        return warningBeforeMsgObsolete;
    }

    /**
     * Legt den Wert der warningBeforeMsgObsolete-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setWarningBeforeMsgObsolete(BigInteger value) {
        this.warningBeforeMsgObsolete = value;
    }

    /**
     * Ruft den Wert der policyRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyRef() {
        return policyRef;
    }

    /**
     * Legt den Wert der policyRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyRef(String value) {
        this.policyRef = value;
    }

}
