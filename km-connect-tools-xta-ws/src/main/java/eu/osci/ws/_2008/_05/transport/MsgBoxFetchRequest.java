
package eu.osci.ws._2008._05.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.osci.eu/ws/2008/05/transport}MsgBoxRequestType">
 *       &lt;attribute name="MsgPart" default="Envelope">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *             &lt;enumeration value="Envelope"/>
 *             &lt;enumeration value="Header"/>
 *             &lt;enumeration value="Body"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "MsgBoxFetchRequest")
public class MsgBoxFetchRequest
    extends MsgBoxRequestType
{

    @XmlAttribute(name = "MsgPart")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String msgPart;

    /**
     * Ruft den Wert der msgPart-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgPart() {
        if (msgPart == null) {
            return "Envelope";
        } else {
            return msgPart;
        }
    }

    /**
     * Legt den Wert der msgPart-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgPart(String value) {
        this.msgPart = value;
    }

}
