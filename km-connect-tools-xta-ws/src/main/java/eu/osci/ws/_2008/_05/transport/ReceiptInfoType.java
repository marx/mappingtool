
package eu.osci.ws._2008._05.transport;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.ws.wsaddressing.W3CEndpointReference;
import eu.osci.ws._2014._10.transport.MessageMetaData;
import org.w3._2005._08.addressing.AttributedURIType;
import org.w3._2005._08.addressing.RelatesToType;


/**
 * <p>Java-Klasse f�r ReceiptInfoType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReceiptInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}MessageID"/>
 *         &lt;element ref="{http://www.osci.eu/ws/2008/05/transport}MsgTimeStamps"/>
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}RelatesTo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="To" type="{http://www.w3.org/2005/08/addressing}EndpointReferenceType"/>
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}From" minOccurs="0"/>
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}ReplyTo"/>
 *         &lt;element name="RequestEcho" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}MessageMetaData" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Id" use="required" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *       &lt;attribute name="ReceiptIssuerRole">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyURI">
 *             &lt;enumeration value="http://www.osci.eu/ws/2008/05/transport/role/MsgBox"/>
 *             &lt;enumeration value="http://www.osci.eu/ws/2008/05/transport/role/Recipient"/>
 *             &lt;enumeration value="http://www.osci.eu/ws/2008/05/transport/role/Sender "/>
 *             &lt;enumeration value="http://www.osci.eu/ws/2008/05/transport/role/Relay "/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceiptInfoType", propOrder = {
    "messageID",
    "msgTimeStamps",
    "relatesTo",
    "to",
    "from",
    "replyTo",
    "requestEcho",
    "messageMetaData"
})
public class ReceiptInfoType {

    @XmlElement(name = "MessageID", namespace = "http://www.w3.org/2005/08/addressing", required = true)
    protected AttributedURIType messageID;
    @XmlElement(name = "MsgTimeStamps", required = true)
    protected MsgTimeStampsType msgTimeStamps;
    @XmlElement(name = "RelatesTo", namespace = "http://www.w3.org/2005/08/addressing")
    protected List<RelatesToType> relatesTo;
    @XmlElement(name = "To", required = true)
    protected W3CEndpointReference to;
    @XmlElement(name = "From", namespace = "http://www.w3.org/2005/08/addressing")
    protected W3CEndpointReference from;
    @XmlElement(name = "ReplyTo", namespace = "http://www.w3.org/2005/08/addressing", required = true)
    protected W3CEndpointReference replyTo;
    @XmlElement(name = "RequestEcho")
    protected byte[] requestEcho;
    @XmlElement(name = "MessageMetaData", namespace = "http://www.osci.eu/ws/2014/10/transport")
    protected MessageMetaData messageMetaData;
    @XmlAttribute(name = "Id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "ReceiptIssuerRole")
    protected String receiptIssuerRole;

    /**
     * Ruft den Wert der messageID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributedURIType }
     *     
     */
    public AttributedURIType getMessageID() {
        return messageID;
    }

    /**
     * Legt den Wert der messageID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributedURIType }
     *     
     */
    public void setMessageID(AttributedURIType value) {
        this.messageID = value;
    }

    /**
     * Ruft den Wert der msgTimeStamps-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MsgTimeStampsType }
     *     
     */
    public MsgTimeStampsType getMsgTimeStamps() {
        return msgTimeStamps;
    }

    /**
     * Legt den Wert der msgTimeStamps-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MsgTimeStampsType }
     *     
     */
    public void setMsgTimeStamps(MsgTimeStampsType value) {
        this.msgTimeStamps = value;
    }

    /**
     * Gets the value of the relatesTo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relatesTo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelatesTo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RelatesToType }
     * 
     * 
     */
    public List<RelatesToType> getRelatesTo() {
        if (relatesTo == null) {
            relatesTo = new ArrayList<RelatesToType>();
        }
        return this.relatesTo;
    }

    /**
     * Ruft den Wert der to-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link W3CEndpointReference }
     *     
     */
    public W3CEndpointReference getTo() {
        return to;
    }

    /**
     * Legt den Wert der to-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link W3CEndpointReference }
     *     
     */
    public void setTo(W3CEndpointReference value) {
        this.to = value;
    }

    /**
     * Ruft den Wert der from-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link W3CEndpointReference }
     *     
     */
    public W3CEndpointReference getFrom() {
        return from;
    }

    /**
     * Legt den Wert der from-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link W3CEndpointReference }
     *     
     */
    public void setFrom(W3CEndpointReference value) {
        this.from = value;
    }

    /**
     * Ruft den Wert der replyTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link W3CEndpointReference }
     *     
     */
    public W3CEndpointReference getReplyTo() {
        return replyTo;
    }

    /**
     * Legt den Wert der replyTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link W3CEndpointReference }
     *     
     */
    public void setReplyTo(W3CEndpointReference value) {
        this.replyTo = value;
    }

    /**
     * Ruft den Wert der requestEcho-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getRequestEcho() {
        return requestEcho;
    }

    /**
     * Legt den Wert der requestEcho-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setRequestEcho(byte[] value) {
        this.requestEcho = value;
    }

    /**
     * Ruft den Wert der messageMetaData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MessageMetaData }
     *     
     */
    public MessageMetaData getMessageMetaData() {
        return messageMetaData;
    }

    /**
     * Legt den Wert der messageMetaData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageMetaData }
     *     
     */
    public void setMessageMetaData(MessageMetaData value) {
        this.messageMetaData = value;
    }

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der receiptIssuerRole-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptIssuerRole() {
        return receiptIssuerRole;
    }

    /**
     * Legt den Wert der receiptIssuerRole-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptIssuerRole(String value) {
        this.receiptIssuerRole = value;
    }

}
