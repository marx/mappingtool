
package eu.osci.ws._2008._05.transport;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r MsgBoxResponseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MsgBoxResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="NoMessageAvailable">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="reason" use="required" type="{http://www.osci.eu/ws/2008/05/transport}MsgBoxReasonOpenEnum" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ItemsPending" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger"/>
 *       &lt;/choice>
 *       &lt;attribute name="MsgBoxRequestID" use="required" type="{http://www.w3.org/2001/XMLSchema}anyURI" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MsgBoxResponseType", propOrder = {
    "noMessageAvailable",
    "itemsPending"
})
public class MsgBoxResponseType {

    @XmlElement(name = "NoMessageAvailable")
    protected MsgBoxResponseType.NoMessageAvailable noMessageAvailable;
    @XmlElement(name = "ItemsPending")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger itemsPending;
    @XmlAttribute(name = "MsgBoxRequestID", required = true)
    @XmlSchemaType(name = "anyURI")
    protected String msgBoxRequestID;

    /**
     * Ruft den Wert der noMessageAvailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MsgBoxResponseType.NoMessageAvailable }
     *     
     */
    public MsgBoxResponseType.NoMessageAvailable getNoMessageAvailable() {
        return noMessageAvailable;
    }

    /**
     * Legt den Wert der noMessageAvailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MsgBoxResponseType.NoMessageAvailable }
     *     
     */
    public void setNoMessageAvailable(MsgBoxResponseType.NoMessageAvailable value) {
        this.noMessageAvailable = value;
    }

    /**
     * Ruft den Wert der itemsPending-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getItemsPending() {
        return itemsPending;
    }

    /**
     * Legt den Wert der itemsPending-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setItemsPending(BigInteger value) {
        this.itemsPending = value;
    }

    /**
     * Ruft den Wert der msgBoxRequestID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgBoxRequestID() {
        return msgBoxRequestID;
    }

    /**
     * Legt den Wert der msgBoxRequestID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgBoxRequestID(String value) {
        this.msgBoxRequestID = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="reason" use="required" type="{http://www.osci.eu/ws/2008/05/transport}MsgBoxReasonOpenEnum" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class NoMessageAvailable {

        @XmlAttribute(name = "reason", required = true)
        protected String reason;

        /**
         * Ruft den Wert der reason-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReason() {
            return reason;
        }

        /**
         * Legt den Wert der reason-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReason(String value) {
            this.reason = value;
        }

    }

}
