
package eu.osci.ws._2008._05.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.ws.wsaddressing.W3CEndpointReference;


/**
 * <p>Java-Klasse f�r ReceiptDemandType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReceiptDemandType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}ReplyTo"/>
 *       &lt;/sequence>
 *       &lt;attribute name="qualTSPForReceipt" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *       &lt;attribute name="echoRequest" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceiptDemandType", propOrder = {
    "replyTo"
})
@XmlSeeAlso({
    ReceptionReceiptDemandType.class,
    DeliveryReceiptDemandType.class
})
public class ReceiptDemandType {

    @XmlElement(name = "ReplyTo", namespace = "http://www.w3.org/2005/08/addressing", required = true)
    protected W3CEndpointReference replyTo;
    @XmlAttribute(name = "qualTSPForReceipt")
    protected Boolean qualTSPForReceipt;
    @XmlAttribute(name = "echoRequest")
    protected Boolean echoRequest;

    /**
     * Ruft den Wert der replyTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link W3CEndpointReference }
     *     
     */
    public W3CEndpointReference getReplyTo() {
        return replyTo;
    }

    /**
     * Legt den Wert der replyTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link W3CEndpointReference }
     *     
     */
    public void setReplyTo(W3CEndpointReference value) {
        this.replyTo = value;
    }

    /**
     * Ruft den Wert der qualTSPForReceipt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isQualTSPForReceipt() {
        if (qualTSPForReceipt == null) {
            return false;
        } else {
            return qualTSPForReceipt;
        }
    }

    /**
     * Legt den Wert der qualTSPForReceipt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setQualTSPForReceipt(Boolean value) {
        this.qualTSPForReceipt = value;
    }

    /**
     * Ruft den Wert der echoRequest-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isEchoRequest() {
        if (echoRequest == null) {
            return false;
        } else {
            return echoRequest;
        }
    }

    /**
     * Legt den Wert der echoRequest-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEchoRequest(Boolean value) {
        this.echoRequest = value;
    }

}
