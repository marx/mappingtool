
package eu.osci.ws._2014._10.transport;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}OtherReaders" maxOccurs="unbounded"/>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}CcReaders" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "otherReaders",
    "ccReaders"
})
@XmlRootElement(name = "OtherDestinations")
public class OtherDestinations {

    @XmlElement(name = "OtherReaders", required = true)
    protected List<PartyIdentifierType> otherReaders;
    @XmlElement(name = "CcReaders")
    protected List<PartyIdentifierType> ccReaders;

    /**
     * Gets the value of the otherReaders property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherReaders property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherReaders().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyIdentifierType }
     * 
     * 
     */
    public List<PartyIdentifierType> getOtherReaders() {
        if (otherReaders == null) {
            otherReaders = new ArrayList<PartyIdentifierType>();
        }
        return this.otherReaders;
    }

    /**
     * Gets the value of the ccReaders property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ccReaders property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCcReaders().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyIdentifierType }
     * 
     * 
     */
    public List<PartyIdentifierType> getCcReaders() {
        if (ccReaders == null) {
            ccReaders = new ArrayList<PartyIdentifierType>();
        }
        return this.ccReaders;
    }

}
