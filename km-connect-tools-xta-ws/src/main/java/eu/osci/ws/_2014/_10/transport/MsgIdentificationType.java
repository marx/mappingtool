
package eu.osci.ws._2014._10.transport;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3._2005._08.addressing.AttributedURIType;


/**
 * <p>Java-Klasse f�r MsgIdentificationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MsgIdentificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.w3.org/2005/08/addressing}MessageID"/>
 *         &lt;element name="In-Reply-To" type="{http://www.w3.org/2005/08/addressing}AttributedURIType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProcessRef" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Requester" type="{http://www.osci.eu/ws/2014/10/transport}ProcessIdentifierType" minOccurs="0"/>
 *                   &lt;element name="Responder" type="{http://www.osci.eu/ws/2014/10/transport}ProcessIdentifierType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MsgIdentificationType", propOrder = {
    "messageID",
    "inReplyTo",
    "processRef"
})
public class MsgIdentificationType {

    @XmlElement(name = "MessageID", namespace = "http://www.w3.org/2005/08/addressing", required = true)
    protected AttributedURIType messageID;
    @XmlElement(name = "In-Reply-To")
    protected List<AttributedURIType> inReplyTo;
    @XmlElement(name = "ProcessRef")
    protected MsgIdentificationType.ProcessRef processRef;

    /**
     * Ruft den Wert der messageID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AttributedURIType }
     *     
     */
    public AttributedURIType getMessageID() {
        return messageID;
    }

    /**
     * Legt den Wert der messageID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributedURIType }
     *     
     */
    public void setMessageID(AttributedURIType value) {
        this.messageID = value;
    }

    /**
     * Gets the value of the inReplyTo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inReplyTo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInReplyTo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributedURIType }
     * 
     * 
     */
    public List<AttributedURIType> getInReplyTo() {
        if (inReplyTo == null) {
            inReplyTo = new ArrayList<AttributedURIType>();
        }
        return this.inReplyTo;
    }

    /**
     * Ruft den Wert der processRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MsgIdentificationType.ProcessRef }
     *     
     */
    public MsgIdentificationType.ProcessRef getProcessRef() {
        return processRef;
    }

    /**
     * Legt den Wert der processRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MsgIdentificationType.ProcessRef }
     *     
     */
    public void setProcessRef(MsgIdentificationType.ProcessRef value) {
        this.processRef = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Requester" type="{http://www.osci.eu/ws/2014/10/transport}ProcessIdentifierType" minOccurs="0"/>
     *         &lt;element name="Responder" type="{http://www.osci.eu/ws/2014/10/transport}ProcessIdentifierType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "requester",
        "responder"
    })
    public static class ProcessRef {

        @XmlElement(name = "Requester")
        protected ProcessIdentifierType requester;
        @XmlElement(name = "Responder")
        protected ProcessIdentifierType responder;

        /**
         * Ruft den Wert der requester-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ProcessIdentifierType }
         *     
         */
        public ProcessIdentifierType getRequester() {
            return requester;
        }

        /**
         * Legt den Wert der requester-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessIdentifierType }
         *     
         */
        public void setRequester(ProcessIdentifierType value) {
            this.requester = value;
        }

        /**
         * Ruft den Wert der responder-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ProcessIdentifierType }
         *     
         */
        public ProcessIdentifierType getResponder() {
            return responder;
        }

        /**
         * Legt den Wert der responder-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessIdentifierType }
         *     
         */
        public void setResponder(ProcessIdentifierType value) {
            this.responder = value;
        }

    }

}
