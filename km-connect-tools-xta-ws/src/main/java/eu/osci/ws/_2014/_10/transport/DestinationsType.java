
package eu.osci.ws._2014._10.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r DestinationsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DestinationsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}Reader"/>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}OtherDestinations" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DestinationsType", propOrder = {
    "reader",
    "otherDestinations"
})
public class DestinationsType {

    @XmlElement(name = "Reader", required = true)
    protected PartyType reader;
    @XmlElement(name = "OtherDestinations")
    protected OtherDestinations otherDestinations;

    /**
     * Ultimate target of the message
     * 
     * @return
     *     possible object is
     *     {@link PartyType }
     *     
     */
    public PartyType getReader() {
        return reader;
    }

    /**
     * Legt den Wert der reader-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyType }
     *     
     */
    public void setReader(PartyType value) {
        this.reader = value;
    }

    /**
     * Ruft den Wert der otherDestinations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OtherDestinations }
     *     
     */
    public OtherDestinations getOtherDestinations() {
        return otherDestinations;
    }

    /**
     * Legt den Wert der otherDestinations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OtherDestinations }
     *     
     */
    public void setOtherDestinations(OtherDestinations value) {
        this.otherDestinations = value;
    }

}
