
package eu.osci.ws._2014._10.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0.BinarySecurityTokenType;
import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0.SecurityTokenReferenceType;
import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0.UsernameTokenType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd}BinarySecurityToken"/>
 *         &lt;element ref="{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd}SecurityTokenReference"/>
 *         &lt;element ref="{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd}UsernameToken"/>
 *       &lt;/choice>
 *       &lt;attribute name="usage" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *             &lt;enumeration value="AUTHENTICATION"/>
 *             &lt;enumeration value="ENCRYPTION"/>
 *             &lt;enumeration value="SIGNATURE"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="payloadRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "binarySecurityToken",
    "securityTokenReference",
    "usernameToken"
})
@XmlRootElement(name = "SecurityToken")
public class SecurityToken {

    @XmlElement(name = "BinarySecurityToken", namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
    protected BinarySecurityTokenType binarySecurityToken;
    @XmlElement(name = "SecurityTokenReference", namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
    protected SecurityTokenReferenceType securityTokenReference;
    @XmlElement(name = "UsernameToken", namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
    protected UsernameTokenType usernameToken;
    @XmlAttribute(name = "usage", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String usage;
    @XmlAttribute(name = "payloadRef")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object payloadRef;

    /**
     * Ruft den Wert der binarySecurityToken-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BinarySecurityTokenType }
     *     
     */
    public BinarySecurityTokenType getBinarySecurityToken() {
        return binarySecurityToken;
    }

    /**
     * Legt den Wert der binarySecurityToken-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BinarySecurityTokenType }
     *     
     */
    public void setBinarySecurityToken(BinarySecurityTokenType value) {
        this.binarySecurityToken = value;
    }

    /**
     * Ruft den Wert der securityTokenReference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SecurityTokenReferenceType }
     *     
     */
    public SecurityTokenReferenceType getSecurityTokenReference() {
        return securityTokenReference;
    }

    /**
     * Legt den Wert der securityTokenReference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityTokenReferenceType }
     *     
     */
    public void setSecurityTokenReference(SecurityTokenReferenceType value) {
        this.securityTokenReference = value;
    }

    /**
     * Ruft den Wert der usernameToken-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UsernameTokenType }
     *     
     */
    public UsernameTokenType getUsernameToken() {
        return usernameToken;
    }

    /**
     * Legt den Wert der usernameToken-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UsernameTokenType }
     *     
     */
    public void setUsernameToken(UsernameTokenType value) {
        this.usernameToken = value;
    }

    /**
     * Ruft den Wert der usage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsage() {
        return usage;
    }

    /**
     * Legt den Wert der usage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsage(String value) {
        this.usage = value;
    }

    /**
     * Ruft den Wert der payloadRef-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getPayloadRef() {
        return payloadRef;
    }

    /**
     * Legt den Wert der payloadRef-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setPayloadRef(Object value) {
        this.payloadRef = value;
    }

}
