
package eu.osci.ws._2014._10.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.ws.wsaddressing.W3CEndpointReference;


/**
 * <p>Java-Klasse f�r ReceiptRequestType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ReceiptRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Submission" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="Relay" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="Delivery" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="Fetch" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="Reception" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="ReceiptTo" type="{http://www.w3.org/2005/08/addressing}EndpointReferenceType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceiptRequestType", propOrder = {
    "submission",
    "relay",
    "delivery",
    "fetch",
    "reception",
    "receiptTo"
})
public class ReceiptRequestType {

    @XmlElement(name = "Submission")
    protected Object submission;
    @XmlElement(name = "Relay")
    protected Object relay;
    @XmlElement(name = "Delivery")
    protected Object delivery;
    @XmlElement(name = "Fetch")
    protected Object fetch;
    @XmlElement(name = "Reception")
    protected Object reception;
    @XmlElement(name = "ReceiptTo")
    protected W3CEndpointReference receiptTo;

    /**
     * Ruft den Wert der submission-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getSubmission() {
        return submission;
    }

    /**
     * Legt den Wert der submission-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setSubmission(Object value) {
        this.submission = value;
    }

    /**
     * Ruft den Wert der relay-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getRelay() {
        return relay;
    }

    /**
     * Legt den Wert der relay-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setRelay(Object value) {
        this.relay = value;
    }

    /**
     * Ruft den Wert der delivery-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getDelivery() {
        return delivery;
    }

    /**
     * Legt den Wert der delivery-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setDelivery(Object value) {
        this.delivery = value;
    }

    /**
     * Ruft den Wert der fetch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getFetch() {
        return fetch;
    }

    /**
     * Legt den Wert der fetch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setFetch(Object value) {
        this.fetch = value;
    }

    /**
     * Ruft den Wert der reception-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getReception() {
        return reception;
    }

    /**
     * Legt den Wert der reception-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setReception(Object value) {
        this.reception = value;
    }

    /**
     * Ruft den Wert der receiptTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link W3CEndpointReference }
     *     
     */
    public W3CEndpointReference getReceiptTo() {
        return receiptTo;
    }

    /**
     * Legt den Wert der receiptTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link W3CEndpointReference }
     *     
     */
    public void setReceiptTo(W3CEndpointReference value) {
        this.receiptTo = value;
    }

}
