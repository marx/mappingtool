
package eu.osci.ws._2014._10.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Message delivery time instants, quality and receipts requested
 * 
 * <p>Java-Klasse f�r DeliveryAttributesType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DeliveryAttributesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Origin" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InitialSend" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="NotBefore" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ObsoleteAfter" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="Delivery" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InitialFetch" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Reception" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ServiceQuality" type="{http://www.osci.eu/ws/2014/10/transport}NonEmptyStringType" minOccurs="0"/>
 *         &lt;element name="ReceiptRequests" type="{http://www.osci.eu/ws/2014/10/transport}ReceiptRequestType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeliveryAttributesType", propOrder = {
    "origin",
    "initialSend",
    "notBefore",
    "obsoleteAfter",
    "delivery",
    "initialFetch",
    "reception",
    "serviceQuality",
    "receiptRequests"
})
public class DeliveryAttributesType {

    @XmlElement(name = "Origin")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar origin;
    @XmlElement(name = "InitialSend")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar initialSend;
    @XmlElement(name = "NotBefore")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar notBefore;
    @XmlElement(name = "ObsoleteAfter")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar obsoleteAfter;
    @XmlElement(name = "Delivery")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar delivery;
    @XmlElement(name = "InitialFetch")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar initialFetch;
    @XmlElement(name = "Reception")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reception;
    @XmlElement(name = "ServiceQuality")
    protected String serviceQuality;
    @XmlElement(name = "ReceiptRequests")
    protected ReceiptRequestType receiptRequests;

    /**
     * Ruft den Wert der origin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOrigin() {
        return origin;
    }

    /**
     * Legt den Wert der origin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOrigin(XMLGregorianCalendar value) {
        this.origin = value;
    }

    /**
     * Ruft den Wert der initialSend-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInitialSend() {
        return initialSend;
    }

    /**
     * Legt den Wert der initialSend-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInitialSend(XMLGregorianCalendar value) {
        this.initialSend = value;
    }

    /**
     * Ruft den Wert der notBefore-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNotBefore() {
        return notBefore;
    }

    /**
     * Legt den Wert der notBefore-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNotBefore(XMLGregorianCalendar value) {
        this.notBefore = value;
    }

    /**
     * Ruft den Wert der obsoleteAfter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getObsoleteAfter() {
        return obsoleteAfter;
    }

    /**
     * Legt den Wert der obsoleteAfter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setObsoleteAfter(XMLGregorianCalendar value) {
        this.obsoleteAfter = value;
    }

    /**
     * Ruft den Wert der delivery-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDelivery() {
        return delivery;
    }

    /**
     * Legt den Wert der delivery-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDelivery(XMLGregorianCalendar value) {
        this.delivery = value;
    }

    /**
     * Ruft den Wert der initialFetch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInitialFetch() {
        return initialFetch;
    }

    /**
     * Legt den Wert der initialFetch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInitialFetch(XMLGregorianCalendar value) {
        this.initialFetch = value;
    }

    /**
     * Ruft den Wert der reception-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReception() {
        return reception;
    }

    /**
     * Legt den Wert der reception-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReception(XMLGregorianCalendar value) {
        this.reception = value;
    }

    /**
     * Ruft den Wert der serviceQuality-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceQuality() {
        return serviceQuality;
    }

    /**
     * Legt den Wert der serviceQuality-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceQuality(String value) {
        this.serviceQuality = value;
    }

    /**
     * Ruft den Wert der receiptRequests-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReceiptRequestType }
     *     
     */
    public ReceiptRequestType getReceiptRequests() {
        return receiptRequests;
    }

    /**
     * Legt den Wert der receiptRequests-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceiptRequestType }
     *     
     */
    public void setReceiptRequests(ReceiptRequestType value) {
        this.receiptRequests = value;
    }

}
