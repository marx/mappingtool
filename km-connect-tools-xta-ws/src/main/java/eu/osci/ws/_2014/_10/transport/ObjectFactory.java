
package eu.osci.ws._2014._10.transport;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.osci.ws._2014._10.transport package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OtherReaders_QNAME = new QName("http://www.osci.eu/ws/2014/10/transport", "OtherReaders");
    private final static QName _Sender_QNAME = new QName("http://www.osci.eu/ws/2014/10/transport", "Sender");
    private final static QName _Author_QNAME = new QName("http://www.osci.eu/ws/2014/10/transport", "Author");
    private final static QName _Reader_QNAME = new QName("http://www.osci.eu/ws/2014/10/transport", "Reader");
    private final static QName _MsgIdentification_QNAME = new QName("http://www.osci.eu/ws/2014/10/transport", "MsgIdentification");
    private final static QName _Destinations_QNAME = new QName("http://www.osci.eu/ws/2014/10/transport", "Destinations");
    private final static QName _Qualifier_QNAME = new QName("http://www.osci.eu/ws/2014/10/transport", "Qualifier");
    private final static QName _DeliveryAttributes_QNAME = new QName("http://www.osci.eu/ws/2014/10/transport", "DeliveryAttributes");
    private final static QName _CcReaders_QNAME = new QName("http://www.osci.eu/ws/2014/10/transport", "CcReaders");
    private final static QName _Originators_QNAME = new QName("http://www.osci.eu/ws/2014/10/transport", "Originators");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.osci.ws._2014._10.transport
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QualifierType }
     * 
     */
    public QualifierType createQualifierType() {
        return new QualifierType();
    }

    /**
     * Create an instance of {@link MsgIdentificationType }
     * 
     */
    public MsgIdentificationType createMsgIdentificationType() {
        return new MsgIdentificationType();
    }

    /**
     * Create an instance of {@link MessageMetaData }
     * 
     */
    public MessageMetaData createMessageMetaData() {
        return new MessageMetaData();
    }

    /**
     * Create an instance of {@link DeliveryAttributesType }
     * 
     */
    public DeliveryAttributesType createDeliveryAttributesType() {
        return new DeliveryAttributesType();
    }

    /**
     * Create an instance of {@link OriginatorsType }
     * 
     */
    public OriginatorsType createOriginatorsType() {
        return new OriginatorsType();
    }

    /**
     * Create an instance of {@link DestinationsType }
     * 
     */
    public DestinationsType createDestinationsType() {
        return new DestinationsType();
    }

    /**
     * Create an instance of {@link MessageProperties }
     * 
     */
    public MessageProperties createMessageProperties() {
        return new MessageProperties();
    }

    /**
     * Create an instance of {@link PropertyType }
     * 
     */
    public PropertyType createPropertyType() {
        return new PropertyType();
    }

    /**
     * Create an instance of {@link PartyType }
     * 
     */
    public PartyType createPartyType() {
        return new PartyType();
    }

    /**
     * Create an instance of {@link PartyIdentifierType }
     * 
     */
    public PartyIdentifierType createPartyIdentifierType() {
        return new PartyIdentifierType();
    }

    /**
     * Create an instance of {@link SecurityToken }
     * 
     */
    public SecurityToken createSecurityToken() {
        return new SecurityToken();
    }

    /**
     * Create an instance of {@link OtherDestinations }
     * 
     */
    public OtherDestinations createOtherDestinations() {
        return new OtherDestinations();
    }

    /**
     * Create an instance of {@link ProcessIdentifierType }
     * 
     */
    public ProcessIdentifierType createProcessIdentifierType() {
        return new ProcessIdentifierType();
    }

    /**
     * Create an instance of {@link ReceiptRequestType }
     * 
     */
    public ReceiptRequestType createReceiptRequestType() {
        return new ReceiptRequestType();
    }

    /**
     * Create an instance of {@link KeyCodeType }
     * 
     */
    public KeyCodeType createKeyCodeType() {
        return new KeyCodeType();
    }

    /**
     * Create an instance of {@link MessagePropertiesType }
     * 
     */
    public MessagePropertiesType createMessagePropertiesType() {
        return new MessagePropertiesType();
    }

    /**
     * Create an instance of {@link AnyType }
     * 
     */
    public AnyType createAnyType() {
        return new AnyType();
    }

    /**
     * Create an instance of {@link QualifierType.BusinessScenario }
     * 
     */
    public QualifierType.BusinessScenario createQualifierTypeBusinessScenario() {
        return new QualifierType.BusinessScenario();
    }

    /**
     * Create an instance of {@link QualifierType.MessageType }
     * 
     */
    public QualifierType.MessageType createQualifierTypeMessageType() {
        return new QualifierType.MessageType();
    }

    /**
     * Create an instance of {@link MsgIdentificationType.ProcessRef }
     * 
     */
    public MsgIdentificationType.ProcessRef createMsgIdentificationTypeProcessRef() {
        return new MsgIdentificationType.ProcessRef();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartyIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2014/10/transport", name = "OtherReaders")
    public JAXBElement<PartyIdentifierType> createOtherReaders(PartyIdentifierType value) {
        return new JAXBElement<PartyIdentifierType>(_OtherReaders_QNAME, PartyIdentifierType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2014/10/transport", name = "Sender")
    public JAXBElement<PartyType> createSender(PartyType value) {
        return new JAXBElement<PartyType>(_Sender_QNAME, PartyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2014/10/transport", name = "Author")
    public JAXBElement<PartyType> createAuthor(PartyType value) {
        return new JAXBElement<PartyType>(_Author_QNAME, PartyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2014/10/transport", name = "Reader")
    public JAXBElement<PartyType> createReader(PartyType value) {
        return new JAXBElement<PartyType>(_Reader_QNAME, PartyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsgIdentificationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2014/10/transport", name = "MsgIdentification")
    public JAXBElement<MsgIdentificationType> createMsgIdentification(MsgIdentificationType value) {
        return new JAXBElement<MsgIdentificationType>(_MsgIdentification_QNAME, MsgIdentificationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DestinationsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2014/10/transport", name = "Destinations")
    public JAXBElement<DestinationsType> createDestinations(DestinationsType value) {
        return new JAXBElement<DestinationsType>(_Destinations_QNAME, DestinationsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QualifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2014/10/transport", name = "Qualifier")
    public JAXBElement<QualifierType> createQualifier(QualifierType value) {
        return new JAXBElement<QualifierType>(_Qualifier_QNAME, QualifierType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeliveryAttributesType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2014/10/transport", name = "DeliveryAttributes")
    public JAXBElement<DeliveryAttributesType> createDeliveryAttributes(DeliveryAttributesType value) {
        return new JAXBElement<DeliveryAttributesType>(_DeliveryAttributes_QNAME, DeliveryAttributesType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartyIdentifierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2014/10/transport", name = "CcReaders")
    public JAXBElement<PartyIdentifierType> createCcReaders(PartyIdentifierType value) {
        return new JAXBElement<PartyIdentifierType>(_CcReaders_QNAME, PartyIdentifierType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OriginatorsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.osci.eu/ws/2014/10/transport", name = "Originators")
    public JAXBElement<OriginatorsType> createOriginators(OriginatorsType value) {
        return new JAXBElement<OriginatorsType>(_Originators_QNAME, OriginatorsType.class, null, value);
    }

}
