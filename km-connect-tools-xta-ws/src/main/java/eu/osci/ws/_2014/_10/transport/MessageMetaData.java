
package eu.osci.ws._2014._10.transport;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}DeliveryAttributes"/>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}Originators"/>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}Destinations"/>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}MsgIdentification"/>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}Qualifier"/>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}MessageProperties" minOccurs="0"/>
 *         &lt;element name="MsgSize" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="TestMsg" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "deliveryAttributes",
    "originators",
    "destinations",
    "msgIdentification",
    "qualifier",
    "messageProperties",
    "msgSize"
})
@XmlRootElement(name = "MessageMetaData")
public class MessageMetaData {

    @XmlElement(name = "DeliveryAttributes", required = true)
    protected DeliveryAttributesType deliveryAttributes;
    @XmlElement(name = "Originators", required = true)
    protected OriginatorsType originators;
    @XmlElement(name = "Destinations", required = true)
    protected DestinationsType destinations;
    @XmlElement(name = "MsgIdentification", required = true)
    protected MsgIdentificationType msgIdentification;
    @XmlElement(name = "Qualifier", required = true)
    protected QualifierType qualifier;
    @XmlElement(name = "MessageProperties")
    protected MessageProperties messageProperties;
    @XmlElement(name = "MsgSize")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger msgSize;
    @XmlAttribute(name = "TestMsg")
    protected Boolean testMsg;

    /**
     * Ruft den Wert der deliveryAttributes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryAttributesType }
     *     
     */
    public DeliveryAttributesType getDeliveryAttributes() {
        return deliveryAttributes;
    }

    /**
     * Legt den Wert der deliveryAttributes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryAttributesType }
     *     
     */
    public void setDeliveryAttributes(DeliveryAttributesType value) {
        this.deliveryAttributes = value;
    }

    /**
     * Ruft den Wert der originators-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OriginatorsType }
     *     
     */
    public OriginatorsType getOriginators() {
        return originators;
    }

    /**
     * Legt den Wert der originators-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginatorsType }
     *     
     */
    public void setOriginators(OriginatorsType value) {
        this.originators = value;
    }

    /**
     * Ruft den Wert der destinations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DestinationsType }
     *     
     */
    public DestinationsType getDestinations() {
        return destinations;
    }

    /**
     * Legt den Wert der destinations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DestinationsType }
     *     
     */
    public void setDestinations(DestinationsType value) {
        this.destinations = value;
    }

    /**
     * Ruft den Wert der msgIdentification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MsgIdentificationType }
     *     
     */
    public MsgIdentificationType getMsgIdentification() {
        return msgIdentification;
    }

    /**
     * Legt den Wert der msgIdentification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MsgIdentificationType }
     *     
     */
    public void setMsgIdentification(MsgIdentificationType value) {
        this.msgIdentification = value;
    }

    /**
     * Ruft den Wert der qualifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QualifierType }
     *     
     */
    public QualifierType getQualifier() {
        return qualifier;
    }

    /**
     * Legt den Wert der qualifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QualifierType }
     *     
     */
    public void setQualifier(QualifierType value) {
        this.qualifier = value;
    }

    /**
     * Ruft den Wert der messageProperties-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MessageProperties }
     *     
     */
    public MessageProperties getMessageProperties() {
        return messageProperties;
    }

    /**
     * Legt den Wert der messageProperties-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageProperties }
     *     
     */
    public void setMessageProperties(MessageProperties value) {
        this.messageProperties = value;
    }

    /**
     * Ruft den Wert der msgSize-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMsgSize() {
        return msgSize;
    }

    /**
     * Legt den Wert der msgSize-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMsgSize(BigInteger value) {
        this.msgSize = value;
    }

    /**
     * Ruft den Wert der testMsg-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isTestMsg() {
        if (testMsg == null) {
            return false;
        } else {
            return testMsg;
        }
    }

    /**
     * Legt den Wert der testMsg-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTestMsg(Boolean value) {
        this.testMsg = value;
    }

}
