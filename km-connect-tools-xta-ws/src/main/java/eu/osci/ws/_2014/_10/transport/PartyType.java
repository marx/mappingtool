
package eu.osci.ws._2014._10.transport;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Logical identifier and optional security tokens of that entity (binary, may carry SAML, too) 
 * 
 * <p>Java-Klasse f�r PartyType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PartyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Identifier" type="{http://www.osci.eu/ws/2014/10/transport}PartyIdentifierType"/>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}SecurityToken" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyType", propOrder = {
    "identifier",
    "securityToken"
})
public class PartyType {

    @XmlElement(name = "Identifier", required = true)
    protected PartyIdentifierType identifier;
    @XmlElement(name = "SecurityToken")
    protected List<SecurityToken> securityToken;

    /**
     * Ruft den Wert der identifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PartyIdentifierType }
     *     
     */
    public PartyIdentifierType getIdentifier() {
        return identifier;
    }

    /**
     * Legt den Wert der identifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyIdentifierType }
     *     
     */
    public void setIdentifier(PartyIdentifierType value) {
        this.identifier = value;
    }

    /**
     * Gets the value of the securityToken property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the securityToken property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSecurityToken().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SecurityToken }
     * 
     * 
     */
    public List<SecurityToken> getSecurityToken() {
        if (securityToken == null) {
            securityToken = new ArrayList<SecurityToken>();
        }
        return this.securityToken;
    }

}
