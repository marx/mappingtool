
package eu.osci.ws._2014._10.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Process ID message is realated to
 * 
 * <p>Java-Klasse f�r ProcessIdentifierType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProcessIdentifierType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.osci.eu/ws/2014/10/transport>NonEmptyStringType">
 *       &lt;attribute name="ProccesName" type="{http://www.osci.eu/ws/2014/10/transport}NonEmptyStringType" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcessIdentifierType", propOrder = {
    "value"
})
public class ProcessIdentifierType {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "ProccesName")
    protected String proccesName;

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Ruft den Wert der proccesName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProccesName() {
        return proccesName;
    }

    /**
     * Legt den Wert der proccesName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProccesName(String value) {
        this.proccesName = value;
    }

}
