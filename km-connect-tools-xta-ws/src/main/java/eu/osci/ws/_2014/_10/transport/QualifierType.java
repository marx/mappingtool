
package eu.osci.ws._2014._10.transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java-Klasse f�r QualifierType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="QualifierType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Subject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *         &lt;element name="BusinessScenario">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="Defined" type="{http://www.osci.eu/ws/2014/10/transport}KeyCodeType"/>
 *                   &lt;element name="Undefined" type="{http://www.w3.org/2001/XMLSchema}normalizedString"/>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="MessageType">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.osci.eu/ws/2014/10/transport}KeyCodeType">
 *                 &lt;attribute name="payloadSchema" use="required" type="{http://www.osci.eu/ws/2014/10/transport}NonEmptyURIType" />
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QualifierType", propOrder = {
    "subject",
    "service",
    "businessScenario",
    "messageType"
})
public class QualifierType {

    @XmlElement(name = "Subject")
    protected String subject;
    @XmlElement(name = "Service", required = true)
    @XmlSchemaType(name = "anyURI")
    protected String service;
    @XmlElement(name = "BusinessScenario", required = true)
    protected QualifierType.BusinessScenario businessScenario;
    @XmlElement(name = "MessageType", required = true)
    protected QualifierType.MessageType messageType;

    /**
     * Ruft den Wert der subject-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Legt den Wert der subject-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubject(String value) {
        this.subject = value;
    }

    /**
     * Ruft den Wert der service-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getService() {
        return service;
    }

    /**
     * Legt den Wert der service-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setService(String value) {
        this.service = value;
    }

    /**
     * Ruft den Wert der businessScenario-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QualifierType.BusinessScenario }
     *     
     */
    public QualifierType.BusinessScenario getBusinessScenario() {
        return businessScenario;
    }

    /**
     * Legt den Wert der businessScenario-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QualifierType.BusinessScenario }
     *     
     */
    public void setBusinessScenario(QualifierType.BusinessScenario value) {
        this.businessScenario = value;
    }

    /**
     * Ruft den Wert der messageType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QualifierType.MessageType }
     *     
     */
    public QualifierType.MessageType getMessageType() {
        return messageType;
    }

    /**
     * Legt den Wert der messageType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QualifierType.MessageType }
     *     
     */
    public void setMessageType(QualifierType.MessageType value) {
        this.messageType = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="Defined" type="{http://www.osci.eu/ws/2014/10/transport}KeyCodeType"/>
     *         &lt;element name="Undefined" type="{http://www.w3.org/2001/XMLSchema}normalizedString"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "defined",
        "undefined"
    })
    public static class BusinessScenario {

        @XmlElement(name = "Defined")
        protected KeyCodeType defined;
        @XmlElement(name = "Undefined")
        @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
        @XmlSchemaType(name = "normalizedString")
        protected String undefined;

        /**
         * Ruft den Wert der defined-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link KeyCodeType }
         *     
         */
        public KeyCodeType getDefined() {
            return defined;
        }

        /**
         * Legt den Wert der defined-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link KeyCodeType }
         *     
         */
        public void setDefined(KeyCodeType value) {
            this.defined = value;
        }

        /**
         * Ruft den Wert der undefined-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUndefined() {
            return undefined;
        }

        /**
         * Legt den Wert der undefined-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUndefined(String value) {
            this.undefined = value;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.osci.eu/ws/2014/10/transport}KeyCodeType">
     *       &lt;attribute name="payloadSchema" use="required" type="{http://www.osci.eu/ws/2014/10/transport}NonEmptyURIType" />
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class MessageType
        extends KeyCodeType
    {

        @XmlAttribute(name = "payloadSchema", required = true)
        protected String payloadSchema;

        /**
         * Ruft den Wert der payloadSchema-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPayloadSchema() {
            return payloadSchema;
        }

        /**
         * Legt den Wert der payloadSchema-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPayloadSchema(String value) {
            this.payloadSchema = value;
        }

    }

}
