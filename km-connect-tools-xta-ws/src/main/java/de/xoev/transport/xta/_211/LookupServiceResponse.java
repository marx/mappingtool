
package de.xoev.transport.xta._211;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LookupServiceResultList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LookupServiceResult" type="{http://xoev.de/transport/xta/211}LookupServiceResultType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lookupServiceResultList"
})
@XmlRootElement(name = "LookupServiceResponse")
public class LookupServiceResponse {

    @XmlElement(name = "LookupServiceResultList", required = true)
    protected LookupServiceResponse.LookupServiceResultList lookupServiceResultList;

    /**
     * Ruft den Wert der lookupServiceResultList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LookupServiceResponse.LookupServiceResultList }
     *     
     */
    public LookupServiceResponse.LookupServiceResultList getLookupServiceResultList() {
        return lookupServiceResultList;
    }

    /**
     * Legt den Wert der lookupServiceResultList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LookupServiceResponse.LookupServiceResultList }
     *     
     */
    public void setLookupServiceResultList(LookupServiceResponse.LookupServiceResultList value) {
        this.lookupServiceResultList = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LookupServiceResult" type="{http://xoev.de/transport/xta/211}LookupServiceResultType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "lookupServiceResult"
    })
    public static class LookupServiceResultList {

        @XmlElement(name = "LookupServiceResult", required = true, nillable = true)
        protected List<LookupServiceResultType> lookupServiceResult;

        /**
         * Gets the value of the lookupServiceResult property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the lookupServiceResult property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLookupServiceResult().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LookupServiceResultType }
         * 
         * 
         */
        public List<LookupServiceResultType> getLookupServiceResult() {
            if (lookupServiceResult == null) {
                lookupServiceResult = new ArrayList<LookupServiceResultType>();
            }
            return this.lookupServiceResult;
        }

    }

}
