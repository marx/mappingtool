
package de.xoev.transport.xta._211;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import eu.osci.ws._2014._10.transport.PartyType;


/**
 * Dies ist die Struktur einer Service-Anfrage: Sie enth�lt die Daten �ber den Diensteanbieter (Leser) und den Dienst des Lesers, den der Autor in Anspruch nehmen will. Diese Anfrage dient dazu, zu ermitteln, ob der Dienst von diesem Anbieter angeboten wird und �ber welche technischen Parameter er angesprochen werden kann.
 * 
 * <p>Java-Klasse f�r LookupServiceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LookupServiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}Reader"/>
 *         &lt;element name="ServiceType" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LookupServiceType", propOrder = {
    "reader",
    "serviceType"
})
@XmlSeeAlso({
    LookupServiceResultType.class
})
public class LookupServiceType {

    @XmlElement(name = "Reader", namespace = "http://www.osci.eu/ws/2014/10/transport", required = true)
    protected PartyType reader;
    @XmlElement(name = "ServiceType", required = true)
    @XmlSchemaType(name = "anyURI")
    protected String serviceType;

    /**
     * Dies ist die fachliche Identifizierung des Lesers. Der Wert entspricht z.B. dem DVDV-Beh�rdenschl�ssel.
     * 
     * @return
     *     possible object is
     *     {@link PartyType }
     *     
     */
    public PartyType getReader() {
        return reader;
    }

    /**
     * Legt den Wert der reader-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyType }
     *     
     */
    public void setReader(PartyType value) {
        this.reader = value;
    }

    /**
     * Ruft den Wert der serviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * Legt den Wert der serviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceType(String value) {
        this.serviceType = value;
    }

}
