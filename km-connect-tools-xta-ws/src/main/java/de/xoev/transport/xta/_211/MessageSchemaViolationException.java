
package de.xoev.transport.xta._211;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "MessageSchemaViolationException", targetNamespace = "http://xoev.de/transport/xta/211")
public class MessageSchemaViolationException
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private MessageSchemaViolationExceptionType faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public MessageSchemaViolationException(String message, MessageSchemaViolationExceptionType faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public MessageSchemaViolationException(String message, MessageSchemaViolationExceptionType faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: de.xoev.transport.xta._211.MessageSchemaViolationExceptionType
     */
    public MessageSchemaViolationExceptionType getFaultInfo() {
        return faultInfo;
    }

}
