
package de.xoev.transport.xta._211;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dieser Typ gestattet das Ablegen weiterer Pr�fberichte, welche das XTA-Protokoll (TransportReport) erg�nzen sollen.
 * 
 * <p>Java-Klasse f�r AdditionalReportListType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AdditionalReportListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Report" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Key" type="{http://xoev.de/transport/xta/211}Code.ReportType"/>
 *                   &lt;element name="Data" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalReportListType", propOrder = {
    "report"
})
public class AdditionalReportListType {

    @XmlElement(name = "Report", required = true)
    protected List<AdditionalReportListType.Report> report;

    /**
     * Gets the value of the report property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the report property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReport().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalReportListType.Report }
     * 
     * 
     */
    public List<AdditionalReportListType.Report> getReport() {
        if (report == null) {
            report = new ArrayList<AdditionalReportListType.Report>();
        }
        return this.report;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Key" type="{http://xoev.de/transport/xta/211}Code.ReportType"/>
     *         &lt;element name="Data" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "key",
        "data"
    })
    public static class Report {

        @XmlElement(name = "Key", required = true)
        protected CodeReportType key;
        @XmlElement(name = "Data", required = true)
        protected byte[] data;

        /**
         * Ruft den Wert der key-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link CodeReportType }
         *     
         */
        public CodeReportType getKey() {
            return key;
        }

        /**
         * Legt den Wert der key-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeReportType }
         *     
         */
        public void setKey(CodeReportType value) {
            this.key = value;
        }

        /**
         * Ruft den Wert der data-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getData() {
            return data;
        }

        /**
         * Legt den Wert der data-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setData(byte[] value) {
            this.data = value;
        }

    }

}
