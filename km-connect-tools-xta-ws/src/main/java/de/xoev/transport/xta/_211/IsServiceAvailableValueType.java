
package de.xoev.transport.xta._211;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Das Feld enth�lt die ben�tigten Attribute zum Ergebnis der Dienstanfrage: ob der Dienst angeboten wird oder nicht, oder ob diese Information generell nicht bekannt ist.
 * 
 * <p>Java-Klasse f�r IsServiceAvailableValueType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="IsServiceAvailableValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="ServiceIsAvailable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ServiceIsAvailableUnknown" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IsServiceAvailableValueType", propOrder = {
    "serviceIsAvailable",
    "serviceIsAvailableUnknown"
})
public class IsServiceAvailableValueType {

    @XmlElement(name = "ServiceIsAvailable")
    protected Boolean serviceIsAvailable;
    @XmlElement(name = "ServiceIsAvailableUnknown")
    protected Boolean serviceIsAvailableUnknown;

    /**
     * Ruft den Wert der serviceIsAvailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isServiceIsAvailable() {
        return serviceIsAvailable;
    }

    /**
     * Legt den Wert der serviceIsAvailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServiceIsAvailable(Boolean value) {
        this.serviceIsAvailable = value;
    }

    /**
     * Ruft den Wert der serviceIsAvailableUnknown-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isServiceIsAvailableUnknown() {
        return serviceIsAvailableUnknown;
    }

    /**
     * Legt den Wert der serviceIsAvailableUnknown-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServiceIsAvailableUnknown(Boolean value) {
        this.serviceIsAvailableUnknown = value;
    }

}
