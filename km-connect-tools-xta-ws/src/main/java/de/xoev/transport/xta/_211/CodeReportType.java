
package de.xoev.transport.xta._211;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import de.xoev.schemata.basisdatentypen._1_1.Code;


/**
 * Dieser Typ gestattet die Kennzeichnung der Art eines zus�tzlichen Reports. Es wird eine zu w�hlende Codeliste eingebunden, die m�gliche Arten von Reports nennt (spezielles Format, innerhalb oder au�erhalb von XTA definiert), die in das XTA-Protokoll (TransportReport) eingef�gt werden k�nnen.
 * Die KoSIT gibt f�r den Standard XTA eine Codeliste heraus, welche Eintr�ge f�r einschl�gige Arten von Reports auflistet. Diese Codeliste kann auf Antrag erweitert bzw. ge�ndert werden. Sie ist durch XTA-konforme Systeme f�r �bergreifende Prozesse zu verwenden.
 * Diese Codeliste ist im XRepository (www.xrepository.de) unter Nennung ihrer Codelisten-URI urn:de:xta:codeliste:report.type auffindbar und kann dort im XML-Format OASIS Genericode in der aktuellen Version abgerufen werden (ggf. sind auch fr�here Versionen verf�gbar). In die Attribute des vorliegenden Typs sind entsprechend ihre Codelisten-URI und die Nummer der ausgew�hlten Version einzutragen.
 * F�r lokale Zwecke k�nnen XTA-Kommunikationspartner auch eigene Codelisten definieren (welche bilateral abgestimmte Reportformate benennen) und an dieser Stelle einbinden. In die Attribute des vorliegenden Typs werden dann Codelisten-URI und Versionsnummer der selbstdefinierten Codeliste  eingetragen.
 * 
 * <p>Java-Klasse f�r Code.ReportType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Code.ReportType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://xoev.de/schemata/basisdatentypen/1_1}Code">
 *       &lt;sequence>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}token" form="unqualified"/>
 *       &lt;/sequence>
 *       &lt;attribute name="listURI" use="required" type="{http://www.w3.org/2001/XMLSchema}anyURI" />
 *       &lt;attribute name="listVersionID" use="required" type="{http://www.w3.org/2001/XMLSchema}normalizedString" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Code.ReportType")
public class CodeReportType
    extends Code
{


}
