
package de.xoev.transport.xta._211;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Dieser Datentyp legt die grundlegende Struktur einer Exception im Rahmen des XTA Webservice fest. Sie kapselt Information zu Identit�t und Bedeutung eines aufgetretenen Fehlers.
 * 
 * <p>Java-Klasse f�r ExceptionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ExceptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="errorCode" type="{http://xoev.de/transport/xta/211}Code.Fehlernummer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExceptionType", propOrder = {
    "errorCode"
})
@XmlSeeAlso({
    SyncAsyncExceptionType.class,
    CancelDeniedExceptionType.class,
    XTAWSTechnicalProblemExceptionType.class,
    MessageVirusDetectionExceptionType.class,
    MessageSchemaViolationExceptionType.class,
    PermissionDeniedExceptionType.class,
    InvalidMessageIDExceptionType.class,
    ParameterIsNotValidExceptionType.class
})
public class ExceptionType {

    @XmlElement(required = true)
    protected CodeFehlernummer errorCode;

    /**
     * Ruft den Wert der errorCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeFehlernummer }
     *     
     */
    public CodeFehlernummer getErrorCode() {
        return errorCode;
    }

    /**
     * Legt den Wert der errorCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeFehlernummer }
     *     
     */
    public void setErrorCode(CodeFehlernummer value) {
        this.errorCode = value;
    }

}
