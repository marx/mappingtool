
package de.xoev.transport.xta._211;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.xoev.transport.xta._211 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ParameterIsNotValidException_QNAME = new QName("http://xoev.de/transport/xta/211", "ParameterIsNotValidException");
    private final static QName _MessageSchemaViolationException_QNAME = new QName("http://xoev.de/transport/xta/211", "MessageSchemaViolationException");
    private final static QName _MessageVirusDetectionException_QNAME = new QName("http://xoev.de/transport/xta/211", "MessageVirusDetectionException");
    private final static QName _PermissionDeniedException_QNAME = new QName("http://xoev.de/transport/xta/211", "PermissionDeniedException");
    private final static QName _SyncAsyncException_QNAME = new QName("http://xoev.de/transport/xta/211", "SyncAsyncException");
    private final static QName _InvalidMessageIDException_QNAME = new QName("http://xoev.de/transport/xta/211", "InvalidMessageIDException");
    private final static QName _XTAWSTechnicalProblemException_QNAME = new QName("http://xoev.de/transport/xta/211", "XTAWSTechnicalProblemException");
    private final static QName _CancelDeniedException_QNAME = new QName("http://xoev.de/transport/xta/211", "CancelDeniedException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.xoev.transport.xta._211
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GenericContentContainer }
     * 
     */
    public GenericContentContainer createGenericContentContainer() {
        return new GenericContentContainer();
    }

    /**
     * Create an instance of {@link LookupServiceResponse }
     * 
     */
    public LookupServiceResponse createLookupServiceResponse() {
        return new LookupServiceResponse();
    }

    /**
     * Create an instance of {@link LookupServiceRequest }
     * 
     */
    public LookupServiceRequest createLookupServiceRequest() {
        return new LookupServiceRequest();
    }

    /**
     * Create an instance of {@link LookupServiceResultType }
     * 
     */
    public LookupServiceResultType createLookupServiceResultType() {
        return new LookupServiceResultType();
    }

    /**
     * Create an instance of {@link AdditionalReportListType }
     * 
     */
    public AdditionalReportListType createAdditionalReportListType() {
        return new AdditionalReportListType();
    }

    /**
     * Create an instance of {@link MessageStatusType }
     * 
     */
    public MessageStatusType createMessageStatusType() {
        return new MessageStatusType();
    }

    /**
     * Create an instance of {@link SyncAsyncExceptionType }
     * 
     */
    public SyncAsyncExceptionType createSyncAsyncExceptionType() {
        return new SyncAsyncExceptionType();
    }

    /**
     * Create an instance of {@link GenericContentContainer.ContentContainer }
     * 
     */
    public GenericContentContainer.ContentContainer createGenericContentContainerContentContainer() {
        return new GenericContentContainer.ContentContainer();
    }

    /**
     * Create an instance of {@link CancelDeniedExceptionType }
     * 
     */
    public CancelDeniedExceptionType createCancelDeniedExceptionType() {
        return new CancelDeniedExceptionType();
    }

    /**
     * Create an instance of {@link XTAWSTechnicalProblemExceptionType }
     * 
     */
    public XTAWSTechnicalProblemExceptionType createXTAWSTechnicalProblemExceptionType() {
        return new XTAWSTechnicalProblemExceptionType();
    }

    /**
     * Create an instance of {@link MessageVirusDetectionExceptionType }
     * 
     */
    public MessageVirusDetectionExceptionType createMessageVirusDetectionExceptionType() {
        return new MessageVirusDetectionExceptionType();
    }

    /**
     * Create an instance of {@link MessageSchemaViolationExceptionType }
     * 
     */
    public MessageSchemaViolationExceptionType createMessageSchemaViolationExceptionType() {
        return new MessageSchemaViolationExceptionType();
    }

    /**
     * Create an instance of {@link LookupServiceResponse.LookupServiceResultList }
     * 
     */
    public LookupServiceResponse.LookupServiceResultList createLookupServiceResponseLookupServiceResultList() {
        return new LookupServiceResponse.LookupServiceResultList();
    }

    /**
     * Create an instance of {@link PermissionDeniedExceptionType }
     * 
     */
    public PermissionDeniedExceptionType createPermissionDeniedExceptionType() {
        return new PermissionDeniedExceptionType();
    }

    /**
     * Create an instance of {@link InvalidMessageIDExceptionType }
     * 
     */
    public InvalidMessageIDExceptionType createInvalidMessageIDExceptionType() {
        return new InvalidMessageIDExceptionType();
    }

    /**
     * Create an instance of {@link TransportReport }
     * 
     */
    public TransportReport createTransportReport() {
        return new TransportReport();
    }

    /**
     * Create an instance of {@link ParameterIsNotValidExceptionType }
     * 
     */
    public ParameterIsNotValidExceptionType createParameterIsNotValidExceptionType() {
        return new ParameterIsNotValidExceptionType();
    }

    /**
     * Create an instance of {@link LookupServiceRequest.LookupServiceRequestList }
     * 
     */
    public LookupServiceRequest.LookupServiceRequestList createLookupServiceRequestLookupServiceRequestList() {
        return new LookupServiceRequest.LookupServiceRequestList();
    }

    /**
     * Create an instance of {@link ExceptionType }
     * 
     */
    public ExceptionType createExceptionType() {
        return new ExceptionType();
    }

    /**
     * Create an instance of {@link LookupServiceType }
     * 
     */
    public LookupServiceType createLookupServiceType() {
        return new LookupServiceType();
    }

    /**
     * Create an instance of {@link ContentType }
     * 
     */
    public ContentType createContentType() {
        return new ContentType();
    }

    /**
     * Create an instance of {@link CodeFehlernummer }
     * 
     */
    public CodeFehlernummer createCodeFehlernummer() {
        return new CodeFehlernummer();
    }

    /**
     * Create an instance of {@link CodeReportType }
     * 
     */
    public CodeReportType createCodeReportType() {
        return new CodeReportType();
    }

    /**
     * Create an instance of {@link IsServiceAvailableValueType }
     * 
     */
    public IsServiceAvailableValueType createIsServiceAvailableValueType() {
        return new IsServiceAvailableValueType();
    }

    /**
     * Create an instance of {@link RecordType }
     * 
     */
    public RecordType createRecordType() {
        return new RecordType();
    }

    /**
     * Create an instance of {@link CodeServiceParameterType }
     * 
     */
    public CodeServiceParameterType createCodeServiceParameterType() {
        return new CodeServiceParameterType();
    }

    /**
     * Create an instance of {@link CodeRecordType }
     * 
     */
    public CodeRecordType createCodeRecordType() {
        return new CodeRecordType();
    }

    /**
     * Create an instance of {@link LookupServiceResultType.ServiceParameter }
     * 
     */
    public LookupServiceResultType.ServiceParameter createLookupServiceResultTypeServiceParameter() {
        return new LookupServiceResultType.ServiceParameter();
    }

    /**
     * Create an instance of {@link AdditionalReportListType.Report }
     * 
     */
    public AdditionalReportListType.Report createAdditionalReportListTypeReport() {
        return new AdditionalReportListType.Report();
    }

    /**
     * Create an instance of {@link MessageStatusType.ErrorList }
     * 
     */
    public MessageStatusType.ErrorList createMessageStatusTypeErrorList() {
        return new MessageStatusType.ErrorList();
    }

    /**
     * Create an instance of {@link MessageStatusType.WarnList }
     * 
     */
    public MessageStatusType.WarnList createMessageStatusTypeWarnList() {
        return new MessageStatusType.WarnList();
    }

    /**
     * Create an instance of {@link MessageStatusType.InfoList }
     * 
     */
    public MessageStatusType.InfoList createMessageStatusTypeInfoList() {
        return new MessageStatusType.InfoList();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParameterIsNotValidExceptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xoev.de/transport/xta/211", name = "ParameterIsNotValidException")
    public JAXBElement<ParameterIsNotValidExceptionType> createParameterIsNotValidException(ParameterIsNotValidExceptionType value) {
        return new JAXBElement<ParameterIsNotValidExceptionType>(_ParameterIsNotValidException_QNAME, ParameterIsNotValidExceptionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageSchemaViolationExceptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xoev.de/transport/xta/211", name = "MessageSchemaViolationException")
    public JAXBElement<MessageSchemaViolationExceptionType> createMessageSchemaViolationException(MessageSchemaViolationExceptionType value) {
        return new JAXBElement<MessageSchemaViolationExceptionType>(_MessageSchemaViolationException_QNAME, MessageSchemaViolationExceptionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageVirusDetectionExceptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xoev.de/transport/xta/211", name = "MessageVirusDetectionException")
    public JAXBElement<MessageVirusDetectionExceptionType> createMessageVirusDetectionException(MessageVirusDetectionExceptionType value) {
        return new JAXBElement<MessageVirusDetectionExceptionType>(_MessageVirusDetectionException_QNAME, MessageVirusDetectionExceptionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PermissionDeniedExceptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xoev.de/transport/xta/211", name = "PermissionDeniedException")
    public JAXBElement<PermissionDeniedExceptionType> createPermissionDeniedException(PermissionDeniedExceptionType value) {
        return new JAXBElement<PermissionDeniedExceptionType>(_PermissionDeniedException_QNAME, PermissionDeniedExceptionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SyncAsyncExceptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xoev.de/transport/xta/211", name = "SyncAsyncException")
    public JAXBElement<SyncAsyncExceptionType> createSyncAsyncException(SyncAsyncExceptionType value) {
        return new JAXBElement<SyncAsyncExceptionType>(_SyncAsyncException_QNAME, SyncAsyncExceptionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidMessageIDExceptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xoev.de/transport/xta/211", name = "InvalidMessageIDException")
    public JAXBElement<InvalidMessageIDExceptionType> createInvalidMessageIDException(InvalidMessageIDExceptionType value) {
        return new JAXBElement<InvalidMessageIDExceptionType>(_InvalidMessageIDException_QNAME, InvalidMessageIDExceptionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XTAWSTechnicalProblemExceptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xoev.de/transport/xta/211", name = "XTAWSTechnicalProblemException")
    public JAXBElement<XTAWSTechnicalProblemExceptionType> createXTAWSTechnicalProblemException(XTAWSTechnicalProblemExceptionType value) {
        return new JAXBElement<XTAWSTechnicalProblemExceptionType>(_XTAWSTechnicalProblemException_QNAME, XTAWSTechnicalProblemExceptionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelDeniedExceptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xoev.de/transport/xta/211", name = "CancelDeniedException")
    public JAXBElement<CancelDeniedExceptionType> createCancelDeniedException(CancelDeniedExceptionType value) {
        return new JAXBElement<CancelDeniedExceptionType>(_CancelDeniedException_QNAME, CancelDeniedExceptionType.class, null, value);
    }

}
