
package de.xoev.transport.xta._211;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Gibt die Struktur f�r die Meldungen (Logging-Informationen) �ber den Transportverlauf vor. Er sieht Meldungszeilen f�r Infos, Warnungen und Fehler vor.
 * 
 * <p>Java-Klasse f�r MessageStatusType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MessageStatusType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="ErrorList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Error" type="{http://xoev.de/transport/xta/211}RecordType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="WarnList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Warning" type="{http://xoev.de/transport/xta/211}RecordType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="InfoList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Info" type="{http://xoev.de/transport/xta/211}RecordType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageStatusType", propOrder = {
    "status",
    "errorList",
    "warnList",
    "infoList"
})
public class MessageStatusType {

    @XmlElement(name = "Status", required = true)
    protected BigInteger status;
    @XmlElement(name = "ErrorList")
    protected MessageStatusType.ErrorList errorList;
    @XmlElement(name = "WarnList")
    protected MessageStatusType.WarnList warnList;
    @XmlElement(name = "InfoList")
    protected MessageStatusType.InfoList infoList;

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStatus(BigInteger value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der errorList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MessageStatusType.ErrorList }
     *     
     */
    public MessageStatusType.ErrorList getErrorList() {
        return errorList;
    }

    /**
     * Legt den Wert der errorList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageStatusType.ErrorList }
     *     
     */
    public void setErrorList(MessageStatusType.ErrorList value) {
        this.errorList = value;
    }

    /**
     * Ruft den Wert der warnList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MessageStatusType.WarnList }
     *     
     */
    public MessageStatusType.WarnList getWarnList() {
        return warnList;
    }

    /**
     * Legt den Wert der warnList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageStatusType.WarnList }
     *     
     */
    public void setWarnList(MessageStatusType.WarnList value) {
        this.warnList = value;
    }

    /**
     * Ruft den Wert der infoList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MessageStatusType.InfoList }
     *     
     */
    public MessageStatusType.InfoList getInfoList() {
        return infoList;
    }

    /**
     * Legt den Wert der infoList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageStatusType.InfoList }
     *     
     */
    public void setInfoList(MessageStatusType.InfoList value) {
        this.infoList = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Error" type="{http://xoev.de/transport/xta/211}RecordType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "error"
    })
    public static class ErrorList {

        @XmlElement(name = "Error")
        protected List<RecordType> error;

        /**
         * Gets the value of the error property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the error property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getError().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RecordType }
         * 
         * 
         */
        public List<RecordType> getError() {
            if (error == null) {
                error = new ArrayList<RecordType>();
            }
            return this.error;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Info" type="{http://xoev.de/transport/xta/211}RecordType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "info"
    })
    public static class InfoList {

        @XmlElement(name = "Info")
        protected List<RecordType> info;

        /**
         * Gets the value of the info property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the info property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RecordType }
         * 
         * 
         */
        public List<RecordType> getInfo() {
            if (info == null) {
                info = new ArrayList<RecordType>();
            }
            return this.info;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Warning" type="{http://xoev.de/transport/xta/211}RecordType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "warning"
    })
    public static class WarnList {

        @XmlElement(name = "Warning")
        protected List<RecordType> warning;

        /**
         * Gets the value of the warning property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the warning property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getWarning().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RecordType }
         * 
         * 
         */
        public List<RecordType> getWarning() {
            if (warning == null) {
                warning = new ArrayList<RecordType>();
            }
            return this.warning;
        }

    }

}
