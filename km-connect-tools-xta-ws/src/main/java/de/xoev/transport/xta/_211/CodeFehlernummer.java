
package de.xoev.transport.xta._211;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import de.xoev.schemata.basisdatentypen._1_1.Code;


/**
 * Diese Codeliste gibt eine �bersicht �ber die in XTA-WS zu verwendenden Fehlernummern (ErrorCodes) und ordnet sie den Exceptions zu, in deren Kontext sie auftreten k�nnen.
 * 
 * <p>Java-Klasse f�r Code.Fehlernummer complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Code.Fehlernummer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://xoev.de/schemata/basisdatentypen/1_1}Code">
 *       &lt;sequence>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}token" form="unqualified"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}normalizedString" form="unqualified"/>
 *       &lt;/sequence>
 *       &lt;attribute name="listURI" type="{http://www.w3.org/2001/XMLSchema}anyURI" fixed="urn:de:xta:webservice:codeliste:fehlernummer" />
 *       &lt;attribute name="listVersionID" type="{http://www.w3.org/2001/XMLSchema}normalizedString" fixed="1.0" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Code.Fehlernummer")
public class CodeFehlernummer
    extends Code
{


}
