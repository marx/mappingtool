
package de.xoev.transport.xta._211;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Dieser abgeleitete Typ wird vom zugeh�rigen Exception-Objekt verwendet.
 * 
 * <p>Java-Klasse f�r PermissionDeniedExceptionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PermissionDeniedExceptionType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xoev.de/transport/xta/211}ExceptionType">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PermissionDeniedExceptionType")
public class PermissionDeniedExceptionType
    extends ExceptionType
{


}
