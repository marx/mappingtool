
package de.xoev.transport.xta._211;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import de.xoev.schemata.basisdatentypen._1_1.Code;


/**
 * In diesen Typ  ist eine auszuw�hlende bzw. selbst zu definierende Codeliste einzubinden, die Arten von Meldungen benennt, welche in das Protokoll zur Abarbeitung eines Transportauftrags (TransportReport) eingetragen werden. Dort k�nnen die Meldungen als Fehler-, Warn- oder Informationseintr�ge eingeordnet sein.
 * In die Attribute des vorliegenden Typs sind die Codelisten-URI und die Nummer der Version der ausgew�hlten Codeliste einzutragen.
 * 
 * Die KoSIT hat die Absicht, f�r den Standard XTA eine passende Codeliste zu definieren und als einheitliches Angebot zur Einbindung f�r diesen Typ  bereitzustellen. Diese Codeliste ist, wenn die Bereitstellung erfolgt ist,  im XRepository (www.xrepository.de) unter der Codelisten-URI urn:de:xta:codeliste:record.type  auffindbar und kann von dort im XML-Format OASIS Genericode abgerufen werden.
 * 
 * <p>Java-Klasse f�r Code.RecordType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Code.RecordType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://xoev.de/schemata/basisdatentypen/1_1}Code">
 *       &lt;sequence>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}token" form="unqualified"/>
 *       &lt;/sequence>
 *       &lt;attribute name="listURI" use="required" type="{http://www.w3.org/2001/XMLSchema}anyURI" />
 *       &lt;attribute name="listVersionID" use="required" type="{http://www.w3.org/2001/XMLSchema}normalizedString" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Code.RecordType")
public class CodeRecordType
    extends Code
{


}
