
package de.xoev.transport.xta._211;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.w3._2001._04.xmlenc_.EncryptedDataType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{http://www.w3.org/2001/04/xmlenc#}EncryptedData"/>
 *         &lt;element name="ContentContainer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Message" type="{http://xoev.de/transport/xta/211}ContentType"/>
 *                   &lt;element name="Attachment" type="{http://xoev.de/transport/xta/211}ContentType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "encryptedData",
    "contentContainer"
})
@XmlRootElement(name = "GenericContentContainer")
public class GenericContentContainer {

    @XmlElement(name = "EncryptedData", namespace = "http://www.w3.org/2001/04/xmlenc#")
    protected EncryptedDataType encryptedData;
    @XmlElement(name = "ContentContainer")
    protected GenericContentContainer.ContentContainer contentContainer;

    /**
     * Dieses Objekt ist daf�r vorgesehen, den Container-Inhalt verschl�sselt zu hinterlegen. Im entschl�sselten Zustand m�ssen die Daten dem Schwester-Element ContentContainer entsprechen.
     * 
     * @return
     *     possible object is
     *     {@link EncryptedDataType }
     *     
     */
    public EncryptedDataType getEncryptedData() {
        return encryptedData;
    }

    /**
     * Legt den Wert der encryptedData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EncryptedDataType }
     *     
     */
    public void setEncryptedData(EncryptedDataType value) {
        this.encryptedData = value;
    }

    /**
     * Ruft den Wert der contentContainer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GenericContentContainer.ContentContainer }
     *     
     */
    public GenericContentContainer.ContentContainer getContentContainer() {
        return contentContainer;
    }

    /**
     * Legt den Wert der contentContainer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericContentContainer.ContentContainer }
     *     
     */
    public void setContentContainer(GenericContentContainer.ContentContainer value) {
        this.contentContainer = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Message" type="{http://xoev.de/transport/xta/211}ContentType"/>
     *         &lt;element name="Attachment" type="{http://xoev.de/transport/xta/211}ContentType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "message",
        "attachment"
    })
    public static class ContentContainer {

        @XmlElement(name = "Message", required = true)
        protected ContentType message;
        @XmlElement(name = "Attachment")
        protected List<ContentType> attachment;

        /**
         * Ruft den Wert der message-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ContentType }
         *     
         */
        public ContentType getMessage() {
            return message;
        }

        /**
         * Legt den Wert der message-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ContentType }
         *     
         */
        public void setMessage(ContentType value) {
            this.message = value;
        }

        /**
         * Gets the value of the attachment property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the attachment property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAttachment().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ContentType }
         * 
         * 
         */
        public List<ContentType> getAttachment() {
            if (attachment == null) {
                attachment = new ArrayList<ContentType>();
            }
            return this.attachment;
        }

    }

}
