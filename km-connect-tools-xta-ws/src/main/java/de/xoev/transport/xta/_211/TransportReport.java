
package de.xoev.transport.xta._211;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import eu.osci.ws._2014._10.transport.MessageMetaData;
import org.w3._2000._09.xmldsig_.SignatureType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.osci.eu/ws/2014/10/transport}MessageMetaData"/>
 *         &lt;element name="ReportTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="XTAServerIdentity" type="{http://www.w3.org/2001/XMLSchema}token"/>
 *         &lt;element name="MessageStatus" type="{http://xoev.de/transport/xta/211}MessageStatusType"/>
 *         &lt;element name="AdditionalReports" type="{http://xoev.de/transport/xta/211}AdditionalReportListType" minOccurs="0"/>
 *         &lt;element ref="{http://www.w3.org/2000/09/xmldsig#}Signature" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "messageMetaData",
    "reportTime",
    "xtaServerIdentity",
    "messageStatus",
    "additionalReports",
    "signature"
})
@XmlRootElement(name = "TransportReport")
public class TransportReport {

    @XmlElement(name = "MessageMetaData", namespace = "http://www.osci.eu/ws/2014/10/transport", required = true)
    protected MessageMetaData messageMetaData;
    @XmlElement(name = "ReportTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reportTime;
    @XmlElement(name = "XTAServerIdentity", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String xtaServerIdentity;
    @XmlElement(name = "MessageStatus", required = true)
    protected MessageStatusType messageStatus;
    @XmlElement(name = "AdditionalReports")
    protected AdditionalReportListType additionalReports;
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#")
    protected SignatureType signature;

    /**
     * Dieser Container umfasst alle Daten des Transportauftrags, auf dessen Ausf�hrung sich der TransportReport bezieht. Zu den Informationen geh�ren die Identifizierung von Absender und (einem oder mehreren) Empf�ngern, Metainformation zu Inhalt und Identit�t der zu transportierenden Fachnachricht (Payload) sowie weitere Attribute, die Auslieferung, Quittungen und Service Qualit�t betreffen.
     * Weitere Informationen zu diesem Objekt sind in  zu finden.
     * 
     * @return
     *     possible object is
     *     {@link MessageMetaData }
     *     
     */
    public MessageMetaData getMessageMetaData() {
        return messageMetaData;
    }

    /**
     * Legt den Wert der messageMetaData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageMetaData }
     *     
     */
    public void setMessageMetaData(MessageMetaData value) {
        this.messageMetaData = value;
    }

    /**
     * Ruft den Wert der reportTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReportTime() {
        return reportTime;
    }

    /**
     * Legt den Wert der reportTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReportTime(XMLGregorianCalendar value) {
        this.reportTime = value;
    }

    /**
     * Ruft den Wert der xtaServerIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXTAServerIdentity() {
        return xtaServerIdentity;
    }

    /**
     * Legt den Wert der xtaServerIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXTAServerIdentity(String value) {
        this.xtaServerIdentity = value;
    }

    /**
     * Ruft den Wert der messageStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MessageStatusType }
     *     
     */
    public MessageStatusType getMessageStatus() {
        return messageStatus;
    }

    /**
     * Legt den Wert der messageStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageStatusType }
     *     
     */
    public void setMessageStatus(MessageStatusType value) {
        this.messageStatus = value;
    }

    /**
     * Ruft den Wert der additionalReports-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalReportListType }
     *     
     */
    public AdditionalReportListType getAdditionalReports() {
        return additionalReports;
    }

    /**
     * Legt den Wert der additionalReports-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalReportListType }
     *     
     */
    public void setAdditionalReports(AdditionalReportListType value) {
        this.additionalReports = value;
    }

    /**
     * Falls der TransportReport signiert ist, findet sich hier die Signatur.
     * 
     * @return
     *     possible object is
     *     {@link SignatureType }
     *     
     */
    public SignatureType getSignature() {
        return signature;
    }

    /**
     * Legt den Wert der signature-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SignatureType }
     *     
     */
    public void setSignature(SignatureType value) {
        this.signature = value;
    }

}
