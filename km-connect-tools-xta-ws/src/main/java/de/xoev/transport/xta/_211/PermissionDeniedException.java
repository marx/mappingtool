
package de.xoev.transport.xta._211;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "PermissionDeniedException", targetNamespace = "http://xoev.de/transport/xta/211")
public class PermissionDeniedException
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private PermissionDeniedExceptionType faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public PermissionDeniedException(String message, PermissionDeniedExceptionType faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public PermissionDeniedException(String message, PermissionDeniedExceptionType faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: de.xoev.transport.xta._211.PermissionDeniedExceptionType
     */
    public PermissionDeniedExceptionType getFaultInfo() {
        return faultInfo;
    }

}
