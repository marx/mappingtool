
package de.xoev.transport.xta._211;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Das Ergebnis zu einer Dienstanfrage, das die Information enth�lt, ob der Dienst angeboten wird. Au�erdem sind die n�tigen technischen Paramter f�r die Erreichbarkeit vorhanden.
 * 
 * <p>Java-Klasse f�r LookupServiceResultType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LookupServiceResultType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xoev.de/transport/xta/211}LookupServiceType">
 *       &lt;sequence>
 *         &lt;element name="IsServiceAvailableValue" type="{http://xoev.de/transport/xta/211}IsServiceAvailableValueType"/>
 *         &lt;element name="ServiceParameter" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ParameterType" type="{http://xoev.de/transport/xta/211}Code.ServiceParameterType"/>
 *                   &lt;element name="Resource" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LookupServiceResultType", propOrder = {
    "isServiceAvailableValue",
    "serviceParameter"
})
public class LookupServiceResultType
    extends LookupServiceType
{

    @XmlElement(name = "IsServiceAvailableValue", required = true)
    protected IsServiceAvailableValueType isServiceAvailableValue;
    @XmlElement(name = "ServiceParameter")
    protected List<LookupServiceResultType.ServiceParameter> serviceParameter;

    /**
     * Ruft den Wert der isServiceAvailableValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IsServiceAvailableValueType }
     *     
     */
    public IsServiceAvailableValueType getIsServiceAvailableValue() {
        return isServiceAvailableValue;
    }

    /**
     * Legt den Wert der isServiceAvailableValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IsServiceAvailableValueType }
     *     
     */
    public void setIsServiceAvailableValue(IsServiceAvailableValueType value) {
        this.isServiceAvailableValue = value;
    }

    /**
     * Gets the value of the serviceParameter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceParameter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceParameter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LookupServiceResultType.ServiceParameter }
     * 
     * 
     */
    public List<LookupServiceResultType.ServiceParameter> getServiceParameter() {
        if (serviceParameter == null) {
            serviceParameter = new ArrayList<LookupServiceResultType.ServiceParameter>();
        }
        return this.serviceParameter;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ParameterType" type="{http://xoev.de/transport/xta/211}Code.ServiceParameterType"/>
     *         &lt;element name="Resource" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "parameterType",
        "resource"
    })
    public static class ServiceParameter {

        @XmlElement(name = "ParameterType", required = true)
        protected CodeServiceParameterType parameterType;
        @XmlElement(name = "Resource", required = true)
        protected byte[] resource;

        /**
         * Ruft den Wert der parameterType-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link CodeServiceParameterType }
         *     
         */
        public CodeServiceParameterType getParameterType() {
            return parameterType;
        }

        /**
         * Legt den Wert der parameterType-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeServiceParameterType }
         *     
         */
        public void setParameterType(CodeServiceParameterType value) {
            this.parameterType = value;
        }

        /**
         * Ruft den Wert der resource-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getResource() {
            return resource;
        }

        /**
         * Legt den Wert der resource-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setResource(byte[] value) {
            this.resource = value;
        }

    }

}
