package de.kommone.webservice;

import java.nio.charset.StandardCharsets;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.BindingType;
import javax.xml.ws.FaultAction;
import javax.xml.ws.Holder;

import de.kommone.webservice.utils.FileUtils;
import de.xoev.transport.xta._211.GenericContentContainer;
import de.xoev.transport.xta._211.MessageSchemaViolationException;
import de.xoev.transport.xta._211.MessageVirusDetectionException;
import de.xoev.transport.xta._211.ParameterIsNotValidException;
import de.xoev.transport.xta._211.PermissionDeniedException;
import de.xoev.transport.xta._211.SendSynchronPortType;
import de.xoev.transport.xta._211.SyncAsyncException;
import de.xoev.transport.xta._211.XTAWSTechnicalProblemException;

@WebService(name = "sendSynchronPortType", targetNamespace = "http://xoev.de/transport/xta/211")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@BindingType(value = javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@XmlSeeAlso({ de.xoev.transport.xta._211.ObjectFactory.class, eu.osci.ws._2008._05.transport.ObjectFactory.class,
        eu.osci.ws._2014._10.transport.ObjectFactory.class, de.xoev.schemata.basisdatentypen._1_1.ObjectFactory.class,
        org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0.ObjectFactory.class,
        org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_utility_1_0.ObjectFactory.class,
        org.w3._2000._09.xmldsig_.ObjectFactory.class, org.w3._2001._04.xmlenc_.ObjectFactory.class,
        org.w3._2003._05.soap_envelope.ObjectFactory.class, org.w3._2005._08.addressing.ObjectFactory.class,
        org.w3.ns.ws_policy.ObjectFactory.class })
public class XTASendSyncService implements SendSynchronPortType {

    @WebMethod(action = "http://www.xta.de/XTA/SendMessageSync")
    @Action(input = "http://www.xta.de/XTA/SendMessageSync", output = "http://www.xta.de/XTA/SendMessageSync", fault = {
            @FaultAction(className = PermissionDeniedException.class, value = "http://www.xta.de/XTA/SendMessageSync"),
            @FaultAction(className = ParameterIsNotValidException.class, value = "http://www.xta.de/XTA/SendMessageSync"),
            @FaultAction(className = XTAWSTechnicalProblemException.class, value = "http://www.xta.de/XTA/SendMessageSync"),
            @FaultAction(className = MessageSchemaViolationException.class, value = "http://www.xta.de/XTA/SendMessageSync"),
            @FaultAction(className = MessageVirusDetectionException.class, value = "http://www.xta.de/XTA/SendMessageSync"),
            @FaultAction(className = SyncAsyncException.class, value = "http://www.xta.de/XTA/SendMessageSync") })
    public void sendMessageSync(
            @WebParam(name = "GenericContentContainer", targetNamespace = "http://xoev.de/transport/xta/211", mode = WebParam.Mode.INOUT, partName = "GenericContainer") Holder<GenericContentContainer> genericContainer)
            throws MessageSchemaViolationException, MessageVirusDetectionException, ParameterIsNotValidException,
            PermissionDeniedException, SyncAsyncException, XTAWSTechnicalProblemException {

        GenericContentContainer container = genericContainer.value;
        String originalMessage = new String(container.getContentContainer().getMessage().getValue(), StandardCharsets.UTF_8);
        String returnedMessage = null;
        // Erika Maier
        if(originalMessage.contains("Erika")) {
        	returnedMessage = new FileUtils().readFileAsString("xmeld/1701_Erfolg.xml");
        } else {
        	returnedMessage = new FileUtils().readFileAsString("xmeld/1701_Miss_Erfolg.xml");
        }              
        container.getContentContainer().getMessage().setValue(returnedMessage.getBytes());

    }

}
