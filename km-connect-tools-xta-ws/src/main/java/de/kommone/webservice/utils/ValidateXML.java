package de.kommone.webservice.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.sun.org.apache.xerces.internal.dom.DOMInputImpl;

public class ValidateXML {

    private static final String PROXY_HOST = "squid.dzbw.de";
    private static final String PROXY_PORT = "3128";

    private static final String WASSER_ANTRAG_SCHEME = "xfall_daten_wee_erklaerung_nachricht.xsd";
    private static final String XFALL_RESOURCE_DIR = "xsd/xfalldaten/";

    public static void validateWasser(String xml, List<String> errors) {
        try {
            validateXML(xml, XFALL_RESOURCE_DIR + WASSER_ANTRAG_SCHEME, XFALL_RESOURCE_DIR, errors);
        } catch (Exception e) {
            errors.add(e.getMessage());
        }
    }

    public static void validateXML(String xml, String schemaName, String resourceDir, List<String> errors)
            throws Exception {

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);

        DocumentBuilder parser = builderFactory.newDocumentBuilder();

        StringBuilder xmlStringBuilder = new StringBuilder();
        xmlStringBuilder.append(xml);
        ByteArrayInputStream input = new ByteArrayInputStream(xmlStringBuilder.toString().getBytes("UTF-8"));

        Document document = parser.parse(input);

        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        ResourceResolver.resourceDir = resourceDir;
        factory.setResourceResolver(new ResourceResolver());

        Source schemaFile = new StreamSource(ValidateXML.class.getClassLoader().getResourceAsStream(schemaName));
        Schema schema = factory.newSchema(schemaFile);

        Validator validator = schema.newValidator();

        final List<SAXParseException> exceptions = new LinkedList<SAXParseException>();

        validator.setErrorHandler(new ErrorHandler() {
            @Override
            public void warning(SAXParseException exception) throws SAXException {
                exceptions.add(exception);
            }

            @Override
            public void fatalError(SAXParseException exception) throws SAXException {
                exceptions.add(exception);
            }

            @Override
            public void error(SAXParseException exception) throws SAXException {
                exceptions.add(exception);
            }
        });

        validator.validate(new DOMSource(document));

        errors.addAll(exceptions.stream().map(e -> e.getMessage()).collect(Collectors.toList()));
    }

    public static class ResourceResolver implements LSResourceResolver {
        
        static String resourceDir;

        @Override
        public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId,
                String baseURI) {

            InputStream resourceAsStream = this.getClass().getClassLoader()
                    .getResourceAsStream(resourceDir + systemId);

            System.setProperty("http.proxyHost", PROXY_HOST);
            System.setProperty("http.proxyPort", PROXY_PORT);

            if (resourceAsStream == null) {
                try {
                    URL url = new URL(systemId);
                    resourceAsStream = url.openStream();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Objects.requireNonNull(resourceAsStream,
                    String.format("Could not find the specified xsd file: %s", systemId));

            System.clearProperty("http.proxyHost");

            return new DOMInputImpl(publicId, systemId, baseURI, resourceAsStream, "UTF-8");
        }
    }
}
