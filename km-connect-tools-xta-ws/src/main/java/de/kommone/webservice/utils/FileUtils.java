package de.kommone.webservice.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import de.xoev.transport.xta._211.ContentType;
import de.xoev.transport.xta._211.GenericContentContainer;

public class FileUtils {
	
	public String readFileAsString(String input) {
		return readStringFromFile(input);
	}
	
	private String readStringFromFile(String input) {
        InputStream in = getClass().getClassLoader().getResourceAsStream(input);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
            return sb.toString();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void writeContentContainerToFile(de.kommone.webservice.sources.GenericContentContainer container) {

        if (container != null && container.getContentContainer() != null && container.getContentContainer()
                .getMessage() != null) {
            de.kommone.webservice.sources.ContentType contentType = container.getContentContainer().getMessage();

            contentType.setContentType("application/xml");
            contentType.setFilename("nachricht");

            // Schreiben der Message
            writeFileForContentType(contentType);

            // Schreiben der Attachements
            if (container.getContentContainer().getAttachment() != null) {
                for (de.kommone.webservice.sources.ContentType attachement : container.getContentContainer()
                        .getAttachment()) {
                    writeFileForContentType(attachement);
                }
            }
        }
    }

    private void writeFileForContentType(de.kommone.webservice.sources.ContentType contentType) {
        String fileName = contentType.getFilename();
        String fileType = getType(contentType.getContentType());
        String calculatedFileName = null;
        if (fileName == null) {
            calculatedFileName = fileType;
        } else {
            calculatedFileName = fileName + "." + fileType;
        }
        File file = new File(getResource() + "/" + calculatedFileName);
        writeFileToPath(file, contentType.getValue());
    }

    public void writeContentContainerToFile(GenericContentContainer container) {

        if (container != null && container.getContentContainer() != null && container.getContentContainer()
                .getMessage() != null) {
            ContentType contentType = container.getContentContainer().getMessage();

            contentType.setContentType("application/xml");
            contentType.setFilename("nachricht");

            // Schreiben der Message
            writeFileForContentType(contentType);

            // Schreiben der Attachements
            if (container.getContentContainer().getAttachment() != null) {
                for (ContentType attachement : container.getContentContainer().getAttachment()) {
                    writeFileForContentType(attachement);
                }
            }
        }
    }

    private void writeFileForContentType(ContentType contentType) {
        String fileName = contentType.getFilename();
        String fileType = getType(contentType.getContentType());
        String calculatedFileName = null;
        if (fileName == null) {
            calculatedFileName = fileType;
        } else {
            calculatedFileName = fileName + "." + fileType;
        }
        File file = new File(getResource() + "/" + calculatedFileName);
        writeFileToPath(file, contentType.getValue());
    }

    private void writeFileToPath(File file, byte[] data) {
        try {
            if (file.getParentFile().mkdirs()) {
                // erst Parent erzeugen, dann File
                createNewFile(file, data);
            } else {
                createNewFile(file, data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createNewFile(File file, byte[] data) throws IOException {
        if (file.createNewFile()) {
            Files.write(file.toPath(), data);
        }
    }

    private String getType(String type) {
        if (type.indexOf("/") != -1) {
            type = type.substring(type.lastIndexOf("/") + 1);
        }
        return type;
    }

    private String getResource() {
        String resource = System.getProperty("user.home");
        resource += File.separator + ".xtaws";
        return resource;
    }

}
