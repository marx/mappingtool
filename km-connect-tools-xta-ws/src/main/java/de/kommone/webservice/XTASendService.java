package de.kommone.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.Action;
import javax.xml.ws.BindingType;
import javax.xml.ws.FaultAction;
import javax.xml.ws.Holder;

import de.kommone.webservice.sources.GenericContentContainer;
import de.kommone.webservice.sources.MessageSchemaViolationException;
import de.kommone.webservice.sources.MessageVirusDetectionException;
import de.kommone.webservice.sources.ParameterIsNotValidException;
import de.kommone.webservice.sources.PermissionDeniedException;
import de.kommone.webservice.sources.SendPortType;
import de.kommone.webservice.sources.SyncAsyncException;
import de.kommone.webservice.sources.XTAWSTechnicalProblemException;
import de.kommone.webservice.utils.FileUtils;

@WebService(name = "sendPortType", targetNamespace = "http://xoev.de/transport/xta/211")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@BindingType(value = javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
public class XTASendService implements SendPortType {

    @WebMethod(action = "http://www.xta.de/XTA/SendMessage")
    @Action(input = "http://www.xta.de/XTA/SendMessage", output = "http://www.xta.de/XTA/SendMessage", fault = {
        @FaultAction(className = PermissionDeniedException.class, value = "http://www.xta.de/XTA/SendMessage"),
        @FaultAction(className = ParameterIsNotValidException.class, value = "http://www.xta.de/XTA/SendMessage"),
        @FaultAction(className = XTAWSTechnicalProblemException.class, value = "http://www.xta.de/XTA/SendMessage"),
        @FaultAction(className = MessageSchemaViolationException.class, value = "http://www.xta.de/XTA/SendMessage"),
        @FaultAction(className = MessageVirusDetectionException.class, value = "http://www.xta.de/XTA/SendMessage"),
        @FaultAction(className = SyncAsyncException.class, value = "http://www.xta.de/XTA/SendMessage")
    })
    public void sendMessage(
        @WebParam(name = "GenericContentContainer", targetNamespace = "http://xoev.de/transport/xta/211", partName = "GenericContainer")
        GenericContentContainer genericContainer)
        throws MessageSchemaViolationException, MessageVirusDetectionException, ParameterIsNotValidException, PermissionDeniedException, SyncAsyncException, XTAWSTechnicalProblemException
    {
        new FileUtils().writeContentContainerToFile(genericContainer);

    }

    @Override
    public void sendMessageSync(Holder<GenericContentContainer> genericContainer)
            throws MessageSchemaViolationException, MessageVirusDetectionException, ParameterIsNotValidException,
            PermissionDeniedException, SyncAsyncException, XTAWSTechnicalProblemException {
        // TODO Auto-generated method stub

    }

}
