package de.kommone.webservice;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.Action;
import javax.xml.ws.BindingType;
import javax.xml.ws.FaultAction;

import de.kommone.webservice.sources.ContentType;
import de.kommone.webservice.sources.GenericContentContainer;
import de.kommone.webservice.sources.GenericContentContainer.ContentContainer;
import de.kommone.webservice.sources.InvalidMessageIDException;
import de.kommone.webservice.sources.MsgBoxCloseRequestType;
import de.kommone.webservice.sources.MsgBoxFetchRequest;
import de.kommone.webservice.sources.MsgBoxGetNextRequestType;
import de.kommone.webservice.sources.MsgBoxPortType;
import de.kommone.webservice.sources.MsgBoxStatusListRequestType;
import de.kommone.webservice.sources.MsgStatusListType;
import de.kommone.webservice.sources.PermissionDeniedException;
import de.kommone.webservice.sources.XTAWSTechnicalProblemException;

@WebService(name = "msgBoxPortType", targetNamespace = "http://xoev.de/transport/xta/211")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@BindingType(value = javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
public class XTAGetMessageService implements MsgBoxPortType {

    @WebMethod(action = "http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxFetchRequest")
    @WebResult(name = "GenericContentContainer", targetNamespace = "http://xoev.de/transport/xta/211", partName = "GenericContainer")
    @Action(input = "http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxFetchRequest", output = "http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxFetchRequest", fault = {
        @FaultAction(className = PermissionDeniedException.class, value = "http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxFetchRequest"),
        @FaultAction(className = XTAWSTechnicalProblemException.class, value = "http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxFetchRequest"),
        @FaultAction(className = InvalidMessageIDException.class, value = "http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxFetchRequest")
    })
    public GenericContentContainer getMessage(
        @WebParam(name = "MsgBoxFetchRequest", targetNamespace = "http://www.osci.eu/ws/2008/05/transport", partName = "FetchRequest")
        MsgBoxFetchRequest fetchRequest)
        throws InvalidMessageIDException, PermissionDeniedException, XTAWSTechnicalProblemException{
        return getContentContainer();
    }

    private GenericContentContainer getContentContainer() {
        GenericContentContainer contentContainer = new GenericContentContainer();
        String directoryName = getResource();
        List<File> files = new ArrayList<>();
        try (Stream<Path> paths = Files.walk(Paths.get(directoryName))) {
            paths.filter(Files::isRegularFile).forEach(f -> files.add(f.toFile()));
        } catch (IOException e) {
            // nichts tun
        }
        File nachricht = files.stream().filter(f -> f.getAbsolutePath().endsWith(".xml")).findFirst().orElse(null);
        List<File> anhaenge = files.stream().filter(f -> !f.getAbsolutePath().endsWith(".xml")).collect(Collectors
                .toList());
        if (nachricht == null) {
            return null;
        }
        contentContainer.setContentContainer(createContentContainer(getContentTypeFromFile(nachricht)));
        for (File file : anhaenge) {
            contentContainer.getContentContainer().getAttachment().add(getContentTypeFromFile(file));
        }
        return contentContainer;
    }

    private ContentContainer createContentContainer(ContentType contentTypeFromFile) {
        ContentContainer container = new ContentContainer();
        container.setMessage(contentTypeFromFile);
        return container;
    }

    private String getResource() {
        String resource = System.getProperty("user.home");
        resource += File.separator + ".xtaws";
        return resource;
    }

    private ContentType getContentTypeFromFile(File file) {
        ContentType att = new ContentType();
        try {
            att.setValue(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            // nichts tun
        }
        att.setFilename(file.getName());
        att.setContentType(getType(file.getName()));
        att.setContentDescription("1");
        return att;
    }

    private String getType(String name) {
        if (name.lastIndexOf(".") != -1) {
            return name.substring(name.lastIndexOf(".") + 1);
        }
        return name;
    }

    @Override
    public MsgStatusListType getStatusList(MsgBoxStatusListRequestType fetchRequest) throws PermissionDeniedException,
            XTAWSTechnicalProblemException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GenericContentContainer getNextMessage(MsgBoxGetNextRequestType fetchRequest)
            throws InvalidMessageIDException, PermissionDeniedException, XTAWSTechnicalProblemException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public MsgStatusListType getNextStatusList(MsgBoxGetNextRequestType fetchRequest) throws PermissionDeniedException,
            XTAWSTechnicalProblemException {
        // TODO Auto-generated method stub
        return null;
    }

    @WebMethod(action = "http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxCloseRequest")
    @Action(input = "http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxCloseRequest", output = "http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxCloseRequest", fault = {
        @FaultAction(className = PermissionDeniedException.class, value = "http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxCloseRequest"),
        @FaultAction(className = XTAWSTechnicalProblemException.class, value = "http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxCloseRequest"),
        @FaultAction(className = InvalidMessageIDException.class, value = "http://www.osci.eu/ws/2008/05/transport/urn/messageTypes/MsgBoxCloseRequest")
    })
    public void close(
        @WebParam(name = "MsgBoxCloseRequest", targetNamespace = "http://www.osci.eu/ws/2008/05/transport", partName = "FetchRequest")
        MsgBoxCloseRequestType fetchRequest)
        throws InvalidMessageIDException, PermissionDeniedException, XTAWSTechnicalProblemException {
        String directoryName = getResource();
        List<File> files = new ArrayList<>();
        try (Stream<Path> paths = Files.walk(Paths.get(directoryName))) {
            paths.filter(Files::isRegularFile).forEach(f -> files.add(f.toFile()));
        } catch (IOException e) {
            // nichts tun
        }
        
        for (File file : files) {
            try {
                Files.delete(file.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
