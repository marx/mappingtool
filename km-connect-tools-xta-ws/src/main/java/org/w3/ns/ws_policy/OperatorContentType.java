
package org.w3.ns.ws_policy;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java-Klasse f�r OperatorContentType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OperatorContentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice maxOccurs="unbounded" minOccurs="0">
 *           &lt;element ref="{http://www.w3.org/ns/ws-policy}Policy"/>
 *           &lt;element ref="{http://www.w3.org/ns/ws-policy}All"/>
 *           &lt;element ref="{http://www.w3.org/ns/ws-policy}ExactlyOne"/>
 *           &lt;element ref="{http://www.w3.org/ns/ws-policy}PolicyReference"/>
 *           &lt;any processContents='lax' namespace='##other'/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperatorContentType", propOrder = {
    "policyOrAllOrExactlyOne"
})
@XmlSeeAlso({
    Policy.class
})
public class OperatorContentType {

    @XmlElementRefs({
        @XmlElementRef(name = "All", namespace = "http://www.w3.org/ns/ws-policy", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ExactlyOne", namespace = "http://www.w3.org/ns/ws-policy", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Policy", namespace = "http://www.w3.org/ns/ws-policy", type = Policy.class, required = false),
        @XmlElementRef(name = "PolicyReference", namespace = "http://www.w3.org/ns/ws-policy", type = PolicyReference.class, required = false)
    })
    @XmlAnyElement(lax = true)
    protected List<Object> policyOrAllOrExactlyOne;

    /**
     * Gets the value of the policyOrAllOrExactlyOne property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyOrAllOrExactlyOne property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyOrAllOrExactlyOne().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link OperatorContentType }{@code >}
     * {@link Object }
     * {@link Policy }
     * {@link PolicyReference }
     * {@link Element }
     * {@link JAXBElement }{@code <}{@link OperatorContentType }{@code >}
     * 
     * 
     */
    public List<Object> getPolicyOrAllOrExactlyOne() {
        if (policyOrAllOrExactlyOne == null) {
            policyOrAllOrExactlyOne = new ArrayList<Object>();
        }
        return this.policyOrAllOrExactlyOne;
    }

}
