<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:xbau="http://www.xleitstelle.de/xbau/2/3-E2"
           xmlns:xbauk="http://www.xleitstelle.de/xbau/kernmodul/1/1-E2"
           targetNamespace="http://www.xleitstelle.de/xbau/2/3-E2"
           version="2.3"
           elementFormDefault="qualified"
           attributeFormDefault="unqualified">
   <xs:annotation>
      <xs:appinfo>
         <standard>
            <nameLang>XBau - Standard für die Datenkommunikation der Bauaufsichtsbehörde</nameLang>
            <nameKurz>XBau</nameKurz>
            <nameTechnisch>xbau</nameTechnisch>
            <kennung>urn:xoev-de:bmk:standard:xbau</kennung>
            <beschreibung>XBau ist der XÖV-Standard für den Datenaustausch der Bauaufsichtsbehörden mit ihren Kommunikationspartnern (Bauherren, Architektenbüros, Behörden zahlreicher Rechtsbereiche). </beschreibung>
         </standard>
         <versionStandard>
            <version>2.3</version>
            <beschreibung>Erweiterte Fassung des Fachmoduls XBau-Hochbau: Das Verfahren der Bauberatung wurde aufgenommen. Eine Reihe von Datentypen für die OZG-Umsetzung bei der Antragstellung Hochbau wurden optimiert. Die Metadaten für die Übermittlung von Bauvorlagen wurden konsolidiert, so dass Bauvorlagen jetzt entweder als Anhang (Attachment) oder per Verlinkung bereitgestellt werden. Weitere Änderungen und Erweiterungen sind in der Versionshistorie des Spezifikationsdokuments beschrieben.</beschreibung>
            <versionXOEVProfil>1.7.1</versionXOEVProfil>
            <versionXOEVHandbuch>2.3</versionXOEVHandbuch>
            <versionXGenerator>3.0.1</versionXGenerator>
            <versionModellierungswerkzeug>19.0</versionModellierungswerkzeug>
            <nameModellierungswerkzeug>MagicDraw</nameModellierungswerkzeug>
         </versionStandard>
      </xs:appinfo>
   </xs:annotation>
   <xs:include schemaLocation="xbau-baukasten.xsd"/>
   <xs:import schemaLocation="xbau-kernmodul-datentypen.xsd"
              namespace="http://www.xleitstelle.de/xbau/kernmodul/1/1-E2"/>
   <xs:element name="grundstuecksteilung.antrag.0240">
      <xs:annotation>
         <xs:appinfo>
            <title>Antrag auf Grundstücksteilung</title>
         </xs:appinfo>
         <xs:documentation>Mit dieser Nachricht wird der Antrag auf Grundstücksteilung übermittelt.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="xbau:Nachricht.G2G">
               <xs:sequence>
                  <xs:element name="referenzAntragsservice" type="xbauk:ReferenzAntragsservice">
                     <xs:annotation>
                        <xs:documentation>Eine vom Antragsservice vergebene Kennung des Antragsvorgangs. Sofern die Bauaufsichtsbehörde auf die Antragstellung reagiert, bezieht sie sich in ihren Reaktionsnachrichten auf diese Kennung.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="antragsteller" maxOccurs="unbounded" type="xbauk:Akteur">
                     <xs:annotation>
                        <xs:documentation>Mit diesem Element werden die Daten des Antragsstellers übermittelt.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="beteiligte"
                              maxOccurs="unbounded"
                              type="xbau:BeteiligteBauprojekt">
                     <xs:annotation>
                        <xs:documentation>Mit diesem Element werden die Daten der Beteiligten (bspw. Katasteramt, Vermessungsingenieur) übermittelt.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="angabenGrundstueck">
                     <xs:annotation>
                        <xs:documentation>Mit diesem Element werden die Daten zu dem zu teilenden Grundstück übermittelt.</xs:documentation>
                     </xs:annotation>
                     <xs:complexType>
                        <xs:sequence>
                           <xs:element name="grundstueckIstBebaut" type="xs:boolean">
                              <xs:annotation>
                                 <xs:documentation>Mit diesem Element wird angegeben, ob das Grundstück bebaut oder unbebaut ist. Falls es bebaut ist, ist true einzutragen, andernfalls false.</xs:documentation>
                              </xs:annotation>
                           </xs:element>
                           <xs:element name="grundstueck" type="xbau:Grundstueck">
                              <xs:annotation>
                                 <xs:documentation>Mit diesem Element werden die Daten zu dem Grundstück übermittelt, auf das sich die beantragte Teilung bezieht.</xs:documentation>
                              </xs:annotation>
                           </xs:element>
                        </xs:sequence>
                     </xs:complexType>
                  </xs:element>
                  <xs:element name="anlagen" type="xbau:Anlagen">
                     <xs:annotation>
                        <xs:documentation>Mit diesem Element werden die Anlagen (u.a. Lageplan und Bauzeichnungen) zu einem Teilungsantrag übermittelt.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
   <xs:element name="grundstuecksteilung.formellePruefung.0241">
      <xs:annotation>
         <xs:appinfo>
            <title>Ergebnis der formellen Prüfung</title>
         </xs:appinfo>
         <xs:documentation>Diese Nachricht bezieht sich auf einen bereits gestellten Teilungsantrag. Enthalten sind die Ergebnisse der formellen Prüfung (Befunde) sowie eine Frist, innerhalb derer der Antragsteller den Antrag anzupassen hat.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="xbau:Nachricht.G2G">
               <xs:sequence>
                  <xs:element name="bezug" type="xbau:Bezug">
                     <xs:annotation>
                        <xs:documentation>Unterhalb dieses Elements werden Eintragungen vorgenommen, um auf den Vorgang (oder den Antrag) bzw. die Nachricht grundstuecksteilung.antrag.0240 Bezug zu nehmen, durch deren Eingang bei der Bauaufsichtsbehörde der Vorgang ausgelöst worden ist. Damit wird gleichzeitig dem Antragsteller die entsprechende Vorgangsnummer für künftige Referenzierung mitgeteilt. Dadurch ist der Bezug zum Bauantrag hergestellt, zu dessen formeller Prüfung die vorliegende Nachricht Ergebnisse enthält.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="befundliste"
                              minOccurs="0"
                              type="xbau:BefundlisteAntragFormell">
                     <xs:annotation>
                        <xs:documentation>Hier können die Ergebnisse der formellen Prüfung in Form einer strukturierten Befundliste eingetragen werden (die Liste zählt Mängel des Teilungsantrags auf).</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="frist" minOccurs="0" type="xs:date">
                     <xs:annotation>
                        <xs:documentation>Hier kann eine durch die Behörde gesetzte Frist eingetragen werden, innerhalb derer die genannten Mängel durch den Antragsteller zu beseitigen sind und ein überarbeiteter Antrag einzureichen ist.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="anschreiben" minOccurs="0" type="xbauk:Text">
                     <xs:annotation>
                        <xs:documentation>In dieses Element kann ein Anschreibentext eingetragen werden.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="anlagen" minOccurs="0" type="xbau:Anlagen">
                     <xs:annotation>
                        <xs:documentation>Falls die Behörde der Nachricht Anlagen beifügen möchte, ist dieses Element zu verwenden.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
   <xs:element name="grundstuecksteilung.antragGeandert.0242">
      <xs:annotation>
         <xs:appinfo>
            <title>Korrekturen / Modifikationen zum Antrag</title>
         </xs:appinfo>
         <xs:documentation>Diese Nachricht enthält geänderte oder ergänzte Daten der Antragstellung zur Grundstücksteilung. Sie bezieht sich auf einen bereits gestellten Teilungsantrag, zu dem noch kein Bescheid erteilt wurde. Entweder ist sie eine Reaktion auf eine zum Teilungsantrag vorliegende Mängelliste der Bauaufsichtsbehörde, oder der Antragsteller reicht aus anderen Gründen eine modifizierte Planung im laufenden Verfahren nach.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="xbau:Nachricht.G2G">
               <xs:sequence>
                  <xs:element name="bezug" type="xbau:Bezug">
                     <xs:annotation>
                        <xs:documentation>Unterhalb dieses Elements werden Eintragungen vorgenommen, um auf einen Vorgang (oder den Antrag) und ggf. auf eine Nachricht grundstuecksteilung.antrag.0240, auf die die vorliegende Nachrichteninstanz eine Reaktion ist, Bezug zu nehmen. Falls mit der Nachricht initiativ eine geänderte Planung eingereicht wird, ist der Bezug auf eine solche Nachricht nicht enthalten.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="beteiligteGeaendert"
                              minOccurs="0"
                              maxOccurs="unbounded"
                              type="xbau:BeteiligteBauprojekt">
                     <xs:annotation>
                        <xs:documentation>Mit diesem Element werden die Daten der Beteiligten (bspw. Katasteramt, Vermessungsingenieur) übermittelt.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="angabenGrundstueckGeaendert">
                     <xs:annotation>
                        <xs:documentation>Mit diesem Element werden die Daten zu dem zu teilenden Grundstück übermittelt.</xs:documentation>
                     </xs:annotation>
                     <xs:complexType>
                        <xs:sequence>
                           <xs:element name="grundstueckIstBebaut" type="xs:boolean">
                              <xs:annotation>
                                 <xs:documentation>Mit diesem Element wird angegeben, ob das Grundstück bebaut oder unbebaut ist. Falls es bebaut ist, ist true einzutragen, andernfalls false.</xs:documentation>
                              </xs:annotation>
                           </xs:element>
                           <xs:element name="grundstueck" type="xbau:Grundstueck">
                              <xs:annotation>
                                 <xs:documentation>Mit diesem Element werden die Daten zu dem Grundstück übermittelt, auf das sich die beantragte Teilung bezieht.</xs:documentation>
                              </xs:annotation>
                           </xs:element>
                        </xs:sequence>
                     </xs:complexType>
                  </xs:element>
                  <xs:element name="anlagen" minOccurs="0" type="xbau:Anlagen">
                     <xs:annotation>
                        <xs:documentation>Hier sind ggf. überarbeitete oder nachgereichte Anlagen bzw. Verweise auf überarbeitete oder nachgereichte Anlagen enthalten. Nachgereichte Anlagen: Aus der Beschreibung bzw. Bezeichnung der Anlage muss (wie immer) der Charakter der nachgereichten Unterlage hervorgehen. Ein gesonderter Bezug auf den Befund der Befundliste (z.B. Befund-Nr.), der ggf. zur Übermittlung dieser Unterlage führte, ist nicht erforderlich. Geänderte Anlagen: In diesem Fall wird eine neue Version (jüngeres Erstellungsdatum) einer bereits vorher gelieferten Bauvorlage übermittelt. Falls der Antragsteller initiativ mit der Nachricht eine modifizierte Planung einreicht, muss unter den Anlagen (je nach Landesbauordnung) ggf. eine Übereinstimmungserklärung enthalten sein. Außerdem muss unter den Anlagen eine Begründung bzw. Erläuterung zur eingereichten geänderten Planung enthalten sein.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
   <xs:element name="grundstuecksteilung.anhoerung.0243">
      <xs:annotation>
         <xs:appinfo>
            <title>Hinweis auf Anhörung</title>
         </xs:appinfo>
         <xs:documentation>Diese Nachricht fordert den Bauherrn zur Stellungnahme auf, nachdem die materielle Prüfung des Teilungsantrags ergeben hat, dass Gründe vorliegen, die zu seiner Ablehnung durch die Bauaufsichtsbehörde führen können. Es sind die Befunde enthalten, die das Ergebnis der materiellen Prüfung sind. Diese werden hier aufgelistet bzw. im Anhang der Nachricht dokumentiert mit dem Hinweis, dass Gelegenheit zu Stellungnahme bzw. Nachbesserung besteht.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="xbau:Nachricht.G2G">
               <xs:sequence>
                  <xs:element name="bezug" type="xbau:Bezug">
                     <xs:annotation>
                        <xs:documentation>Unterhalb dieses Elements werden Eintragungen vorgenommen, um auf den laufenden Vorgang zum Teilungsantrag Bezug zu nehmen; außerdem ggf. auf die Nachricht grundstuecksteilung.antrag.0240, die den Vorgang ausgelöst hat.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="aufforderungZurStellungnahme" type="xbauk:Text">
                     <xs:annotation>
                        <xs:documentation>Hier ist die Aufforderung zu Stellungnahme in Form eines Anschreibens formuliert.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="bescheidlage" minOccurs="0" type="xbau:BefundlisteMateriell">
                     <xs:annotation>
                        <xs:documentation>Hier können die Gründe, die nach aktueller Lage zur Ablehnung des Antrags durch die Bauaufsichtsbehörde führen können, in Form einer strukturierten Liste angegeben werden. Es handelt sich um eine Liste von Abweichungen von den öffentlich-rechtlichen Vorschriften, die sich aus der laufenden Prüfung des Teilungsantrages ergeben hat. Zu jedem Eintrag der Liste fordert die Nachricht zu einer Stellungnahme bzw. Nachbesserung auf.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="frist" minOccurs="0" type="xs:date">
                     <xs:annotation>
                        <xs:documentation>Hier kann eine durch die Behörde gesetzte Frist angegeben werden, innerhalb derer eine Stellungnahme des Antragstellers vorliegen muss, damit sie berücksichtigt werden kann.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="anlagen" minOccurs="0" type="xbau:Anlagen">
                     <xs:annotation>
                        <xs:documentation>Für die Aufforderung zur Stellungnahme ggf. notwendige Anhänge können hier der Nachricht als Anlage beigefügt werden.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
   <xs:element name="grundstuecksteilung.stellungnahme.0244">
      <xs:annotation>
         <xs:appinfo>
            <title>Stellungnahme oder Nachbesserung</title>
         </xs:appinfo>
         <xs:documentation>Diese Nachricht bezieht sich auf einen bereits gestellten Teilungsantrag und auf eine zu diesem vorliegende Anhörungsnachricht. Sie enthält Stellungnahmen des Bauherrn zu den Eigenschaften des Teilungsvorhabens, die von der Bauaufsichtsbehörde beanstandet wurden. Sie enthält ggf. außerdem den nachgebesserten Antrag bzw. nachgebesserte Bauvorlagen.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="xbau:Nachricht.G2G">
               <xs:sequence>
                  <xs:element name="bezug" type="xbau:Bezug">
                     <xs:annotation>
                        <xs:documentation>Unterhalb dieses Elements werden Eintragungen vorgenommen, um auf den laufenden Vorgang zum Teilungsantrag Bezug zu nehmen und außerdem auf die Nachricht grundstuecksteilung.anhoerung.0244, auf deren Inhalt sich die vorliegende Nachricht bezieht.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="stellungnahme"
                              maxOccurs="unbounded"
                              type="xbau:StellungnahmeBelastenderVerwaltungsakt">
                     <xs:annotation>
                        <xs:documentation>Hier ist die Reaktion auf die in der Anhörungsnachricht enthaltenen Punkte einzutragen. Dies kann eine Verteidigung der ursprünglichen Konzeption sein, dies kann aber auch eine geänderte Antragsunterlage sein, welche in diesem Fall im Abschnitt Anpassungen dokumentiert ist.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="anlagen" minOccurs="0" type="xbau:Anlagen">
                     <xs:annotation>
                        <xs:documentation>Hier sind ggf. überarbeitete oder nachgereichte Anlagen bzw. Verweise auf überarbeitete oder nachgereichte Anlagen enthalten, passend zur weiter oben gegebenen Stellungnahme bzw. Erläuterung der Anpassung.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
   <xs:element name="grundstuecksteilung.bescheid.0245">
      <xs:annotation>
         <xs:appinfo>
            <title>Bescheid</title>
         </xs:appinfo>
         <xs:documentation>Diese Nachricht stellt den Bescheid dar, in den die Bauaufsichtsbehörde die Ergebnisse ihrer materiellen Prüfung einträgt. Der Teilungsantrag ist entweder genehmigt, genehmigt mit Nebenbestimmungen (zum Beispiel unter Auflagen) oder abgelehnt. Das Absenden dieser Nachricht stellt das Erteilen des Bescheids dar.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="xbau:Nachricht.G2G">
               <xs:sequence>
                  <xs:element name="bezug" type="xbau:Bezug">
                     <xs:annotation>
                        <xs:documentation>Dieses Objekt enthält die Eintragungen, die auf den Vorgang bzw. die Nachricht grundstuecksteilung.antrag.0240 mit dem Antrag Bezug nehmen, zu dem die vorliegende Nachricht den Bescheid enthält.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="bescheid" type="xbau:Bescheid">
                     <xs:annotation>
                        <xs:documentation>In diesem Objekt sind die verschiedenen Komponenten des Bescheides enthalten.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="anlagen" type="xbau:Anlagen">
                     <xs:annotation>
                        <xs:documentation>Hier sind die Anlagen zum Bescheid bzw. Verweise auf diese Anlagen enthalten. Es handelt sich ggf. um Bauvorlagen, die von der Bauaufsichtsbehörde fortgeschrieben wurden.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
   <xs:element name="grundstuecksteilung.gebuehrenbescheid.0246">
      <xs:annotation>
         <xs:appinfo>
            <title>Gebührenbescheid</title>
         </xs:appinfo>
         <xs:documentation>In dieser Nachricht sind die Angaben zu den für die Durchführung des Verfahrens Grundstücksteilung zu entrichtenden Gebühren enthalten.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="xbau:Nachricht.G2G">
               <xs:sequence>
                  <xs:element name="bezug" type="xbau:Bezug">
                     <xs:annotation>
                        <xs:documentation>Dieses Objekt enthält die Eintragungen, die auf den Vorgang bzw. die Nachricht grundstuecksteilung.antrag.0240 mit dem Antrag Bezug nehmen, zu dem die vorliegende Nachricht den Bescheid enthält.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="gebuehrenbescheid" type="xbau:Gebuehrenbescheid">
                     <xs:annotation>
                        <xs:documentation>Unterhalb dieses Element finden sich die Angaben zur Gebührenberechnung und -begründung, sowie die Zahlungsdaten und weitere Informationen.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="frist" type="xs:date">
                     <xs:annotation>
                        <xs:documentation>Dies ist die durch die Behörde gesetzte Zahlungsfrist.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
</xs:schema>
