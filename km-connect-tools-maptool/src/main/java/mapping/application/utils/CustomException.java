package mapping.application.utils;

public class CustomException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -3481036779618090387L;

    public CustomException(String message) {
        super(message);
    }

    public CustomException() {
        super();
    }

}
