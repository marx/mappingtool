package mapping.application.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mapping.application.MappingHelper;

public class CodeGen {

    private List<String> lines = new ArrayList<>();

    private static List<String> methods = new ArrayList<>();

    private Map<String, Node> pathToNode = new HashMap<>();

    private static Map<String, List<String>> methodsMap = new HashMap<>();

    private Map<String, String> pathToSource = new HashMap<>();

    private static Map<String, String> abstractToConcrete = new HashMap<>();

    private Map<String, List<String>> createdGetMethodsMap = new HashMap<>();

    private static Map<String, String> sourcePathsToSources = new HashMap<>();

    private List<String> generatedGetterCode = new ArrayList<>();

    private String converterCode = "";

    private StringBuilder generatedCodeBuilder = new StringBuilder();

    private Node root;

    private String source;

    private String target;

    public static String getTypeForSourcePath(String path) {
        return sourcePathsToSources.get(path);
    }

    public static void addMethod(String methodname) {
        methods.add(methodname);
    }

    public static boolean isMethodContained(String methodName) {
        return methods.contains(methodName);
    }

    public static void addMethod(String methodname, String paramname) {
        if (methodsMap.get(methodname) != null) {
            List<String> list = methodsMap.get(methodname);
            list.add(paramname);
            methodsMap.put(methodname, list);
        } else {
            List<String> list = new ArrayList<>();
            list.add(paramname);
            methodsMap.put(methodname, list);
        }
    }

    public static boolean isMethodContained(String methodName, String paramName) {
        return (methodsMap.get(methodName) != null && methodsMap.get(methodName).contains(paramName));
    }

    public static void main(String[] args) throws ClassNotFoundException {

        CodeGen codeGenarator = new CodeGen();
        String code = codeGenarator.generateCode(false, "de.kommone.kmc.webservice", "PojoToBauAntrag",
                "C:/Projekte/km-connect-tools/src/main/resources/SavedProgress.txt");

        codeGenarator.output(new File(
                "C:/Projekte/km-connect-tools/src/main/java/de/kommone/kmc/webservice/PojoToBauAntrag.java"), code);
    }

    public void output(File file, String pojoPackage, String xmlPackage, String newPojoPackage, String newXmlPackage,
            String code) {

        code = code.replace(pojoPackage, newPojoPackage);
        code = code.replace(xmlPackage, newXmlPackage);
        output(file, code);
    }

    public void output(File file, String code) {
        String placeHolder = "java.lang.String";
        code = code.replace("List(" + placeHolder + ")", "java.util.List<" + placeHolder + ">");
        try {
            PrintStream out = new PrintStream(file);
            out.println(code);
            out.close();
        } catch (FileNotFoundException e) {
            return;
        }
    }

    public String generateCode(boolean mapToPojo, String packageName, String className, List<String> readLines,
            LocalDate date)
            throws ClassNotFoundException {
        initialize();
        this.lines = readLines;
        return generateCode(mapToPojo, packageName, className, "", date);
    }

    public String generateCode(boolean mapToPojo, String packageName, String className, String inputPath,
            LocalDate date) throws ClassNotFoundException {

        if (!inputPath.isEmpty()) {
            initialize();
            lines = readStringFromFile(inputPath);
        }

        generatedCodeBuilder.append("package " + packageName + ";\n");
        generatedCodeBuilder.append("/** Dieser Quellcode wurde mit dem KM-Connect-Mapping-Tool am " + date.toString()
                + " generiert, evtl. wurde der Code manuell ergänzt \n @author KM-Connect-DEV-TEAM**/ \n");
        generatedCodeBuilder.append("\n" + "public class " + className + " {");

        boolean first = true;
        boolean getConverter = false;
        for (String line : lines) {

            if (getConverter) {
                String currentConverter = line.split("@")[1];
                converterCode += currentConverter;
                continue;
            }

            if (line.contains("FULL-TREE")) {
                getConverter = false;
                continue;
            }
            if (line.contains("GEN-TREE")) {
                getConverter = false;
                break;
            }
            if (line.contains("CONVERTERS")) {
                getConverter = true;
                continue;
            }

            if (line.startsWith("$T$") || line.startsWith("$X$")) {
                line = line.substring(3);
            }

            String[] pojoXML = line.split("=");

            String pojo = pojoXML[0];

            // es gibt kein Mapping auf der rechten Seite -> weiter mit der
            // nächsten Zeile
            if (pojoXML.length < 2) {
                continue;
            }

            String xml = pojoXML[1];
            String converter = pojoXML.length > 2 ? line.split("=")[2] : null;

            if (mapToPojo) {
                pojo = line.split("=")[1];
                xml = line.split("=")[0];
            }

            String[] xmlObjects = xml.split("->");

            String parentPath = "";
            String currentPath = "";
            String pojoCurrentPath = "";

            for (int i = 0; i < xmlObjects.length; i++) {

                String xmlType = null;
                String xmlName = null;

                if (xmlObjects[i].split("/").length > 1) {
                    xmlType = xmlObjects[i].split("/")[1];
                    xmlName = xmlObjects[i].split("/")[0];
                } // should be the case for a child of an abstract type
                else {
                    xmlType = xmlObjects[i].split("/")[0];
                }

                // determine the target
                target = xmlType;

                String pojoType = null;
                String pojoName = null;
                String pureType = null;

                boolean updatedSource = false;
                String[] pojoElements = pojo.split("->");

                // determine the source
                if (i == xmlObjects.length - 1) {

                    pojoName = xmlForm(pojoElements[pojoElements.length - 1].split("/")[0]);
                    pojoType = xmlForm(pojoElements[pojoElements.length - 1].split("/")[1]);
                    pureType = xmlType;
                    if (pureType.startsWith("List")) {
                        pureType = xmlType.substring(5, xmlType.length() - 1);
                    }
                    if (pureType.startsWith("Complex")) {
                        pureType = pureType.substring(8, pureType.length() - 1);
                    }
                    if (pojoType.startsWith("Complex")) {
                        pojoType = pojoType.substring(8, pojoType.length() - 1);
                    }
                    pureType = MappingHelper.mapPrimitiveType(pureType) == null ? pureType
                            : MappingHelper.mapPrimitiveType(pureType).getName();
                    if (!isSimpleType(pureType)) {
                        updatedSource = true;
                        source = pojoType;
                    }

                }

                // path for the map
                if (xmlName == null) {
                    xmlName = "";
                }
                currentPath = parentPath.isEmpty() ? xmlName
                        : currentPath + "->" + (xmlName.isEmpty() ? xmlType : xmlName);

                if (updatedSource) {
                    pathToSource.put(currentPath, source);
                }

                if (pathToSource.get(parentPath) != null && !updatedSource) {
                    source = pathToSource.get(parentPath);
                }

                // the first check is needed to map a different source to an
                // already mapped
                // target (e. g. x -> z AND y -> z)
                if (i == xmlObjects.length - 1 || pathToNode.get(currentPath) == null) {
                    Node current = createNode(source, target, xmlName, xmlType, pojoName, methods, converter,
                            i == xmlObjects.length - 1);
                    pojoCurrentPath = getPathFromSource(source, pojoElements, pojoName);
                    sourcePathsToSources.put(pojoCurrentPath, source);
                    current.setSourcePath(pojoCurrentPath);
                    if (first) {
                        first = false;
                        root = current;
                        root.setRoot(true);
                    }
                    pathToNode.put(currentPath, current);

                    Node parent = pathToNode.get(parentPath);

                    if (parent != null) {
                        parent.addChild(current);
                        current.setParent(parent);
                    }
                }

                parentPath = currentPath;

            }
        }

        adjustSources(root);

        buildCodeFromRoot(root);

        // getter (für das Mapping von Nachricht -> POJO relevant)
        for (String code : generatedGetterCode) {
            generatedCodeBuilder.append(code);
        }
        generatedCodeBuilder.append(converterCode);
        generatedCodeBuilder.append("}");
        String result = generatedCodeBuilder.toString();
        result = result.replace("[B", "byte[]");
        return result;
    }

    private void adjustSources(Node node) {
        node.adjustSource(root);

    }

    private String getPathFromSource(String source, String[] pojoElements, String pojoName) {
        String path = "";
        for (int i = 0; i < pojoElements.length; i++) {
            String[] nameType = pojoElements[i].split("/");
            String name = nameType[0];
            String type = nameType.length > 1 ? nameType[1] : nameType[0];
            path += path.isEmpty() ? name + "/" + type : "->" + name + "/" + type;
            if (pojoName != null) {
                if (name.equals(pojoName) && i == pojoElements.length - 1) {
                    return path;
                }
            } else if (type.equals(source)) {
                break;
            }
        }
        return path;
    }

    public String generateCode(boolean mapToPojo, String packageName, String className, String inputPath)
            throws ClassNotFoundException {
        return generateCode(mapToPojo, packageName, className, inputPath, LocalDate.now());
    }

    private void initialize() {
        lines.clear();
        methods.clear();
        pathToNode.clear();
        methodsMap.clear();
        pathToSource.clear();
        createdGetMethodsMap.clear();
        generatedGetterCode.clear();
        sourcePathsToSources.clear();
        converterCode = "";
        Node.init();
        Node.stringToDate = false;
        root = null;
        source = "";
        target = "";
        generatedCodeBuilder = new StringBuilder();
        abstractToConcrete.clear();
        abstractToConcrete.put("NachrichtenkopfG2G", "NachrichtenkopfG2G2");

    }

    public static boolean isInBlacklist(String simpleName) {
        return abstractToConcrete.get(simpleName) != null;
    }

    public static String getConcreteNameFromAbstractName(String abstractName) {
        return abstractToConcrete.get(abstractName);
    }

    private void buildCodeFromRoot(Node root) {

        String code = root.generateCode();
        generatedCodeBuilder.append(code);
        if (root.getGetterCode() != null) {
            generatedCodeBuilder.append(root.getGetterCode());
        }

        for (Node node : root.getChildren()) {
            buildCodeFromRoot(node);
        }

    }

    private Node createNode(String source, String target, String xmlName, String xmlType, String pojoName,
            List<String> methods, String converter, boolean directlyMapped) throws ClassNotFoundException {
        Node n = new Node();
        n.setConverter(converter);
        source = source.replace("$", ".");
        target = target.replace("$", ".");
        n.setNameInGroovy(xmlName);
        n.setSource(source);
        n.setTarget(target);
        n.setDirectlyMapped(directlyMapped);
        String code = "";
        boolean isList = false;
        // child of an abstract class -> not simple
        if (xmlName.isEmpty()) {
            n.setCode(code);
            n.setSimple(false);
            String createName = "create" + target.substring(target.lastIndexOf('.') + 1);
            n.setCreateName(createName);
            return n;
        }
        if (xmlType.startsWith("List")) {
            isList = true;
            xmlType = xmlType.substring(5, xmlType.length() - 1);
            code += "target.get" + firstLetterUppercase(xmlName) + "().addAll(";
        } else if (xmlType.startsWith("Complex")) {
            isList = false;
            xmlType = xmlType.substring(8, xmlType.length() - 1);
            code += "target.set" + firstLetterUppercase(xmlName) + "(";
        } else {
            code += "target.set" + firstLetterUppercase(xmlName) + "(";
        }
        boolean isSimple = MappingHelper.mapPrimitiveType(xmlType) != null || isSimpleType(xmlType);
        boolean isEnum = isEnum(xmlType);
        xmlType = xmlType.replace("$", ".");
        n.setTargetName(xmlName);
        n.setTargetType(xmlType);
        n.setSourceName(pojoName);
        n.setSimple(isSimple || isEnum);
        n.setEnum(isEnum);
        n.setListType(isList);
        return n;
    }

    private boolean isEnum(String xmlType) {
        try {
            Class<?> className = Class.forName(xmlType);
            return className.isEnum();
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    private String xmlForm(String name) {
        while (name.contains("_")) {
            int dashLoc = name.indexOf("_");
            name = name.substring(0, dashLoc) + name.substring(dashLoc + 1).toUpperCase().substring(0, 1) + name
                    .substring(dashLoc + 2);
        }
        return name;
    }

    private String firstLetterUppercase(String name) {
        return xmlForm(name.toUpperCase().substring(0, 1) + name.substring(1));
    }

    private List<String> readStringFromFile(String input) {
        List<String> result = new ArrayList<>();
        InputStream in = CodeGen.class.getClassLoader().getResourceAsStream(input);
        boolean isFile = in == null;

        try (BufferedReader br = new BufferedReader(isFile ? new FileReader(new File(input))
                : new InputStreamReader(in, StandardCharsets.UTF_8))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                result.add(line);
                sb.append('\n');
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Node getRoot() {
        return root;
    }

    private boolean isSimpleType(String classname) {
        if (isSimpleClass(classname)) {
            return true;
        }
        try {
            Class<?> className = Class.forName(classname);
            return isSimpleType(className);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    private boolean isSimpleClass(String className) {
        String[] names = new String[] { "boolean", "long", "byte", "char", "double", "float", "int", "short", "void",
                "boolean[]", "long[]", "byte[]", "char[]", "double[]", "float[]", "int[]", "short[]" };
        return Arrays.asList(names).contains(className);
    }

    /**
     * Prüft, ob es sich um einen direkt zuweisbaren Wert handelt
     */
    private boolean isSimpleType(Class<?> field) {
        return isWrapperType(field) || isPrimitiveType(field) || isMath(field) || isString(field);
    }

    private boolean isString(Class<?> type) {
        return type.getSimpleName().equals("String");
    }

    private boolean isMath(Class<?> type) {
        return type.getName().contains("java.math");
    }

    private boolean isPrimitiveType(Class<?> type) {
        return type.isArray() || type.isPrimitive() || type.isAssignableFrom(java.lang.String.class) || type
                .isAssignableFrom(javax.xml.datatype.XMLGregorianCalendar.class) || type.isAssignableFrom(
                        java.util.GregorianCalendar.class);
    }

    /**
     * Handelt es sich um einen Wrapper- Typ?
     */
    private boolean isWrapperType(Class<?> field) {
        String typname = field.getName();
        String[] names = new String[] { "java.lang.Boolean", "java.lang.Long", "java.lang.Byte", "java.lang.Character",
                "java.lang.Double", "java.lang.Float", "java.lang.Integer", "java.lang.Short", "java.lang.Void" };
        return Arrays.asList(names).contains(typname);
    }

}
