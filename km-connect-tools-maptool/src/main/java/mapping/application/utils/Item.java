package mapping.application.utils;

import java.io.Serializable;

/**
 * Ein Item spiegelt ein Java-Objekt wieder.
 * 
 * @author Erdmanns
 *
 */
public class Item implements Serializable {
    private static final long serialVersionUID = 4829660681521583288L;

    private String typeName;

    private String attributeName;

    private String mappedTo = null;

    private String converterName = null;

    private boolean isList;

    private boolean isComplex;

    private boolean isRequired;

    private boolean isEnum;

    private boolean isChoice;

    public Item(String typeName, String attributeName, boolean isList, boolean isComplex, boolean isRequired) {
        this.typeName = typeName;
        this.attributeName = attributeName;
        this.isList = isList;
        this.isComplex = isComplex;
        this.isRequired = isRequired;
    }

    public Item(Item copy) {
        this.typeName = copy.getTypeName();
        this.attributeName = copy.getAttributeName();
        // mit Vorsicht zu genießen, bei Template-Objekten und anschließendem
        // Aufruf von treeViewHelper.mapTrees(...) würde das zu einem falschen
        // Mapping führen
        this.mappedTo = copy.getMappedTo();
        this.isList = copy.isList;
        this.isComplex = copy.isComplex;
        this.isRequired = copy.isRequired;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public boolean isList() {
        return isList;
    }

    public void setList(boolean isList) {
        this.isList = isList;
    }

    public boolean isComplex() {
        return isComplex;
    }

    public void setComplex(boolean isComplex) {
        this.isComplex = isComplex;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean isRequired) {
        this.isRequired = isRequired;
    }

    public boolean isChoice() {
        return isChoice;
    }

    public void setChoice(boolean choice) {
        isChoice = choice;
    }

    public String getMappedTo() {
        return mappedTo;
    }

    public void setMappedTo(String mappedTo) {
        this.mappedTo = mappedTo;
    }

    public boolean isEnum() {
        return isEnum;
    }

    public void setEnum(boolean isEnum) {
        this.isEnum = isEnum;
    }

    public String getConverterName() {
        return converterName;
    }

    public void setConverterName(String converterName) {
        this.converterName = converterName;
    }

}
