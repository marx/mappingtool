package mapping.application.utils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javassist.Modifier;

/**
 * Dieser Baum repräsentiert den Code. Jeder Knoten repräsentiert eine
 * createXXX- Methode und eine Zeile Code im Elternknoten
 * 
 * @author Erdmanns
 *
 */
public class Node {

    private int counter = 0;

    private List<Node> children;

    private boolean isSimple;

    private boolean isRoot;

    private boolean isListType;

    private String converter;

    private String source;

    private String target;

    private String code;

    private String nameInGroovy;

    private String sourcePath;

    private String createName;
    private String createParam;

    private String targetName;

    private String targetType;

    private String sourceName;

    private String getterCode;

    private Node parent;

    private boolean isEnum;

    private boolean directlyMapped;

    public static boolean stringToDate = false;

    private static Map<String, String> DEFAULTS = new HashMap<>();

    private static Map<String, String> converterMap = new HashMap<>();

    private static Map<String, List<String>> createdGetMethodsMap = new HashMap<>();

    private static final String STRING_TO_DATE = "private javax.xml.datatype.XMLGregorianCalendar stringToXMLGregorianCalendar(String dateAsString) {\n"
            + "        java.time.LocalDate date = java.time.LocalDate.parse(dateAsString,\n"
            + "                java.time.format.DateTimeFormatter.ofPattern(\"dd.MM.yyyy\"));\n"
            + "        java.util.GregorianCalendar gcal = java.util.GregorianCalendar\n"
            + "                .from(date.atStartOfDay(java.time.ZoneId.systemDefault()));\n"
            + "        javax.xml.datatype.XMLGregorianCalendar xcal;\n" + "        try {\n"
            + "            xcal = javax.xml.datatype.DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);\n"
            + "            xcal.setTimezone(javax.xml.datatype.DatatypeConstants.FIELD_UNDEFINED);\n"
            + "            return xcal;\n" + "        } catch (javax.xml.datatype.DatatypeConfigurationException e) {\n"
            + "            throw new IllegalArgumentException(e);\n" + "        }\n" + "    }";

    public static void init() {
        createdGetMethodsMap.clear();
    }

    static {
        DEFAULTS.put("boolean", "false");
        DEFAULTS.put("int", "0");
        converterMap.put("java.lang.String->Enum", "{0}.fromValue(");
        converterMap.put("java.lang.String->javax.xml.datatype.XMLGregorianCalendar", "stringToXMLGregorianCalendar(");
        converterMap.put("java.lang.String->java.lang.Boolean", "java.lang.Boolean.valueOf(");
        converterMap.put("java.lang.String->boolean", "java.lang.Boolean.valueOf(");
        converterMap.put("java.lang.String->java.math.BigInteger", "new java.math.BigInteger(");
        converterMap.put("java.lang.String->java.math.BigDecimal", "new java.math.BigDecimal(");
    }

    public String getGetterCode() {
        return getterCode;
    }

    public String getTargetName() {
        return targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getCreateName() {
        if (isListType) {
            return "create" + simpleNameFromSource(targetType);
        } else 
        if (!isSimple) {
            if (targetType == null || targetName == null) {
                return "create" + simpleNameFromSource(target) + "From" + simpleNameFromSource(source);
            }
            return "create" + simpleNameFromSource(targetType) + "From" + firstLetterUppercase(targetName);
        }
        return null;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public boolean isRoot() {
        return isRoot;
    }

    public void setRoot(boolean isRoot) {
        this.isRoot = isRoot;
    }

    public String generateCode() {
        String code = "";
        boolean targetIsList = false;
        boolean sourceIsList = false;
        if (!isSimple) {
            if (source.startsWith("List")) {
                source = source.substring(5, source.length() - 1);
                sourceIsList = directlyMapped;
            }
            if (target.startsWith("List")) {
                target = target.substring(5, target.length() - 1);
                targetIsList = true;
            }

            if (source.startsWith("Complex")) {
                source = source.substring(8, source.length() - 1);
            }
            if (target.startsWith("Complex")) {
                target = target.substring(8, target.length() - 1);
            }

            createName = getCreateName();

            if (createName != null) {
                if (CodeGen.isMethodContained(createName, source)) {
                    return code;
                } else {
                    CodeGen.addMethod(createName, source);
                }
            }

            // List -> List
            if (isListType && sourceIsList) {

                String visibility = "private ";
                code += visibility + "java.util.List<" + target + "> create" + target.substring(target.lastIndexOf('.')
                        + 1) + "enFrom" + simpleNameFromSource(source) + "(java.util.List<" + source + "> source){\n";
                code += "if(source == null) return new java.util.ArrayList<>();\n";
                code += "java.util.List<" + target + "> target = new java.util.ArrayList<>();\n";
                code += "for(" + source + " elem : source){\n";
                code += "target.add(create" + target.substring(target.lastIndexOf('.') + 1) + "(elem));\n";
                code += "}\n";
                code += "return target; \n }";

            } // Simple Type -> List
            else if (targetIsList) {

                String visibility = "private ";
                code += visibility + "java.util.List<" + target + "> create"
                        + target.substring(target.lastIndexOf('.') + 1) + "enFrom" + simpleNameFromSource(source) + "("
                        + source + " source){\n";
                code += "if(source == null) return new java.util.ArrayList<>();\n";
                code += "java.util.List<" + target + "> target = new java.util.ArrayList<>();\n";
                code += "target.add(create" + target.substring(target.lastIndexOf('.') + 1) + "(source));\n";
                code += "return target; \n }";

            }
            boolean isAbstract = false;
            Class<?> forName = null;
            try {
                forName = Class.forName(target);
            } catch (ClassNotFoundException e) {
                // ignore e. g. inner classes

            }
            if (forName != null && Modifier.isAbstract(forName.getModifiers())) {
                isAbstract = true;
                if (isInBlackList(forName)) {
                    isAbstract = false;
                    String packageName = forName.getName().substring(0, forName.getName().lastIndexOf("."));
                    target = packageName + "." + CodeGen.getConcreteNameFromAbstractName(forName.getSimpleName());
                }
            }

            if (isAbstract) {

                for (Node child : getChildren()) {
                    String visibility = isRoot ? "public " : "private ";
                    code += visibility + target + " " + createName + "(" + source + " source){\n";
                    code += "if(source == null) return null;\n";
                    code += "return " + child.getCreateName() + "(source);\n}";
                }
            } else {
                String visibility = isRoot ? "public " : "private ";
                code += visibility + target + " " + createName + "(" + source + " source){\n";
                code += "if(source == null) return null;\n";
                code += target + " target = new " + target + "();\n";
                for (Node node : getChildren()) {
                    code += node.getCode();
                }
                code += "return target;\n}";
            }

        }
        return code;
    }

    private boolean isInBlackList(Class<?> forName) {
        return CodeGen.isInBlacklist(forName.getSimpleName());
    }

    private String simpleNameFromSource(String source2) {
        if (source2 == null) {
            return source2;
        }
        if (source2.startsWith("List")) {
            source2 = source2.substring(5, source2.length() - 1);
        }
        source2 = source2.substring(source2.lastIndexOf('.') + 1);
        return source2;
    }

    public String getCode() {
        if (!isListType) {
            this.code = "target.set" + firstLetterUppercase(targetName) + "(";
        } else {
            this.code = "target.get" + firstLetterUppercase(targetName) + "().addAll(";
        }

        String getterCode = getCodeForGetters();
        if (!isSimple) {
            if (isListType) {
                String createName = "create" + simpleNameFromSource(targetType) + "enFrom"
                        + firstLetterUppercase(simpleNameFromSource(source));
                this.code += createName + "(";
            } else {
                this.code += getCreateName() + "(";
            }
            this.code += getterCode;
            this.code += ")";
        } else {
            this.code += getterCode;
        }
        this.code += ");";
        return code;
    }

    private String getCodeForGetters() {
        String path = getSourcePath();
        String parentPaht = parent.getSourcePath();
        String sourceType = null;
        if (path.contains("->")) {
            sourceType = getSourceFromPath(path);
        }

        if (path.equals(parentPaht)) {
            return "source";
        } else if (path.contains(parentPaht)) {
            String getterPath = path.substring(parentPaht.length() + 2);
            String[] elements = getterPath.split("->");
            String typeOfTarget = elements[elements.length - 1].split("/")[1];
            if (elements.length > 1) {
                String currentConverter = "";
                if (this.converter != null) {
                    currentConverter = this.converter + "(";
                }
                createGetterCode(elements, typeOfTarget);
                String getterCode = createGetterName(elements[0].split("/")[0]) + "(source.get"
                        + firstLetterUppercase(elements[0].split("/")[0]) + "())";
                return currentConverter.isEmpty() ? getterCode : currentConverter + getterCode + ")";
            } else {

                // einfacher Wert
                String convertKey = null;
                String currentConverter = "";
                if (sourceType != null && !sourceType.startsWith("List") && !sourceType.equals(targetType)) {
                    convertKey = sourceType + "->" + (isEnum() ? "Enum" : targetType);
                    currentConverter = converterMap.get(convertKey);
                    if (currentConverter != null && currentConverter.contains("{")) {
                        currentConverter = MessageFormat.format(currentConverter, targetType);
                    } else if (currentConverter != null && currentConverter.contains("stringTo") && !stringToDate) {
                        stringToDate = true;
                        if (getterCode == null) {
                            getterCode = STRING_TO_DATE;
                        } else {
                            getterCode += STRING_TO_DATE;
                        }
                    }

                }
                // wenn der "Custom Converter" (vom User definiert) aktiv ist,
                // erhält er immer Vorrang
                if (this.converter != null) {
                    currentConverter = this.converter + "(";
                }
                String getter = "source." + (getterPath.contains("oolean") ? "is" : "get")
                        + firstLetterUppercase(getterPath.split("/")[0]) + "()";
                return currentConverter == null || currentConverter.isEmpty() ? getter : currentConverter + getter + ")";
            }
        } else {
            return null;
        }
    }

    public void adjustSource(Node node) {
        if (node.isSimple) {
            return;
        }
        String parentPath = node.getSourcePath();
        for (Node child : node.getChildren()) {
            adjustSource(child);
            String childPath = child.getSourcePath();
            if (!childPath.contains(parentPath)) {
                parentPath = findCommonPath(childPath, parentPath);
                node.setSourcePath(parentPath);
                node.setSource(getSourceFromPath(parentPath));
                isListType = false;
            }
        }
    }


    private String getSourceFromPath(String parentPath) {
        String[] strings = parentPath.split("->");
        String nameType = strings[strings.length - 1];
        String sourceType = nameType.split("/")[1];
        // beim Anpasen darf es keine Liste geben!
        return removeList(sourceType);
    }

    private String removeList(String sourceType) {
        if (sourceType.contains("List")) {
            sourceType = sourceType.substring(5, sourceType.length() - 1);
        }
        return sourceType;
    }

    private String findCommonPath(String childPath, String parentPath) {
        while (!childPath.contains(parentPath)) {
            parentPath = reducePath(parentPath);
        }
        return parentPath;
    }

    private String reducePath(String parentPath) {
        return combine(parentPath.split("->"));
    }

    private String combine(String[] parts) {
        String result = "";
        for (int i = 0; i < parts.length - 1; i++) {
            result += (i == 0) ? parts[i] : "->" + parts[i];
        }
        return result;
    }

    private String createGetterName(String getter) {
        return "get" + firstLetterUppercase(targetName) + "From" + firstLetterUppercase(getter);
    }

    private void createGetterCode(String[] forGetters, String targetType) {
        String sourceCode = "";
        for (int i = 0; i < forGetters.length - 1; i++) {
            String source = forGetters[i].split("/")[1];
            boolean listType = targetType.startsWith("List");
            String target = removeList(targetType);
            if (listType) {
                target = "java.util.List<" + target + ">";
            }
            String createdGetterName = createGetterName(forGetters[i].split("/")[0]);
            if (createdGetMethodsMap.get(createdGetterName) != null
                    && createdGetMethodsMap.get(createdGetterName).contains(source)) {
                continue;
            } else {
                if (createdGetMethodsMap.get(createdGetterName) == null) {
                    List<String> list = new ArrayList<>();
                    list.add(source);
                    createdGetMethodsMap.put(createdGetterName, list);
                } else {
                    List<String> sources = createdGetMethodsMap.get(createdGetterName);
                    sources.add(source);
                }
            }
            sourceCode += "private " + target + " " + createdGetterName + "(" + source
                    + " source){\n";
            sourceCode += "if(source == null) return " + getDefault(target) + ";";
            if (i + 1 < forGetters.length - 1) {
                String nextSourceType = forGetters[i + 1].split("/")[1];
                String nextGetterName = createGetterName(forGetters[i + 1].split("/")[0]);
                sourceCode += "return " + nextGetterName + "(source."
                        + (nextSourceType.contains("oolean") ? "is" : "get")
                        + firstLetterUppercase(forGetters[i + 1].split("/")[0]) + "()\n" + ");\n";
            } else {
                String nextSourceType = forGetters[i + 1].split("/")[1];
                if (!nextSourceType.startsWith("List") && !nextSourceType.equals(targetType)) {
                    System.out.println("In Getter " + nextSourceType + " -> " + targetType);
                }
                String getter = "source." + (nextSourceType.contains("oolean") ? "is" : "get") + firstLetterUppercase(
                        forGetters[i + 1].split("/")[0]) + "()";
                sourceCode += "return " + getter + ";\n";
            }
            sourceCode += "}";
            if (sourceCode.contains("$")) {
                sourceCode = sourceCode.replace("$", ".");
            }
        }
        this.getterCode = sourceCode;
    }


    private String getDefault(String target2) {
        if (DEFAULTS.containsKey(target2)) {
            return DEFAULTS.get(target2);
        }
        return "null";
    }

    private String firstLetterUppercase(String name) {
        if (name == null) {
            return name;
        }
        return xmlForm(name.toUpperCase().substring(0, 1) + name.substring(1));
    }

    private String xmlForm(String name) {
        while (name.contains("_")) {
            int dashLoc = name.indexOf("_");
            name = name.substring(0, dashLoc) + name.substring(dashLoc + 1).toUpperCase().substring(0, 1)
                    + name.substring(dashLoc + 2);
        }
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public boolean isSimple() {
        return isSimple;
    }

    public void setSimple(boolean isSimple) {
        this.isSimple = isSimple;
    }

    public boolean isListType() {
        return isListType;
    }

    public void setListType(boolean isListType) {
        this.isListType = isListType;
    }

    public List<Node> getChildren() {
        if (children == null) {
            children = new ArrayList<>();
        }
        return children;
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    public boolean addChild(Node node) {
        return getChildren().add(node);
    }

    public String getCreateParam() {
        return createParam;
    }

    public void setCreateParam(String createParam) {
        this.createParam = createParam;
    }

    public String getNameInGroovy() {
        return nameInGroovy;
    }

    public void setNameInGroovy(String nameInGroovy) {
        this.nameInGroovy = nameInGroovy;
    }

    /**
     * Einen Baum auf die Standardausgabe printen
     * 
     * @param tree
     */
    public void printTree(Node tree) {
        printTree(tree, true);
    }

    /**
     * Einen Baum auf die Standardausgabe printen
     * 
     * @param tree
     * @param isRoot
     */
    private void printTree(Node tree, boolean isRoot) {
        if (isRoot) {
            counter = 0;
        }
        for (int i = 0; i < counter; i++) {
            System.out.print("-");
        }
        System.out
                .println(tree.getTarget() + " FROM PATH " + tree.getSourcePath() + " xmlName: " + tree.getTargetName()
                        + " pojoname: " + tree.getSourceName());
        counter++;
        for (Node son : tree.getChildren()) {
            printTree(son, false);
        }
        counter--;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public void setGetterCode(String getterCode) {
        this.getterCode = getterCode;
    }

    public boolean isEnum() {
        return isEnum;
    }

    public void setEnum(boolean isEnum) {
        this.isEnum = isEnum;
    }

    public String getConverter() {
        return converter;
    }

    public void setConverter(String converter) {
        this.converter = converter;
    }

    public boolean isDirectlyMapped() {
        return directlyMapped;
    }

    public void setDirectlyMapped(boolean directlyMapped) {
        this.directlyMapped = directlyMapped;
    }

}
