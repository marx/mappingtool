package mapping.application.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Converter {

    private Map<String, String> converterMap = new HashMap<>();

    public Converter loadConverterFromConfig(List<String> elements) {

        boolean readConverter = false;
        for (String line : elements) {

            if (line.isEmpty()) {
                break;
            }

            if (line.contains("CONVERTERS")) {
                readConverter = true;
                continue;
            }

            if (readConverter) {
                String converterName = line.split("@")[0];
                String converterCode = line.split("@")[1];
                converterMap.put(converterName, converterCode);
            }
        }
        return this;
    }

    public String getConverters() {
        String result = "";
        Set<String> keySet = converterMap.keySet();
        for (String key : keySet) {
            result += "\n" + key + "@" + converterMap.get(key);
        }
        return result;
    }

    public String getCodeByName(String name) {
        return converterMap.get(name);
    }

    public void putCodeByName(String name, String code) {
        converterMap.put(name, code);
    }

    public boolean hasConversions() {
        return !this.converterMap.isEmpty();
    }

}
