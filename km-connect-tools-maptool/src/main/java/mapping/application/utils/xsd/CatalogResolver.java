package mapping.application.utils.xsd;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.xerces.util.XMLCatalogResolver;
import org.w3c.dom.ls.LSInput;


public class CatalogResolver extends XMLCatalogResolver {

    private static final String PREFIX = "xsd/xbau/v22/";

    @Override
    public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {

        LSInputImpl input = new LSInputImpl();
        InputStream stream = null;
        if (systemId.startsWith("http")) {
            stream = getClass().getClassLoader().getResourceAsStream(PREFIX + "extern/" + systemId
                    .substring("http://".length()));
        } else if (systemId.startsWith("https")) {
            stream = getClass().getClassLoader().getResourceAsStream(PREFIX + "extern/" + systemId
                    .substring("https://".length()));
        } else {
            stream = getClass().getClassLoader().getResourceAsStream(PREFIX + systemId);
        }

        input.setPublicId(publicId);
        input.setSystemId(systemId);
        input.setBaseURI(baseURI);
        input.setCharacterStream(new InputStreamReader(stream));
        return input;
    }

}
