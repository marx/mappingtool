package mapping.application.utils.xsd;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.poi.util.SystemOutLogger;
import org.apache.xerces.impl.xs.XSComplexTypeDecl;
import org.apache.xerces.xs.XSAttributeDeclaration;
import org.apache.xerces.xs.XSAttributeUse;
import org.apache.xerces.xs.XSComplexTypeDefinition;
import org.apache.xerces.xs.XSConstants;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSImplementation;
import org.apache.xerces.xs.XSLoader;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSModelGroup;
import org.apache.xerces.xs.XSNamedMap;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSParticle;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.apache.xerces.xs.XSTerm;
import org.apache.xerces.xs.XSTypeDefinition;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMError;
import org.w3c.dom.DOMErrorHandler;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;

import mapping.application.utils.Item;
import mapping.application.utils.TreeOfClasses;

public class XSDParser {

    private List<String> paths = new ArrayList<>();
    private final List<String> parseErrors = new ArrayList<>();
    private Map<String, QName> Qnames = new HashMap<>();
    private List<String> elements = new ArrayList<>();
    private XSModel model = null;

    public TreeOfClasses getClassAsTree(String pathToScheme, String elementName) throws ClassNotFoundException,
            InstantiationException, IllegalAccessException, FileNotFoundException {

        if (this.model == null) {
            initXsModel(pathToScheme);
        }
        return getClassAsTree(this.Qnames.get(elementName), this.model);

    }

    public List<String> getElementsDefinedInModel() {
        return this.elements;
    }

    public void initXsModel(String pathToScheme) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        this.Qnames.clear();
        this.elements.clear();
        DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
        XSImplementation impl = (XSImplementation) registry.getDOMImplementation("XS-Loader");

        XSLoader schemaLoader = impl.createXSLoader(null);
        File file = new File(pathToScheme);
        DOMErrorHandler errorHandler = new DOMErrorHandler() {

            @Override
            public boolean handleError(DOMError error) {
                parseErrors.add(error.getMessage());
                return false;
            }
        };
        DOMConfiguration config = schemaLoader.getConfig();
        // TODO : CatalogResolver sollte dynamisch geladen werden (dabei sollte
        // zunächst ein Download der externen Resourcen in ein temporäres
        // Verzeichnis geladen werden, dabei sollte es sich um das
        // Mapping-Tool-Verzeichnis handeln, Ordner sollte mit tmp beginnen,
        // dann extern, dann die jeweiligen Schemata, siehe XSD-Fetcher)
        // XMLCatalogResolver catalogResolver = new CatalogResolver();
        // config.setParameter("resource-resolver", catalogResolver);
        config.setParameter("error-handler", errorHandler);
        XSModel model = schemaLoader.loadURI(file.toURI().toString());
        if (!parseErrors.isEmpty()) {
            throw new IllegalStateException(getErrorStringFromList());
        }
        XSNamedMap map = model.getComponents(XSConstants.ELEMENT_DECLARATION);
        for (Object entry : map.keySet()) {
            if (entry instanceof QName) {
                QName name = (QName) entry;
                this.Qnames.put(name.getLocalPart(), name);
                this.elements.add(name.getLocalPart());
            }
        }
        this.model = model;
    }

    private String getErrorStringFromList() {
        String result = "";
        for (String error : parseErrors) {
            result += "\n" + error;
        }
        return result;
    }

    private TreeOfClasses getClassAsTree(QName name, XSModel model) {
        paths.clear();
        XSElementDeclaration element = model.getElementDeclaration(name.getLocalPart(), name.getNamespaceURI());
        return getClassAsTree(new XMLElement(element, 1, 1, false), "root");

    }

    private TreeOfClasses getClassAsTree(XMLElement xmlElement, String path) {

        TreeOfClasses tree = null;
        XSElementDeclaration element = xmlElement.getElement();
        XSTypeDefinition definition = element.getTypeDefinition();

        String currentPath = path + "->" + camelCaseWithoutDot(element.getName(), false);
        paths.add(currentPath);

        if (definition instanceof XSSimpleTypeDefinition) {

            XSSimpleTypeDefinition simple = (XSSimpleTypeDefinition) definition;
            String name = null;
            if (definition.getName() != null) {
                name = getType(definition.getName(), xmlElement.getMinoccurs() == 0);
                if (name.equals("unknown")) {
                    name = getType(simple.getPrimitiveType().toString(), xmlElement.getMinoccurs() == 0);
                }
                // unbekannte Typen als String behandeln
                if (name.equals("unknown")) {
                    name = "java.lang.String";
                }
            } else {
                name = getType(simple.getPrimitiveType().toString(), xmlElement.getMinoccurs() == 0);
            }
            Item item = new Item(name, camelCaseWithoutDot(element.getName(), false), xmlElement.getMaxoccurs() == -1
                    || xmlElement
                    .getMaxoccurs() > 1, false, xmlElement.getMinoccurs() == 1 && !xmlElement.isChoice());
            item.setChoice(xmlElement.isChoice());
            tree = new TreeOfClasses(item);
            addAttributes(definition, tree);
            return tree;

        } else if (definition instanceof XSComplexTypeDecl) {

            XSComplexTypeDecl complexDef = (XSComplexTypeDecl) definition;
            String typeName = complexDef.getTypeName();
            typeName = camelCaseWithoutDot(typeName, true);
            Item item = new Item(typeName, camelCaseWithoutDot(element.getName(), false), xmlElement
                    .getMaxoccurs() == -1 || xmlElement.getMaxoccurs() > 1, true, xmlElement.getMinoccurs() == 1 && !xmlElement.isChoice());
            item.setChoice(xmlElement.isChoice());
            tree = new TreeOfClasses(item);
            addAttributes(definition, tree);
            buildTreeFromComplexType(tree, complexDef, currentPath);
            while (complexDef.getBaseType() instanceof XSComplexTypeDecl) {
                XSComplexTypeDecl newComplexType = (XSComplexTypeDecl) complexDef.getBaseType();
                if (newComplexType.getName().contains("anyType")) {
                    break;
                }
                buildTreeFromComplexType(tree, newComplexType, currentPath);
                complexDef = newComplexType;
            }
            return tree;
        } else {
            throw new IllegalStateException("Unknown type " + definition.getClass().toString());
        }
    }

    private TreeOfClasses buildTreeFromComplexType(TreeOfClasses tree,
                                                   XSComplexTypeDecl complexDef, String currentPath) {
        XSParticle particle = complexDef.getParticle();
        if (particle != null) {
            XSTerm term = particle.getTerm();
            if (term instanceof XSModelGroup) {
                XSModelGroup xsModelGroup = (XSModelGroup) term;
                short compositor = xsModelGroup.getCompositor();
                List<XMLElement> elementDeclarations = getElementDeclarations(xsModelGroup, compositor == XSModelGroup.COMPOSITOR_CHOICE);
                for (XMLElement elem : elementDeclarations) {
                    if (pathContainsElement(elem, currentPath)) {
                        continue;
                    }
                    tree.addChild(getClassAsTree(elem, currentPath));
                }
                return tree;
            } else {
                throw new IllegalStateException("Unknown type: " + term.getClass().toString());
            }
        } else {
            return tree;
        }
    }

    private boolean pathContainsElement(XMLElement elem, String currentPath) {
        String pathToCheck = currentPath + "->" + camelCaseWithoutDot(elem.getElement().getName(), false);
        return paths.contains(pathToCheck);
    }

    private void addAttributes(XSTypeDefinition definition, TreeOfClasses tree) {
        // Attribute auslesen, Attribute werden direkt in den parent eingeklinkt
        if (definition instanceof XSComplexTypeDefinition) {
            XSObjectList xsAttrList = ((XSComplexTypeDefinition) definition).getAttributeUses();
            for (int i = 0; i < xsAttrList.getLength(); i++) {
                XSAttributeUse attrUse = ((XSAttributeUse) xsAttrList.item(i));
                XSAttributeDeclaration declaration = attrUse.getAttrDeclaration();
                if (declaration != null && declaration.getName() != null && declaration.getTypeDefinition() != null
                        && declaration.getTypeDefinition().getName() != null) {
                    XSSimpleTypeDefinition simpleTypeDefinition = declaration.getTypeDefinition();
                    String typeName = getType(simpleTypeDefinition.getName(), false);
                    if (typeName.equals("unknown")) {
                        typeName = getType(simpleTypeDefinition.getBaseType().getName(), false);
                        if (typeName.equals("unknown")) {
                            typeName = "java.lang.String";
                        }
                    }
                    TreeOfClasses attr = new TreeOfClasses(new Item(typeName,
                            declaration.getName(), false, false, false));
                    tree.addChild(attr);
                }
            }
        }

    }

    private String getType(String string, boolean isWrapper) {
        if (string.contains("string")) {
            return "java.lang.String";
        } else if (string.contains("date")) {
            return "javax.xml.datatype.XMLGregorianCalendar";
        } else if (string.contains("Boolean")) {
            return "java.lang.Boolean";
        } else if (string.contains("boolean")) {
            if (isWrapper) {
                return "java.lang.Boolean";
            }
            return "java.lang.boolean";
        } else if (string.contains("gYear")) {
            return "javax.xml.datatype.XMLGregorianCalendar";
        } else if (string.contains("String")) {
            return "java.lang.String";
        } else if (string.contains("anyURI")) {
            return "java.lang.String";
        } else if (string.contains("base64")) {
            return "java.lang.byte";
        } else if (string.contains("decimal")) {
            return "java.lang.BigDecimal";
        } else if (string.contains("int")) {
            if (isWrapper) {
                return "java.lang.Integer";
            }
            return "java.lang.int";
        } else if (string.contains("long")) {
            if (isWrapper) {
                return "java.lang.Long";
            }
            return "java.lang.long";
        }
        return "unknown";
    }

    private String camelCaseWithoutDot(String typeName, boolean firstLetterUppercase) {
        if (typeName.startsWith("#AnonType_")) {
            typeName = typeName.replace("#AnonType_", "");
        }
        while (typeName.lastIndexOf('.') != -1) {
            typeName = replaceFirstOccurence(typeName);
        }
        return firstLetterUppercase ? firstLetterUppercase(typeName) : firstLetterLowercase(typeName);
    }

    private String replaceFirstOccurence(String typeName) {
        return typeName.substring(0, typeName.lastIndexOf('.')) + firstLetterUppercase(typeName.substring(typeName
                .lastIndexOf('.') + 1));
    }

    private String firstLetterUppercase(String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private String firstLetterLowercase(String name) {
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }

    private List<XMLElement> getElementDeclarations(XSModelGroup modelgroup, boolean isChoice) {
        List<XMLElement> result = new ArrayList<>();

        XSObjectList xsol = modelgroup.getParticles();
        for (Object p : xsol) {
            XSParticle part = (XSParticle) p;
            int minoccurs = part.getMinOccurs();
            int maxoccurs = part.getMaxOccurs();
            XSTerm pterm = part.getTerm();
            if (pterm instanceof XSElementDeclaration) {
                XSElementDeclaration elem = (XSElementDeclaration) pterm;
                result.add(new XMLElement(elem, minoccurs, maxoccurs, isChoice));
            } else if (pterm instanceof XSModelGroup) {
                XSModelGroup mg = (XSModelGroup) pterm;
                result.addAll(getElementDeclarations((XSModelGroup) pterm, mg.getCompositor() == XSModelGroup.COMPOSITOR_CHOICE));
            } else {
                // TODO : instance org.apache.xerces.impl.xs.XSWildcardDecl, AZR
            }
        }
        return result;
    }

}
