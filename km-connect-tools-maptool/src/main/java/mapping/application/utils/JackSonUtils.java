package mapping.application.utils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javafx.scene.control.TreeItem;
import mapping.application.utils.javafx.MappableItem;

public class JackSonUtils {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public String convertTreeToJSon(TreeItem<MappableItem> tree) {
        return createJSonFromTree(tree);
    }

    private String createJSonFromTree(TreeItem<MappableItem> tree) {
        JsonNode jsonNode = createJSonNodeFromTree(tree);
        try {
            return OBJECT_MAPPER.writeValueAsString(jsonNode);
        } catch (JsonProcessingException e) {

        }
        return "Something unexpected happened! :(";
    }

    private JsonNode createJSonNodeFromTree(TreeItem<MappableItem> tree) {
        ObjectNode jsonNode = OBJECT_MAPPER.createObjectNode();
        Item item = tree.getValue().getItem();
        if (item.isList()) {
            ArrayNode arrayNode = OBJECT_MAPPER.createArrayNode();
            ObjectNode childNode = OBJECT_MAPPER.createObjectNode();
            if (item.isComplex()) {
                handleChildren(tree, childNode);
                arrayNode.add(childNode);
            } else {
                fillArrayNode(arrayNode, item.getTypeName(), item.getAttributeName());
            }
            return arrayNode;
        } else if (item.isComplex()) {
            handleChildren(tree, jsonNode);
        } else {
            fillPrimitiveField(jsonNode, item);
        }
        return jsonNode;
    }

    private void fillArrayNode(ArrayNode arrayNode, String typeName, String fieldName) {
        if (typeName.contains("String")) {
            arrayNode.add(fieldName);
        } else if (typeName.contains("long")) {
            arrayNode.add(2l);
        } else if (typeName.contains("Long")) {
            arrayNode.add(2l);
        } else if (typeName.contains("int")) {
            arrayNode.add(5);
        } else if (typeName.contains("Int")) {
            arrayNode.add(new Integer(5));
        } else if (typeName.contains("BigDecimal")) {
            arrayNode.add(BigDecimal.valueOf(2.5));
        } else if (typeName.contains("byte")) {
            arrayNode.add("hallo".getBytes());
        } else if (typeName.contains("date") || typeName.contains("Date") || typeName.contains("XML")) {
            LocalDate date = LocalDate.now();
            ZoneId zoneId = ZoneId.systemDefault();
            long epoch = date.atStartOfDay(zoneId).toEpochSecond();
            arrayNode.add(epoch);
        } else if (typeName.contains("boolean") || typeName.contains("Boolean")) {
            arrayNode.add(true);
        }
    }

    private void handleChildren(TreeItem<MappableItem> tree, ObjectNode jsonNode) {
        for (TreeItem<MappableItem> child : tree.getChildren()) {
            Item childItem = child.getValue().getItem();
            if (childItem.isComplex() || childItem.isList()) {
                jsonNode.set(childItem.getAttributeName(), createJSonNodeFromTree(child));
            } else {
                fillPrimitiveField(jsonNode, childItem);
            }
        }
    }

    private void fillPrimitiveField(ObjectNode jsonNode, Item item) {
        fillPrimitiveField(jsonNode, item.getAttributeName(), item.getTypeName());
    }

    private void fillPrimitiveField(ObjectNode jsonNode, String fieldName, String typeName) {
        if (typeName.contains("String")) {
            jsonNode.put(fieldName, fieldName);
        } else if (typeName.contains("long")) {
            jsonNode.put(fieldName, 2l);
        } else if (typeName.contains("Long")) {
            jsonNode.put(fieldName, 2l);
        } else if (typeName.contains("int")) {
            jsonNode.put(fieldName, 5);
        } else if (typeName.contains("Int")) {
            jsonNode.put(fieldName, new Integer(5));
        } else if (typeName.contains("BigDecimal")) {
            jsonNode.put(fieldName, BigDecimal.valueOf(2.5));
        } else if (typeName.contains("byte")) {
            jsonNode.put(fieldName, "hallo".getBytes());
        } else if (typeName.contains("date") || typeName.contains("Date") || typeName.contains("XML")) {
            LocalDate date = LocalDate.now();
            ZoneId zoneId = ZoneId.systemDefault();
            long epoch = date.atStartOfDay(zoneId).toEpochSecond();
            jsonNode.put(fieldName, epoch);
        } else if (typeName.contains("boolean") || typeName.contains("Boolean")) {
            jsonNode.put(fieldName, true);
        }
    }

    public String convertTreeToJSonScheme(TreeItem<MappableItem> tree) {
        return createJSonSchemeFromTree(tree);
    }

    private String createJSonSchemeFromTree(TreeItem<MappableItem> tree) {
        JsonNode jsonNode = createJSonNodeFromTreeForScheme(tree);
        try {
            return OBJECT_MAPPER.writeValueAsString(jsonNode);
        } catch (JsonProcessingException e) {

        }
        return "Something unexpected happened! :(";
    }

    private JsonNode createJSonNodeFromTreeForScheme(TreeItem<MappableItem> tree) {
        return createSchemeNode(tree, true);
    }

    private ObjectNode createSchemeNode(TreeItem<MappableItem> tree, boolean start) {
        ObjectNode jsonNode = OBJECT_MAPPER.createObjectNode();
        Item item = tree.getValue().getItem();
        boolean required = start || isItemRequired(tree.getValue());
        if (start) {
            jsonNode.put("$schema", "http://json-schema.org/draft-07/schema");
        }
        setType(jsonNode, item, required);
        if (item.isList()) {
            ObjectNode items = OBJECT_MAPPER.createObjectNode();
            if (item.isComplex()) {
                items.put("type", "object");
                setPropertiesForComplexNode(tree, items);
            } else {
                setSimpleType(item.getTypeName(), required, items);
            }
            jsonNode.set("items", items);
        } else if (item.isComplex()) {
            setPropertiesForComplexNode(tree, jsonNode);
        }
        return jsonNode;
    }

    private void setSimpleType(String typeName, boolean required, ObjectNode items) {
        if (!required) {
            ArrayNode arrayNode = OBJECT_MAPPER.createArrayNode();
            arrayNode.add(returnJsonType(typeName));
            arrayNode.add("null");
            items.set("type", arrayNode);
        } else {
            ObjectNode objectNode = OBJECT_MAPPER.createObjectNode();
            items.put("type", returnJsonType(typeName));
        }
    }

    private boolean isItemRequired(MappableItem value) {
        if (value.getMappedTreeview() != null && value.getMappedTreeview().getValue() != null && value.getMappedTreeview().getValue().getItem() != null) {
            return value.getMappedTreeview().getValue().getItem().isRequired();
        }
        return false;
    }

    private void setType(ObjectNode jsonNode, Item item, boolean required) {
        String typeName = item.getTypeName();

        if (item.isList()) {
            jsonNode.put("type", "array");
        } else if (item.isComplex()) {
            jsonNode.put("type", "object");
        } else {
            // simple data-types
            setSimpleType(typeName, required, jsonNode);
        }
    }

    private String returnJsonType(String typeName) {
        if (typeName.contains("String")) {
            return "string";
        } else if (typeName.contains("long")) {
            return "number";
        } else if (typeName.contains("Long")) {
            return "number";
        } else if (typeName.contains("int")) {
            return "number";
        } else if (typeName.contains("Int")) {
            return "number";
        } else if (typeName.contains("BigDecimal")) {
            return "number";
        } else if (typeName.contains("byte")) {
            return "string";
        } else if (typeName.contains("date") || typeName.contains("Date") || typeName.contains("XML")) {
            return "number";
        } else if (typeName.contains("boolean") || typeName.contains("Boolean")) {
            return "boolean";
        }
        return "string";
    }

    private void setPropertiesForComplexNode(TreeItem<MappableItem> tree, ObjectNode jsonNode) {
        ObjectNode properties = OBJECT_MAPPER.createObjectNode();
        List<String> requiredTypes = new ArrayList<>();
        for (TreeItem<MappableItem> child : tree.getChildren()) {
            Item childItem = child.getValue().getItem();
            properties.set(childItem.getAttributeName(), createSchemeNode(child, false));
            if (isItemRequired(child.getValue())) {
                requiredTypes.add(child.getValue().getItem().getAttributeName());
            }
        }
        jsonNode.set("properties", properties);
        // keine zusätzlichen Attribute zulassen, damit nur Daten aus dem kanonischen Modell erlaubt sind
        jsonNode.put("additionalProperties", false);
        if (!requiredTypes.isEmpty()) {
            jsonNode.set("required", getArrayNodeFromList(requiredTypes));
        }
    }

    private ArrayNode getArrayNodeFromList(List<String> requiredTypes) {
        ArrayNode arrayNode = OBJECT_MAPPER.createArrayNode();
        for (String entry : requiredTypes) {
            arrayNode.add(entry);
        }
        return arrayNode;
    }

}
