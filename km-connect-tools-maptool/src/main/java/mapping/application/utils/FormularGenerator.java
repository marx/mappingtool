package mapping.application.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import de.kommone.webservice.seitenbauformular.Field;
import de.kommone.webservice.seitenbauformular.Fieldgroup;
import de.kommone.webservice.seitenbauformular.Row;
import de.kommone.webservice.seitenbauformular.Section;
import de.kommone.webservice.seitenbauformular.Seitenbauformular;

/**
 * Die Klasse generiert aus dem linken Teilbaum (im Progress-File / POJO- Baum)
 * ein JSon, das einem Seitenbau- Formular entspricht
 *
 * @author Erdmanns
 */
public class FormularGenerator {

    private int fieldsPerRow = 4;

    private Map<String, String> values = new HashMap<>();

    private Map<String, TreeOfClasses> controlsToTrees = new HashMap<>();

    /**
     * Die Methode bekommt die Liste der Zeilen aus der Progress-File übergeben
     *
     * @param lines
     * @return
     */
    public String generateSeitenbauJSon(List<String> lines) throws ClassNotFoundException {
        Seitenbauformular formular = generateSeitenbauFormular(lines);
        // einen JSon-String erzeugen und zurückgeben
        return generateJson(formular);
    }

    public Seitenbauformular generateSeitenbauFormular(List<String> lines) throws ClassNotFoundException {
        values.clear();
        controlsToTrees.clear();

        // den Baum laden
        ClassTreeLoader treeLoader = new ClassTreeLoader();
        TreeOfClasses leftTree = treeLoader.loadTreeFromConfig(lines);

        // Die erste "richtige" Zeile mit dem root-Element auslesen und Klassennamen extrahieren
        String msgline = lines.get(0).contains("FULL-TREE") ? lines.get(1) : lines.get(0);
        Class<?> classForName = Class.forName(msgline.split("=")[1].split("/")[1]);
        TreeOfClasses rightTree = treeLoader.loadTreeFromClass(classForName);


        TreeUtils.copyRequiredFields(leftTree, rightTree, true);
        // das Seitenbauformular aus dem Baum erzeugen
        return buildFormularFromTree(leftTree);

    }


    /**
     * Die Methode erzeugt aus allen Elementen auf der obersten Ebene sections.
     * <p/>
     * Auf einer Ebene darunter werden Fieldgroups erzeugt
     * <p/>
     * Alle darauf folgenden Elemente werden in Fields kategorisiert (hier ist
     * theoretisch eine Einordung in Rows möglich)
     *
     * @param pojoBaum
     * @return
     */
    private Seitenbauformular buildFormularFromTree(TreeOfClasses pojoBaum) {
        Seitenbauformular formular = new Seitenbauformular();

        // Erzeugung der Sections
        formular.getSections().addAll(createSectionsFromTopLevelElements(pojoBaum));

        return formular;
    }

    private List<Section> createSectionsFromTopLevelElements(TreeOfClasses pojoBaum) {
        List<Section> sections = new ArrayList<>();
        List<TreeOfClasses> simpleItems = new ArrayList<>();
        for (TreeOfClasses child : pojoBaum.getChildren()) {
            if (child.getItem().isComplex()) {
                sections.add(createSectionFromComplexItem(child));
            } else {
                simpleItems.add(child);
            }
        }
        sections.add(createSectionFromSimpleItems(simpleItems, pojoBaum));
        return sections;
    }

    /**
     * Eine Sections, eine FieldGroup, evtl. wäre hier die Einteilung in Rows
     * möglich
     *
     * @param simpleItems
     * @param parentAttributeName
     * @return
     */
    private Section createSectionFromSimpleItems(List<TreeOfClasses> simpleItems, TreeOfClasses pojoBaum) {
        Section result = new Section();
        Fieldgroup fg = createFieldGroupFromSimpleItems(simpleItems, pojoBaum);
        result.getFieldGroups().add(fg);
        return result;
    }

    private Fieldgroup createFieldGroupFromSimpleItems(List<TreeOfClasses> simpleItems, TreeOfClasses pojoBaum) {
        Fieldgroup fg = new Fieldgroup();
        String parentAttributeName = pojoBaum.getItem().getAttributeName();
        fg.setId(getUniqueIdWithPrefix("", "", firstLetterUpperCase(parentAttributeName), "fg"));
        controlsToTrees.put(fg.getId(),pojoBaum);
        fg.setTitle("Allgemeine Daten zu " + firstLetterUpperCase(splitCamelCase(parentAttributeName)));
        createRowsFromSimpleItems(simpleItems, fg, 0, parentAttributeName);
        return fg;
    }

    private String firstLetterUpperCase(String name) {
        if (name == null) {
            return null;
        }
        return name.toUpperCase().substring(0, 1) + name.substring(1);
    }

    private void createRowsFromSimpleItems(List<TreeOfClasses> simpleItems, Fieldgroup fg, int counter,
                                           String parentAttributeName) {
        List<TreeOfClasses> tempItems = new ArrayList<>();
        for (TreeOfClasses item : simpleItems) {
            counter++;
            tempItems.add(item);
            if (counter % fieldsPerRow == 0 || counter == simpleItems.size()) {
                Row row = createRowFromSimpleItem(tempItems, parentAttributeName);
                fg.getRows().add(row);
                tempItems.clear();
            }

        }
    }

    private Row createRowFromSimpleItem(List<TreeOfClasses> items, String parentAttributeName) {
        Row row = new Row();
        for (TreeOfClasses item : items) {
            Field field = new Field();
            field.setId(getUniqueIdWithPrefix("", firstLetterUpperCase(parentAttributeName), firstLetterUpperCase(item.getItem().getAttributeName()), "f"));
            if (item.getItem().isRequired()) {
                field.setRequired(true);
            }
            field.setLabel(firstLetterUpperCase(splitCamelCase(item.getItem().getAttributeName())));
            field.setType(mapToSeitebau(item.getItem().getTypeName()));
            row.getFields().add(field);
            this.controlsToTrees.put(field.getId(), item);
        }
        return row;
    }

    private String mapToSeitebau(String typeName) {
        switch (typeName) {
            case "java.lang.String":
                return "STRING";
            case "java.lang.Boolean":
                return "CHECKBOX";
            case "boolean":
                return "CHECKBOX";
            case "javax.xml.datatype.XMLGregorianCalendar":
                return "DATE";
            case "java.math.BigInteger":
                return "STRING";
            default:
                // wenn als Default etwas Anderes übergeben wird, dann kann es nicht
                // mehr eingelesen werden
                return "STRING";
        }
    }

    private String splitCamelCase(String s) {
        return s.replaceAll(String.format("%s|%s|%s", "(?<=[A-Z])(?=[A-Z][a-z])", "(?<=[^A-Z])(?=[A-Z])",
                "(?<=[A-Za-z])(?=[^A-Za-z])"), " ");
    }

    private Section createSectionFromComplexItem(TreeOfClasses pojoBaum) {
        Section section = new Section();
        section.setTitle(firstLetterUpperCase(splitCamelCase(pojoBaum.getItem().getAttributeName())));
        List<Fieldgroup> fgList = new ArrayList<>();
        List<TreeOfClasses> simpleItems = new ArrayList<>();
        for (TreeOfClasses child : pojoBaum.getChildren()) {
            if (child.getItem().isComplex()) {
                fgList.add(createFieldGroupFromComplexItem(child, pojoBaum));
            } else {
                simpleItems.add(child);
            }
        }
        section.getFieldGroups().addAll(fgList);
        section.getFieldGroups().add(createFieldGroupFromSimpleItems(simpleItems, pojoBaum));
        return section;
    }

    private Fieldgroup createFieldGroupFromComplexItem(TreeOfClasses pojoBaum, TreeOfClasses parent) {
        Fieldgroup fg = new Fieldgroup();
        boolean isListType = pojoBaum.getItem().isList();
        if (isListType) {
            fg.setMultiple(isListType);
            fg.setLayout("MULTIPLE");
            String name = firstLetterUpperCase(splitCamelCase(pojoBaum.getItem().getAttributeName()));
            fg.setAddRowButton("Weitere " + name + " hinzufügen");
            fg.setDeleteRowButton(name + " entfernen");
        }
        fg.setId(getUniqueIdWithPrefix(firstLetterUpperCase(parent.getItem().getAttributeName()), "",
                firstLetterUpperCase(pojoBaum.getItem().getAttributeName()), isListType
                        ? "fgmulti"
                        : "fg"));
        controlsToTrees.put(fg.getId(),pojoBaum);
        fg.setTitle(firstLetterUpperCase(splitCamelCase(pojoBaum.getItem().getAttributeName())));
        List<TreeOfClasses> simpleItems = getSimpleItemsRecursively(pojoBaum);
        createRowsFromSimpleItems(simpleItems, fg, 0, pojoBaum.getItem().getAttributeName());
        return fg;
    }

    private String getUniqueIdWithPrefix(String parentParentAttributeName, String parentAttributeName, String attributeName, String prefix) {
        String name = prefix + parentAttributeName + attributeName;
        int counter = 2;
        String candidate = name;
        if (values.get(candidate) != null && parentParentAttributeName != null) {
            candidate = prefix + parentParentAttributeName + parentAttributeName + attributeName;
        }

        while (values.get(candidate) != null) {
            candidate = name + counter++;
        }
        values.put(candidate, candidate);
        return candidate;
    }

    private List<TreeOfClasses> getSimpleItemsRecursively(TreeOfClasses pojoBaum) {
        List<TreeOfClasses> simpleItems = new ArrayList<>();
        for (TreeOfClasses child : pojoBaum.getChildren()) {
            if (child.getItem().isComplex()) {
                simpleItems.addAll(getSimpleItemsRecursively(child));
            } else {
                simpleItems.add(child);
            }
        }
        return simpleItems;
    }

    private String generateJson(Seitenbauformular data) {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();

        String json;
        try {
            json = ow.writeValueAsString(data);
            return json;
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Map<String, TreeOfClasses> getFieldMapping() {
        return this.controlsToTrees;
    }

}
