package mapping.application.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroovyUtil {

    private String code = "";

    private List<String> groovyClasses = new ArrayList<>();

    private Map<String, String> typeMap = new HashMap<>();

    public GroovyUtil() {
        typeMap.put("XMLGregorianCalendar", "long");
        typeMap.put("GregorianCalendar", "long");
        typeMap.put("[B", "byte[]");
    }

    public String generateClasses(Node root) {
        for (Node child : root.getChildren()) {
            code += generateGroovyClasses(child);
        }
        return code;
    }

    private String generateGroovyClasses(Node root) {
        if (root.isSimple()) {
            return "";
        }
        // Bestimme den Klassennamen
        String className = getSimpleName(root.getTarget());
        if (groovyClasses.contains(className)) {
            return "";
        } else {
            groovyClasses.add(className);
        }

        String newCode = "";

        newCode += "\n class " + className + "{ \n";
        for (Node child : root.getChildren()) {
            if (child.isListType()) {
                newCode += "def " + child.getNameInGroovy() + " = []\n";
            } else {
                newCode += getSimpleName(child.getTarget()) + " " + child.getNameInGroovy() + "\n";
            }
        }
        newCode += "}";
        for (Node child : root.getChildren()) {
            newCode += generateGroovyClasses(child);
        }
        return newCode;
    }

    private String getSimpleName(String nameInGroovy) {
        if (nameInGroovy.lastIndexOf('.') != -1) {
            String simpleName = nameInGroovy.substring(nameInGroovy.lastIndexOf('.') + 1);
            return typeMap.containsKey(simpleName) ? typeMap.get(simpleName) : simpleName;
        }
        return typeMap.containsKey(nameInGroovy) ? typeMap.get(nameInGroovy) : nameInGroovy;
    }

}
