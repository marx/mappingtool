package mapping.application.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Rekursive Datenstruktur, die die Information über ein Java-Objekt enthält.
 * Dabei ist das Item das eigentliche Objekt, die Kinder sind die Kindobjekte
 * 
 * @author Erdmanns
 *
 */
public class TreeOfClasses {

    private Item item;

    private List<TreeOfClasses> children = new ArrayList<>();

    public TreeOfClasses() {
    }

    public TreeOfClasses(Item item) {
        this.item = item;
    }

    /**
     * Rekursiver Copy- Konstruktor
     * 
     * @param copy
     */
    public TreeOfClasses(TreeOfClasses copy) {
        this.item = new Item(copy.getItem());
        for (TreeOfClasses child : copy.getChildren()) {
            this.children.add(new TreeOfClasses(child));
        }
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public void addChild(TreeOfClasses child) {
        this.children.add(child);
    }

    public List<TreeOfClasses> getChildren() {
        return children;
    }

    public void setChildren(List<TreeOfClasses> children) {
        this.children = children;
    }

}
