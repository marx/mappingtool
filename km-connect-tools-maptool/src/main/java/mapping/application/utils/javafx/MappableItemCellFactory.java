package mapping.application.utils.javafx;

import java.util.Objects;

import javafx.scene.control.Button;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.util.Callback;

public class MappableItemCellFactory implements Callback<TreeView<MappableItem>, TreeCell<MappableItem>> {
    private static final DataFormat JAVA_FORMAT = new DataFormat("application/x-java-serialized-object");
    private static final String DROP_HINT_STYLE = "-fx-border-color: #eea82f; -fx-border-width: 0 0 2 0; -fx-padding: 3 3 1 3";
    private TreeCell<MappableItem> dropZone;
    private TreeItem<MappableItem> draggedItem;
    private Button callBackButton;
    private boolean isSource = false;

    public MappableItemCellFactory(boolean isSource, Button callBackButton) {
        this.isSource = isSource;
        this.callBackButton = callBackButton;
    }

    @Override
    public TreeCell<MappableItem> call(TreeView<MappableItem> treeView) {
        if (isSource) {
            return getTreeCellEditable(treeView);
        } else {
            return getTreeCellNonEditable(treeView);
        }
    }

    private TreeCell<MappableItem> getTreeCellEditable(TreeView<MappableItem> treeView) {
        TreeCell<MappableItem> cell = new TreeCell<MappableItem>() {

            @Override
            protected void updateItem(MappableItem item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty && item != null) {
                    ImageView iv1 = new TreeViewHelper().getGraphic(item.getItem(), item.isRoot());
                    setGraphic(iv1);
                    setText(item.toString());
                } else {
                    setText(null);
                    setGraphic(null);
                }
            }
        };

        // bei der Source nur das DragDetected-Event!
        cell.setOnDragDetected((MouseEvent event) -> dragDetected(event, cell, treeView));
        return cell;
    }

    private TreeCell<MappableItem> getTreeCellNonEditable(TreeView<MappableItem> treeView) {
        TreeCell<MappableItem> cell = new TreeCell<MappableItem>() {

            @Override
            protected void updateItem(MappableItem item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty && item != null) {
                    ImageView iv1 = new TreeViewHelper().getGraphic(item.getItem(), item.isRoot());
                    setGraphic(iv1);
                    setText(item.toString());
                } else {
                    setText(null);
                    setGraphic(null);
                }
            }
        };

        // bei Target DragOver+Drop!
        cell.setOnDragOver((DragEvent event) -> dragOver(event, cell, treeView));
        cell.setOnDragDropped((DragEvent event) -> drop(event, cell, treeView));
        cell.setOnDragDone((DragEvent event) -> clearDropLocation());
        return cell;
    }

    private void dragDetected(MouseEvent event, TreeCell<MappableItem> treeCell, TreeView<MappableItem> treeView) {
        draggedItem = treeCell.getTreeItem();

        // root can't be dragged
        if (draggedItem.getParent() == null)
            return;
        Dragboard db = treeCell.startDragAndDrop(TransferMode.MOVE);

        ClipboardContent content = new ClipboardContent();
        content.put(JAVA_FORMAT, copyTree(draggedItem, new TreeViewHelper()));
        db.setContent(content);
        db.setDragView(treeCell.snapshot(null, null));
        event.consume();

    }

    private void dragOver(DragEvent event, TreeCell<MappableItem> treeCell, TreeView<MappableItem> treeView) {
        if (!event.getDragboard().hasContent(JAVA_FORMAT))
            return;

        TreeItem<MappableItem> thisItem = getTreeItem(getTreeItemFromDragBoard(event));

        // can't drop on itself
        if (thisItem == null)
            return;

        // Treeview aufklappen (expand) und selektieren! Funktioniert noch nicht
        // richtig, ist bisschen schwierig mit der Scrollbar
        // TreeItem<MappableItem> currentItem = treeCell.getTreeItem();
        // currentItem.setExpanded(true);

        event.acceptTransferModes(TransferMode.MOVE);
        if (!Objects.equals(dropZone, treeCell)) {
            clearDropLocation();
            this.dropZone = treeCell;
            dropZone.setStyle(DROP_HINT_STYLE);
        }

    }

    private TreeItem<MappableItem> getTreeItem(MappableTree mappableTree) {
        TreeItem<MappableItem> item = new TreeItem<MappableItem>(mappableTree.getItem());
        for (MappableTree child : mappableTree.getChildren()) {
            item.getChildren().add(getTreeItem(child));
        }
        return item;
    }

    private TreeItem<MappableItem> getMappedTreeItem(MappableTree mappableTree, TreeViewHelper helper) {
        mappableTree.getItem().getItem().setMappedTo(null);
        mappableTree.getItem().setMapped(false);
        mappableTree.getItem().setPojo(true);
        mappableTree.getItem().getItem().setMappedTo(mappableTree.getMappedTo());
        helper.transformItem(mappableTree.getItem());
        TreeItem<MappableItem> item = new TreeItem<MappableItem>(mappableTree.getItem());
        for (MappableTree child : mappableTree.getChildren()) {
            item.getChildren().add(getMappedTreeItem(child, helper));
        }
        return item;
    }

    private MappableTree getTreeItemFromDragBoard(DragEvent event) {
        // Item aus Dragboard holen
        Object content = event.getDragboard().getContent(JAVA_FORMAT);
        if (!(content instanceof MappableTree)) {
            return null;
        }
        return ((MappableTree) content);
    }

    private void drop(DragEvent event, TreeCell<MappableItem> treeCell, TreeView<MappableItem> treeView) {
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (!db.hasContent(JAVA_FORMAT))
            return;

        TreeItem<MappableItem> thisItem = treeCell.getTreeItem();
        TreeItem<MappableItem> draggedItem = getMappedTreeItem(getTreeItemFromDragBoard(event), new TreeViewHelper());

        // root
        if (thisItem.getParent() == null) {
            thisItem.getChildren().add(0, draggedItem);
        } // einfügen, wo es hingezogen wird
        else {
            int indexInParent = thisItem.getParent().getChildren().indexOf(thisItem);
            thisItem.getParent().getChildren().add(indexInParent + 1, draggedItem);
            treeView.getSelectionModel().select(draggedItem);
        }

        clearDropLocation();
        event.setDropCompleted(success);
        callBackButton.fire();
    }

    private void clearDropLocation() {
        if (dropZone != null)
            dropZone.setStyle("");
    }

    private MappableTree copyTree(TreeItem<MappableItem> item, TreeViewHelper helper) {
        MappableTree tree = new MappableTree();
        tree.setItem(item.getValue());
        tree.setMappedTo(helper.getMappedTo(item));
        for (TreeItem<MappableItem> child : item.getChildren()) {
            tree.addChild(copyTree(child, helper));
        }
        return tree;
    }

}
