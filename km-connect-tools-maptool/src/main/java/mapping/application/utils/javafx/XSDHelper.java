package mapping.application.utils.javafx;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.control.TreeItem;
import mapping.application.utils.Item;

public class XSDHelper {

    /**
     * <ul>
     * <li>0 = Name</li>
     * <li>1 = Type-Prefix</li>
     * <li>2 = Type</li>
     * <li>3 = Min/Max- Occurs</li>
     * </ul>
     */
    private String element = "<xs:element name=\"{0}\" type=\"{1}:{2}\" {3}/>\n";

    private String minoccurs = "minOccurs=\"{0}\"";

    private String maxoccurs = "maxOccurs=\"{0}\"";

    private String dataStart = "<xs:complexType name=\"{0}\">\r\n" + "        <xs:complexContent>\r\n"
            + "            <xs:extension base=\"tns:data\">";

    private String dataEnd = "</xs:extension>\r\n" + "        </xs:complexContent>\r\n" + "    </xs:complexType>";

    private String sequenceStart = "<xs:sequence>";

    private String sequenceEnd = "</xs:sequence>";

    private String complexTypeStart = "<xs:complexType name=\"{0}\">";

    private String complexTypeEnd = "</xs:complexType>";

    private String schemaStart = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n"
            + "<xs:schema version=\"1.0\"\r\n" + "           targetNamespace=\"http://webservice.kmc.kommone.de/\"\r\n"
            + "           xmlns:tns=\"http://webservice.kmc.kommone.de/\"\r\n"
            + "           xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"\r\n" + ">";

    private String schemaEnd = "<xs:complexType name=\"data\" abstract=\"true\">\r\n" + "        <xs:sequence>\r\n"
            + "            <xs:element name=\"message\" type=\"xs:string\"\r\n"
            + "                        minOccurs=\"0\"/>\r\n"
            + "            <xs:element name=\"usecase\" type=\"xs:string\"\r\n"
            + "                        minOccurs=\"0\"/>\r\n" + "        </xs:sequence>\r\n"
            + "    </xs:complexType>\r\n" + "</xs:schema>";

    private List<String> writtenElements = new ArrayList<>();

    public String buildSchemeFromTree(TreeItem<MappableItem> tree, List<String> warnings) {
        writtenElements.clear();
        return buildScheme(tree, true, warnings);
    }

    private String buildScheme(TreeItem<MappableItem> tree, boolean isRoot, List<String> warnings) {
        String result = "";
        if (isRoot) {
            result += schemaStart;
        }

        result += writeItem(tree, isRoot, warnings);

        for (TreeItem<MappableItem> child : tree.getChildren()) {
            result += buildScheme(child, false, warnings);
        }

        if (isRoot) {
            result += schemaEnd;
        }
        return result;
    }

    private String writeItem(TreeItem<MappableItem> tree, boolean isRoot, List<String> warnings) {
        String result = "";
        Item item = tree.getValue().getItem();

        if (writtenElements.contains(item.getTypeName())) {
            return result;
        }

        if (item.isComplex()) {
            // Definition
            result += "\n" + MessageFormat.format((isRoot ? dataStart : complexTypeStart), tree.getValue()
                    .getSimpleTypeName());

            result += "\n" + sequenceStart;

            // Kindelemente
            for (TreeItem<MappableItem> child : tree.getChildren()) {
                result += writeChildItem(child, warnings);
            }

            result += sequenceEnd;

            result += "\n" + (isRoot ? dataEnd : complexTypeEnd);
        }
        writtenElements.add(item.getTypeName());
        return result;
    }

    private String writeChildItem(TreeItem<MappableItem> tree, List<String> warnings) {
        String result = "";
        Item item = tree.getValue().getItem();

        if (item.isComplex()) {
            // Definition
            result += MessageFormat.format(element, tree.getValue().getItem().getAttributeName(), "tns", tree
                    .getValue()
                    .getSimpleTypeName(), getMinoccursMaxoccurs(tree.getValue().getItem()));
        } else {
            result += MessageFormat.format(element, tree.getValue().getItem().getAttributeName(), "xs",
                    getSimpleTypeName(tree
                    .getValue().getItem().getTypeName(), warnings), getMinoccursMaxoccurs(tree.getValue().getItem()));
        }
        return result;
    }

    private String getSimpleTypeName(String typeName, List<String> warnings) {
        Map<String, String> map = new HashMap<>();
        map.put("java.lang.Boolean", "boolean");
        map.put("boolean", "boolean");
        map.put("java.lang.Long", "long");
        map.put("long", "long");
        map.put("java.lang.String", "string");
        map.put("java.math.BigInteger", "integer");
        map.put("java.lang.Integer", "int");
        map.put("int", "int");
        map.put("java.lang.Short", "short");
        map.put("short", "short");
        map.put("java.math.BigDecimal", "decimal");
        map.put("java.math.BigDecimal", "decimal");
        map.put("java.lang.Float", "float");
        map.put("float", "float");
        map.put("java.lang.Double", "double");
        map.put("double", "double");
        map.put("java.lang.Byte", "byte");
        map.put("byte", "byte");
        map.put("javax.xml.datatype.XMLGregorianCalendar", "dateTime");
        map.put("byte[]", "base64Binary");
        map.put("[B", "base64Binary");
        
        if (map.get(typeName) == null) {
            warnings.add("Typ " + typeName + " nicht bekannt [String wurde stattdessen gesetzt]!");
            return "string";
        }
        return map.get(typeName);
    }

    private String getMinoccursMaxoccurs(Item item) {
        if (item.isList()) {
            return MessageFormat.format(minoccurs, "0") + " " + MessageFormat.format(maxoccurs, "unbounded");
        } else if (!item.isComplex()) {
            // ist das Item ein Wrapper-Typ
            if (!isPrimitiveType(item.getTypeName())) {
                return MessageFormat.format(minoccurs, "0");
            }
        } else if (item.isComplex()) {
            return MessageFormat.format(minoccurs, "0");
        }
        return "";
    }

    private boolean isPrimitiveType(String typeName) {
        return !isWrapperType(typeName) && !belongsToOtherSimpleTypes(typeName);
    }

    private boolean belongsToOtherSimpleTypes(String typeName) {
        String[] names = new String[] { "java.lang.String", "javax.xml.datatype.XMLGregorianCalendar" };
        return Arrays.asList(names).contains(typeName);
    }

    /**
     * Handelt es sich um einen Wrapper- Typ?
     */
    private boolean isWrapperType(String typeName) {
        String[] names = new String[] { "java.lang.Boolean", "java.lang.Long", "java.lang.Byte", "java.lang.Character",
                "java.lang.Double", "java.lang.Float", "java.lang.Integer", "java.lang.Short", "java.lang.Void" };
        return Arrays.asList(names).contains(typeName);
    }
}
