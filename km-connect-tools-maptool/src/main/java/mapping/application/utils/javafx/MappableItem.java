package mapping.application.utils.javafx;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.TreeItem;
import mapping.application.utils.Item;

public class MappableItem implements Serializable {
    private static final long serialVersionUID = 8481423658196332797L;

    private boolean isPojo;

    private boolean isMapped;

    private Item item;

    private boolean missing;

    private boolean isChecked;

    private boolean isRoot;

    /**
     * Links -> Rechts
     */
    private transient TreeItem<MappableItem> mappedTreeview;

    /**
     * Rechts <- Links (hier gibt es mehrere Mappings)
     */
    private transient List<TreeItem<MappableItem>> mappedTreeviews = new ArrayList<>();

    public MappableItem(MappableItem other, boolean isPojo) {
        this.isPojo = isPojo;
        this.item = new Item(other.getItem());
    }

    public MappableItem(Item item, boolean isPojo, boolean isRoot) {
        this.item = item;
        this.isPojo = isPojo;
        this.isRoot = isRoot;
    }

    public boolean isMapped() {
        return isMapped;
    }

    public void setMapped(boolean isMapped) {
        this.isMapped = isMapped;
    }

    public TreeItem<MappableItem> getMappedTreeview() {
        return mappedTreeview;
    }

    public void setMappedTreeview(TreeItem<MappableItem> mappedTreeview) {
        this.mappedTreeview = mappedTreeview;
    }

    @Override
    public String toString() {
        String attribute = "";
        if (getItem().getAttributeName() == null || getItem().getAttributeName().isEmpty()) {
            // kein Attribut gesetzt
        } else {
            attribute = getItem().getAttributeName() + " : ";
        }
        return attribute + getSimpleType(item.getTypeName()) + isRequired()
                + hasBeenMapped();
    }

    private String hasBeenMapped() {
        if (isPojo && !isMapped) {
            return " [Not Mapped]";
        }
        if(this.item.isChoice()) {
            return " [One of]";
        }
        return "";
    }

    private String isRequired() {
        if (!isPojo && item.isRequired()) {
            return "*";
        }
        return "";
    }

    private String getSimpleType(String typeName) {
        int index = typeName.lastIndexOf('.');
        if (index == -1) {
            return typeName;
        } else {
            return typeName.substring(index + 1);
        }
    }

    public String getSimpleTypeName() {
        return getSimpleType(getItem().getTypeName());
    }

    public boolean isMissing() {
        return missing;
    }

    public void setMissing(boolean missing) {
        this.missing = missing;
    }

    public boolean isPojo() {
        return isPojo;
    }

    public void setPojo(boolean isPojo) {
        this.isPojo = isPojo;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public boolean addMappedTreeview(TreeItem<MappableItem> item) {
        return this.mappedTreeviews.add(item);
    }

    public boolean removeMappedTreeview(TreeItem<MappableItem> item) {
        return this.mappedTreeviews.remove(item);
    }

    public List<TreeItem<MappableItem>> getMappedTreeViews() {
        return this.mappedTreeviews;
    }

    public String getAttributeType() {
        String attribute = "";
        if (getItem().getAttributeName() == null || getItem().getAttributeName().isEmpty()) {
            // kein Attribut gesetzt
        } else {
            attribute = getItem().getAttributeName() + "/";
        }
        return attribute + (getItem().isList() ? "List(" + getItem().getTypeName() + ")"
                : getItem().getTypeName());
    }

    public boolean isRoot() {
        return isRoot;
    }

    public void setRoot(boolean isRoot) {
        this.isRoot = isRoot;
    }

}
