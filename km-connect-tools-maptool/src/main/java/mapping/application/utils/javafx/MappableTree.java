package mapping.application.utils.javafx;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MappableTree implements Serializable {
    private static final long serialVersionUID = 7286439281119701525L;

    private MappableItem item;

    private List<MappableTree> children = new ArrayList<>();

    private String mappedTo;

    public boolean addChild(MappableTree tree) {
        return children.add(tree);
    }

    public MappableItem getItem() {
        return item;
    }

    public void setItem(MappableItem item) {
        this.item = item;
    }

    public List<MappableTree> getChildren() {
        return children;
    }

    public void setChildren(List<MappableTree> children) {
        this.children = children;
    }

    public String getMappedTo() {
        return mappedTo;
    }

    public void setMappedTo(String mappedTo) {
        this.mappedTo = mappedTo;
    }

}
