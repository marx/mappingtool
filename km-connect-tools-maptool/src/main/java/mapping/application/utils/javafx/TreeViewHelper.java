package mapping.application.utils.javafx;

import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import mapping.application.MappingApp;
import mapping.application.model.AppTable;
import mapping.application.model.ErrorType;
import mapping.application.utils.Item;
import mapping.application.utils.TreeOfClasses;

public class TreeViewHelper {

    private int counter = 0;

    private final Image messageIcon = new Image(getClass().getClassLoader().getResourceAsStream("MessageImage.png"));
    private final Image listIcon = new Image(getClass().getClassLoader().getResourceAsStream("ListImage.png"));
    private final Image complexIcon = new Image(getClass().getClassLoader().getResourceAsStream("ComplexImage.png"));
    private final Image stringIcon = new Image(getClass().getClassLoader().getResourceAsStream("StringImage.png"));
    private final Image longIcon = new Image(getClass().getClassLoader().getResourceAsStream("LongImage.png"));
    private final Image dateIcon = new Image(getClass().getClassLoader().getResourceAsStream("DateImage.png"));
    private final Image jaxbIcon = new Image(getClass().getClassLoader().getResourceAsStream("JaxbImage.png"));
    private final Image booleanIcon = new Image(getClass().getClassLoader().getResourceAsStream("BooleanImage.png"));
    private final Image byteIcon = new Image(getClass().getClassLoader().getResourceAsStream("ByteImage.png"));
    private final Image enumIcon = new Image(getClass().getClassLoader().getResourceAsStream("Enum.png"));

    public TreeItem<MappableItem> getTreeItem(TreeOfClasses tree, boolean isPojo) {
        return getTreeItem(tree, true, isPojo);
    }

    public TreeItem<MappableItem> getTreeItem(TreeOfClasses tree, boolean root, boolean isPojo) {

        if(tree == null) {
            return null;
        }
        
        TreeItem<MappableItem> result = new TreeItem<MappableItem>(new MappableItem(tree.getItem(), isPojo, root),
                getGraphic(
                tree.getItem(),
                root));
        for (TreeOfClasses child : tree.getChildren()) {
            result.getChildren().add(getTreeItem(child, false, isPojo));
        }
        return result;
    }

    public ImageView getGraphic(Item item, boolean root) {
        if (root) {
            return new ImageView(messageIcon);
        }
        if (item.isList()) {
            return new ImageView(listIcon);
        } else if (item.isComplex()) {
            return new ImageView(complexIcon);
        } else if (item.isEnum()) {
            return new ImageView(enumIcon);
        } else {
            return getImageViewByType(item.getTypeName());
        }
    }

    private ImageView getImageViewByType(String typeName) {
        if (typeName.contains("String")) {
            return new ImageView(stringIcon);
        } else if (typeName.contains("long")) {
            return new ImageView(longIcon);
        } else if (typeName.contains("Long")) {
            return new ImageView(longIcon);
        } else if (typeName.contains("int")) {
            return new ImageView(longIcon);
        } else if (typeName.contains("Int")) {
            return new ImageView(longIcon);
        } else if (typeName.contains("BigDecimal")) {
            return new ImageView(longIcon);
        } else if (typeName.contains("byte")) {
            return new ImageView(byteIcon);
        } else if (typeName.contains("jaxb") || typeName.contains("JAXB")) {
            return new ImageView(jaxbIcon);
        } else if (typeName.contains("date") || typeName.contains("Date") || typeName.contains("XML")) {
            return new ImageView(dateIcon);
        } else if (typeName.contains("boolean") || typeName.contains("Boolean")) {
            return new ImageView(booleanIcon);
        }
        return null;
    }

    public void mapTrees(TreeItem<MappableItem> leftTree, TreeItem<MappableItem> rightTree, boolean isRoot,
            TableView<AppTable> table) {

        String mappedTo = leftTree.getValue().getItem().getMappedTo();
        TreeItem<MappableItem> mappedItem = findTreeByPath(mappedTo, rightTree, isRoot, true);

        leftTree.getValue().setMappedTreeview(mappedItem);
        leftTree.getValue().setMapped(mappedItem != null);
        if (mappedItem != null) {
            mappedItem.getValue().addMappedTreeview(leftTree);
        } else if (!isRoot) {
            // Fehlendes Attribut hinzufügen (-> konnte nicht gemappt werden)
            AppTable missingValue = new AppTable(leftTree, rightTree, rightTree.getValue().getItem().getAttributeName(),
                    "Could Not be Mapped", ErrorType.MISSING);
            table.getItems().add(missingValue);
        }

        for (TreeItem<MappableItem> child : leftTree.getChildren()) {
            mapTrees(child, rightTree, false, table);
        }

    }

    public TreeItem<MappableItem> findTreeByPath(String path, TreeItem<MappableItem> parent, boolean isRoot,
            boolean setMapping) {
        if (path == null) {
            return null;
        }
        String[] ancestors = path.split("->");
        TreeItem<MappableItem> tempParent = parent;
        boolean parentFound = false;
        for (int i = (isRoot ? 1 : 0); i < ancestors.length; i++) {
            for (TreeItem<MappableItem> child : tempParent.getChildren()) {
                String attributeName = child.getValue().getItem().getAttributeName();
                String typeName = child.getValue().getSimpleTypeName();
                if (attributeName.equals(ancestors[i]) || typeName.equals(ancestors[i])) {
                    // auf dem Weg zum Zielknoten werden alle Knoten als gemappt
                    // gesetzt
                    if (setMapping) {
                        child.getValue().setMapped(true);  
                    }                   
                    parentFound = ancestors.length - 1 == i;
                    tempParent = child;
                    break;
                }
            }
        }
        return parentFound ? tempParent : null;
    }

    public void transformItem(MappableItem mapItem) {
        Item item = mapItem.getItem();
        if (item.isComplex()) {
            item.setTypeName(getTypeName(item.getTypeName()));
        }
    }

    private String getTypeName(String typeName) {
        int index = 0;
        // $
        if (typeName.indexOf('$') != -1) {
            index = typeName.lastIndexOf('$');
            typeName = typeName.substring(index + 1);
            return MappingApp.PACKAGE_NAME + "." + typeName;
        }
        // .
        if (typeName.indexOf('.') != -1) {
            index = typeName.lastIndexOf('.');
            typeName = typeName.substring(index + 1);
            return MappingApp.PACKAGE_NAME + "." + typeName;
        }
        return typeName;
    }

    public String getMappedTo(TreeItem<MappableItem> item) {
        String result = "";
        boolean start = true;
        TreeItem<MappableItem> currentItem = item;
        while (currentItem.getParent() != null) {
            result = currentItem.getValue().getItem().getAttributeName() + ((!start) ? "->" + result : result);
            start = false;
            currentItem = currentItem.getParent();
        }
        return "root->" + result;
    }

    public void printTree(TreeItem<MappableItem> tree) {
        System.out.println("------------------------ Baum -------------------------");
        printTree(tree, true);
    }

    /**
     * Einen Baum auf die Standardausgabe printen
     * 
     * @param tree
     * @param isRoot
     */
    private void printTree(TreeItem<MappableItem> tree, boolean isRoot) {
        if (isRoot) {
            counter = 0;
        }
        MappableItem item = tree.getValue();
        if (item.isMapped()) {
            for (int i = 0; i < counter; i++) {
                System.out.print("-");
            }
            System.out.println(item.getAttributeType() + " -> " + item.isMapped());

        }
        counter++;
        for (TreeItem<MappableItem> son : tree.getChildren()) {
            printTree(son, false);
        }
        counter--;
    }

}
