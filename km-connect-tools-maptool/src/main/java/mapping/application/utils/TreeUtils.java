package mapping.application.utils;

public class TreeUtils {

    /**
     * Mapped Eigenschafen ("required") vom rechten Baum auf den linken Baum
     *
     * @param leftTree  der linke Teil des Baums
     * @param rightTree der rechte Teil des Baums
     * @param isRoot    true, wenn es das Root-Element ist
     */
    public static void copyRequiredFields(TreeOfClasses leftTree, TreeOfClasses rightTree, boolean isRoot) {
        String mappedTo = leftTree.getItem().getMappedTo();
        // Manche Elemente sind gar nicht gemappt, bspw der root - diese ignorieren
        if (mappedTo != null) {
            TreeOfClasses mappedRightItem = findItemByPath(mappedTo, rightTree, isRoot);
            if (mappedRightItem == null)
                throw new IllegalStateException("Item '" + mappedTo + "' should exist in the right tree, but does not");
            boolean required = mappedRightItem.getItem().isRequired();
            leftTree.getItem().setRequired(required);
        }

        // rekursiv alle Items durchgehen
        for (TreeOfClasses child : leftTree.getChildren()) {
            copyRequiredFields(child, rightTree, false);
        }
    }

    /**
     * @param path      der Pfad des Items
     * @param rightTree der (komplette) rechte Baum
     * @param isRoot    True wenn das linke Item ein Root-Element ist
     * @return das zum linken Item gehörige rechte Item oder Null, falls es nicht gefunden wird
     */
    public static TreeOfClasses findItemByPath(String path, TreeOfClasses rightTree, boolean isRoot) {
        if (path == null)
            return null;
        String[] ancestors = path.split("->");
        int start = isRoot ? 1 : 0;
        TreeOfClasses candidate = rightTree;
        for (int i = start; i < ancestors.length; i++) {
            for (TreeOfClasses child : candidate.getChildren()) {
                String attributeName = child.getItem().getAttributeName();
                String typeName = child.getItem().getTypeName();
                typeName = typeName.lastIndexOf('.') != -1 ? typeName.substring(typeName.lastIndexOf('.') + 1) : typeName;

                boolean endOfPathReached = ancestors.length - 1 == i;
                boolean candidateIdentified = (attributeName.equals(ancestors[i]) || typeName.equals(ancestors[i]));
                if (endOfPathReached && candidateIdentified) {
                    return child;
                } else if (candidateIdentified) {
                    candidate = child;
                    break;
                }
            }
        }
        return null;
    }

}
