package mapping.application.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;

import org.reflections.Reflections;

/**
 * Die Klasse lädt einen Klassenbaum aus einer Datei oder per Reflection. Der
 * Klassenbaum wird dann in der GUI als TreeView dargestellt
 * 
 * @author Erdmanns
 *
 */
public class ClassTreeLoader {

    private int counter = 0;

    /**
     * Liest den (POJO)- Baum (linke TreeView im Mapping-Tool) aus der
     * Progress-File
     * 
     * @param elements
     * @return
     */
    public TreeOfClasses loadTreeFromConfig(List<String> elements) {

        TreeOfClasses parent = new TreeOfClasses();

        TreeOfClasses tempParent = parent;
        boolean start = true;

        for (String element : elements) {

            if (element.contains("FULL-TREE")) {
                continue;
            }

            if (element.contains("GEN-TREE")) {
                break;
            }

            if (element.contains("CONVERTERS")) {
                break;
            }

            if (element.startsWith("$T$")) {
                element = element.substring(3);
            }

            if (element.startsWith("$X$")) {
                element = element.substring(3);
            }

            // die Attribute für Root holen
            if (start) {
                String typeName = element.split("=")[0].split("/")[1];
                String attributeName = element.split("=")[0].split("/")[0];
                Item item = new Item(typeName, attributeName, false, true, true);
                parent.setItem(item);
                start = false;
            }

            String[] equalsSeparatedLine = element.split("=");
            String[] generatedItems = equalsSeparatedLine[0].split("->");
            String converter = equalsSeparatedLine.length > 2 ? equalsSeparatedLine[2] : null;
            String mappedTo = null;
            if (equalsSeparatedLine.length > 1) {
                mappedTo = equalsSeparatedLine[1];
                mappedTo = getAttributeNames(mappedTo);
            }

            String path = "";
            tempParent = parent;

            boolean isMapped = false;

            for (int i = 1; i < generatedItems.length; i++) {
                
                boolean isList = false;
                boolean isComplex = false;
                boolean isEnum = false;
                // ein Mapping liegt nur vor, wenn das zu mappende "POJO"-
                // Objekt ganz rechts steht
                isMapped = (i == generatedItems.length - 1);

                String attributName = generatedItems[i].split("/")[0];
                path += path.isEmpty() ? attributName : "->" + attributName;
                String typName = generatedItems[i].split("/")[1];

                if (typName.contains("List(")) {
                    typName = typName.substring("List(".length(), typName.length() - 1);
                    isList = true;
                } else if (typName.contains("Complex(")) {
                    typName = typName.substring("Complex(".length(), typName.length() - 1);
                    isComplex = true;
                } else if (typName.contains("[B")) {
                    typName = "byte[]";
                    isComplex = false;
                }
                
                try {
                    if (isSimpleClass(typName)) {
                        isComplex = false;
                    } else {
                        isComplex = isComplex || !isPrimitiveType(Class.forName(typName));
                    }
                    isEnum = isEnum(typName);
                } catch(ClassNotFoundException cne) {
                    isComplex = true;
                }

                // Ensure generation does not result in duplicates
                boolean found = false;
                TreeOfClasses nodeIfAny = findTreeByPath(path, parent);
                if (nodeIfAny != null) {
                    found = objectAlreadyCreated(path, attributName, parent);
                    tempParent = nodeIfAny;
                }

                // Class has not been added yet
                if (!found) {
                    Item genItem = new Item(typName, attributName, isList, isComplex, false);
                    genItem.setEnum(isEnum);
                    genItem.setConverterName(converter);
                    TreeOfClasses tree = new TreeOfClasses(genItem);
                    if (isMapped) {
                        genItem.setMappedTo(mappedTo);
                    }
                    tempParent.getChildren().add(tree);
                }
            }
        }
        return parent;
    }

    private boolean isEnum(String typName) {
        try {
            Class<?> name = Class.forName(typName);
            return name.isEnum();
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    private boolean isSimpleClass(String className) {
        String[] names = new String[] { "boolean", "long", "byte", "char", "double", "float", "int", "short", "void",
                "boolean[]", "long[]", "byte[]", "char[]", "double[]", "float[]", "int[]", "short[]" };
        return Arrays.asList(names).contains(className);
    }

    private String getAttributeNames(String mappedTo) {
        if (mappedTo.isEmpty()) {
            return "";
        }
        String result = "";
        boolean isStart = true;
        String[] attributes = mappedTo.split("->");
        for (String attribute : attributes) {
            attribute = getSimpleType(attribute.split("/")[0]);
            if (attribute.contains("List(")) {
                attribute = attribute.substring("List(".length(), attribute.length() - 1);
            } else if (attribute.contains("Complex(")) {
                attribute = attribute.substring("Complex(".length(), attribute.length() - 1);
            }
            result += (isStart ? "root" : "->" + attribute);
            isStart = false;
        }
        return result;
    }

    private String getSimpleType(String typeName) {
        int index = typeName.lastIndexOf('.');
        if (index == -1) {
            return typeName;
        } else {
            return typeName.substring(index + 1);
        }
    }

    /**
     * Sucht nach dem Teilbaum anhand des übergebenen Pfades. Der Pfad wird
     * dabei so lange durchlaufen, wie es Kindknoten im Baum gibt.
     * <p>
     * Bsp.: Folgende Baumstruktur sei gegeben:
     * <p>
     * -de.kommone.kmc.webservice.NatuerlichePerson / antragsstellerNatPerson
     * <br />
     * --de.kommone.kmc.webservice.Anschrift / anschrift <br />
     * ---java.lang.String / staat <br />
     * ---java.lang.String / postleitzahl <br />
     * ---java.lang.String / ort <br />
     * ---java.lang.String / strasse <br />
     * ---java.lang.String / strasseHausnummer
     * <p>
     * Der Pfad "antragsstellerNatPerson->anschrift->staat" liefert den Staat
     * als Teilbaum. Wird hingegen der Pfad
     * "antragsstellerNatPerson->anschrift->telefon" übergeben, wird die
     * Anschrift zurückgeliefert, da das Telefon im Baum noch nicht enthalten
     * ist
     * 
     * @param path
     * @param parent
     * @return
     */
    private TreeOfClasses findTreeByPath(String path, TreeOfClasses parent) {
        String[] ancestors = path.split("->");
        TreeOfClasses tempParent = parent;
        boolean parentFound = false;
        for (String ancestor : ancestors) {
            for (TreeOfClasses child : tempParent.getChildren()) {
                if (child.getItem().getAttributeName().equals(ancestor)) {
                    parentFound = true;
                    tempParent = child;
                    break;
                }
            }
        }
        return parentFound ? tempParent : null;
    }

    /**
     * Übergeben wird der Pfad des Objektes im Baum und der Name des Objektes
     * (Attributname). Der Name steht dabei immer an der letzten Stelle des
     * Pfades. Die Methode funktioniert ähnlich wie
     * {@link #findTreeByPath(String, TreeOfClasses)}, man kann es sich so
     * vorstellen, wie das Aufklappen des Baumes anhand des übergebenen Pfades.
     * Liegt das Attribut am Ende des Pfades und ist damit bereits im Baum, gibt
     * die Methode <code>true</code> zurück, andernfalls <code>false</code>.
     * 
     * @param path
     * @param attributeName
     * @param parent
     * @return
     */
    private boolean objectAlreadyCreated(String path, String attributeName, TreeOfClasses parent) {
        String[] ancestors = path.split("->");
        TreeOfClasses tempParent = parent;
        boolean objectFound = false;
        for (int i = 0; i < ancestors.length; i++) {
            for (TreeOfClasses child : tempParent.getChildren()) {
                if (child.getItem().getAttributeName().equals(attributeName)) {
                    // es könnte sein, dass 2 Attribute, die auf dem gleichen
                    // Pfad liegen, gleich heißen. Das Objekt darf demnach erst
                    // als "gefunden" gelten, wenn es ganz hinten im Pfad liegt
                    objectFound = ancestors.length - 1 == i;
                    if (objectFound) {
                        break;
                    }
                }
                if (child.getItem().getAttributeName().equals(ancestors[i])) {
                    tempParent = child;
                    break;
                }
            }
        }

        return objectFound;
    }

    /**
     * Liest den (Schema)- Baum (rechte TreeView im Mapping-Tool) per Reflection
     * aus einer Klasse
     * 
     * @param javaClass
     * @return
     */
    public TreeOfClasses loadTreeFromClass(Class<?> javaClass) {
        return loadTreeFromClass(javaClass, null);
    }

    /**
     * Einen Baum auf die Standardausgabe printen
     * 
     * @param tree
     */
    public void printTree(TreeOfClasses tree) {
        printTree(tree, true);
    }

    /**
     * Einen Baum auf die Standardausgabe printen
     * 
     * @param tree
     * @param isRoot
     */
    private void printTree(TreeOfClasses tree, boolean isRoot) {
        if (isRoot) {
            counter = 0;
        }
        for (int i = 0; i < counter; i++) {
            System.out.print("-");
        }
        System.out.println(tree.getItem().getTypeName() + " / " + tree.getItem().getAttributeName() + (tree.getItem()
                .isList() ? " / -> List" : "") + (tree.getItem().getMappedTo() != null ? " / " + tree.getItem()
                        .getMappedTo()
                        : ""));
        counter++;
        for (TreeOfClasses son : tree.getChildren()) {
            printTree(son, false);
        }
        counter--;
    }

    /**
     * Liest den (Schema)- Baum (rechte TreeView im Mapping-Tool) per Reflection
     * aus einer Klasse
     * 
     * @param javaClass
     * @param item
     * @return
     */
    private TreeOfClasses loadTreeFromClass(Class<?> javaClass, Item item) {

        // handelt es sich um Root oder nicht?
        TreeOfClasses tree = new TreeOfClasses(item == null ? new Item(javaClass.getTypeName(), javaClass
                .getSimpleName(), false, true,
                true) : item);

        // zunächst alle Felder holen
        Field[] allFields = getAllFieldsUpToObject(javaClass);

        for (Field field : allFields) {
            
            // List- Typ
            if(field.getType().isAssignableFrom(List.class)) {
                Class<?> listClass = returnTypeOfListElements(field);
                if (listClass == null) {
                    continue;
                }
                // 1. Komplexer innerer Typ
                if (!isPrimitiveType(listClass)) {

                    // bei einer abstrakten Klasse über die Kindklassen
                    // iterieren
                    if (Modifier.isAbstract(listClass.getModifiers())) {

                        // die abstrakte Klasse selbst nur als einfachen Baum,
                        // da alle Kindknoten ohnehin alle Attribute von der
                        // Mutter erben (TODO : Auslagern in eine Methode)
                        TreeOfClasses abstractClass = new TreeOfClasses(new Item(listClass.getTypeName(),
                                field.getName(), true, true, isRequired(field)));

                        for (Class<?> child : getSubtypesOf(listClass)) {
                            abstractClass.addChild(loadTreeFromClass(child, new Item(child.getTypeName(), "", false,
                                    true, isRequired(field))));
                        }
                        tree.addChild(abstractClass);

                    } else {
                        tree.addChild(loadTreeFromClass(listClass, new Item(listClass.getTypeName(), field.getName(),
                                true, true, isRequired(field))));
                    }
                } // 2. einfacher innnerer Typ
                else {
                    tree.addChild(new TreeOfClasses(new Item(listClass.getTypeName(), field.getName(), true, false,
                            isRequired(field))));
                }

            } // primitiver Typ
            else if (isPrimitiveType(field.getType())) {
                Item primitiveItem = new Item(field.getType().getTypeName(), field.getName(), false, false, isRequired(
                        field));
                primitiveItem.setEnum(field.getType().isEnum());
                tree.addChild(new TreeOfClasses(primitiveItem));
            } // komplexer Typ
            else {

                // TODO : hier wäre auch ein abstrakter Typ möglich (gleich
                // behandelbar wie oben)
                // folgenden Codeblock später ersetzen (das Problem ist, dass
                // sich hier vor Allem beim Nachrichtenköpfchen nicht alle
                // Attribute mehr mappen lassen)
                // REPLACE - start
                tree.addChild(loadTreeFromClass(field.getType(), new Item(field.getType().getTypeName(), field
                        .getName(), false, true, isRequired(field))));
                // REPLACE -end
                // REPLACEMENT durch - start
                // // bei einer abstrakten Klasse über die Kindklassen
                // // iterieren
                // if (Modifier.isAbstract(field.getType().getModifiers())) {
                //
                // // die abstrakte Klasse selbst nur als einfachen Baum,
                // // da alle Kindknoten ohnehin alle Attribute von der
                // // Mutter erben (TODO : Auslagern in eine Methode)
                // TreeOfClasses abstractClass = new TreeOfClasses(new
                // Item(field.getType().getTypeName(), field
                // .getName(), false, true, isRequired(field)));
                //
                // for (Class<?> child : getSubtypesOf(field.getType())) {
                // abstractClass.addChild(loadTreeFromClass(child, new
                // Item(child.getTypeName(), "", false, true,
                // isRequired(field))));
                // }
                // tree.addChild(abstractClass);
                //
                // } else {
                // tree.addChild(loadTreeFromClass(field.getType(), new
                // Item(field.getType().getTypeName(), field
                // .getName(), false, true, isRequired(field))));
                // }
                // REPLACEMENT durch - end

            }

        }

        return tree;
    }

    private boolean isPrimitiveType(Class<?> type) {
        return type.isArray() || type.isPrimitive() || type.isAssignableFrom(java.lang.String.class) || type
                .isAssignableFrom(javax.xml.datatype.XMLGregorianCalendar.class) || isWrapperType(type) || isMath(type)
                || type.isEnum() || isJAXB(type);
    }

    private boolean isWrapperType(Class<?> field) {
        String typname = field.getName();
        String[] names = new String[] { "java.lang.Boolean", "java.lang.Long", "java.lang.Byte", "java.lang.Character",
                "java.lang.Double", "java.lang.Float", "java.lang.Integer", "java.lang.Short", "java.lang.Void" };
        return Arrays.asList(names).contains(typname);
    }

    private boolean isMath(Class<?> type) {
        return type.getName().contains("java.math");
    }

    private boolean isJAXB(Class<?> type) {
        return type.getName().contains("JAXBElement");
    }

    private Class<?> returnTypeOfListElements(Field field) {
        ParameterizedType pt = null;
        try {
            pt = (ParameterizedType) field.getGenericType();
        } catch (ClassCastException e) {
            return null;
        }
        String typeName = pt.getActualTypeArguments()[0].getTypeName();

        if (typeName.contains("? extends ")) {
            typeName = typeName.replace("? extends ", "");
        }
        if (typeName.contains("JAXBElement<?>")) {
            return null;
        } else if (typeName.contains("javax.xml.bind.JAXBElement")) {
            typeName = typeName.substring(27, typeName.length() - 1);
        }
        try {
            return Class.forName(typeName);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    private Field[] getAllFieldsUpToObject(Class<?> javaClass) {

        // alle Felder holen
        List<Field> allFields = new ArrayList<>();

        // zunächst die Felder des Objekts selbst hinzufügen
        allFields.addAll(Arrays.asList(javaClass.getDeclaredFields()));

        // dann rekursiv alle Felder der Basisklasse holen
        Class<?> superClass = javaClass.getSuperclass();
        while (superClass != null && !superClass.equals(Object.class)) {
            allFields.addAll(Arrays.asList(superClass.getDeclaredFields()));
            superClass = superClass.getSuperclass();
        }

        return allFields.toArray(new Field[allFields.size()]);
    }

    /**
     * Prüfen, ob es sich um ein verpflichtendes Feld handelt
     * 
     * @param field
     * @return
     */
    private boolean isRequired(Field field) {
        if (field.isAnnotationPresent(XmlElement.class)) {
            XmlElement xmlElementAnnotation = field.getAnnotation(XmlElement.class);
            return xmlElementAnnotation.required();
        } else {
            return false;
        }
    }

    /**
     * Im Falle einer abstrakten Klasse, müssen wir die konkreten Kindklassen
     * holen
     * 
     * @param clazz
     * @return
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Set<Class<?>> getSubtypesOf(Class<?> clazz) {
        Reflections reflections = new Reflections(clazz.getPackage().getName());
        Set subTypes = reflections.getSubTypesOf(clazz);
        return subTypes;
    }

}
