package mapping.application.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Diese Klasse generiert den JUnit-Testcode für den Mapping-WS. Dabei sollen
 * folgende Schritte durchlaufen werden:
 * <ol>
 * <li>Auf Basis der Progress-File wird der Objekt-Baum geladen und ein POJO-
 * Objekt erzeugt und zufällig mit Werten befüllt</li>
 * <li>Dann wird das Mapping vorgenommen auf das jeweilige JAXB-Artefakt</li>
 * <li>Schließlich werden alle im ersten Schritt zufällig befüllten Werte auf
 * die in das JAXB-Artefakt gemappten Werte miteinander verglichen</li>
 * </ol>
 *
 * @author Erdmanns
 */
public class JUnitGenerator {

    private Map<String, String> names = new HashMap<>();
    private List<String> listAttributesPOJO = new ArrayList<>();
    private List<String> listAttributesXML = new ArrayList<>();

    private String path = "";
    private StringBuilder code = new StringBuilder();
    private Map<String, StringBuilder> currentMethod = new HashMap<>();
    private String currentKey = null;

    private void init() {
        names.clear();
        listAttributesPOJO.clear();
        listAttributesXML.clear();
        path = "";
        code = new StringBuilder();
        currentMethod.clear();
        currentKey = null;
    }

    public String generateTestCode(TreeOfClasses tree) {
        init();
        code.append("@Test\n public void testMapping(){\n");
        generateCodeForPOJOWithArbitraryValues(tree, null);
        generateTODO();
        generateAssertsFromProgressFile(tree);
        code.append("\n}");
        for (String key : currentMethod.keySet()) {
            code.append(currentMethod.get(key));
        }
        return code.toString();
    }

    private void generateTODO() {
        code.append("// TODO : Erstellen Sie ein Objekt mit dem Namen root durch Aufruf des generierten Mappers\n");
    }

    private void generateAssertsFromProgressFile(TreeOfClasses tree) {

        Item item = tree.getItem();
        item.setTypeName(adjustTypeName(item.getTypeName()));
        boolean isBoolean = item.getTypeName().contains("oolean");
        String tempPath = path;

        path += path.isEmpty() ? item.getAttributeName() : "->" + item.getAttributeName();

        // keine Assertion, aber
        if (item.isComplex() || item.isList()) {
            if (item.isList() && item.getMappedTo() != null) {
                listAttributesPOJO.add(item.getAttributeName());
                String[] strings = item.getMappedTo().split("->");
                String xmlName = strings[strings.length - 1];
                listAttributesXML.add(xmlName);
            }
            for (TreeOfClasses child : tree.getChildren()) {
                generateAssertsFromProgressFile(child);
            }
        } else {
            code.append(generateAssertFromPaths(path, item.getMappedTo(), isBoolean));
        }

        path = tempPath;
    }

    private String generateAssertFromPaths(String pathLeft, String pathRight, boolean isBoolean) {
        String getterLeft = generateGetterFromPath(pathLeft, listAttributesPOJO, isBoolean);
        String getterRight = generateGetterFromPath(pathRight, listAttributesXML, isBoolean);
        return getterRight.isEmpty() ? "" : "Assert.assertEquals(" + getterLeft + ", " + getterRight + ");\n";
    }

    private String generateGetterFromPath(String path, List<String> listOfAttributes, boolean isBoolean) {
        String result = "";
        if (path == null) {
            return "";
        }
        String[] elements = path.split("->");
        for (int i = 0; i < elements.length; i++) {
            boolean isRoot = result.isEmpty();
            String newGetter = null;
            if (isRoot) {
                newGetter = elements[i];
            } else {
                newGetter = generateGetterForAttribute(isBoolean && i == elements.length - 1, elements[i]) + "()";
            }
            if (listOfAttributes.contains(elements[i])) {
                newGetter = newGetter + ".get(0)";
            }
            result += result.isEmpty() ? newGetter : "." + newGetter;
        }
        return result;
    }

    private void generateCodeForPOJOWithArbitraryValues(TreeOfClasses tree, String nameOfParent) {

        Item item = tree.getItem();
        item.setTypeName(adjustTypeName(item.getTypeName()));
        String attributeName = getAttributeName(item);
        boolean isBoolean = item.getTypeName().contains("oolean");

        if (item.isList()) {
            String listName = attributeName + "List";
            if (currentKey != null) {
                currentMethod.get(currentKey).append("java.util.List<" + item.getTypeName() + "> " + listName
                        + " = new java.util.ArrayList<>();\n");
            } else {
                code.append("java.util.List<" + item.getTypeName() + "> " + listName
                        + " = new java.util.ArrayList<>();\n");
            }

            createMethod(tree, item, attributeName);

            // Element wird in Liste gehängt
            if (currentKey != null) {
                currentMethod.get(currentKey).append(listName + ".add(" + "create_" + attributeName + "());\n");
            } else {
                code.append(listName + ".add(" + "create_" + attributeName + "());\n");
            }

            // Abschluss Liste
            if (currentKey != null) {
                currentMethod.get(currentKey).append(nameOfParent + "." + generateGetterForAttribute(isBoolean, item
                        .getAttributeName()) + "().addAll(" + listName + ");\n");
            } else {
                code.append(nameOfParent + "." + generateGetterForAttribute(isBoolean, item.getAttributeName())
                        + "().addAll(" + listName + ");\n");
            }

        } else if (item.isComplex()) {
            createMethod(tree, item, attributeName);
            if (nameOfParent != null) {
                if (currentKey != null) {
                    currentMethod.get(currentKey).append(nameOfParent + "." + generateSetterForAttribute(item
                            .getAttributeName()) + "(" + "create_" + attributeName + "());\n");
                } else {
                    code.append(nameOfParent + "." + generateSetterForAttribute(item.getAttributeName()) + "("
                            + "create_" + attributeName + "());\n");
                }

            }
        }
        // einfaches Objekt
        else {
            if (nameOfParent != null) {
                if (currentKey != null) {
                    currentMethod.get(currentKey).append(nameOfParent + "." + generateSetterForAttribute(item
                            .getAttributeName()) + "(" + getValue(item) + ");\n");
                } else {
                    code.append(nameOfParent + "." + generateSetterForAttribute(item.getAttributeName()) + "("
                            + getValue(item) + ");\n");
                }
            }
        }

    }

    private void createMethod(TreeOfClasses tree, Item item, String attributeName) {
        String createName = "create_" + attributeName;

        // Code, der zur alten Methode gehört
        if (currentKey == null) {
            code.append(item.getTypeName() + " " + attributeName + " = " + createName + "();\n");
        } else {
            StringBuilder stringBuilder = currentMethod.get(currentKey);
            if (stringBuilder == null) {
                stringBuilder = new StringBuilder();
            }
            currentMethod.put(currentKey, stringBuilder);
        }

        String oldKey = currentKey;

        // Code, der zur neuen Methode gehört
        currentKey = createName;
        currentMethod.put(currentKey, new StringBuilder());
        currentMethod.get(currentKey).append("private " + item.getTypeName() + " " + createName + "(){\n" + item
                .getTypeName() + " " + attributeName + " = new " + item.getTypeName() + "();\n");
        for (TreeOfClasses child : tree.getChildren()) {
            generateCodeForPOJOWithArbitraryValues(child, attributeName);
        }
        currentMethod.get(currentKey).append("return " + attributeName + ";\n } \n");

        // Methoden-Ende
        currentKey = oldKey;
    }

    private String adjustTypeName(String typeName) {
        int index = typeName.lastIndexOf('.');
        if (index == -1) {
            return typeName;
        }
        String result = typeName.substring(0, index) + ".model" + typeName.substring(index);
        return result;
    }

    private String getValue(Item item) {
        if (item.getTypeName().contains("String")) {
            return "\"" + item.getAttributeName() + "\"";
        } else if (item.getTypeName().contains("BigDecimal")) {
            return "java.math.BigDecimal.valueOf(12)";
        } else if (item.getTypeName().contains("int")) {
            return "5";
        } else if (item.getTypeName().contains("oolean")) {
            return "false";
        }
        return null;
    }

    private String generateSetterForAttribute(String attributeName) {
        String firstLetterUpperCase = "set" + attributeName.substring(0, 1).toUpperCase() + attributeName.substring(1);
        return firstLetterUpperCase;
    }

    private String generateGetterForAttribute(boolean isBoolean, String attributeName) {
        String firstLetterUpperCase = (isBoolean ? "is" : "get") + attributeName.substring(0, 1).toUpperCase()
                + attributeName.substring(1);
        return firstLetterUpperCase;
    }

    private String getAttributeName(Item item) {
        int counter = 1;
        String attributeName = item.getAttributeName();
        while (names.containsKey(attributeName)) {
            attributeName = item.getAttributeName() + counter++;
        }
        names.put(attributeName, attributeName);
        return attributeName;
    }

}
