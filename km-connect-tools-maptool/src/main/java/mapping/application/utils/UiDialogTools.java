package mapping.application.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Optional;

public class UiDialogTools {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(UiDialogTools.class);

    public static final FileChooser.ExtensionFilter EXTENSION_FILTER_TXT = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");

    public static final FileChooser.ExtensionFilter EXTENSION_FILTER_XSD = new FileChooser.ExtensionFilter("XSD Files (*.xsd)", "*.xsd");
    public static final FileChooser.ExtensionFilter EXTENSION_FILTER_JSON = new FileChooser.ExtensionFilter("JSON files (*.json)", "*.json");
    public static final FileChooser.ExtensionFilter EXTENSION_FILTER_EXCEL = new FileChooser.ExtensionFilter("Excel files (*.xlsx)", "*.xlsx");
    public static final FileChooser.ExtensionFilter EXTENSION_FILTER_JAVA = new FileChooser.ExtensionFilter("Java Files (*.java)", "*.java");
    public static final FileChooser.ExtensionFilter EXTENSION_FILTER_GROOVY = new FileChooser.ExtensionFilter("Groovy Files (*.groovy)", "*.groovy");


    /**
     * @param title            title of the file chooser
     * @param directory        the directory to start with. Invalid or non-existing directories will be filtered out
     * @param save             true if the file save dialog should be shown, false if the file open dialog should be shown
     * @param extensionFilters an array of extension filters, see UITools.EXTENSION_FILTER_TXT as an example
     * @return
     */
    public static Optional<File> useFileChooser(String title, String directory, boolean save, FileChooser.ExtensionFilter... extensionFilters) {
        final Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);

        stage.setTitle(title);

        FileChooser fileChooser = new FileChooser();
        for (FileChooser.ExtensionFilter extensionFilter : extensionFilters) {
            fileChooser.getExtensionFilters().add(extensionFilter);
        }

        //Set directory if not null, not empty and is an existing file
        if (directory != null && !directory.isEmpty()) {
            File directoryFile = new File(directory);
            if (directoryFile.exists()) {
                fileChooser.setInitialDirectory(directoryFile);
            } else {
                log.warn("Directory '" + directory + "' does not exist");
            }
        }

        File file = save ? fileChooser.showSaveDialog(stage) : fileChooser.showOpenDialog(stage);
        if (file == null) {
            log.info("File selection aborted");
            return Optional.empty();
        }
        log.info("File selected: " + file);
        return Optional.of(file);
    }

    public static void showException(Exception ex, String ueberschrift) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Exception Dialog");
        alert.setHeaderText(ueberschrift);
        alert.setContentText(ex.getMessage());

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(200.0);
        textArea.setMaxHeight(200.0);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(200.0);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);
        expContent.setMaxWidth(200.0);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    public static void showWarning(List<String> warnings, String ueberschrift) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning Dialog");
        alert.setHeaderText(ueberschrift);
        alert.setContentText("Für unbekannte Typen wird 'string' als Wert gesetzt.");

        String warningText = String.join(System.lineSeparator(), warnings);

        Label label = new Label("Auflistung aller unbekannter Typen:");

        TextArea textArea = new TextArea(warningText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(600.0);
        textArea.setMaxHeight(200.0);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(200.0);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);
        expContent.setMaxWidth(200.0);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }
}
