package mapping.application.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlType;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import de.kommone.webservice.seitenbauformular.Field;
import de.kommone.webservice.seitenbauformular.Fieldgroup;
import de.kommone.webservice.seitenbauformular.Section;
import de.kommone.webservice.seitenbauformular.Seitenbauformular;

/**
 * Generiert eine Excel-Datei aus den vorhandenen Daten
 *
 * @author SchulerM
 */
public class LegacyExcelGenerator {
    private Workbook workbook;
    private Sheet sheet;

    public LegacyExcelGenerator() {
        setupWorkbook();
    }

    /**
     * Erstellt das Workbook
     */
    private void setupWorkbook() {
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("Prozessdesign");
        generateTitleCell();
    }

    Workbook getWorkbook() {
        return this.workbook;
    }

    /**
     * Erstellt eine Titelzeile
     */
    private void generateTitleCell() {
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("FeldgruppenId");
        row.createCell(1).setCellValue("FeldId");
        row.createCell(2).setCellValue("Multi");
        row.createCell(3).setCellValue("Pflicht");
        row.createCell(4).setCellValue("XOEV");
        row.createCell(5).setCellValue("Codeliste");

        // Eine Font dafür erstellen
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);

        row.cellIterator().forEachRemaining(cell -> cell.setCellStyle(style));
    }

    /**
     * Generiert die Daten aus dem Dateiinhalt
     *
     * @param lines Den Dateiinhalt
     * @throws ClassNotFoundException wenn die rechte Baumhälfte nicht richtig gefunden wird
     */
    public void generate(List<String> lines) throws ClassNotFoundException {
        ClassTreeLoader treeLoader = new ClassTreeLoader();
        TreeOfClasses leftTree = treeLoader.loadTreeFromConfig(lines);

        // Die erste "richtige" Zeile mit dem root-Element auslesen und Klassennamen extrahieren
        String msgline = lines.get(0).contains("FULL-TREE") ? lines.get(1) : lines.get(0);
        Class<?> classForName = Class.forName(msgline.split("=")[1].split("/")[1]);
        TreeOfClasses rightTree = treeLoader.loadTreeFromClass(classForName);

        TreeUtils.copyRequiredFields(leftTree, rightTree, true);
        FormularGenerator formularGenerator = new FormularGenerator();
        Seitenbauformular formular = formularGenerator.generateSeitenbauFormular(lines);
        generate(formular, formularGenerator.getFieldMapping(), leftTree, rightTree);
    }

    /**
     * Generiert die Daten aus dem Seitenbau-Formular. Löscht nicht vorher angelegte Daten
     *
     * @param formular Das Seitenbau-Formular
     * @param leftTree Den linken Baum
     */
    public void generate(Seitenbauformular formular, Map<String, TreeOfClasses> fieldMapping, TreeOfClasses leftTree,
                         TreeOfClasses rightTree) throws ClassNotFoundException, IllegalStateException {
        // Alle Felder bekommen und darauf Zellen generieren
        // Wäre per Streams und flatMap zwar schöner, die Zellen brauchen aber Felder und Feldgruppen

        for (Section section : formular.getSections()) {
            for (Fieldgroup fieldGroup : section.getFieldGroups()) {
                TreeOfClasses fieldGroupItem = fieldMapping.get(fieldGroup.getId());
                generateCell(fieldGroup.getId(),
                        fieldGroup.isMultiple() != null ? fieldGroup.isMultiple() : false,
                        "",
                        fieldGroupItem.getItem().isRequired(),
                        fieldGroupItem.getItem().getMappedTo(),
                        "");
                for (de.kommone.webservice.seitenbauformular.Row row : fieldGroup.getRows()) {
                    for (Field field : row.getFields()) {
                        TreeOfClasses leftItem = fieldMapping.get(field.getId());
                        if (leftItem == null)
                            throw new IllegalStateException("No left item exists for field " + field.getId() + " (" + field.getLabel() + ")");

                        generateCell("",
                                fieldGroup.isMultiple() != null ? fieldGroup.isMultiple() : false,
                                field.getId(),
                                field.isRequired() != null && field.isRequired(),
                                leftItem.getItem().getMappedTo(),
                                getCodelist(leftItem, rightTree));
                    }
                }
            }
        }
        postGenerate();
    }

    /**
     * @param leftItem  Das linke Baum-Item
     * @param rightTree
     * @return Den Typ Namen, falls vorhanden und ein komplexes XML Objekt,
     * ansonsten einen leeren String
     * @throws ClassNotFoundException wenn die Klasse nicht gefunden wird
     */
    private String getCodelist(TreeOfClasses leftItem, TreeOfClasses rightTree) throws ClassNotFoundException {
        while (leftItem != null && !leftItem.getItem().isComplex()) {
            leftItem = findParentByPath(leftItem.getItem().getMappedTo(), rightTree);
        }
        if (leftItem == null) {
            return "";
        }
        //Komplexe XML Objekte sind mit @XmlType auf Klassen-Ebene annotiert. Die "name" Variable enthält den Typnamen.
        Class<?> rightClass = Class.forName(leftItem.getItem().getTypeName());
        XmlType type = rightClass.getAnnotation(XmlType.class);
        return type != null ? type.name() : "";
    }

    private TreeOfClasses findParentByPath(String mappedTo, TreeOfClasses rightTree) {
        mappedTo = reducePath(mappedTo);
        return TreeUtils.findItemByPath(mappedTo, rightTree, false);
    }

    private String reducePath(String mappedTo) {
        String[] elements = mappedTo.split("->");
        String result = "";
        for (int i = 0; i < elements.length - 1; i++) {
            result += (i == 0) ? elements[i] : "->" + elements[i];
        }
        return result;
    }

    /**
     * Setzt die automatische Spaltenbreite - kann u.U. langsam sein
     */
    private void postGenerate() {
        for (int n = 0; n < 6; n++) {
            sheet.autoSizeColumn(n);
        }
    }

    /**
     * Generiert eine Zelle mit den geforderten Daten
     *
     * @param feldgruppenId Die FeldgruppenId wie im Seitenbau Formular
     * @param multi         Wenn die Feldgruppe eine Liste ist
     * @param feldId        die ID des Feldes
     * @param pflicht       Ob das Feld required ist
     * @param xoevPath      Das xsd Pfad
     * @param codeliste     Die xrepository codeliste
     */
    private void generateCell(String feldgruppenId, boolean multi, String feldId, boolean pflicht, String xoevPath, String codeliste) {
        Row row = sheet.createRow(sheet.getLastRowNum() + 1);
        row.createCell(0).setCellValue(feldgruppenId);
        row.createCell(1).setCellValue(feldId);
        row.createCell(2).setCellValue(multi ? "Ja" : "Nein");
        row.createCell(3).setCellValue(pflicht ? "Ja" : "Nein");
        row.createCell(4).setCellValue(xoevPath);
        row.createCell(5).setCellValue(codeliste);
    }


    /**
     * Schreibt das aktuelle Worksheet als Excel-Datei
     *
     * @param file Der Pfad der .xslx Datei
     * @throws IOException Wenn die Datei nicht geschrieben werden kann
     */
    public void outputFile(File file) throws IOException {
        try (OutputStream out = new FileOutputStream(file)) {
            workbook.write(out);
        }
    }

}
