package mapping.application.pl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.kommone.webservice.seitenbauformular.Seitenbauformular;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import mapping.application.utils.ClassTreeLoader;
import mapping.application.utils.TreeOfClasses;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.model.StyleSpans;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PLController {

    private static final Logger log = Logger.getLogger(PLController.class.getName());

    @FXML
    private Pane mainPane;
    @FXML
    private CodeArea jsonArea;
    @FXML
    private CheckBox cardinalityCheckbox;
    @FXML
    private CheckBox openCheckbox;
    @FXML
    private Label errorLabel;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private TextField standardTextfield;

    private final StringProperty jsonContent = new SimpleStringProperty("");
    private ObservableList<Class<?>> rootClasses = FXCollections.observableArrayList();

    public void setRootClasses(List<Class<?>> list) {
        this.rootClasses.clear();
        if (list != null)
            this.rootClasses.addAll(list);
    }

    @FXML
    public void initialize() {
        jsonContent.addListener((observable, oldValue, newValue) -> jsonArea.replaceText(newValue));
        jsonContent.addListener((observable, oldValue, newValue) -> {
            try {
                new ObjectMapper().readTree(newValue);
                errorLabel.setText("");
            } catch (JsonProcessingException e) {
                log.warning("JSON is invalid: " + e.getLocalizedMessage());
                errorLabel.setText("Ungültiges JSON");
                return;
            }
            try {
                PLExcelGenerator.MAPPER.readValue(newValue, Seitenbauformular.class);
                errorLabel.setText("");
            } catch (JsonProcessingException e) {
                log.warning("JSON is invalid seitenbau-formular: " + e.getLocalizedMessage());
                errorLabel.setText("Ungültiges Seitenbau Formular");
            }
        });
        jsonArea.textProperty().addListener((obs, oldText, newText) -> {
            log.info("Changing style");
            StyleSpans<? extends Collection<String>> styleSpans = generateStyle(newText);
            jsonArea.setStyleSpans(0, styleSpans);
            jsonContent.set(newText);
        });

        mainPane.setOnDragOver(event -> {
            if (event.getDragboard().hasFiles())
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            event.consume();
        });
        mainPane.setOnDragDropped(event -> {
            Dragboard dragboard = event.getDragboard();
            if (!dragboard.hasFiles())
                return;
            importJsonFromPath(dragboard.getFiles().get(0).toPath());
            event.consume();
        });
        rootClasses.addListener((ListChangeListener<? super Class<?>>) c -> {
            String names = rootClasses.stream()
                    .map(Class::getName)
                    .collect(Collectors.joining());
          //  standardTextfield.textProperty().set(String.join(", ", names));
        });
    }

    @FXML
    public void importJson() {
        log.info("Importing JSON");
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("JSON Datei");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JSON Files", "*.json"),
                new FileChooser.ExtensionFilter("All Files", "*.*")
        );
        File file = fileChooser.showOpenDialog(null);
        if (file == null)
            return;
        importJsonFromPath(file.toPath());
    }

    private void importJsonFromPath(Path path) {
        try {
            jsonContent.set(prettyPrintJson(new String(Files.readAllBytes(path))));
        } catch (IOException e) {
            e.printStackTrace();
            showErrorAlert("JSON konnte nicht gelesen werden", e.getLocalizedMessage());
        }
    }

    private String prettyPrintJson(String jsonString) throws JsonProcessingException {
        JsonNode json = new ObjectMapper().readTree(jsonString);
        return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(json);
    }

    private StyleSpans<? extends Collection<String>> generateStyle(String jsonText) {
        return new JsonStyle().highlight(jsonText);
    }

    @FXML
    public void export() {
        log.info("Exporting Excel");
        if (getJson() == null || Objects.equals(getJson(), "")) {
            showErrorAlert("Kein JSON geladen", "Kein JSON wurde geladen");
        }

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Excel Datei");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel Files", "*.xlsx"),
                new FileChooser.ExtensionFilter("All Files", "*.*")
        );
        File file = fileChooser.showSaveDialog(null);
        if (file == null)
            return;
        Path outputFile = file.toPath();

        TreeOfClasses treeOfClasses = null;
        if (!rootClasses.isEmpty()) {
            treeOfClasses = new ClassTreeLoader().loadTreeFromClass(rootClasses.get(0));
        }

        PLExcelGenerator PLExcelGenerator = new PLExcelGenerator(getJson(), treeOfClasses);
        PLExcelGenerator.setGenerateCardinalitySheet(cardinalityCheckbox.isSelected());

        progressIndicator.setVisible(true);
        try {
            PLExcelGenerator.generate();
        } catch (Exception e) {
            e.printStackTrace();
            showErrorAlert("Excel konnte nicht generiert werden", e.getLocalizedMessage());
            progressIndicator.setVisible(false);
            return;
        }

        try {
            Files.write(outputFile, PLExcelGenerator.getExcelOutput());
        } catch (Exception e) {
            e.printStackTrace();
            showErrorAlert("Datei konnte nicht exportiert werden", e.getLocalizedMessage());
        }

        progressIndicator.setVisible(false);
        if (openCheckbox.isSelected()) {
            try {
                Desktop.getDesktop().open(outputFile.toFile());
            } catch (IOException e) {
                e.printStackTrace();
                showErrorAlert("Datei konnte nicht geöffnet werden", e.getLocalizedMessage());
            }
        }
    }

    public String getJson() {
        return jsonContent.getValue();
    }

    private void showErrorAlert(String header, String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message);
        alert.setHeaderText(header);
        alert.showAndWait();
    }
}
