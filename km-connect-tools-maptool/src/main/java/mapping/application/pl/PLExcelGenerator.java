package mapping.application.pl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import de.kommone.webservice.seitenbauformular.*;
import de.kommone.webservice.xfallcontainer.Index;
import mapping.application.utils.TreeOfClasses;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PLExcelGenerator {

    private static final Logger log = Logger.getLogger(PLExcelGenerator.class.getName());

    private static final List<String> jsonSheetCellNames = Arrays.asList(
            "Formularabschnitt", "ID Formulardesigner Service-BW", "Beschreibung/Label", "Format/Art",
            "Feldbelegung Label", "Feldbelegung ID (Wert)", "Feldbelegung ID Mapping ", "Kardinalität",
            "Erläuterung", "Sichtbarkeit", "Validierung", "complextype.attribute", "Erläuterung Fachverfahrensanbindung ",
            "Zeichenlänge", "XÖV-Datentypen", "XÖV-Codelisten", "Verweis zur xRepository-Liste (URI)"
    );

    private static final List<String> standardSheetCellNames = Arrays.asList(
            "Beschreibung/Label", "Typ", "Anzahl", "Mapping zu Datenfeld", "Erläuterung",
            "Prozessparameter", "Prozessparameter Erläuterung", "Erläuterung", "Sichtbarkeit", "Hilfetext (Info-Button)",
            "Individuelle Fehlermeldung bei Pflichtfeldverletzung", "Platzhaltertext",
            "Individuelle Fehlermeldung bei falschem Eingabeformat", "Validierung",
            "Standardisierungsvorhaben complextype.attribute", "Erläuterung Fachverfahrensanbindung", "Zeichenlänge",
            "XÖV-Datentypen", "XÖV-Codelisten", "Verweis zur xRepository-Liste (URI)"
    );

    private static final List<String> exkludedTypes = Collections.singletonList("H1");

    private static final Map<String, String> TYPEMAP = ImmutableMap.<String, String>builder()
            .put("RADIO_BUTTONS", "Radio-Button")
            .put("STRING", "Textfeld")
            .put("TEXT", "Textfeld")
            .put("DROPDOWN_SINGLE_SELECT", "Select-Feld")
            .put("SINGLE_CHECKBOX", "Einfache Checkbox")
            .put("DROPDOWN_SINGLE_SELECT_AJAX", "")
            .put("STRING_AJAX_AUTOCOMPLETE", "Textfeld")
            .put("CHECKBOX", "Einfache Checkbox")
            .put("TEXTAREA", "Textbereich")
            .put("FILE", "Upload").build();
    //.put("H1")

    public static final ObjectMapper MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);


    private final String json;
    private final TreeOfClasses treeOfClasses;

    private Workbook workbook;

    public boolean isGenerateCardinalitySheet() {
        return generateCardinalitySheet;
    }

    public void setGenerateCardinalitySheet(boolean generateCardinalitySheet) {
        this.generateCardinalitySheet = generateCardinalitySheet;
    }

    private boolean generateCardinalitySheet;

    public PLExcelGenerator(String json, TreeOfClasses treeOfClasses) {
        this.json = json;
        this.treeOfClasses = treeOfClasses;
    }

    public void generate() throws JsonProcessingException {
        workbook = new XSSFWorkbook();
        generateJsonMapping();
        generateMessageMapping();

        if (generateCardinalitySheet)
            generateCardinalitySheet();
    }

    private void generateMessageMapping() {
        Sheet sheet = workbook.createSheet("standard");
        Row topRow = sheet.createRow(0);

        createCell(topRow, 0, "ComplexType", styleDark());
        createCell(topRow, 1, "Kindelement", styleDark());

        for (int n = 2; n < getMaximumDepth(treeOfClasses); n++) {
            createCell(topRow, n, "", styleDark());
        }

        for (int n = getMaximumDepth(treeOfClasses); n < standardSheetCellNames.size(); n++) {
            createCell(topRow, n, standardSheetCellNames.get(n), styleDark());
        }

        if (treeOfClasses == null) {
            log.info("No message rows provided - skip generation");
            return;
        }
        workbook.setSheetName(workbook.getSheetIndex(sheet), treeOfClasses.getItem().getAttributeName());
        log.info("Generating " + countRecursive(treeOfClasses, TreeOfClasses::getChildren) + " message rows");
        int rowNr = 1;
        for (TreeOfClasses child : treeOfClasses.getChildren()) {
            rowNr = generateMessageRow(child, new ArrayList<>(), sheet, rowNr);
        }
        for (int n = 0; n < getMaximumDepth(treeOfClasses); n++) {
            autoMergeRegions(sheet, n, false);
            sheet.autoSizeColumn(n);
        }
    }

    private int generateMessageRow(TreeOfClasses tree, List<TreeOfClasses> parents, Sheet sheet, int rowNr) {
        Row row = sheet.createRow(rowNr);
        rowNr++;
        int depth = getMaximumDepth(treeOfClasses);

        for (int n = 0; n < parents.size(); n++) {
            createCell(row, n, parents.get(n).getItem().getAttributeName(), styleCentered());
        }
        createCell(row, parents.size(), tree.getItem().getAttributeName(), styleCentered());

        createCell(row, 2 + depth, getType(tree));
        createCell(row, 4 + depth, getCardinality(tree));

        for (TreeOfClasses child : tree.getChildren()) {
            List<TreeOfClasses> childParents = new ArrayList<>(parents);
            childParents.add(tree);
            rowNr = generateMessageRow(child, childParents, sheet, rowNr);
        }


        return rowNr;
    }

    private void autoMergeRegions(Sheet sheet, int column, boolean mergeSpaces) {
        int startCell = 0;
        String value = sheet.getRow(0).getCell(column).getStringCellValue();
        for (int i = 0; i < sheet.getLastRowNum(); i += 1) {
            Cell cell = sheet.getRow(i).getCell(column);
            String newValue = cell != null ? cell.getStringCellValue() : null;

            if (Objects.equals(value, newValue) && i < sheet.getLastRowNum() - 1
                    && (mergeSpaces || !Objects.equals(newValue, ""))) {
                continue;
            }
            if (i - startCell >= 2) {
                log.info("Merged Region " + startCell + "->" + i);
                sheet.addMergedRegion(new CellRangeAddress(startCell, i - 1, column, column));
            }
            startCell = i;
            value = newValue;
        }
    }

    private String getType(TreeOfClasses toc) {
        if (toc.getItem().getTypeName() != null)
            return toc.getItem().getTypeName();
        return "";
    }

    private int getMaximumDepth(TreeOfClasses toc) {
        int depth = 0;
        if (toc == null)
            return depth;
        for (TreeOfClasses child : toc.getChildren()) {
            depth = Math.max(depth, getMaximumDepth(child));
        }
        depth++;
        return depth;
    }

    private <T> int countRecursive(T obj, Function<T, Collection<T>> childFunction) {
        int count = 1;
        for (T child : childFunction.apply(obj)) {
            count += countRecursive(child, childFunction);
        }
        return count;
    }

    private void generateJsonMapping() throws JsonProcessingException {
        log.info("Generate JSON mapping");
        Sheet sheet = workbook.createSheet("SACHBEARBEITERSEITE");
        Row topRow = sheet.createRow(0);
        for (int n = 0; n < jsonSheetCellNames.size(); n++) {
            createCell(topRow, n, jsonSheetCellNames.get(n), styleDark());
        }
        Seitenbauformular seitenbauformular = MAPPER.readValue(json, Seitenbauformular.class);

        int rowNr = 1;
        for (Section section : seitenbauformular.getSections()) {
            int sectionStartRownr = rowNr;
            for (Fieldgroup fieldgroup : section.getFieldGroups()) {
                for (de.kommone.webservice.seitenbauformular.Row row : fieldgroup.getRows()) {
                    for (Field field : row.getFields()) {
                        if (exkludedTypes.contains(field.getType()))
                            continue;

                        if (!field.getPossibleValues().isEmpty()) {
                            for (PossibleValue possibleValue : field.getPossibleValues()) {
                                Row pvRow = createFieldRow(sheet, rowNr, section, fieldgroup, field);
                                createCell(pvRow, 4, possibleValue.getLabel());
                                createCell(pvRow, 5, possibleValue.getValue());
                                rowNr++;
                            }
                            if (field.getPossibleValues().size() > 1) {
                                sheet.addMergedRegion(new CellRangeAddress(rowNr - field.getPossibleValues().size(), rowNr - 1, 1, 1));
                                sheet.addMergedRegion(new CellRangeAddress(rowNr - field.getPossibleValues().size(), rowNr - 1, 2, 2));
                                sheet.addMergedRegion(new CellRangeAddress(rowNr - field.getPossibleValues().size(), rowNr - 1, 3, 3));
                                sheet.addMergedRegion(new CellRangeAddress(rowNr - field.getPossibleValues().size(), rowNr - 1, 7, 7));
                            }
                        } else {
                            createFieldRow(sheet, rowNr, section, fieldgroup, field);
                            rowNr++;
                        }
                    }
                }
            }
            if (rowNr - sectionStartRownr > 1)
                sheet.addMergedRegion(new CellRangeAddress(sectionStartRownr, rowNr - 1, 0, 0));
        }

        for (int n = 0; n < jsonSheetCellNames.size(); n++) {
            if (n != 2 && n != 4)
                sheet.autoSizeColumn(n, true);
        }

    }

    private Row createFieldRow(Sheet sheet, int rowNr, Section section, Fieldgroup fieldGroup, Field field) {
        Row fieldRow = sheet.createRow(rowNr);
        createCell(fieldRow, 0, section.getTitle(), styleCentered());
        createCell(fieldRow, 1, field.getId(), styleCentered());
        if (field.getLabel() != null)
            createCell(fieldRow, 2, field.getLabel().replaceAll("<[^>]*>", ""), styleCentered());
        createCell(fieldRow, 3, getFieldType(field.getType()), styleCentered());
        createCell(fieldRow, 7, getCardinality(fieldGroup, field));
        createCell(fieldRow, 8, field.getHelptext());
        createCell(fieldRow, 10, getJsonValidatioNString(field.getValidationRules()));
        return fieldRow;
    }

    private String getJsonValidatioNString(List<ValidationRule> validationRules) {
        if (validationRules == null)
            return "";
        return validationRules.stream()
                .map(v -> {
                    if (v.getRegex() != null)
                        return v.getRegex();
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.joining());
    }

    private Cell createCell(Row row, int column, String value) {
        return createCell(row, column, value, null);
    }

    private Cell createCell(Row row, int column, String value, CellStyle style) {
        Cell cell = row.createCell(column);
        cell.setCellValue(value);
        if (style != null)
            cell.setCellStyle(style);
        return cell;
    }

    private String getFieldType(String type) {
        return TYPEMAP.getOrDefault(type, type);
    }

    private String getCardinality(Fieldgroup fieldgroup, Field field) {
        boolean required = Boolean.TRUE.equals(field.isRequired());
        boolean multi = Boolean.TRUE.equals(fieldgroup.isMultiple());
        if (field.getPossibleValues().isEmpty() && required)
            return "";

        return (required ? "1" : "0")
                + ":"
                + (multi ? "n" : "1");
    }

    private String getCardinality(TreeOfClasses treeOfClasses) {
        boolean required = treeOfClasses.getItem().isRequired();
        boolean multi = treeOfClasses.getItem().isList();

        return (required ? "1" : "0")
                + ":"
                + (multi ? "n" : "1");
    }

    private void generateCardinalitySheet() {
        Sheet sheet = workbook.createSheet("Erläuterung Kardinalität");
        Row row = sheet.createRow(2);
        createCell(row, 0, "Kardinalität: Angabe über die Anzahl der an einer Beziehung beteiligten Entitäten (Einheiten)", styleBold());

        row = sheet.createRow(3);
        row.createCell(0).setCellValue("1:1");
        row.createCell(1).setCellValue("Das Datenfeld ist eine Pflichtangabe");
        row = sheet.createRow(4);
        row.createCell(0).setCellValue("0:1");
        row.createCell(1).setCellValue("Das Datenfeld ist optional");
        row = sheet.createRow(5);
        row.createCell(0).setCellValue("1:n");
        row.createCell(1).setCellValue("Das Datenfeld ist eine Pflichtangabe, es muss mindestens eine Einheit und es können beliebig viele Einheiten angegeben werden (häufig in Form von Akkordeons) - z.B. Waffenbesitzkarte: Waffen");
        row = sheet.createRow(6);
        row.createCell(0).setCellValue("0:n");
        row.createCell(1).setCellValue("Das Datenfeld ist optional, es kann keine Einheit oder beliebig viele Einheiten angegeben werden (häufig in Form eines Akkordeons) - diese Variante kommt nur selten vor");
    }

    private CellStyle styleBold() {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        return style;
    }

    private CellStyle styleCentered() {
        CellStyle style = workbook.createCellStyle();
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        return style;
    }

    private CellStyle styleDark() {
        CellStyle style = styleBold();
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
        workbook.getFontAt(style.getFontIndex()).setColor(IndexedColors.WHITE.getIndex());
        return style;
    }

    public byte[] getExcelOutput() throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        workbook.write(outputStream);
        return outputStream.toByteArray();
    }

}
