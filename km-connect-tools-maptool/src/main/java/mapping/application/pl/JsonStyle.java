package mapping.application.pl;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.google.common.collect.Lists;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

public class JsonStyle {

    private static final Logger log = Logger.getLogger(JsonStyle.class.getName());

    private final JsonFactory jsonFactory;

    public JsonStyle() {
        jsonFactory = new JsonFactory();
    }

    public StyleSpans<Collection<String>> highlight(String code) {
        List<Match> matches = new ArrayList<>();

        try (JsonParser parser = jsonFactory.createParser(code)) {
            while (!parser.isClosed()) {
                JsonToken jsonToken = parser.nextToken();
                int start = (int) parser.getTokenLocation().getCharOffset();
                int end = start + parser.getTextLength();


                // Because getTextLength() does contain the surrounding ""
                if (jsonToken == JsonToken.VALUE_STRING || jsonToken == JsonToken.FIELD_NAME) {
                    end += 2;
                }

                String className = jsonTokenToClassName(jsonToken, parser.getText());
                if (!className.isEmpty()) {
                    Match m = new Match(className, start, end);
                    matches.add(m);
                }
            }
        } catch (IOException e) {
            log.warning("Could not parse style: " + e.getLocalizedMessage());
        }

        StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();
        int lastPos = 0;
        for (Match match : matches) {
            // Fill the gaps, since Style Spans need to be contiguous.
            if (match.start > lastPos) {
                int length = match.start - lastPos;
                spansBuilder.add(Collections.emptyList(), length);
            }

            int length = match.end - match.start;
            spansBuilder.add(Collections.singleton(match.kind), length);
            lastPos = match.end;
        }

        if (lastPos == 0) {
            spansBuilder.add(Collections.emptyList(), code.length());
        }

        return spansBuilder.create();
    }

    public static String jsonTokenToClassName(JsonToken jsonToken, String text) {
        if (jsonToken == null) {
            return "";
        }
        List<String> keys = Lists.newArrayList("sections", "fieldGroups", "rows", "fields");

        if (jsonToken == JsonToken.FIELD_NAME && keys.contains(text)) {
            return "json-" + text;
        }

        switch (jsonToken) {
            case FIELD_NAME:
                return "json-property";
            case VALUE_STRING:
                return "json-string";
            case VALUE_NUMBER_FLOAT:
            case VALUE_NUMBER_INT:
                return "json-number";
            default:
                return "";
        }
    }

    public static class Match implements Comparable<Match> {
        public final String kind;
        public final int start;
        public final int end;

        public Match(String kind, int start, int end) {
            this.kind = kind;
            this.start = start;
            this.end = end;
        }

        @Override
        public int compareTo(Match o) {
            return Integer.compare(start, o.start);
        }

        @Override
        public String toString() {
            return "Match{" +
                    "kind='" + kind + '\'' +
                    ", start=" + start +
                    ", end=" + end +
                    '}';
        }
    }

}
