package mapping.application;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import mapping.application.model.AppTable;
import mapping.application.model.ErrorType;
import mapping.application.pl.PLController;
import mapping.application.utils.*;
import mapping.application.utils.javafx.MappableItem;
import mapping.application.utils.javafx.MappableItemCellFactory;
import mapping.application.utils.javafx.TreeViewHelper;
import mapping.application.utils.javafx.XSDHelper;
import mapping.application.utils.xsd.XSDParser;
import org.slf4j.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class MappingApp extends Application {


    public final static String PACKAGE_NAME = "de.kommone.kmc.webservice";
    private final static int MAX_SIZE_OF_OPENED_FILES = 10;
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(MappingApp.class);

    @FXML
    private TreeView<MappableItem> generatedTreeView;
    @FXML
    private TreeView<MappableItem> templateTreeView;
    @FXML
    private TreeView<MappableItem> schemaTreeView;
    @FXML
    private Button generateButton;
    @FXML
    private TextField nameTextArea;
    @FXML
    private Text genNameHeader;
    @FXML
    private Text schemaNameHeader;
    @FXML
    private TextArea logTextArea;
    @FXML
    private CheckBox listCheckBox;
    @FXML
    TableView<AppTable> attributeTable;
    @FXML
    private TableColumn<AppTable, ?> attributeColumn;
    @FXML
    private TableColumn<AppTable, ?> missingColumn;
    @FXML
    private TextField renameTextField;
    @FXML
    private TextField searchTemplateField;
    @FXML
    private Button connectButton;
    @FXML
    private TextArea logBox;

    private static Stage myStage;
    private TextField messageTextField;
    private Button messageButton;
    private MenuButton menuButton;
    private Text messageErrorField;
    private ComboBox<Class<?>> comboBox;
    private File workingFile = null;
    private ObservableList<Class<?>> obsList;
    private ArrayList<Class<?>> classList;
    private ArrayList<Class<?>> schemaRootClasses;
    private ArrayList<TreeItem<MappableItem>> storageList;
    private final List<String> notMappedAttributes = new ArrayList<>();
    private Converter converter;

    private static final String CURRENTLY_LOADED_FILES = "Loaded Files:";
    private static final String CURRENTLY_LOADED_DIRECTORY = "Loaded Directory:";
    private static final String CURRENTLY_SAVED_DIRECTORY = "Saved Directory:";
    private static final String POJO_DEFAULT = "POJO Message";
    private static final String SCHEME_NAME_DEFAULT = "XOEV/XFall Message";
    private static final String TITLE = "KM Connect Tools";

    // die zuletzt besuchten Pfade / Ordner werden als Properties gespeichert,
    // damit man einen ChangeListener installieren kann, der die Persistierung
    // triggert
    private final ObservableList<String> currentlyLoadedFiles = FXCollections.observableArrayList();
    private final StringProperty currentlyLoadedDirectory = new SimpleStringProperty("");
    private final StringProperty currentlySavedDirectory = new SimpleStringProperty("");

    private final Map<String, String> standards = new TreeMap<>();

    private Properties configProperties = null;

    public MappingApp() {
    }

    /*
     * The initialize method is automatically invoked by the FXMLLoader
     */
    @FXML
    public void initialize() throws
            SecurityException {

        storageList = new ArrayList<>();
        readConfigFile();
        initMap();
        populateFileMenu();
        populateTemplate();
        connectButton.setOnAction(event -> new TreeViewHelper().mapTrees(getSelectedGeneratedClass(), schemaTreeView.getRoot(), true, null));
        schemaTreeView.setCellFactory(new MappableItemCellFactory(true, null));
        generatedTreeView.setCellFactory(new MappableItemCellFactory(false, connectButton));
        generatedTreeView.setEditable(true);
        System.setProperty("http.proxyHost", "squid.dzbw.de");
        System.setProperty("http.proxyPort", "3128");
    }

    /**
     * Hier werden die unterstützten Standards eingetragen. Zu jedem Standard
     * gibt es einen Package-Namen, aus dem die JAXB-Klassen geladen werden
     */
    private void initMap() {
        standards.put("xbau", "de.kommone.webservice.xbau");
        standards.put("xjustiz", "de.kommone.webservice.xjustiz");
        standards.put("Wee", "de.kommone.webservice.wee");
        standards.put("xFallHund", "de.kommone.webservice.xfallHund");
        standards.put("diwo", "de.kommone.webservice.diwo");
        standards.put("diwo_neu", "de.kommone.webservice.diwo.v4_1");
        standards.put("xmeld", "de.kommone.webservice.xmeld.vW");
        standards.put("xmeldv30", "de.kommone.webservice.xmeld.v30");
        standards.put("laif", "de.kommone.webservice.laif");
        standards.put("xauslaender", "de.kommone.webservice.xauslaender");
        standards.put("xpersonenstandV174", "de.kommone.webservice.xpersonenstand.v174");
        standards.put("xpersonenstandV175", "de.kommone.webservice.xpersonenstand.v175");
        standards.put("xpersonenstandV176", "de.kommone.webservice.xpersonenstand.v176");
        standards.put("Ver", "de.kommone.webservice.ver");
        standards.put("xbauV22", "de.kommone.webservice.xbau.v22");
        standards.put("xbauV23", "de.kommone.webservice.xbau.v23");
        standards.put("UVOProsoz", "de.kommone.webservice.uvo");
        standards.put("UVOLaemmkomm", "de.kommone.webservice.uvo_laemkomm");
        standards.put("xmeldVy", "de.kommone.webservice.xmeld.vY");
        standards.put("xfallcontainer", "de.kommone.webservice.xfallcontainer");
        standards.put("xtaV1", "de.kommone.xta.ws1");
        standards.put("xtaV3", "de.kommone.xta.ws3");
        standards.put("patras", "de.kommone.patras");
        standards.put("aus-alva", "de.kommone.webservice.aus.alva");
        standards.put("sstr-alva-antrag", "de.kommone.webservice.sstr.alva.antrag");
        standards.put("sstr-alva-verlaengerung", "de.kommone.webservice.sstr.alva.verlaengerung");
        standards.put("sstr-alva-endemeldung", "de.kommone.webservice.sstr.alva.endemeldung");
        standards.put("xfamilie", "de.kommone.webservice.xfamilie.v020");
        standards.put("seitenbau", "de.kommone.webservice.seitenbauformular");
        standards.put("kojo", "de.kommone.webservice.kojo");
        standards.put("octoware", "de.kommone.webservice.infi");
    }

    /*
     * Main Function
     */
    public static void main(String[] args) {
        Application.launch(args);
    }

    /*
     * Starts the application scene
     */
    @Override
    public void start(Stage stage) throws IOException {

        final BorderPane application = new FXMLLoader(getClass().getClassLoader().getResource(
                "MessageNumberArea.fxml")).load();

        final Scene scene = new Scene(application, 1550, 900);
        scene.setFill(Color.LIGHTGRAY);

        myStage = stage;
        myStage.setScene(scene);
        myStage.setTitle(TITLE);
        myStage.show();
    }

    /**
     * Lesen der Konfigurationsdatei (hier sind die zuletzt besuchten Pfade /
     * geöffneten Dateien des Benutzers gespeichert)
     */
    private void readConfigFile() {
        try (InputStream input = new FileInputStream(prepareFileToSaveProgress().getAbsolutePath())) {
            configProperties = new Properties();
            // load a properties file
            configProperties.load(input);
            initConfig();
        } catch (IOException e) {
            UiDialogTools.showException(e, e.getMessage());
        }
    }

    /**
     * Einlesen aus Properties-File, die im User-Verzeichnis gespeichert ist.
     * Weiterhin werden hier Listener registriert, die die Konfig- Datei
     * abspeichern, wenn sich ein Attribut verändert. Das hat den positiven
     * Effekt, dass die Methode {@link #saveToConfigFile(File)} nicht explizit
     * im Code aufgerufen werden muss
     */
    private void initConfig() {

        // zuerst werden die Daten aus der Konfig-File geladen, WICHTIG ist,
        // dass die Listener erst danach installiert werden
        currentlyLoadedFiles.addAll(getCurrentlyLoadedFileFromProperties());
        currentlyLoadedDirectory.setValue(configProperties.getProperty(CURRENTLY_LOADED_DIRECTORY));
        currentlySavedDirectory.setValue(configProperties.getProperty(CURRENTLY_SAVED_DIRECTORY));

        // die zuletzt gespeicherten Files
        currentlyLoadedFiles.addListener((ListChangeListener<String>) arg0 -> saveToConfigFile(prepareFileToSaveProgress()));

        // das Directory aus dem zuletzt geladen wurde
        currentlyLoadedDirectory.addListener(addTextListener());

        // das Directory in das zuletzt gespeichert wurde
        currentlySavedDirectory.addListener(addTextListener());
    }

    /**
     * Ändert sich der Pfad zu der zuletzt geöffneten / gespeicherten Datei,
     * wird der Pfad in der Properties-File aktualisiert, sodass er beim
     * nächsten Einlesen zur Verfügung steht
     *
     * @return
     */
    private ChangeListener<String> addTextListener() {
        return (observable, oldValue, newValue) -> {
            if (newValue != null && !newValue.isEmpty()) {
                saveToConfigFile(prepareFileToSaveProgress());
            }
        };
    }

    /**
     * Die zuletzt geladenen Dateien sind mit einem ; separiert
     *
     * @return
     */
    private List<String> getCurrentlyLoadedFileFromProperties() {
        if (configProperties.getProperty(CURRENTLY_LOADED_FILES) != null) {
            String[] files = configProperties.getProperty(CURRENTLY_LOADED_FILES).split(";");
            if (files.length == 0 || files[0].isEmpty()) {
                return new ArrayList<>();
            }
            return Arrays.asList(files);
        }
        return new ArrayList<>();
    }

    /**
     * Für jede zusätzlich geöffnete Datei muss ein weiterer Pfad- Eintrag in
     * die Menüleiste geschrieben werden, damit diese Datei dann über eine
     * Shortcut-Funktion schnell wieder geöffnet werden kann
     *
     * @param file
     */
    public void addFileMenuItem(File file) {

        // 1. File ist schon vorhanden -> Reordering
        if (currentlyLoadedFiles.contains(file.getPath())) {
            currentlyLoadedFiles.remove(file.getPath());
            currentlyLoadedFiles.add(0, file.getPath());
        } // 2. File ist noch nicht vorhanden -> vorne einfügen
        else {
            currentlyLoadedFiles.add(0, file.getPath());
            if (currentlyLoadedFiles.size() > MAX_SIZE_OF_OPENED_FILES) {
                currentlyLoadedFiles.remove(currentlyLoadedFiles.size() - 1);
            }
        }

        populateFileMenu();
    }

    /**
     * Speichert die Directories und die zuletzt geladenen / gespeicherten Files
     * in einer Datei
     *
     * @param file
     */
    private void
    saveToConfigFile(File file) {

        try (OutputStream output = new FileOutputStream(file.getAbsolutePath())) {

            configProperties = new Properties();

            // set the properties value
            configProperties.setProperty(CURRENTLY_LOADED_FILES, getCurrentlyLoadedFiles());
            if (currentlyLoadedDirectory.get() != null) {
                configProperties.setProperty(CURRENTLY_LOADED_DIRECTORY, currentlyLoadedDirectory.get());
            }
            if (currentlySavedDirectory.get() != null) {
                configProperties.setProperty(CURRENTLY_SAVED_DIRECTORY, currentlySavedDirectory.get());
            }

            // save properties to project root folder
            configProperties.store(output, null);

        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    /**
     * Bereitet die zuletzt geladenen Files für die Properties-Datei auf. Diese
     * werden strichpunkt-separiert gespeichert
     *
     * @return
     */
    private String getCurrentlyLoadedFiles() {
        String result = "";
        boolean firstEntry = true;
        for (String entry : currentlyLoadedFiles) {
            result += (firstEntry ? "" : ";") + entry;
            firstEntry = false;
        }
        return result;
    }

    /**
     * Methode, die den übergebenen Inhalt in die übergebene File schreibt
     *
     * @param file
     * @param content
     */
    private void saveContentToFile(File file, String content) {
        try (PrintWriter writer = new PrintWriter(file)) {
            writer.println(content);
        } catch (FileNotFoundException e) {
            UiDialogTools.showException(e, e.getMessage());
        }
    }

    /**
     * Im File-Menu werden die Pfade der zuletzt geöffnete Dateien abgelegt.
     * Hier muss drauf geachtet werden, dass die anderen Menu-Items nicht
     * gelöscht werden (also die, die keinen Shortcut zu einer zuletzt
     * geöffneten Datei darstellen)
     */
    public void populateFileMenu() {

        // alle Items erstmal weg, die einen Pfad enthalten
        newMessageMenu.getItems().removeIf(item -> item.getText() != null && item.getText().contains(" - "));

        int counter = 1;
        for (String line : currentlyLoadedFiles) {

            MenuItem pathItem = new MenuItem();
            pathItem.setText("" + (counter++) + " - " + line);
            newMessageMenu.getItems().add(pathItem);

            pathItem.setOnAction(e -> loadProgress(new File(line)));
        }

    }

    /**
     * Die Methode erzeugt im Home-Verzeichnis des Users eine Datei, die die
     * zuletzt geöffneten Pfade und die zuletzt geöffneten Dateien abspeichert.
     * Dieses Vorgehen ermöglicht, dass auch nach einem neuen Build oder einem
     * Versionsupgrade zuletzt geöffnete Dateien / Pfade nicht verloren gehen
     *
     * @return
     */
    private File prepareFileToSaveProgress() {

        String resource = System.getProperty("user.home");
        resource += File.separator + ".mappingtool/config.properties";
        File savedFiles = new File(resource);

        //noinspection StatementWithEmptyBody
        if (savedFiles.exists()) {
            // existiert bereits -> nichts zu tun
        } else if (savedFiles.getParentFile().mkdirs()) {
            // erst Parent erzeugen, dann File
            createNewFile(savedFiles);
        } else {
            createNewFile(savedFiles);
        }
        return savedFiles;
    }

    private void createNewFile(File savedFiles, String... errormessage) {
        try {
            //noinspection ResultOfMethodCallIgnored
            savedFiles.createNewFile();
        } catch (IOException e) {
            // wenn etwas schief geht, Fehlermeldung
            UiDialogTools.showException(e, errormessage != null && errormessage.length > 0 ? errormessage[0] : e.getMessage());
        }
    }

    @FXML
    public Menu newMessageMenu;

    /*
     * Validates the currently mapped message
     */
    @FXML
    public void validateProgress() {
        initTable();
        validateMapping();
        logErrors();
    }

    private void logErrors() {
        if (attributeTable.getItems().isEmpty()) {
            logMessage("Mapping ist valide");
        } else {
            logMessage("Es gibt " + attributeTable.getItems().size() + " Probleme mit dem Mapping");
        }
        attributeTable.setVisible(!attributeTable.getItems().isEmpty());

    }

    private void initTable() {
        attributeTable.getItems().clear();

        // Double click of a row in the table - selects the relevant items
        attributeTable.setRowFactory(tv -> {

            TableRow<AppTable> row = new TableRow<>();
            row.setOnMouseClicked(event -> selectMissingAttributes(row, event));
            return row;
        });

        attributeColumn.setCellValueFactory(new PropertyValueFactory<>("missingName"));
        missingColumn.setCellValueFactory(new PropertyValueFactory<>("missingPath"));
    }

    /**
     * <ol>
     * <li>Schritt : Prüfung, ob alle Pflichtfelder gesetzt sind</li>
     * <li>Schritt : Prüfung, ob die gemappten Objekte richtig gemappt
     * wurden</li>
     * </ol>
     */
    private void validateMapping() {
        resetCheckedValues(schemaTreeView.getRoot());
        checkRequiredFieldsSet(schemaTreeView.getRoot(), true);
        validateMappings(generatedTreeView.getRoot());
    }

    /**
     * Prüft, ob alle Pflichtfelder gesetzt sind. <b>Wichtig:</b> Die Prüfung
     * erfolgt rekursiv durch das XSD (Baum auf der rechten Seite) und sucht
     * nach ALLEN Pflichtfeldern. Der Algorithmus stoppt bei optionalen
     * Teilbäumen <b>nur</b>, wenn der optionale Baum nicht gemappt ist. Ist der
     * optionale Baum gemappt, d. h., er hat mindestens einen Teilbaum, der
     * gemappt ist, wird weitergeprüft. Dazu muss man wissen, dass beim Einlesen
     * des Baumes <b>alle</b> Elternknoten eines Kindknotens auf gemappt gesetzt
     * werden, sollte Dieser gemappt sein
     *
     * @param root
     * @param isRoot
     */
    private void checkRequiredFieldsSet(TreeItem<MappableItem> root, boolean isRoot) {

        if (!isRoot) {
            MappableItem item = root.getValue();
            if (!item.isChecked()) {
                item.setChecked(true);
            }
            boolean required = root.getValue().getItem().isRequired();
            boolean mapped = root.getValue().isMapped();
            // notwendig und nicht gemappt -> Fehler
            if (required && !mapped) {
                TreeItem<MappableItem> leftFather = getLastMappedLeftFather(root.getParent());
                AppTable missingValue = new AppTable(leftFather, root, root.getValue().getItem().getAttributeName(),
                        "Missing Attribute", ErrorType.MISSING);
                attributeTable.getItems().add(missingValue);

            }
            // optional und nicht gemappt -> nicht notwendig
            else if (!required && !mapped) {
                // Stopp, keine weitere Prüfung von Teilbäumen
                return;
            } else {
                // required und mapped -> rekursiv weiter
                // !required und mapped -> rekursiv weiter
            }
        }


        // rekursiver Aufruf, wenn
        for (TreeItem<MappableItem> child : root.getChildren()) {
            checkRequiredFieldsSet(child, false);
        }

    }

    /**
     * Bekommt einen (nicht gemappten) rechten Teilbaum (-> XSD) übergeben und
     * sucht für diesen Teilbaum den letzten Vater, der gemappt ist
     *
     * @param tree
     * @return
     */
    private TreeItem<MappableItem> getLastMappedLeftFather(TreeItem<MappableItem> tree) {
        TreeItem<MappableItem> parent = tree;
        while (parent != null) {
            if (!parent.getValue().getMappedTreeViews().isEmpty()) {
                // bei mehreren Mappings trotzdem den ersten gemappten
                // zurückliefern
                return parent.getValue().getMappedTreeViews().get(0);
            }
            parent = parent.getParent();
        }
        log.warn("Mapped tree views are empty for " + tree.getValue());
        return null;
    }

    private void resetCheckedValues(TreeItem<MappableItem> root) {

        // For template objects that have not been mapped
        if (root.getValue().isMissing() || root.getValue().isChecked()) {
            root.getValue().setMissing(false);
            root.getValue().setChecked(false);
        }

        // rekursiver Aufruf
        for (TreeItem<MappableItem> subChild : root.getChildren()) {
            resetCheckedValues(subChild);
        }

    }

    /**
     * Die Funktion validiert das Mapping. Sie durchläuft den linken Teilbaum
     * und prüft für jeden einfachen Knoten, ob der gemappte Knoten (auf der
     * rechten Seite, also in der XSD) ein Pflichtknoten ist und einen
     * optionalen Vater hat, der nicht gemappt ist. Ist diese Bedingung erfüllt,
     * wird ein invalides XML erzeugt, wenn der Knoten auf der linken Seite
     * (also der Knoten, der hier geprüft wird) null ist
     *
     * @param child
     */
    private void validateMappings(TreeItem<MappableItem> child) {

        // für einfache Knoten prüfen, ob sie richtig gemappt wurden
        if (!child.getValue().getItem().isComplex()) {
            TreeItem<MappableItem> mapped = child.getValue().getMappedTreeview();
            if (parentContainsAtLeastOneRequiredChild(mapped)) {
                // suche einen Vater, auf dem Weg zu Root, der optional ist
                TreeItem<MappableItem> optionalFather = getOptionalFather(mapped);
                // TODO : Prüfung eventuell abschwächen in Bezug auf Element,
                // das sich in einer Liste befindet
                if (optionalFather != null) {
                    // wenn dieser Knoten nicht gemappt ist, handelt es sich um
                    // einen potentiellen Fehler
                    if (optionalFather.getValue().getMappedTreeViews().isEmpty()) {
                        AppTable missingValue = new AppTable(child, optionalFather, optionalFather.getValue().getItem()
                                .getAttributeName(), "Invalid Mapping", ErrorType.WRONG_MAPPING);
                        attributeTable.getItems().add(missingValue);
                    } else {
                        // Es gibt den unwahrscheinlichen Fall, dass
                        // zwischenzeitlich ein Mapping vorgenommen wurde,
                        // allerdings der relevante Knoten, der geprüft wird,
                        // aber noch nicht Kindknoten dieses Vaters ist
                        TreeItem<MappableItem> father = optionalFather.getValue().getMappedTreeViews().get(0);
                        if (!containsChild(father, child)) {
                            AppTable missingValue = new AppTable(child, optionalFather, optionalFather.getValue()
                                    .getItem().getAttributeName(), "Invalid Mapping", ErrorType.WRONG_MAPPING);
                            attributeTable.getItems().add(missingValue);
                        }
                    }
                }
            }
        }

        // rekursiver Aufruf
        for (TreeItem<MappableItem> subChild : child.getChildren()) {
            validateMappings(subChild);
        }

    }

    private boolean parentContainsAtLeastOneRequiredChild(TreeItem<MappableItem> mapped) {
        if (mapped != null && mapped.getValue() != null) {
            return mapped.getValue().getItem().isRequired() || hasRequiredSibbling(mapped.getParent());
        }
        return false;
    }

    private boolean hasRequiredSibbling(TreeItem<MappableItem> parent) {
        if (parent != null) {
            for (TreeItem<MappableItem> child : parent.getChildren()) {
                if (child.getValue().getItem().isRequired()) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean containsChild(TreeItem<MappableItem> father, TreeItem<MappableItem> child) {
        boolean contained = false;
        if (father == child) {
            return true;
        }
        for (TreeItem<MappableItem> son : father.getChildren()) {
            contained = contained || containsChild(son, child);
        }
        return contained;
    }

    /**
     * Sucht einen optionalen Vater im übergebenen Baum
     *
     * @param mapped
     * @return
     */
    private TreeItem<MappableItem> getOptionalFather(TreeItem<MappableItem> mapped) {

        TreeItem<MappableItem> item = mapped.getParent();
        while (item != null) {
            // Das Item muss required sein und darf nicht root sein
            if (!item.getValue().getItem().isRequired() && item.getParent() != null) {
                return item;
            }
            item = item.getParent();
        }
        return null;
    }

    /**
     * Die Funktion selektiert zunächst den Teilbaum auf der linken Seite. Dann
     * wird der dem linken Teilbaum zugeordnete rechte Teilbaum selektiert. Im
     * rechten Teilbaum suchen wir dann anhand des übergebenen Pfades das
     * fehlende Attribut
     *
     * @param row
     * @param event
     */
    private void selectMissingAttributes(TableRow<AppTable> row, MouseEvent event) {

        // beim Doppelklick
        if (event.getClickCount() == 2 && (!row.isEmpty())) {

            AppTable rowData = row.getItem();
            expandLeftTree(rowData.getLeftItem());
            expandRightTree(rowData.getRightItem());

            // abhängig vom Fehlertyp, kann noch ein Alert gesetzt werden
            if (rowData.getError().equals(ErrorType.WRONG_MAPPING)) {
                String text = getText(rowData);
                UiDialogTools.showException(new CustomException(text), "Invalides Mapping!");
            }
        }
    }

    /**
     * Text, der dem User das falsche Mapping erklärt
     *
     * @param rowData
     * @return
     */
    private String getText(AppTable rowData) {
        TreeItem<MappableItem> leftItem = rowData.getLeftItem();
        TreeItem<MappableItem> rightItem = rowData.getRightItem();
        String attributeLeft = leftItem.getValue().getItem().getAttributeName();
        String mappedAttribute = leftItem.getValue().getMappedTreeview().getValue().getItem().getAttributeName();
        String attributeRight = rightItem.getValue().getItem().getAttributeName();
        String leftFather = leftItem.getParent().getValue().getItem().getAttributeName();
        return "Das Attribut " + attributeLeft + " wurde falsch gemappt! Das Optionale Feld " + attributeRight
                + " (siehe aufgeklapptes Objekt im rechten Teilbaum) und das Pflichtfeld " + mappedAttribute
                + " werden erzeugt, auch wenn der Wert von "
                + attributeLeft
                + " null ist! Das führt zu einer Schemaverletzung! Wenn Sie sicher sind, dass gewährleistet ist, dass "
                + attributeLeft
                + " immer verschieden von null ist, kann die Warnung ignoriert werden. Ist das nicht der Fall, dann mappen Sie bitte "
                + attributeRight + " auf ein entsprechendes Objekt im Teilbaum " + leftFather
                + " (auf der linken Seite), in dem dann auch das Attribut " + attributeLeft
                + " enthalten ist, Welches auf das Pflichtattribut " + mappedAttribute
                + " gemappt wird. Auf diese Weise, kann das optionale Attribut auch null sein, ohne, dass es zu einer Schemaverletzung kommt.";
    }

    /**
     * Der linke Baum wird anhand des in der Tabelle gespeicherten Knotens
     * geöffnet
     */
    private void expandLeftTree(TreeItem<MappableItem> tree) {
        if (tree != null) {
            expandTreeView(tree);
            tree.setExpanded(true);
            int genRow = generatedTreeView.getRow(tree);
            generatedTreeView.getSelectionModel().select(genRow);
        }
    }

    private void expandRightTree(TreeItem<MappableItem> tree) {
        if (tree != null) {
            expandTreeView(tree);
            tree.setExpanded(true);
            int genRow = schemaTreeView.getRow(tree);
            schemaTreeView.getSelectionModel().select(genRow);
        }
    }

    /*
     * Create a new mapping between schema and generated class
     */
    @FXML
    public void mapClasses() {

        TreeItem<MappableItem> selSchemaClass = getSelectedSchemaClass();
        if (selSchemaClass == schemaTreeView.getRoot()) {
            UiDialogTools.showException(new CustomException("Keine Kopie von Root möglich!"), "Mapping Fehler");
            return;
        }
        TreeItem<MappableItem> selGenClass = getSelectedGeneratedClass();
        if (selGenClass == null) {
            UiDialogTools.showWarning(Collections.singletonList("Keine Auswahl auf linker Seite vorhanden"), "Mapping Fehler");
            return;
        }

        TreeViewHelper helper = new TreeViewHelper();
        TreeItem<MappableItem> copiedItem = copyTree(selSchemaClass, true, helper, false, true);

        helper.mapTrees(copiedItem, schemaTreeView.getRoot(), false, null);

        selGenClass.getChildren().add(copiedItem);
        selGenClass.setExpanded(true);
    }

    @FXML
    public void loadProgress() {
        loadProgress(null);
    }

    /*
     * Populate trees from selected file
     */
    public void loadProgress(File preloaded) {

        try {
            loadFromFile(preloaded);
        } catch (Exception e) {
            UiDialogTools.showException(e, "Fehler beim Laden!");
        }

    }

    private void editItem() {

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Werte ändern");
        alert.setHeaderText("Ändern Sie Typ und Name des selektierten Attributs");

        Label name = new Label("Name: ");
        Label typ = new Label("Typ: ");
        Label converterName = new Label("Converter Name: ");
        Label converterCode = new Label("Converter Code: ");

        TextField textName = new TextField();
        textName.prefWidth(200.0);
        TextField textTyp = new TextField();
        textTyp.prefWidth(200.0);
        TextField converterNameField = new TextField();
        converterNameField.prefWidth(200.0);
        TextArea converterCodeArea = new TextArea();
        converterCodeArea.setWrapText(true);
        converterCodeArea.prefWidth(200.0);

        GridPane expContent = new GridPane();
        expContent.setVgap(10.0);
        expContent.setHgap(10.0);
        expContent.setPrefWidth(500.0);
        expContent.add(name, 0, 0);
        expContent.add(typ, 0, 2);
        expContent.add(converterName, 0, 4);
        expContent.add(converterCode, 0, 6);
        expContent.add(textName, 0, 1);
        expContent.add(textTyp, 0, 3);
        expContent.add(converterNameField, 0, 5);
        expContent.add(converterCodeArea, 0, 7);
        GridPane.setVgrow(textName, Priority.ALWAYS);
        GridPane.setVgrow(textTyp, Priority.ALWAYS);
        GridPane.setVgrow(name, Priority.ALWAYS);
        GridPane.setVgrow(typ, Priority.ALWAYS);
        GridPane.setVgrow(converterName, Priority.ALWAYS);
        GridPane.setVgrow(converterCode, Priority.ALWAYS);
        GridPane.setVgrow(converterNameField, Priority.ALWAYS);
        GridPane.setVgrow(converterCodeArea, Priority.ALWAYS);

        alert.getDialogPane().setExpandableContent(expContent);
        alert.getDialogPane().setExpanded(true);

        if (getSelectedGeneratedClass() == null) {
            return;
        }

        TreeItem<MappableItem> item = getSelectedGeneratedClass();
        MappableItem mapItem = item.getValue();
        String attributeName = mapItem.getItem().getAttributeName();
        String type = mapItem.getItem().getTypeName();
        textName.setText(attributeName);
        textTyp.setText(type);
        converterCodeArea.setText(converter.getCodeByName(mapItem.getItem().getConverterName()));
        converterNameField.setText(mapItem.getItem().getConverterName());

        Optional<ButtonType> result = alert.showAndWait();
        //noinspection StatementWithEmptyBody
        if (result.get() == ButtonType.OK) {
            // ... user chose OK
            // update Item
            Item value = mapItem.getItem();
            value.setAttributeName(textName.getText());
            value.setTypeName(textTyp.getText());
            // Converter- Name und -Code müssen definiert sein
            if (converterNameField.getText() != null && !converterNameField.getText().isEmpty() && converterCodeArea
                    .getText() != null && !converterCodeArea.getText().isEmpty()) {
                String code = converterCodeArea.getText().replaceAll("[\\n\\t ]", " ");
                value.setConverterName(converterNameField.getText());
                converter.putCodeByName(converterNameField.getText(), code);
            } else {
                value.setConverterName(null);
            }

            // nach der Änderung den Baum refreshen
            generatedTreeView.refresh();
        } else {
            // ... user chose CANCEL or closed the dialog
        }

    }

    @FXML
    private void openXSD() {
        Optional<File> file = UiDialogTools.useFileChooser("Save as...", currentlySavedDirectory.get(), false, UiDialogTools.EXTENSION_FILTER_XSD);
        file.ifPresent(f -> {
            workingFile = f;
            currentlySavedDirectory.setValue(f.getParent());
            processXSD(f);
        });
    }

    private void processXSD(File xsd) {
        schemeSelection(xsd.getAbsolutePath());
    }

    private void loadFromFile(File preloaded) throws ClassNotFoundException {

        // zunächst Alles schließen
        close();

        // When loading from a chosen file (vs. a shortcut file path)
        if (preloaded == null) {
            Optional<File> file = UiDialogTools.useFileChooser("Load from File", currentlyLoadedDirectory.get(), false, UiDialogTools.EXTENSION_FILTER_TXT);
            file.ifPresent(f -> {
                workingFile = f;
                currentlyLoadedDirectory.setValue(f.getParent());
                addFileMenuItem(f);
            });
        } else {
            workingFile = preloaded;
        }

        List<String> lines = readStringFromFile(workingFile.getAbsolutePath());

        // Skip first line
        String msgline = lines.get(1);

        // Get message name, class, and package of root element
        String messageName = msgline.split("=")[1].split("/")[0];
        Class<?> classForName = Class.forName(msgline.split("=")[1].split("/")[1]);
        loadTrees(classForName, null, messageName, lines);
        // TODO : optionales Laden von der Platte sollte möglich sein

        setStageTitle(TITLE + " - " + workingFile.getAbsolutePath());
    }

    public void setStageTitle(String newTitle) {
        myStage.setTitle(newTitle);
    }

    private List<String> readStringFromFile(String input) {
        List<String> result = new ArrayList<>();
        InputStream in = CodeGen.class.getClassLoader().getResourceAsStream(input);
        boolean isFile = in == null;

        try {
            return Files.readAllLines(Paths.get(input));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
     * Saves the current message being mapped
     */
    @FXML
    public void save() {

        // Choose file if it has not been saved yet
        if (workingFile == null) {
            // Fehlermeldung, da es keine Working-File gibt
            UiDialogTools.showException(new CustomException("Es wurde noch keine Datei geladen!"), "Fehler beim Speichern");
            return;
        }

        // Write mapped values into saved file
        TreeItem<MappableItem> messageItem = generatedTreeView.getRoot();
        notMappedAttributes.clear();
        String fileContent = "FULL-TREE\n" + buildMappingTree(messageItem);
        if (this.converter.hasConversions()) {
            fileContent += "\nCONVERTERS" + this.converter.getConverters();
        }
        // Warnings ausgeben, aber speichern
        if (!notMappedAttributes.isEmpty()) {
            UiDialogTools.showException(new NotMappedException(), notMappedAttributes.toString());
        }
        saveContentToFile(workingFile, fileContent);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        logMessage("Message Saved (" + dtf.format(now) + ")");

    }

    public void logMessage(String text) {
        logBox.setText(logBox.getText() + text + "\n");
    }

    @FXML
    public void saveAs() {
        Optional<File> file = UiDialogTools.useFileChooser("Save as...", currentlySavedDirectory.get(), true, UiDialogTools.EXTENSION_FILTER_TXT);

        file.ifPresent(f -> {
            workingFile = f;
            currentlySavedDirectory.setValue(f.getParent());
            addFileMenuItem(f);
            save();
            // den Titel aktualisieren
            setStageTitle(TITLE + " - " + workingFile.getAbsolutePath());
        });
    }


    private String buildMappingTree(TreeItem<MappableItem> generatedItem) {
        String savedPath = "";
        try {
            String mappedTo = getPathFromRoot(generatedItem.getValue()
                    .getMappedTreeview());
            String converterName = generatedItem.getValue().getItem().getConverterName();
            if (converterName != null) {
                savedPath = getPathFromRoot(generatedItem) + "=" + mappedTo + "=" + converterName;
            } else {
                savedPath = getPathFromRoot(generatedItem) + "=" + mappedTo;
            }
            if (mappedTo.isEmpty()) {
                throw new NotMappedException();
            }
        } catch (NotMappedException e) {
            notMappedAttributes.add("Attribut " + generatedItem.getValue().getItem().getAttributeName()
                    + " wurde noch nicht gemappt!");
            expandTreeView(generatedItem);
            int row = generatedTreeView.getRow(generatedItem);
            generatedTreeView.getSelectionModel().select(row);
        }

        ObservableList<TreeItem<MappableItem>> childItems = generatedItem.getChildren();

        for (TreeItem<MappableItem> childItem : childItems) {
            String childPath = buildMappingTree(childItem);
            if (!childPath.isEmpty() && !savedPath.isEmpty()) {
                savedPath += "\n" + childPath;
            } else if (!childPath.isEmpty() && savedPath.isEmpty()) {
                savedPath += childPath;
            }
        }

        return savedPath;
    }

    private String getPathFromRoot(TreeItem<MappableItem> mappedTreeview) {
        List<String> paths = new ArrayList<>();
        // Attribut noch nicht gemappt
        if (mappedTreeview == null) {
            return "";
        }
        paths.add(mappedTreeview.getValue().getAttributeType());
        while (mappedTreeview.getParent() != null) {
            mappedTreeview = mappedTreeview.getParent();
            paths.add(mappedTreeview.getValue().getAttributeType());
        }

        String result = "";
        for (int i = paths.size() - 1; i >= 0; i--) {
            if (i == paths.size() - 1) {
                result += paths.get(i);
            } else {
                result += "->" + paths.get(i);
            }
        }
        return result;
    }

    /*
     * Creates the xsd file for the generated POJO
     */
    @FXML
    private void generateObject() {
        Optional<File> genFile = UiDialogTools.useFileChooser("Generate XSD Object", currentlySavedDirectory.get(), true, UiDialogTools.EXTENSION_FILTER_XSD);
        genFile.ifPresent(f -> {
            currentlySavedDirectory.setValue(f.getParent());
            List<String> warnings = new LinkedList<>();
            saveContentToFile(f, new XSDHelper().buildSchemeFromTree(generatedTreeView.getRoot(), warnings));
            UiDialogTools.showWarning(warnings, "Das Mapping enthält unbekannte Typen!");
            logMessage("Object generation successful");
        });
    }

    /**
     * Saves a seitenbau-compatible json file
     */
    @FXML
    private void generateSeitenbauJson() {
        Optional<File> file = UiDialogTools.useFileChooser("Save Seitenbau JSON", currentlySavedDirectory.get(), true, UiDialogTools.EXTENSION_FILTER_JSON);


        //konvertiere vom javafx-TreeItem zu den lines.
        TreeItem<MappableItem> messageItem = generatedTreeView.getRoot();
        notMappedAttributes.clear();
        if (messageItem == null) {
            UiDialogTools.showException(new IllegalStateException("Keine Daten geladen"), "Keine Daten geladen");
            return;
        }
        List<String> lines = Arrays.asList(buildMappingTree(messageItem).split("\\r?\\n"));
        String jsonData;
        try {
            jsonData = new FormularGenerator().generateSeitenbauJSon(lines);
        } catch (ClassNotFoundException e) {
            UiDialogTools.showException(e, "Could not genrate json file");
            return;
        }
        saveContentToFile(file.orElse(null), jsonData);
    }

    @FXML
    public void generateExcel() {
        Optional<File> fileOptional = UiDialogTools.useFileChooser("Generate Excel", currentlySavedDirectory.get(), true, UiDialogTools.EXTENSION_FILTER_EXCEL);
        if (!fileOptional.isPresent())
            return;
        File file = fileOptional.get();

        //konvertiere vom javafx-TreeItem zu den lines.
        TreeItem<MappableItem> messageItem = generatedTreeView.getRoot();
        notMappedAttributes.clear();
        if (messageItem == null) {
            UiDialogTools.showException(new IllegalStateException("Keine Daten geladen"), "Keine Daten geladen");
            return;
        }
        List<String> lines = Arrays.asList(buildMappingTree(messageItem).split("\\r?\\n"));
        try {
            LegacyExcelGenerator generator = new LegacyExcelGenerator();
            generator.generate(lines);
            generator.outputFile(file);
        } catch (ClassNotFoundException | IOException e) {
            UiDialogTools.showException(e, "Could not genrate excel file");
        }
    }

    @FXML
    public void generateJSon() {
        TreeItem<MappableItem> root = generatedTreeView.getRoot();
        String fileContent = new JackSonUtils().convertTreeToJSon(root);
        Optional<File> fileOptional = UiDialogTools.useFileChooser("Generate JSON", currentlySavedDirectory.get(), true, UiDialogTools.EXTENSION_FILTER_JSON);
        if (!fileOptional.isPresent())
            return;
        File genFile = fileOptional.get();
        currentlySavedDirectory.setValue(genFile.getParent());
        saveContentToFile(genFile, fileContent);
    }

    @FXML
    public void genJsonScheme() {
        TreeItem<MappableItem> root = generatedTreeView.getRoot();
        String fileContent = new JackSonUtils().convertTreeToJSonScheme(root);
        final Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("File Chooser");
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JSon (*.json)", "*.json");
        if (currentlySavedDirectory.get() != null && !currentlySavedDirectory.get().isEmpty()) {
            fileChooser.setInitialDirectory(new File(currentlySavedDirectory.get()));
        }
        fileChooser.getExtensionFilters().add(extFilter);
        File genFile = fileChooser.showSaveDialog(stage);
        if (genFile == null) {
            return;
        }
        currentlySavedDirectory.setValue(genFile.getParent());
        saveContentToFile(genFile, fileContent);
    }

    /**
     * Ist eine Progress-File geladen, wird auf Basis dieser File
     * JUnit-Test-Code generiert
     */
    @FXML
    private void generateJunitCode() {
        if (workingFile != null && workingFile.exists()) {
            Optional<File> fileOptional = UiDialogTools.useFileChooser("Generate Excel", currentlySavedDirectory.get(), true, UiDialogTools.EXTENSION_FILTER_JAVA);
            if (!fileOptional.isPresent())
                return;
            File genFile = fileOptional.get();

            currentlySavedDirectory.setValue(genFile.getParent());
            List<String> fromFile = readStringFromFile(workingFile.getAbsolutePath());
            ClassTreeLoader treeLoader = new ClassTreeLoader();
            TreeOfClasses loadTreeFromConfig = treeLoader.loadTreeFromConfig(fromFile);
            JUnitGenerator junitCodeGenerator = new JUnitGenerator();
            String code = junitCodeGenerator.generateTestCode(loadTreeFromConfig);
            saveContentToFile(genFile, code);
        } else {
            UiDialogTools.showException(new CustomException("Keine Progress-File geladen!"), "Fehler beim Laden!");
        }
    }

    /*
     * Creates the mapping code: From generated POJO to schema class and
     * vice-versa
     */
    @FXML
    private void generateMapping() {
        if (workingFile == null) {
            UiDialogTools.showException(new FileNotFoundException("No working file is not set, please select a file before generating"), "No working file open");
            return;
        }

        MenuButton genMenuButton = new MenuButton();
        Button genMessageButton = new Button();

        genMessageButton.setText("Generate");

        Text pojoPackageText = new Text("Pojo Package:");
        Text xmlPackageText = new Text("XML Package:");

        TextField pojoPackageField = new TextField();
        pojoPackageField.setText("de.kommone.webservice.sources");

        TextField xmlPackageField = new TextField();

        try (BufferedReader reader = new BufferedReader(new FileReader(workingFile))) {
            @SuppressWarnings("UnusedAssignment") String line = reader.readLine();
            line = reader.readLine();

            int lastIndexOfSlash = line.lastIndexOf("/");
            int lastIndexOfDot = line.lastIndexOf(".");
            if (lastIndexOfSlash < lastIndexOfDot) {
                String defaultPackage = line.substring(lastIndexOfSlash + 1, lastIndexOfDot);
                defaultPackage = defaultPackage.replace("de.kommone.webservice", "de.kommone.kmc.webservice");
                xmlPackageField.setText(defaultPackage);
            }

        } catch (IOException e1) {
            e1.printStackTrace();
        }

        genMenuButton.setText("Select Mapping Type");

        MenuItem mapToScheme = new MenuItem("Scheme -> Pojo");
        MenuItem mapToPojo = new MenuItem("Pojo -> Scheme");
        genMenuButton.getItems().add(mapToScheme);
        genMenuButton.getItems().add(mapToPojo);

        Text genMessageErrorField = new Text();
        genMessageErrorField.setText("");

        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        VBox dialogVbox = new VBox(10);

        dialogVbox.getChildren().add(genMenuButton);
        dialogVbox.getChildren().add(pojoPackageText);
        dialogVbox.getChildren().add(pojoPackageField);
        dialogVbox.getChildren().add(xmlPackageText);
        dialogVbox.getChildren().add(xmlPackageField);
        dialogVbox.getChildren().add(genMessageButton);

        VBox.setMargin(genMenuButton, new Insets(30, 15, 0, 15));
        VBox.setMargin(genMessageButton, new Insets(0, 15, 0, 15));
        VBox.setMargin(pojoPackageText, new Insets(0, 15, 0, 15));
        VBox.setMargin(pojoPackageField, new Insets(0, 15, 0, 15));
        VBox.setMargin(xmlPackageText, new Insets(0, 15, 0, 15));
        VBox.setMargin(xmlPackageField, new Insets(0, 15, 0, 15));

        VBox.setMargin(genMessageErrorField, new Insets(0, 15, 0, 15));

        Scene dialogScene = new Scene(dialogVbox, 300, 240);
        dialog.setScene(dialogScene);
        dialog.setTitle("Generate Mapping");
        dialog.show();

        // Generating Logic
        genMessageButton.setOnAction(e -> {
            Optional<File> file = UiDialogTools.useFileChooser("Generate Mapping", currentlySavedDirectory.get(), true, UiDialogTools.EXTENSION_FILTER_JAVA);
            if (!file.isPresent())
                return;
            File genFile = file.get();
            currentlySavedDirectory.setValue(genFile.getParent());

            boolean mapType = genMenuButton.getText().equals("Scheme -> Pojo");
            CodeGen codeGenarator = new CodeGen();
            String code;
            try {

                String absPath = genFile.getAbsolutePath();
                String className = absPath.substring(absPath.lastIndexOf("\\") + 1, absPath.lastIndexOf("."));

                // Read
                BufferedReader reader;
                reader = new BufferedReader(new FileReader(workingFile));
                @SuppressWarnings("UnusedAssignment") String line = reader.readLine();
                line = reader.readLine();
                reader.close();
                int lastIndexOfSlash = line.lastIndexOf("/");
                int lastIndexOfDot = line.lastIndexOf(".");

                String defaultXmlPackage = null;
                if (lastIndexOfSlash < lastIndexOfDot) {
                    defaultXmlPackage = line.substring(line.lastIndexOf("/") + 1, line.lastIndexOf("."));
                } else {
                    defaultXmlPackage = xmlPackageField.getText();
                }

                code = codeGenarator.generateCode(mapType, PACKAGE_NAME, className, workingFile.getAbsolutePath());

                codeGenarator.output(genFile, PACKAGE_NAME, defaultXmlPackage, pojoPackageField.getText(),
                        xmlPackageField.getText(), code);

            } catch (ClassNotFoundException | IOException e1) {
                e1.printStackTrace();
            }
        });

        mapToScheme.setOnAction(e -> genMenuButton.setText(mapToScheme.getText()));
        mapToPojo.setOnAction(e -> genMenuButton.setText(mapToPojo.getText()));
    }

    /**
     * Generiert die Groovy-Klassen und den HTTP- Aufruf, sodass der Entwickler
     * nur noch die Objekte zusammenbauen muss
     */
    @FXML
    private void generateGroovySceleton() {
        Optional<File> fileOptional = UiDialogTools.useFileChooser("Generate Groovy", currentlySavedDirectory.get(), true, UiDialogTools.EXTENSION_FILTER_GROOVY);
        if (!fileOptional.isPresent())
            return;
        File genFile = fileOptional.get();

        currentlySavedDirectory.setValue(genFile.getParent());
        CodeGen codeGenarator = new CodeGen();
        String code = "package de.seitenbau.test.mapping\r\n" + "\r\n" + "import groovy.json.JsonOutput\r\n" + "\r\n"
                + "// Import Mocking\r\n" + "def execution = new Execution()\r\n" + "def log = new Logger()\r\n"
                + "// Import Mocking Ende\r\n" + "// TODO : eventuell weitere Formulare hinzufügen\r\n"
                + "FormContent antrag = execution.getVariable('antrag')\r\n" + "\r\n" + "\r\n" + "def data = [:]\r\n"
                + "\r\n" + "// INSERT CODE HERE" + "\r\n";
        try {

            String absPath = genFile.getAbsolutePath();
            String className = absPath.substring(absPath.lastIndexOf("\\") + 1, absPath.lastIndexOf("."));

            codeGenarator.generateCode(true, PACKAGE_NAME, className, workingFile.getAbsolutePath());

            Node root = codeGenarator.getRoot();

            GroovyUtil groovyUtil = new GroovyUtil();
            String groovyClasses = groovyUtil.generateClasses(root);
            code += groovyClasses + "\r\n";

            code += "def parent = [:]\r\n" + "def context = [:]\r\n" + "context.processInstanceId =  \"1402807\"\r\n"
                    + "context.activityId = \"sid-DC7037DA-F146-4167-A0AA-22D01C35ACE1\"\r\n"
                    + "context.executionId =  \"1402807\"\r\n"
                    + "context.processDefinitionId = \"m476.p110.bewohnerparkausweisEntwicklung:1:1402806\"\r\n"
                    + "context.loggedInUser = null\r\n" + "context.userId = \"userId:824220304111\"\r\n" + "\r\n"
                    + "parent.context = context\r\n" + "parent.data = data\r\n" + "\r\n" + "// Logging JSON Output\r\n"
                    + "/**\r\n" + " \"ä\": \"\\u00e4\"\r\n" + " \"Ä\": \"\\u00c4\"\r\n" + " \"ü\": \"\\u00fc\"\r\n"
                    + " \"Ü\": \"\\u00dc\"\r\n" + " \"ö\": \"\\u00f6\"\r\n" + " \"Ö\": \"\\u00d6\"\r\n"
                    + " \"ß\": \"\\u00df\"\r\n" + " **/\r\n" + "\r\n"
                    + "def restRequest = JsonOutput.prettyPrint(JsonOutput.toJson(parent)).replace(\"\\\\u00e4\", \"ä\").replace(\"\\\\u00c4\", \"Ä\").replace(\"\\\\u00fc\", \"ü\").replace(\"\\\\u00dc\", \"Ü\").replace(\"\\\\u00f6\", \"ö\").replace(\"\\\\u00d6\", \"Ö\").replace(\"\\\\u00df\", \"ß\")\r\n"
                    + "\r\n" + "log.info(restRequest)\r\n" + "\r\n" + "// HTTP Connection\r\n" + "try {\r\n" + "\r\n"
                    + "    def endpoint = \"REPLACE_WITCH_ACTUAL_ENDPOINT\"\r\n" + "\r\n"
                    + "    def connection = new URL(endpoint).openConnection()\r\n" + "    connection.with {\r\n"
                    + "        doOutput = true\r\n" + "        requestMethod = \"POST\"\r\n"
                    + "        setRequestProperty(\"Content-Type\", \"application/json\")\r\n"
                    + "        setRequestProperty(\"Accept\", \"application/json\")\r\n" + "\r\n"
                    + "        outputStream.withPrintWriter({printWriter ->\r\n"
                    + "            printWriter.write(restRequest)\r\n" + "        })\r\n" + "\r\n" + "\r\n"
                    + "        log.info(\"Response Code: \" + responseCode)\r\n"
                    + "        def xmlOutput = content.text.toString()\r\n" + "        log.info(xmlOutput)\r\n" + "\r\n"
                    + "    }\r\n" + "\r\n" + "} catch(Exception e) {\r\n" + "    log.error(e.getMessage())\r\n" + "}";

            codeGenarator.output(genFile, code);

        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }

    }

    @FXML
    private void openPlUi() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(
                "PLGui.fxml"));
        Pane pane = fxmlLoader.load();
        pane.getStylesheets().add(getClass().getClassLoader().getResource("style.css").toExternalForm());
        Stage stage = new Stage();
        stage.setTitle("PL UI");
        stage.setScene(new Scene(pane, 600, 400));
        stage.setMinWidth(400);
        stage.setMinHeight(300);
        stage.show();
        ((PLController) fxmlLoader.getController()).setRootClasses(schemaRootClasses);
    }

    private void schemeSelection(String pathToFile) {

        // lade zunächst die Datei
        XSDParser parser = new XSDParser();
        try {
            parser.initXsModel(pathToFile);
        } catch (Exception e) {
            UiDialogTools.showException(e, "Fehler beim Laden des Schemas aufgetreten");
            return;
        }

        // GUI Set Up Begins...
        Text messageNameTitle = new Text();
        messageNameTitle.setText("Enter Message Name:");
        TextField messageNameField = new TextField();

        messageTextField = new TextField();

        ComboBox<String> comboBox = new ComboBox<>();

        comboBox.setEditable(true);
        classList = new ArrayList<>();
        ObservableList<String> comboList = FXCollections.observableArrayList();
        comboList.addAll(parser.getElementsDefinedInModel());
        comboBox.setItems(comboList);

        messageButton = new Button();
        messageButton.setText("Enter");

        messageErrorField = new Text();
        messageErrorField.setText("");
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        VBox dialogVbox = new VBox(10);
        Text titleText = new Text("Find Message:");
        dialogVbox.getChildren().add(titleText);
        // dialogVbox.getChildren().add(messageTextField);
        dialogVbox.getChildren().add(comboBox);
        dialogVbox.getChildren().add(messageNameTitle);
        dialogVbox.getChildren().add(messageNameField);
        dialogVbox.getChildren().add(messageButton);
        dialogVbox.getChildren().add(messageErrorField);

        VBox.setMargin(titleText, new Insets(30, 15, 0, 15));
        VBox.setMargin(messageTextField, new Insets(0, 15, 0, 15));
        VBox.setMargin(comboBox, new Insets(0, 15, 0, 15));
        VBox.setMargin(messageNameTitle, new Insets(0, 15, 0, 15));
        VBox.setMargin(messageNameField, new Insets(0, 15, 0, 15));
        VBox.setMargin(messageErrorField, new Insets(0, 15, 0, 15));
        VBox.setMargin(messageButton, new Insets(0, 15, 0, 15));
        VBox.setMargin(messageErrorField, new Insets(0, 15, 0, 15));

        Scene dialogScene = new Scene(dialogVbox, 300, 250);
        dialog.setScene(dialogScene);
        dialog.setTitle("New Message");
        dialog.show();
        // ...GUI Set Up Ends

        // Populates trees by loading selected message
        messageButton.setOnAction(e -> {

            if (comboBox.getEditor() != null && comboBox.getEditor().getText() != null && !comboBox.getEditor().getText().isEmpty()) {
                String xmlMessageName = comboBox.getEditor().getText();
                // 1. Laden des rechten Baumes
                TreeOfClasses loadTreeFromClass = null;
                try {
                    loadTreeFromClass = parser.getClassAsTree(pathToFile, xmlMessageName);
                } catch (Exception ex) {
                    UiDialogTools.showException(ex, "Konnte Baum aus Schema nicht laden!");
                    return;
                }
                TreeViewHelper helper = new TreeViewHelper();
                TreeItem<MappableItem> rightTree = helper.getTreeItem(loadTreeFromClass, false);

                schemaTreeView.setRoot(rightTree);
                schemaTreeView.setShowRoot(true);

                // 2. Laden des linken Baumes
                TreeOfClasses loadTreeFromFile;
                TreeItem<MappableItem> leftTree = null;

                String pojoName = messageNameField.getText();

                // neue Nachricht (rechte Seite) bei bereits geladener linken Seite
                if (generatedTreeView.getRoot() != null) {
                    // -> Kopieren des Baumes und Versuch, den Baum auf den
                    // rechten Baum zu mappen
                    TreeItem<MappableItem> currentRoot = generatedTreeView.getRoot();
                    leftTree = copyTree(currentRoot, true, helper, true, false);
                    // converter bleibt

                } // neue Nachricht (rechte Seite), neue linke Seite (vorher muss close
                // aufgerufen werden)
                else if (pojoName != null) {
                    loadTreeFromFile = new TreeOfClasses(new Item(PACKAGE_NAME + "." + pojoName, pojoName, false, true,
                            true));
                    converter = new Converter();
                    leftTree = helper.getTreeItem(loadTreeFromFile, true);

                } // load from progress file
                // TODO: Laden von der Progress File
                //else if (lines != null) {
                //    loadTreeFromFile = treeLoader.loadTreeFromConfig(lines);
                //    leftTree = helper.getTreeItem(loadTreeFromFile, true);
                //    converter = new Converter().loadConverterFromConfig(lines);
                //}


                // ein Mapping zwischen den Bäumen herstellen
                helper.mapTrees(leftTree, rightTree, true, attributeTable);
                leftTree.getValue().setMapped(true);
                leftTree.getValue().setMappedTreeview(rightTree);
                rightTree.getValue().addMappedTreeview(leftTree);
                generatedTreeView.setRoot(leftTree);
                validateProgress();
                generatedTreeView.setShowRoot(true);
                genNameHeader.setText("POJO Message: " + leftTree.getValue().getItem().getAttributeName());
                schemaNameHeader.setText("XOEV/XFall Message: " + xmlMessageName);
                setStageTitle(TITLE);
                dialog.hide();
            }
        });
    }

    /*
     * Loads a brand new message
     */
    @FXML
    private void newMessage() {

        // GUI Set Up Begins...
        Text messageNameTitle = new Text();
        messageNameTitle.setText("Enter Message Name:");
        TextField messageNameField = new TextField();

        messageTextField = new TextField();
        menuButton = new MenuButton();
        menuButton.setText("Select Scheme");

        comboBox = new ComboBox<>();

        comboBox.setEditable(true);
        classList = new ArrayList<>();
        obsList = FXCollections.observableArrayList(classList);
        comboBox.setItems(obsList);

        comboBox.setConverter(new StringConverter<Class<?>>() {

            @Override
            public String toString(Class<?> myClass) {
                if (myClass == null)
                    return null;
                return myClass.getSimpleName();
            }

            @Override
            public Class<?> fromString(String string) {
                if (string == null) {
                    return null;
                }
                return MappingHelper.createMessageFromName(schemaRootClasses, string);
            }
        });
        comboBox.setOnKeyReleased(event -> {
            if (comboBox.getEditor().getText() != null && !comboBox.getEditor().getText().replace(" ", "").equals("")
                    && comboBox.getEditor().getText() != null) {
                String inputNum = comboBox.getEditor().getText();
                classList.clear();
                obsList.clear();
                for (Class<?> rootClass : schemaRootClasses) {
                    if (rootClass.getSimpleName().contains(inputNum)) {
                        obsList.add(rootClass);
                    }
                }
            }

        });

        for (String standard : standards.keySet()) {
            MenuItem item = new MenuItem(standard);
            menuButton.getItems().add(item);
            item.setOnAction(e -> loadClassesFromPackage(item.getText(), standards.get(standard)));
        }

        messageButton = new Button();
        messageButton.setText("Enter");

        messageErrorField = new Text();
        messageErrorField.setText("");
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        VBox dialogVbox = new VBox(10);
        Text titleText = new Text("Find Message:");
        dialogVbox.getChildren().add(titleText);
        // dialogVbox.getChildren().add(messageTextField);
        dialogVbox.getChildren().add(menuButton);
        dialogVbox.getChildren().add(comboBox);
        dialogVbox.getChildren().add(messageNameTitle);
        dialogVbox.getChildren().add(messageNameField);
        dialogVbox.getChildren().add(messageButton);
        dialogVbox.getChildren().add(messageErrorField);

        VBox.setMargin(titleText, new Insets(30, 15, 0, 15));
        VBox.setMargin(messageTextField, new Insets(0, 15, 0, 15));
        VBox.setMargin(menuButton, new Insets(0, 15, 0, 15));
        VBox.setMargin(comboBox, new Insets(0, 15, 0, 15));
        VBox.setMargin(messageNameTitle, new Insets(0, 15, 0, 15));
        VBox.setMargin(messageNameField, new Insets(0, 15, 0, 15));
        VBox.setMargin(messageErrorField, new Insets(0, 15, 0, 15));
        VBox.setMargin(messageButton, new Insets(0, 15, 0, 15));
        VBox.setMargin(messageErrorField, new Insets(0, 15, 0, 15));

        Scene dialogScene = new Scene(dialogVbox, 300, 250);
        dialog.setScene(dialogScene);
        dialog.setTitle("New Message");
        dialog.show();
        // ...GUI Set Up Ends

        // Populates trees by loading selected message
        messageButton.setOnAction(e -> {

            if (MappingHelper.createMessageFromName(schemaRootClasses, comboBox.getEditor().getText()) == null
                    || menuButton.getText().equals("Select Scheme")) {
                messageErrorField.setText("Message not found");
                messageTextField.setPromptText("Try Again");
            } else {
                Class<?> rootClass = getClassBySimpleName(comboBox.getEditor().getText());
                if (rootClass == null) {
                    UiDialogTools.showException(new NullPointerException(), "Die Klasse " + comboBox.getEditor().getText()
                            + " konnte nicht gefunden werden!");
                    return;
                }
                loadTrees(rootClass, messageNameField.getText(), comboBox.getEditor().getText(), null);
                setStageTitle(TITLE);
                dialog.hide();
            }
        });
    }

    /**
     * Lädt den rechten Baum mittels der übergebenen Klasse (per Reflection), es
     * werden 3 Fälle unterschieden:
     * <ul>
     * <li>Neue linke Seite, neue rechte Seite</li>
     * <li>Alte linke Seite, neue rechte Seite (die linke Seite wird anhand der
     * bisherigen Mapping-Info auf die rechte Seite gemappt), hilfreich, wenn
     * bereist ein Mapping (auf eine alte XOEV-Version) existiert und auf eine
     * Neue upgedatet wird)</li>
     * <li>Mapping-Fortschritt wird geladen</li>
     * </ul>
     *
     * @param fromClass
     * @param pojoName
     * @param xmlMessageName
     * @param lines
     */
    private void loadTrees(Class<?> fromClass, String pojoName, String xmlMessageName, List<String> lines) {
        // 1. Laden des rechten Baumes
        ClassTreeLoader treeLoader = new ClassTreeLoader();
        TreeOfClasses loadTreeFromClass = treeLoader.loadTreeFromClass(fromClass);
        TreeViewHelper helper = new TreeViewHelper();
        TreeItem<MappableItem> rightTree = helper.getTreeItem(loadTreeFromClass, false);

        schemaTreeView.setRoot(rightTree);
        schemaTreeView.setShowRoot(true);

        // 2. Laden des linken Baumes
        TreeOfClasses loadTreeFromFile;
        TreeItem<MappableItem> leftTree = null;

        // neue Nachricht (rechte Seite) bei bereits geladener linken Seite
        if (generatedTreeView.getRoot() != null) {
            // -> Kopieren des Baumes und Versuch, den Baum auf den
            // rechten Baum zu mappen
            TreeItem<MappableItem> currentRoot = generatedTreeView.getRoot();
            leftTree = copyTree(currentRoot, true, helper, true, false);
            // converter bleibt

        } // neue Nachricht (rechte Seite), neue linke Seite (vorher muss close
        // aufgerufen werden)
        else if (pojoName != null) {
            loadTreeFromFile = new TreeOfClasses(new Item(PACKAGE_NAME + "." + pojoName, pojoName, false, true,
                    true));
            converter = new Converter();
            leftTree = helper.getTreeItem(loadTreeFromFile, true);

        } // load from progress file
        else if (lines != null) {
            loadTreeFromFile = treeLoader.loadTreeFromConfig(lines);
            leftTree = helper.getTreeItem(loadTreeFromFile, true);
            converter = new Converter().loadConverterFromConfig(lines);
        }

        // ein Mapping zwischen den Bäumen herstellen
        helper.mapTrees(leftTree, rightTree, true, attributeTable);
        leftTree.getValue().setMapped(true);
        leftTree.getValue().setMappedTreeview(rightTree);
        rightTree.getValue().addMappedTreeview(leftTree);
        generatedTreeView.setRoot(leftTree);
        validateProgress();
        generatedTreeView.setShowRoot(true);
        genNameHeader.setText("POJO Message: " + leftTree.getValue().getItem().getAttributeName());
        schemaNameHeader.setText("XOEV/XFall Message: " + xmlMessageName);
    }

    /*
     * Loads all classes from the given package into the list of the GUI
     */
    private void loadClassesFromPackage(String menuButtonText, String packageStr) {

        menuButton.setText(menuButtonText);

        ArrayList<Class<?>> schemaClasses = null;
        try {
            schemaClasses = MappingHelper.getClassNamesFromPackage(packageStr);
        } catch (ClassNotFoundException | IOException | URISyntaxException e1) {
            e1.printStackTrace();
        }
        List<String> webservicePackages = Arrays.asList("de.kommone.patras", "de.kommone.xta.ws1", "de.kommone.xta.ws3");
        schemaRootClasses = MappingHelper.getRootClasses(schemaClasses, webservicePackages.contains(packageStr));
        schemaRootClasses.sort(schemaRootClassComparator);

        classList.clear();
        obsList.clear();
        String inputNum = comboBox.getEditor().getText();
        for (Class<?> rootClass : schemaRootClasses) {
            if (rootClass.getSimpleName().contains(inputNum)) {
                obsList.add(rootClass);
            }
        }
    }

    private Class<?> getClassBySimpleName(String simpleName) {
        for (Class<?> rootClass : schemaRootClasses) {
            if (rootClass.getSimpleName().equals(simpleName)) {
                return rootClass;
            }
        }
        return null;
    }

    /*
     * Deletes the selected mapped class
     */
    @FXML
    public void deleteGenClass() {

        TreeItem<MappableItem> selGenClass = getSelectedGeneratedClass();
        if (selGenClass != null && selGenClass != generatedTreeView.getRoot()) {

            unMapMappedItem(selGenClass);

            int row = generatedTreeView.getRow(selGenClass.getParent());
            generatedTreeView.getSelectionModel().select(row);

            selGenClass.getParent().getChildren().remove(selGenClass);
        }
    }

    /**
     * Das gemappte Item wird ungemappt, wenn es keine Referenzen mehr auf den
     * linken Baum hat, ansonsten nicht. Unmap heißt, dass Kindelemente
     * ungemappt werden müssen und auch Elternelemente
     *
     * @param selectedItem
     */
    private void unMapMappedItem(TreeItem<MappableItem> selectedItem) {

        // das gemappte Item holen
        TreeItem<MappableItem> mappedItem = selectedItem.getValue().getMappedTreeview();

        if (mappedItem != null) {

            mappedItem.getValue().removeMappedTreeview(selectedItem);

            // kein Mapping mehr -> Kinder UND Eltern anpassen
            if (mappedItem.getValue().getMappedTreeViews().isEmpty()) {

                // für Kinder das Mapping zurücksetzen
                unMapChildren(mappedItem);

                // für die Eltern muss nun geprüft werden, ob sie noch gemappt
                // sind, d. h. ob noch Geschwister des gelöschten Knoten
                // existieren, die gemappt sind
                unMapParents(mappedItem, mappedItem.getParent());
            }
        }
    }

    private void unMapChildren(TreeItem<MappableItem> mappedTreeview) {
        mappedTreeview.getValue().setMapped(false);
        mappedTreeview.getValue().setMappedTreeview(null);
        mappedTreeview.getValue().getMappedTreeViews().clear();

        // reset für alle Kindobjekte
        for (TreeItem<MappableItem> child : mappedTreeview.getChildren()) {
            unMapChildren(child);
        }

    }

    /**
     * Ein Elternknoten wird erst dann ungemappt, wenn er neben dem (gelöschten)
     * Kindknoten keinen weiteren gemappten Kindknoten mehr hat. Im
     * letztgenannten Fall müssen dann die Elternknoten rekursiv geprüft werden
     *
     * @param child
     * @param parent
     */
    private void unMapParents(TreeItem<MappableItem> child, TreeItem<MappableItem> parent) {
        if (parent == null) {
            return;
        }
        for (TreeItem<MappableItem> sibling : parent.getChildren()) {
            // den gelöschten Knoten überspringen
            if (sibling == child) {
                continue;
            }
            if (sibling.getValue().isMapped()) {
                return;
            }
        }

        // kein weiterer Geschwisterknoten ist gemappt -> der Vater ist es auch
        // nicht mehr
        parent.getValue().setMapped(false);

        // hier muss rekursiv geprüft werden
        unMapParents(parent, parent.getParent());

    }

    /*
     * Connects a non mapped template class
     */
    @FXML
    public void connectClasses() {

        TreeItem<MappableItem> selSchemaClass = getSelectedSchemaClass();
        TreeItem<MappableItem> selGenClass = getSelectedGeneratedClass();

        if (selSchemaClass == null) {
            UiDialogTools.showException(new CustomException("Select class to connect to"), "Beim Mapping gab es einen Fehler");
        } else if (selGenClass == null) {
            UiDialogTools.showException(new CustomException("Select class to connect from"), "Beim Mapping gab es einen Fehler");
        } else if (selGenClass.getValue().getMappedTreeview() == null) {
            selGenClass.getValue().setMappedTreeview(selSchemaClass);
            selSchemaClass.getValue().addMappedTreeview(selGenClass);
            selGenClass.getValue().setMapped(true);

            selSchemaClass.getValue().setMapped(true);
            TreeItem<MappableItem> parentNode = selSchemaClass.getParent();
            while (parentNode != null && parentNode != schemaTreeView.getRoot()) {
                parentNode.getValue().setMapped(true);
                parentNode = parentNode.getParent();
            }

            logMessage("Connection Successful");

            // Needed to reset 'Not Mapped' tag
            int row = generatedTreeView.getRow(selGenClass);
            generatedTreeView.getRoot().setExpanded(false);
            generatedTreeView.getRoot().setExpanded(true);
            generatedTreeView.getSelectionModel().select(row);
        } else {
            UiDialogTools.showException(new CustomException("Class Already Mapped"), "Beim Mapping gab es einen Fehler");
        }
    }

    /*
     * Edit the selected generated class
     */
    @FXML
    public void editGenClass() {
        editItem();
    }

    /*
     * Expands the tree to the given class
     */
    private void expandTreeView(TreeItem<?> item) {
        if (item != null) {
            item.setExpanded(true);
            TreeItem<?> parent = item.getParent();
            if (parent != null) {
                parent.setExpanded(true);
                expandTreeView(parent);
            }
        }
    }

    /*
     * Shows the corresponding schema class to the selected generated class
     */
    @FXML
    public void showMapping() {
        // hier gibt es kein Mapping
        if (getSelectedGeneratedClass() == null || getSelectedGeneratedClass().getValue().getMappedTreeview() == null) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Warnung");
            alert.setHeaderText("Achtung, hier gibt es kein Mapping");
            alert.setContentText(
                    "Beachten Sie: Es kann in manchen Fällen gewünscht sein, dass kein explizites Mapping erfolgt, da es kein passendes Feld auf der rechten Seite gibt! Sollte dennoch ein Mapping erfolgen, können Sie über den Connect- Button das Mapping auch nachträglich noch vornehmen");
            alert.showAndWait();
            return;
        }
        expandTreeView(getSelectedGeneratedClass().getValue().getMappedTreeview());
        int row = schemaTreeView.getRow(getSelectedGeneratedClass().getValue().getMappedTreeview());
        schemaTreeView.getSelectionModel().select(row);
    }

    /*
     * Gets the selected generated class
     */
    @FXML
    private TreeItem<MappableItem> getSelectedGeneratedClass() {
        return generatedTreeView.getSelectionModel().getSelectedItem();
    }

    /*
     * Gets the selected schema class
     */
    private TreeItem<MappableItem> getSelectedSchemaClass() {
        return schemaTreeView.getSelectionModel().getSelectedItem();
    }

    /*
     * Populates the template tree based on the package
     * "de.kommone.kmc.webservice"
     */
    private void populateTemplate() {

        // Need a hidden root to hold the template classes
        TreeItem<MappableItem> hiddenRoot = new TreeItem<>(new MappableItem(new Item(null, null, false,
                false, false), false, false));
        templateTreeView.setRoot(hiddenRoot);
        templateTreeView.setShowRoot(false);

        // Load classes from package
        ArrayList<Class<?>> templateClasses = null;
        try {
            templateClasses = MappingHelper.getClassNamesFromPackage("de.kommone.kmc.webservice");
        } catch (ClassNotFoundException | IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        ClassTreeLoader treeLoader = new ClassTreeLoader();
        TreeViewHelper helper = new TreeViewHelper();

        for (Class<?> templateClass : templateClasses) {
            templateTreeView.getRoot().getChildren().add(helper.getTreeItem(treeLoader.loadTreeFromClass(
                    templateClass), false, false));
        }

        searchTemplateField.setOnKeyReleased(event -> {

            Stack<TreeItem<MappableItem>> stack = new Stack<>();

            // Go through all elements in the templateTree tree (BFS)
            for (TreeItem<MappableItem> child : templateTreeView.getRoot().getChildren()) {
                stack.push(child);
            }

            while (!stack.empty()) {

                TreeItem<MappableItem> child = stack.pop();

                if (!child.getValue().getItem().getAttributeName().toLowerCase().contains(searchTemplateField
                        .getText()
                        .toLowerCase())) {
                    storageList.add(child);
                    templateTreeView.getRoot().getChildren().remove(child);
                }
            }

            for (Iterator<TreeItem<MappableItem>> it = storageList.iterator(); it.hasNext(); ) {

                TreeItem<MappableItem> item = it.next();

                if (item.getValue().getItem().getAttributeName().toLowerCase().contains(searchTemplateField
                        .getText().toLowerCase())
                        || searchTemplateField.getText().replace(" ", "").equals("")) {

                    // GeneratedClass copyItem = new GeneratedClass(item, true);
                    templateTreeView.getRoot().getChildren().add(item);

                    it.remove();
                }
            }

            templateTreeView.getRoot().getChildren().sort(generatedClassComparator);
            templateTreeView.refresh();
        });
    }

    /*
     * Creates a template class to the generated class tree
     */
    @FXML
    private void addTemplate() {

        if (getSelectedGeneratedClass() == null || getSelectedGeneratedClass().getChildren() == null) {
            UiDialogTools.showException(new CustomException(), "Bitte wählen Sie zuerst eine Nachricht!");
            return;
        }

        Boolean asList = listCheckBox.selectedProperty().getValue();

        // eine Kopie des Template- Objektes machen
        TreeItem<MappableItem> itemToCopy = templateTreeView.getSelectionModel().getSelectedItem();

        if (itemToCopy == null) {
            UiDialogTools.showException(new CustomException("Wählen Sie bitte zuerst ein Template aus, das kopiert werden soll"),
                    "Fehler beim Kopieren!");
            return;
        }

        // Kopieren
        TreeViewHelper helper = new TreeViewHelper();
        TreeItem<MappableItem> copiedItem = copyTree(itemToCopy, true, helper, false, false);

        // update the icon according to List-Attribut
        copiedItem.getValue().getItem().setList(asList);
        copiedItem.setGraphic(helper.getGraphic(copiedItem.getValue().getItem(), false));

        getSelectedGeneratedClass().getChildren().add(copiedItem);
        expandTreeView(getSelectedGeneratedClass());

    }

    @FXML
    private void close() {
        converter = null;
        generatedTreeView.setRoot(null);
        schemaTreeView.setRoot(null);
        genNameHeader.setText(POJO_DEFAULT);
        schemaNameHeader.setText(SCHEME_NAME_DEFAULT);
        attributeTable.getItems().clear();
        attributeTable.setVisible(false);
        setStageTitle(TITLE);
    }

    private TreeItem<MappableItem> copyTree(TreeItem<MappableItem> tree, boolean isPojo, TreeViewHelper helper,
                                            boolean isRoot, boolean copyMapping) {
        TreeItem<MappableItem> result = new TreeItem<>(new MappableItem(tree.getValue(), isPojo), helper
                .getGraphic(tree.getValue().getItem(), isRoot));
        if (isRoot) {
            result.getValue().setRoot(true);
        }
        if (copyMapping) {
            result.getValue().getItem().setMappedTo(helper.getMappedTo(tree));
            helper.transformItem(result.getValue());
        }
        for (TreeItem<MappableItem> child : tree.getChildren()) {
            result.getChildren().add(copyTree(child, isPojo, helper, false, copyMapping));
        }
        return result;
    }

    /*
     * Creates a class in the template tree based on the selected generated
     * class
     */
    @FXML
    private void createTemplate() {
        createTemplate(getSelectedGeneratedClass());
    }

    /*
     * Creates a class in the template tree based on the given generated class
     */
    private void createTemplate(TreeItem<MappableItem> genTreeItem) {

        // TODO : Duplikate erkennen und ggf. austauschen (nach Rückfrage)

        // die kopierte Treeview schließlich einhängen
        TreeItem<MappableItem> copiedItem = copyTree(genTreeItem, false, new TreeViewHelper(), false, false);

        templateTreeView.getRoot().getChildren().add(copiedItem);
        templateTreeView.getRoot().getChildren().sort(generatedClassComparator);
    }

    /*
     * Comparator for schema classes
     */
    final Comparator<Class<?>> schemaRootClassComparator = (c1, c2) -> {

        String c1num = c1.getName().substring(c1.getName().length() - 4);
        String c2num = c2.getName().substring(c2.getName().length() - 4);

        return c1num.compareTo(c2num);
    };

    /*
     * Comparetor for generated classes
     */
    final Comparator<TreeItem<MappableItem>> generatedClassComparator = (c1, c2) -> {

        String c1name = c1.getValue().getItem().getAttributeName();
        String c2name = c2.getValue().getItem().getAttributeName();

        return c1name.compareTo(c2name);
    };

}
