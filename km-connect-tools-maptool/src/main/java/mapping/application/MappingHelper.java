
package mapping.application;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

public class MappingHelper {

    private static Map<String, Class<?>> primitiveTypes = new HashMap<>();

    public static Class<?> mapPrimitiveType(String type) {
        primitiveTypes.put("boolean", boolean.class);
        primitiveTypes.put("byte", byte.class);
        primitiveTypes.put("char", char.class);
        primitiveTypes.put("short", short.class);
        primitiveTypes.put("int", int.class);
        primitiveTypes.put("long", long.class);
        primitiveTypes.put("float", float.class);
        primitiveTypes.put("double", double.class);

        if (primitiveTypes.get(type) != null) {
            return primitiveTypes.get(type);
        }
        return null;
    }

    public static Class<?> createMessageFromName(ArrayList<Class<?>> bauClasses, String message) {

        if (message == null || message.replace(" ", "").equals("")) {
            return null;
        }

        for (Class<?> bauClass : bauClasses) {
      if (bauClass.getSimpleName().toLowerCase().equals(message.toLowerCase())) {
                return bauClass;
            }
        }
        return null;
    }

  public static ArrayList<Class<?>> getRootClasses(ArrayList<Class<?>> bauClasses, boolean isWSDLPackage) {

        ArrayList<Class<?>> bauRootClasses = new ArrayList<Class<?>>();
        for (Class<?> bauClass : bauClasses) {
      if (bauClass.getAnnotation(XmlRootElement.class) != null || isWSDLPackage) {
                bauRootClasses.add(bauClass);
            }
        }
        return bauRootClasses;
    }

    // TODO : am besten in eigenem Thread laden, da die Methode recht lange
    // braucht
    public static ArrayList<Class<?>> getClassNamesFromPackage(String packageName) throws IOException,
            URISyntaxException, ClassNotFoundException {

        ArrayList<Class<?>> result = new ArrayList<>();
        List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());

        // don't exclude Object class
        Reflections reflections = new Reflections(new ConfigurationBuilder().setScanners(new SubTypesScanner(false),
                new ResourcesScanner()).setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(
                        new ClassLoader[0]))).filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(
                                packageName))));

        // TODO : alphabetische Sortierung
        Set<Class<?>> classes = reflections.getSubTypesOf(Object.class);
        result.addAll(classes);
        return result;
    }

    public static String capitalize(String str) {
        if (str == null || str.isEmpty()) {
            return str;
        }
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public static String uncapitalize(String str) {
        return str.substring(0, 1).toLowerCase() + str.substring(1);
    }
}
