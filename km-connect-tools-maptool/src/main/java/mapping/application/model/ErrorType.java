package mapping.application.model;

public enum ErrorType {
    MISSING, WRONG_MAPPING
}
