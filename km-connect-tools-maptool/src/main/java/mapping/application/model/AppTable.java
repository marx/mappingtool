package mapping.application.model;

import javafx.scene.control.TreeItem;
import mapping.application.utils.javafx.MappableItem;

/*
 * Class needed to display the validation information in the application's table
 */
public class AppTable {

    private TreeItem<MappableItem> leftItem;
    private TreeItem<MappableItem> rightItem;
    private String missingName;
    private String missingPath;
    private ErrorType error;

    public AppTable(TreeItem<MappableItem> leftItem, String missingName, String missingPath, ErrorType error) {
        this.leftItem = leftItem;
        this.missingName = missingName;
        this.missingPath = missingPath;
        this.error = error;
    }

    public AppTable(TreeItem<MappableItem> leftItem, TreeItem<MappableItem> rightItem, String missingName,
            String path, ErrorType error) {
        this.leftItem = leftItem;
        this.rightItem = rightItem;
        this.missingName = missingName;
        this.missingPath = path;
        this.error = error;
    }

    public TreeItem<MappableItem> getLeftItem() {
        return leftItem;
    }

    public void setGenClass(TreeItem<MappableItem> leftItem) {
        this.leftItem = leftItem;
    }

    public String getMissingName() {
        return missingName;
    }

    public void setMissingName(String missingName) {
        this.missingName = missingName;
    }

    public String getMissingPath() {
        return missingPath;
    }

    public void setMissingPath(String missingPath) {
        this.missingPath = missingPath;
    }

    public TreeItem<MappableItem> getRightItem() {
        return rightItem;
    }

    public void setRightItem(TreeItem<MappableItem> rightItem) {
        this.rightItem = rightItem;
    }

    public ErrorType getError() {
        return error;
    }

    public void setError(ErrorType error) {
        this.error = error;
    }

}
