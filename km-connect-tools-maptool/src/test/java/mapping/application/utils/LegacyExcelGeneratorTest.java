package mapping.application.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Test;

public class LegacyExcelGeneratorTest {


    @Test
    public void testLeereGenerierung() {
        LegacyExcelGenerator legacyExcelGenerator = new LegacyExcelGenerator();
        Workbook workbook = legacyExcelGenerator.getWorkbook();
        assertNotNull(workbook);
        assertEquals(1, workbook.getNumberOfSheets());
        Sheet sheet = workbook.getSheetAt(0);
        assertEquals(0, sheet.getLastRowNum());
        assertEquals(6, sheet.getRow(0).getLastCellNum());
    }

    @Test
    public void testExcelGenerierung() throws ClassNotFoundException {
        List<String> lines = TestUtils.readListFromFile(
                "SterbeurkundenBestellungen084030.txt");
        LegacyExcelGenerator legacyExcelGenerator = new LegacyExcelGenerator();
        legacyExcelGenerator.generate(lines);
        assertNotNull(legacyExcelGenerator.getWorkbook());
        assertEquals(62 + 1, legacyExcelGenerator.getWorkbook().getSheetAt(0).getLastRowNum());


    }

}