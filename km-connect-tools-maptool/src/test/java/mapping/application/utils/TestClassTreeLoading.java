package mapping.application.utils;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import de.kommone.webservice.xbau.v22.BaugenehmigungAntrag0200;
import de.kommone.webservice.xmeld.vW.FortschreibungGeburt0003;
import mapping.application.utils.xsd.XSDParser;

public class TestClassTreeLoading {

    private List<String> validationErrors = new ArrayList<>();

    @Before
    public void init() {
        System.setProperty("http.proxyHost", "squid.dzbw.de");
        System.setProperty("http.proxyPort", "3128");
    }

    @After
    public void exit() {
        System.setProperty("http.proxyHost", "");
        System.setProperty("http.proxyPort", "");
    }

    @Test
    public void testReadClassesFromFileXMeld() throws ClassNotFoundException, InstantiationException,
            IllegalAccessException, FileNotFoundException {

        compareTrees("src/main/resources/xsd/xmeld/vW/xmeld-nachrichten-fortschreibung.xsd",
                "fortschreibung.geburt.0003", FortschreibungGeburt0003.class);

    }

    private void compareTrees(String pathToXSD, String nameOfXSDElement, Class<?> clazz) throws ClassNotFoundException,
            InstantiationException, IllegalAccessException, FileNotFoundException {

        // 1. Read from xsd
        XSDParser xsdparser = new XSDParser();
        TreeOfClasses treeFromXSD = xsdparser.getClassAsTree(pathToXSD, nameOfXSDElement);
        Assert.assertNotNull(treeFromXSD);

        // 2. Read the same tree from classpath
        TreeOfClasses treeFromClassPath = new ClassTreeLoader().loadTreeFromClass(clazz);
        Assert.assertNotNull(treeFromClassPath);

        // 3. Compare the two trees
        validateTrees(treeFromXSD, treeFromClassPath);

        Assert.assertTrue(getErrorStringFromList(), validationErrors.isEmpty());

        validateTrees(treeFromClassPath, treeFromXSD);

        Assert.assertTrue(getErrorStringFromList(), validationErrors.isEmpty());

        int elementsTree = recursiveCount(treeFromXSD);
        int elementsTreeFromClassPath = recursiveCount(treeFromClassPath);

        Assert.assertEquals(elementsTree, elementsTreeFromClassPath);

    }

    private int recursiveCount(TreeOfClasses tree) {
        if (tree == null) {
            return 0;
        } else {
            if (tree.getChildren().isEmpty()) {
                return 0;
            } else {
                int sum = 0;
                for (TreeOfClasses child : tree.getChildren()) {
                    sum++;
                    sum += recursiveCount(child);
                }
                return sum;
            }
        }
    }

    @Test
    @Ignore
    public void testReadClassesFromFileXBau() throws ClassNotFoundException, InstantiationException,
            IllegalAccessException,
            FileNotFoundException {

        compareTrees("src/main/resources/xsd/xbau/v22/xbau-nachrichten-baugenehmigung.xsd",
                "baugenehmigung.antrag.0200", BaugenehmigungAntrag0200.class);

    }

    private String getErrorStringFromList() {
        String result = "";
        for (String error : validationErrors) {
            result += "\n" + error;
        }
        return result;
    }

    private void validateTrees(TreeOfClasses fromXSD, TreeOfClasses fromClassPath) {
        validationErrors.clear();
        validateTrees(fromXSD, fromClassPath, "root");
    }

    private void validateTrees(TreeOfClasses fromXSD, TreeOfClasses fromClassPath, String path) {
        List<TreeOfClasses> children = fromXSD.getChildren();
        for (TreeOfClasses child : children) {
            TreeOfClasses correspondingChild = getChildFromOtherTree(child, fromClassPath);
            if (correspondingChild == null) {
                validationErrors.add(path + "->" + child.getItem().getAttributeName()
                        + " konnte nicht zugeordnet werden!");
            } else {
                validateTrees(child, correspondingChild, path + "->" + child.getItem().getAttributeName());
            }
        }

    }

    private TreeOfClasses getChildFromOtherTree(TreeOfClasses fromXSD, TreeOfClasses fromClassPath) {
        List<TreeOfClasses> children = fromClassPath.getChildren();
        for (TreeOfClasses child : children) {
            if (fromXSD.getItem().getAttributeName().equals(child.getItem().getAttributeName())) {
                return child;
            }
        }
        return null;
    }

}
