package mapping.application.utils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class TestUtils {

    public static List<String> readListFromFile(String input) {
        List<String> result = new ArrayList<>();
        InputStream in = CodeGen.class.getClassLoader().getResourceAsStream(input);
        boolean isFile = in == null;

        try (BufferedReader br = new BufferedReader(isFile ? new FileReader(input)
                : new InputStreamReader(in, StandardCharsets.UTF_8))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                result.add(line);
                sb.append('\n');
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
