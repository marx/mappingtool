package mapping.application.pl;

import de.kommone.webservice.xbau.v23.BeteiligungStellungnahme0303;
import junit.framework.TestCase;
import mapping.application.utils.ClassTreeLoader;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PLExcelGeneratorTest extends TestCase {

    private static final Path TEST_FOLDER = Paths.get("target", "tests");

    private static final String FILE1 = "seitenbau/KlickDummy_Antrag_Bauvorbescheid-v1.1-de.json";
    private static final String FILE2 = "seitenbau/BauN-Stellungnahme.Nachbesserung.0204-v1.1_XBau_2.2-de.json";
    private static final String FILE3 = "seitenbau/SBWS-BAU3-Kenntnisgabeverfahren-Antrag-v1.1.0-de-mit-Bautechnik-und-Standsicherheit.json";

    @Before
    public void setup() {
        ZipSecureFile.setMinInflateRatio(0);
    }


    @Test
    public void testFileOutput() throws IOException, URISyntaxException {
        Path path = createTestFile(FILE1, true);
         createTestFile(FILE2, true);
        createTestFile(FILE3, true);
        assertTrue(Files.isRegularFile(path));
    }

    @Test
    public void testSheets() throws IOException, InvalidFormatException, URISyntaxException {
        ZipSecureFile.setMinInflateRatio(0);
        Path path = createTestFile(FILE1, true);
        Workbook workbook = new XSSFWorkbook(path.toFile());
        assertEquals(3, workbook.getNumberOfSheets());
        assertNotNull(workbook.getSheet("Erläuterung Kardinalität"));
    }

    @Test
    public void testCardinality() throws IOException, InvalidFormatException, URISyntaxException {
        ZipSecureFile.setMinInflateRatio(0);
        Path path = createTestFile(FILE1, false);
        Workbook workbook = new XSSFWorkbook(path.toFile());
        assertEquals(2, workbook.getNumberOfSheets());
        assertNull(workbook.getSheet("Erläuterung Kardinalität"));
    }

    private Path createTestFile(String filename, boolean generateCardinalitySheet) throws IOException, URISyntaxException {
        Files.createDirectories(TEST_FOLDER);
        String input = new String(Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource(filename).toURI())), StandardCharsets.UTF_8);//IOUtils.resourceToString("KlickDummy_Antrag_Bauvorbescheid-v1.1-de.json", StandardCharsets.UTF_8);
        PLExcelGenerator excelGenerator = new PLExcelGenerator(input, new ClassTreeLoader().loadTreeFromClass(BeteiligungStellungnahme0303.class));
        excelGenerator.setGenerateCardinalitySheet(generateCardinalitySheet);
        excelGenerator.generate();
        Path path = TEST_FOLDER.resolve("data-bau.xlsx").toAbsolutePath();
        if (Files.exists(path))
            Files.delete(path);
        Files.write(path,
                excelGenerator.getExcelOutput());
        return path;
    }

}