package de.kommone.kmc.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;

import de.kommone.webservice.xbau.v22.BaugenehmigungAntrag0200;
import javafx.scene.control.TreeItem;
import mapping.application.utils.ClassTreeLoader;
import mapping.application.utils.CodeGen;
import mapping.application.utils.FormularGenerator;
import mapping.application.utils.JUnitGenerator;
import mapping.application.utils.TestUtils;
import mapping.application.utils.TreeOfClasses;
import mapping.application.utils.javafx.MappableItem;
import mapping.application.utils.javafx.TreeViewHelper;
import mapping.application.utils.javafx.XSDHelper;
import org.junit.Ignore;

public class Test {

    /**
     * Scheme -> POJO für die Wasserentnahme. Die Compare-Datei enthält den
     * erzeugten Source-Code aus der zuletzt gespeicherten Progress-File. Bei
     * dem Test geht es primär darum zu prüfen, ob sich etwas an der
     * Code-Generierung verändert, sollte man Diese refactorn wollen oder müssen
     * 
     * @throws ClassNotFoundException
     */
    @org.junit.Test
    public void testWasserCode() throws ClassNotFoundException {

        // POJO-Package: de.kommone.kmc.webservice
        // Schema-Package: de.kommone.webservice.wee
        CodeGen codegen = new CodeGen();
        String code = codegen.generateCode(true, "de.kommone.kmc.webservice", "WEEResponseToWasserData",
                TestUtils.readListFromFile("weeMapping.txt"), LocalDate.of(2021, 2, 11));
        String code2Compare = readStringFromFile("wasserCompare.txt");
        code = code.replaceAll("\\s+", "");
        code2Compare = code2Compare.replaceAll("\\s+", "");
        Assert.assertEquals(code, code2Compare);

    }

    @org.junit.Test
    public void testFormularGenerierung() throws ClassNotFoundException {

        List<String> lines = TestUtils.readListFromFile("SterbeurkundenBestellungen084030.txt");
        FormularGenerator generator = new FormularGenerator();
        String json = generator.generateSeitenbauJSon(lines);
        System.out.println(json);
        Assert.assertNotNull(json);

        // Erwarteter Output aus bereits exportierter Datei auslesen
        // Zeilenumbrüche sollten beachtet werden sowie die Tatsache, dass nicht gesetzte Felder im JSON nicht vorhanden sind
        String expectedJson = String.join("\r\n", TestUtils.readListFromFile("Seitenbau_Output_SterbeurkundenBestellungen084030.json"));
        Assert.assertEquals(json, expectedJson);

    }

    /**
     * POJO -> Scheme für DIWO (Wohngeld). Die Compare-Datei enthält den
     * erzeugten Source-Code aus der zuletzt gespeicherten Progress-File. Bei
     * dem Test geht es primär darum zu prüfen, ob sich etwas an der
     * Code-Generierung verändert, sollte man Diese refactorn wollen oder müssen
     * 
     * @throws ClassNotFoundException
     */
    @org.junit.Test
    public void testDiwoCode() throws ClassNotFoundException {

        // POJO-Package: de.kommone.kmc.webservice
        // Schema-Package: de.kommone.webservice.diwo
        CodeGen codegen = new CodeGen();
        String code = codegen.generateCode(false, "de.kommone.kmc.webservice", "DiwoDataToScheme", TestUtils.readListFromFile(
                "diwo.txt"), LocalDate
                .of(2021, 2, 11));
        String code2Compare = readStringFromFile("diwoCompare.txt");
        code = code.replaceAll("\\s+", "");
        code2Compare = code2Compare.replaceAll("\\s+", "");
        Assert.assertEquals(code, code2Compare);

    }

    /**
     * POJO -> Scheme für Laemmkomm (Unterhaltsvorschuss). Die Compare-Datei enthält
     * den erzeugten Source-Code aus der zuletzt gespeicherten Progress-File. Bei
     * dem Test geht es primär darum zu prüfen, ob sich etwas an der
     * Code-Generierung verändert, sollte man Diese refactorn wollen oder müssen
     * 
     * @throws ClassNotFoundException
     */
    @org.junit.Test
    @Ignore
    public void testUVOCode() throws ClassNotFoundException {

        // POJO-Package: de.kommone.kmc.webservice
        // Schema-Package: de.kommone.webservice.diwo
        CodeGen codegen = new CodeGen();
        String code = codegen.generateCode(false, "de.kommone.kmc.webservice", "UVOMapper",
                TestUtils.readListFromFile("uvoLaemmkomm.txt"), LocalDate.of(2021, 2, 11));
        String code2Compare = readStringFromFile("uvoCompare.txt");
        code = code.replaceAll("\\s+", "");
        code2Compare = code2Compare.replaceAll("\\s+", "");
        Assert.assertEquals(code, code2Compare);

    }

    /**
     * POJO -> Scheme für DIWO (Wohngeld). Die Compare-Datei enthält den erzeugten
     * Source-Code aus der zuletzt gespeicherten Progress-File. Bei dem Test geht es
     * primär darum zu prüfen, ob sich etwas an der Code-Generierung verändert,
     * sollte man Diese refactorn wollen oder müssen
     * 
     * @throws ClassNotFoundException
     */
    @org.junit.Test
    public void testXMeldCode() throws ClassNotFoundException {

        // POJO-Package: de.kommone.kmc.webservice
        // Schema-Package: de.kommone.webservice.diwo
        CodeGen codegen = new CodeGen();
        String code = codegen.generateCode(false, "de.kommone.kmc.webservice", "Test2",
                TestUtils.readListFromFile("XMeldTest.txt"), LocalDate.of(2021, 2, 11));
        String code2Compare = readStringFromFile("xmeldCompare.txt");
        code = code.replaceAll("\\s+", "");
        code2Compare = code2Compare.replaceAll("\\s+", "");
        Assert.assertEquals(code, code2Compare);

    }

    /**
     * Im Rahmen von xFamilie wird die Konvertierung von einfachen XML-
     * Elementen erprobt
     * 
     * @throws ClassNotFoundException
     */
    @org.junit.Test
    public void testXFamilieCode() throws ClassNotFoundException {

        CodeGen codegen = new CodeGen();
        String code = codegen.generateCode(false, "de.kommone.kmc.webservice", "Test2", TestUtils.readListFromFile(
                "xfamilie.txt"), LocalDate.of(2021, 2, 11));
        String code2Compare = readStringFromFile("xfamilieCompare.txt");
        code = code.replaceAll("\\s+", "");
        code2Compare = code2Compare.replaceAll("\\s+", "");
        Assert.assertEquals(code, code2Compare);

    }

    /**
     * POJO -> Scheme für Hund-anmelden. Die Compare-Datei enthält den erzeugten
     * Source-Code aus der zuletzt gespeicherten Progress-File. Bei dem Test geht es
     * primär darum zu prüfen, ob sich etwas an der Code-Generierung verändert,
     * sollte man Diese refactorn wollen oder müssen
     * 
     * @throws ClassNotFoundException
     */
    @org.junit.Test
    public void testHundCode() throws ClassNotFoundException {

        // POJO-Package: de.kommone.kmc.webservice
        // Schema-Package: de.kommone.webservice.xfallHund
        CodeGen codegen = new CodeGen();
        String code = codegen.generateCode(false, "de.kommone.kmc.webservice", "HundDataToScheme",
                TestUtils.readListFromFile("hund.txt"),
                LocalDate.of(2021, 3, 30));
        String code2Compare = readStringFromFile("hundCompare.txt");
        code = code.replaceAll("\\s+", "");
        code2Compare = code2Compare.replaceAll("\\s+", "");
        Assert.assertEquals(code, code2Compare);

    }

    /**
     * POJO -> Scheme für XBau. Die Compare-Datei enthält den erzeugten
     * Source-Code aus der zuletzt gespeicherten Progress-File. Bei dem Test
     * geht es primär darum zu prüfen, ob sich etwas an der Code-Generierung
     * verändert, sollte man Diese refactorn wollen oder müssen
     * 
     * @throws ClassNotFoundException
     */
    @org.junit.Test
    public void testXbauCode() throws ClassNotFoundException {

        // POJO-Package: de.kommone.kmc.webservice
        // Schema-Package: de.kommone.webservice.xbau.v22
        CodeGen codegen = new CodeGen();
        String code = codegen.generateCode(false, "de.kommone.kmc.webservice", "XbauAntragDataToScheme0200",
                TestUtils.readListFromFile("bauAntrag0200.txt"), LocalDate.of(2021, 2, 11));
        String code2Compare = readStringFromFile("bauCompare.txt");
        code = code.replaceAll("\\s+", "");
        // codegen.output(new File("C:/Tools/Sonstiges/MappingTool/out1.txt"),
        // code);
        code2Compare = code2Compare.replaceAll("\\s+", "");
        // codegen.output(new File("C:/Tools/Sonstiges/MappingTool/out2.txt"),
        // code2Compare);
        Assert.assertEquals(code, code2Compare);

    }

    private String readStringFromFile(String input) {
        InputStream in = CodeGen.class.getClassLoader().getResourceAsStream(input);
        boolean isFile = in == null;

        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(isFile ? new FileReader(new File(input))
                : new InputStreamReader(in, StandardCharsets.UTF_8))) {
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    @org.junit.Test
    public void testLoadTreeViaReflection() {
        ClassTreeLoader treeLoader = new ClassTreeLoader();
        TreeOfClasses tree = treeLoader.loadTreeFromClass(BaugenehmigungAntrag0200.class);
        Assert.assertNotNull(tree);
    }

    @org.junit.Test
    @org.junit.Ignore
    public void testGenerateTest() {
        List<String> fileContent = TestUtils.readListFromFile("diwo.txt");
        ClassTreeLoader treeLoader = new ClassTreeLoader();
        TreeOfClasses tree = treeLoader.loadTreeFromConfig(fileContent);
        JUnitGenerator generator = new JUnitGenerator();
        System.out.println(generator.generateTestCode(tree));
    }

    @org.junit.Test
    @org.junit.Ignore
    public void testGenerateTest1() {
        List<String> fileContent = TestUtils.readListFromFile("bauAntrag0200.txt");
        ClassTreeLoader treeLoader = new ClassTreeLoader();
        TreeOfClasses tree = treeLoader.loadTreeFromConfig(fileContent);
        JUnitGenerator generator = new JUnitGenerator();
        System.out.println(generator.generateTestCode(tree));
    }

    @org.junit.Test
    public void testLoadTreeViaFile() {
        List<String> fileContent = TestUtils.readListFromFile("bauLoad.txt");
        ClassTreeLoader treeLoader = new ClassTreeLoader();
        TreeOfClasses tree = treeLoader.loadTreeFromConfig(fileContent);
        TreeViewHelper helper = new TreeViewHelper();
        TreeItem<MappableItem> treeItem = helper.getTreeItem(tree, true);
        XSDHelper xsdHelper = new XSDHelper();
        List<String> warnings = new LinkedList<String>();
        String xsd = xsdHelper.buildSchemeFromTree(treeItem, warnings);
        Assert.assertNotNull(xsd);
    }



}
