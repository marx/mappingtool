package de.kommone.kmc.webservice;
/** Dieser Quellcode wurde mit dem KM-Connect-Mapping-Tool am 2021-02-11 generiert, evtl. wurde der Code manuell ergänzt 
 @author KM-Connect-DEV-TEAM**/ 

public class WEEResponseToWasserData {public de.kommone.kmc.webservice.WasserData createWasserDataFromWasserData(de.kommone.webservice.wee.WeeResponse source){
if(source == null) return null;
de.kommone.kmc.webservice.WasserData target = new de.kommone.kmc.webservice.WasserData();
target.setAbsenderAenderungAnschrift(getAbsenderAenderungAnschriftFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderAenderungBankDaten(getAbsenderAenderungBankDatenFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderAnschriftPostleitzahl(getAbsenderAnschriftPostleitzahlFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderBankVerbindungIban(getAbsenderBankVerbindungIbanFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderFirmaname(getAbsenderFirmanameFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderKontaktdatenKennung(getAbsenderKontaktdatenKennungFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderNamePersonFamiliename(getAbsenderNamePersonFamilienameFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderNamePersonVorname(getAbsenderNamePersonVornameFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderanSchriftHausnummer(getAbsenderanSchriftHausnummerFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderanSchriftOrt(getAbsenderanSchriftOrtFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderanSchriftStrasse(getAbsenderanSchriftStrasseFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderanSchriftZusatz(getAbsenderanSchriftZusatzFromWeeErklaerung(source.getWeeErklaerung()));target.getEntnahmestelle().addAll(createEntnahmestelleenFromEntnahmestelle(getEntnahmestelleFromWeeErklaerung(source.getWeeErklaerung())));target.setAbsenderWeeNummer(getAbsenderWeeNummerFromWeeErklaerung(source.getWeeErklaerung()));target.setWassermenge(getWassermengeFromWeeErklaerung(source.getWeeErklaerung()));target.setBetragOhneErmaessigungen(getBetragOhneErmaessigungenFromWeeErklaerung(source.getWeeErklaerung()));target.setMengeermaessigungBetrag(getMengeermaessigungBetragFromWeeErklaerung(source.getWeeErklaerung()));target.setWasserentnahmeentgelt(getWasserentnahmeentgeltFromWeeErklaerung(source.getWeeErklaerung()));target.setErfolg(source.isSuccess());target.setFehlermeldung(source.getMessage());target.setAbsenderBankVerbindungKontoinhaber(getAbsenderBankVerbindungKontoinhaberFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderBankVerbindungBic(getAbsenderBankVerbindungBicFromWeeErklaerung(source.getWeeErklaerung()));target.setEmpfaengerName(getEmpfaengerNameFromWeeErklaerung(source.getWeeErklaerung()));target.setJahr(getJahrFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderKontaktDatenKanal(getAbsenderKontaktDatenKanalFromWeeErklaerung(source.getWeeErklaerung()));target.setDefaultListVersionID(getDefaultListVersionIDFromWeeErklaerung(source.getWeeErklaerung()));target.setGeleisteteVorauszahlungen(getGeleisteteVorauszahlungenFromWeeErklaerung(source.getWeeErklaerung()));target.setZuzahlenderBetrag(getZuzahlenderBetragFromWeeErklaerung(source.getWeeErklaerung()));target.setErsteKuenftigeVorauszahlung(getErsteKuenftigeVorauszahlungFromWeeErklaerung(source.getWeeErklaerung()));target.setZweiteKuenftigeVorauszahlung(getZweiteKuenftigeVorauszahlungFromWeeErklaerung(source.getWeeErklaerung()));target.setAbsenderBankVerbindungBankinstitut(getAbsenderBankVerbindungBankinstitutFromWeeErklaerung(source.getWeeErklaerung()));target.setEntgeltM3(getEntgeltM3FromWeeErklaerung(source.getWeeErklaerung()));target.setSEPAerteilt(getSEPAerteiltFromWeeErklaerung(source.getWeeErklaerung()));target.setSEPAzuvorerklaert(getSEPAzuvorerklaertFromWeeErklaerung(source.getWeeErklaerung()));target.setSbwUserId(getSbwUserIdFromWeeErklaerung(source.getWeeErklaerung()));return target;
}private boolean getAbsenderAenderungAnschriftFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return false;return getAbsenderAenderungAnschriftFromAbsender(source.getAbsender()
);
}private boolean getAbsenderAenderungAnschriftFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return false;return source.isAenderungAnschrift();
}private boolean getAbsenderAenderungBankDatenFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return false;return getAbsenderAenderungBankDatenFromAbsender(source.getAbsender()
);
}private boolean getAbsenderAenderungBankDatenFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return false;return source.isAenderungBankdaten();
}private java.lang.String getAbsenderAnschriftPostleitzahlFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderAnschriftPostleitzahlFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderAnschriftPostleitzahlFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderAnschriftPostleitzahlFromAnschrift(source.getAnschrift()
);
}private java.lang.String getAbsenderAnschriftPostleitzahlFromAnschrift(de.kommone.webservice.wee.Anschrift source){
if(source == null) return null;return source.getPostleitzahl();
}private java.lang.String getAbsenderBankVerbindungIbanFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderBankVerbindungIbanFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderBankVerbindungIbanFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderBankVerbindungIbanFromBankverbindung(source.getBankverbindung()
);
}private java.lang.String getAbsenderBankVerbindungIbanFromBankverbindung(de.kommone.webservice.wee.Bankverbindung source){
if(source == null) return null;return source.getIban();
}private java.lang.String getAbsenderFirmanameFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderFirmanameFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderFirmanameFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderFirmanameFromFirma(source.getFirma()
);
}private java.lang.String getAbsenderFirmanameFromFirma(de.kommone.webservice.wee.NameOrganisation source){
if(source == null) return null;return source.getName();
}private java.lang.String getAbsenderKontaktdatenKennungFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderKontaktdatenKennungFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderKontaktdatenKennungFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderKontaktdatenKennungFromKontaktDaten(source.getKontaktDaten()
);
}private java.lang.String getAbsenderKontaktdatenKennungFromKontaktDaten(de.kommone.webservice.wee.Kommunikation source){
if(source == null) return null;return source.getKennung();
}private java.lang.String getAbsenderNamePersonFamilienameFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderNamePersonFamilienameFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderNamePersonFamilienameFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderNamePersonFamilienameFromNamePerson(source.getNamePerson()
);
}private java.lang.String getAbsenderNamePersonFamilienameFromNamePerson(de.kommone.webservice.wee.NameNatuerlichePerson source){
if(source == null) return null;return getAbsenderNamePersonFamilienameFromFamilienname(source.getFamilienname()
);
}private java.lang.String getAbsenderNamePersonFamilienameFromFamilienname(de.kommone.webservice.wee.AllgemeinerName source){
if(source == null) return null;return source.getName();
}private java.lang.String getAbsenderNamePersonVornameFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderNamePersonVornameFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderNamePersonVornameFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderNamePersonVornameFromNamePerson(source.getNamePerson()
);
}private java.lang.String getAbsenderNamePersonVornameFromNamePerson(de.kommone.webservice.wee.NameNatuerlichePerson source){
if(source == null) return null;return getAbsenderNamePersonVornameFromVorname(source.getVorname()
);
}private java.lang.String getAbsenderNamePersonVornameFromVorname(de.kommone.webservice.wee.AllgemeinerName source){
if(source == null) return null;return source.getName();
}private java.lang.String getAbsenderanSchriftHausnummerFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderanSchriftHausnummerFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderanSchriftHausnummerFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderanSchriftHausnummerFromAnschrift(source.getAnschrift()
);
}private java.lang.String getAbsenderanSchriftHausnummerFromAnschrift(de.kommone.webservice.wee.Anschrift source){
if(source == null) return null;return source.getHausnummer();
}private java.lang.String getAbsenderanSchriftOrtFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderanSchriftOrtFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderanSchriftOrtFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderanSchriftOrtFromAnschrift(source.getAnschrift()
);
}private java.lang.String getAbsenderanSchriftOrtFromAnschrift(de.kommone.webservice.wee.Anschrift source){
if(source == null) return null;return source.getOrt();
}private java.lang.String getAbsenderanSchriftStrasseFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderanSchriftStrasseFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderanSchriftStrasseFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderanSchriftStrasseFromAnschrift(source.getAnschrift()
);
}private java.lang.String getAbsenderanSchriftStrasseFromAnschrift(de.kommone.webservice.wee.Anschrift source){
if(source == null) return null;return source.getStrasse();
}private java.lang.String getAbsenderanSchriftZusatzFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderanSchriftZusatzFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderanSchriftZusatzFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderanSchriftZusatzFromAnschrift(source.getAnschrift()
);
}private java.lang.String getAbsenderanSchriftZusatzFromAnschrift(de.kommone.webservice.wee.Anschrift source){
if(source == null) return null;return source.getZusatz();
}private java.util.List<de.kommone.kmc.webservice.Entnahmestelle> createEntnahmestelleenFromEntnahmestelle(java.util.List<de.kommone.webservice.wee.Entnahmestelle> source){
if(source == null) return new java.util.ArrayList<>();
java.util.List<de.kommone.kmc.webservice.Entnahmestelle> target = new java.util.ArrayList<>();
for(de.kommone.webservice.wee.Entnahmestelle elem : source){
target.add(createEntnahmestelle(elem));
}
return target; 
 }private de.kommone.kmc.webservice.Entnahmestelle createEntnahmestelle(de.kommone.webservice.wee.Entnahmestelle source){
if(source == null) return null;
de.kommone.kmc.webservice.Entnahmestelle target = new de.kommone.kmc.webservice.Entnahmestelle();
target.setBezeichnung(source.getBezeichnung());target.getGemeinde().addAll(createGemeindeenFromEntity(source.getGemeinde()));target.setFlurstuecknummer(source.getFlurstueckNummer());target.getHerkunfWasser().addAll(createHerkunfWasserenFromEntity(source.getHerkunftWasser()));target.getRechtsGrundlage().addAll(createRechtsGrundlageenFromEntity(source.getRechtsGrundlage()));target.setAnderweitigeFeststellung(createAnderweitigeFeststellungFromAnderweitigeFeststellung(source.getAnderweitigeFeststellung()));target.setGenehmigung(createGenehmigungFromGenehmigung(source.getErlaubendeWasserbehoerde()));target.getVerwendungszweck().addAll(createVerwendungszweckenFromEntity(source.getVerwendungszweck()));target.getZaehler().addAll(createZaehlerenFromZaehler(source.getZaehler()));target.getFeststellungMenge().addAll(createFeststellungMengeenFromEntity(source.getFeststellungMenge()));target.setEntnommeneMenge(source.getEntnommeneMenge());target.setErmaessigung(source.getErmaessigung());target.setBerechnungsergebnis(createBerechnungsergebnisFromBerechnungsergebnis(source.getBerechnungsergebnis()));target.setZuvorerklaert(source.isZuvorerklaert());return target;
}private java.util.List<de.kommone.webservice.wee.Entnahmestelle> getEntnahmestelleFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return source.getEntnahmestellen();
}private java.util.List<de.kommone.kmc.webservice.Gemeinde> createGemeindeenFromEntity(java.util.List<de.kommone.webservice.wee.Entity> source){
if(source == null) return new java.util.ArrayList<>();
java.util.List<de.kommone.kmc.webservice.Gemeinde> target = new java.util.ArrayList<>();
for(de.kommone.webservice.wee.Entity elem : source){
target.add(createGemeinde(elem));
}
return target; 
 }private de.kommone.kmc.webservice.Gemeinde createGemeinde(de.kommone.webservice.wee.Entity source){
if(source == null) return null;
de.kommone.kmc.webservice.Gemeinde target = new de.kommone.kmc.webservice.Gemeinde();
target.setName(source.getName());target.setId(source.getId());return target;
}private java.util.List<de.kommone.kmc.webservice.HerkunfWasser> createHerkunfWasserenFromEntity(java.util.List<de.kommone.webservice.wee.Entity> source){
if(source == null) return new java.util.ArrayList<>();
java.util.List<de.kommone.kmc.webservice.HerkunfWasser> target = new java.util.ArrayList<>();
for(de.kommone.webservice.wee.Entity elem : source){
target.add(createHerkunfWasser(elem));
}
return target; 
 }private de.kommone.kmc.webservice.HerkunfWasser createHerkunfWasser(de.kommone.webservice.wee.Entity source){
if(source == null) return null;
de.kommone.kmc.webservice.HerkunfWasser target = new de.kommone.kmc.webservice.HerkunfWasser();
target.setName(source.getName());target.setId(source.getId());return target;
}private java.util.List<de.kommone.kmc.webservice.RechtsGrundlage> createRechtsGrundlageenFromEntity(java.util.List<de.kommone.webservice.wee.Entity> source){
if(source == null) return new java.util.ArrayList<>();
java.util.List<de.kommone.kmc.webservice.RechtsGrundlage> target = new java.util.ArrayList<>();
for(de.kommone.webservice.wee.Entity elem : source){
target.add(createRechtsGrundlage(elem));
}
return target; 
 }private de.kommone.kmc.webservice.RechtsGrundlage createRechtsGrundlage(de.kommone.webservice.wee.Entity source){
if(source == null) return null;
de.kommone.kmc.webservice.RechtsGrundlage target = new de.kommone.kmc.webservice.RechtsGrundlage();
target.setName(source.getName());target.setId(source.getId());return target;
}private de.kommone.kmc.webservice.AnderweitigeFeststellung createAnderweitigeFeststellungFromAnderweitigeFeststellung(de.kommone.webservice.wee.AnderweitigeFeststellung source){
if(source == null) return null;
de.kommone.kmc.webservice.AnderweitigeFeststellung target = new de.kommone.kmc.webservice.AnderweitigeFeststellung();
target.setBegruendung(source.getBegruendung());target.setZugelassen(source.isZugelassen());target.setMenge(source.getMenge());target.setGenehmigung(createGenehmigungFromGenehmigung(source.getErlaubendeWasserbehoerde()));return target;
}private de.kommone.kmc.webservice.Genehmigung createGenehmigungFromGenehmigung(de.kommone.webservice.wee.Genehmigung source){
if(source == null) return null;
de.kommone.kmc.webservice.Genehmigung target = new de.kommone.kmc.webservice.Genehmigung();
target.setGeber(source.getGeber());target.setZusatz(source.getZusatz());target.setErteilt(source.isErteilt());target.setDatum(source.getDatum());return target;
}private java.util.List<de.kommone.kmc.webservice.Verwendungszweck> createVerwendungszweckenFromEntity(java.util.List<de.kommone.webservice.wee.Entity> source){
if(source == null) return new java.util.ArrayList<>();
java.util.List<de.kommone.kmc.webservice.Verwendungszweck> target = new java.util.ArrayList<>();
for(de.kommone.webservice.wee.Entity elem : source){
target.add(createVerwendungszweck(elem));
}
return target; 
 }private de.kommone.kmc.webservice.Verwendungszweck createVerwendungszweck(de.kommone.webservice.wee.Entity source){
if(source == null) return null;
de.kommone.kmc.webservice.Verwendungszweck target = new de.kommone.kmc.webservice.Verwendungszweck();
target.setName(source.getName());target.setId(source.getId());return target;
}private java.util.List<de.kommone.kmc.webservice.Zaehler> createZaehlerenFromZaehler(java.util.List<de.kommone.webservice.wee.Zaehler> source){
if(source == null) return new java.util.ArrayList<>();
java.util.List<de.kommone.kmc.webservice.Zaehler> target = new java.util.ArrayList<>();
for(de.kommone.webservice.wee.Zaehler elem : source){
target.add(createZaehler(elem));
}
return target; 
 }private de.kommone.kmc.webservice.Zaehler createZaehler(de.kommone.webservice.wee.Zaehler source){
if(source == null) return null;
de.kommone.kmc.webservice.Zaehler target = new de.kommone.kmc.webservice.Zaehler();
target.setNummer(source.getNummer());target.setDatumAnfang(source.getDatumAnfang());target.setDatumEnde(source.getDatumEnde());target.setStandAnfang(source.getStandAnfang());target.setStandEnde(source.getStandEnde());return target;
}private java.util.List<de.kommone.kmc.webservice.FeststellungMenge> createFeststellungMengeenFromEntity(java.util.List<de.kommone.webservice.wee.Entity> source){
if(source == null) return new java.util.ArrayList<>();
java.util.List<de.kommone.kmc.webservice.FeststellungMenge> target = new java.util.ArrayList<>();
for(de.kommone.webservice.wee.Entity elem : source){
target.add(createFeststellungMenge(elem));
}
return target; 
 }private de.kommone.kmc.webservice.FeststellungMenge createFeststellungMenge(de.kommone.webservice.wee.Entity source){
if(source == null) return null;
de.kommone.kmc.webservice.FeststellungMenge target = new de.kommone.kmc.webservice.FeststellungMenge();
target.setId(source.getId());target.setName(source.getName());return target;
}private de.kommone.kmc.webservice.Berechnungsergebnis createBerechnungsergebnisFromBerechnungsergebnis(de.kommone.webservice.wee.Berechnungsergebnis source){
if(source == null) return null;
de.kommone.kmc.webservice.Berechnungsergebnis target = new de.kommone.kmc.webservice.Berechnungsergebnis();
target.setWassermenge(source.getWassermenge());target.setBetragOhneErmaessigung(source.getBetragOhneErmaessigungen());target.setMengenermaessigungsBetrag(source.getMengenermaessigungsBetrag());target.setWasserentnahmeentgelt(source.getWasserentnahmeentgelt());target.setZuzahlenderBetrag(source.getZuzahlenderBetrag());target.setEntgeltM3(source.getEntgeltM3());return target;
}private java.math.BigDecimal getAbsenderWeeNummerFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderWeeNummerFromAbsender(source.getAbsender()
);
}private java.math.BigDecimal getAbsenderWeeNummerFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return source.getWeeNummer();
}private java.math.BigDecimal getWassermengeFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getWassermengeFromAggregierteBerechnung(source.getAggregierteBerechnung()
);
}private java.math.BigDecimal getWassermengeFromAggregierteBerechnung(de.kommone.webservice.wee.Berechnungsergebnis source){
if(source == null) return null;return source.getWassermenge();
}private java.math.BigDecimal getBetragOhneErmaessigungenFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getBetragOhneErmaessigungenFromAggregierteBerechnung(source.getAggregierteBerechnung()
);
}private java.math.BigDecimal getBetragOhneErmaessigungenFromAggregierteBerechnung(de.kommone.webservice.wee.Berechnungsergebnis source){
if(source == null) return null;return source.getBetragOhneErmaessigungen();
}private java.math.BigDecimal getMengeermaessigungBetragFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getMengeermaessigungBetragFromAggregierteBerechnung(source.getAggregierteBerechnung()
);
}private java.math.BigDecimal getMengeermaessigungBetragFromAggregierteBerechnung(de.kommone.webservice.wee.Berechnungsergebnis source){
if(source == null) return null;return source.getMengenermaessigungsBetrag();
}private java.math.BigDecimal getWasserentnahmeentgeltFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getWasserentnahmeentgeltFromAggregierteBerechnung(source.getAggregierteBerechnung()
);
}private java.math.BigDecimal getWasserentnahmeentgeltFromAggregierteBerechnung(de.kommone.webservice.wee.Berechnungsergebnis source){
if(source == null) return null;return source.getWasserentnahmeentgelt();
}private java.lang.String getAbsenderBankVerbindungKontoinhaberFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderBankVerbindungKontoinhaberFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderBankVerbindungKontoinhaberFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderBankVerbindungKontoinhaberFromBankverbindung(source.getBankverbindung()
);
}private java.lang.String getAbsenderBankVerbindungKontoinhaberFromBankverbindung(de.kommone.webservice.wee.Bankverbindung source){
if(source == null) return null;return source.getKontoinhaber();
}private java.lang.String getAbsenderBankVerbindungBicFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderBankVerbindungBicFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderBankVerbindungBicFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderBankVerbindungBicFromBankverbindung(source.getBankverbindung()
);
}private java.lang.String getAbsenderBankVerbindungBicFromBankverbindung(de.kommone.webservice.wee.Bankverbindung source){
if(source == null) return null;return source.getBic();
}private java.lang.String getEmpfaengerNameFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getEmpfaengerNameFromEmpfaenger(source.getEmpfaenger()
);
}private java.lang.String getEmpfaengerNameFromEmpfaenger(de.kommone.webservice.wee.Behoerde source){
if(source == null) return null;return getEmpfaengerNameFromBehoerdenname(source.getBehoerdenname()
);
}private java.lang.String getEmpfaengerNameFromBehoerdenname(de.kommone.webservice.wee.NameOrganisation source){
if(source == null) return null;return source.getName();
}private int getJahrFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return 0;return source.getJahr();
}private java.lang.String getAbsenderKontaktDatenKanalFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderKontaktDatenKanalFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderKontaktDatenKanalFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderKontaktDatenKanalFromKontaktDaten(source.getKontaktDaten()
);
}private java.lang.String getAbsenderKontaktDatenKanalFromKontaktDaten(de.kommone.webservice.wee.Kommunikation source){
if(source == null) return null;return getAbsenderKontaktDatenKanalFromKanal(source.getKanal()
);
}private java.lang.String getAbsenderKontaktDatenKanalFromKanal(de.kommone.webservice.wee.CodeKanal source){
if(source == null) return null;return source.getCode();
}private java.lang.String getDefaultListVersionIDFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getDefaultListVersionIDFromAbsender(source.getAbsender()
);
}private java.lang.String getDefaultListVersionIDFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getDefaultListVersionIDFromKontaktDaten(source.getKontaktDaten()
);
}private java.lang.String getDefaultListVersionIDFromKontaktDaten(de.kommone.webservice.wee.Kommunikation source){
if(source == null) return null;return getDefaultListVersionIDFromKanal(source.getKanal()
);
}private java.lang.String getDefaultListVersionIDFromKanal(de.kommone.webservice.wee.CodeKanal source){
if(source == null) return null;return source.getListVersionID();
}private java.math.BigDecimal getGeleisteteVorauszahlungenFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return source.getGeleisteteVorauszahlungen();
}private java.math.BigDecimal getZuzahlenderBetragFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getZuzahlenderBetragFromAggregierteBerechnung(source.getAggregierteBerechnung()
);
}private java.math.BigDecimal getZuzahlenderBetragFromAggregierteBerechnung(de.kommone.webservice.wee.Berechnungsergebnis source){
if(source == null) return null;return source.getZuzahlenderBetrag();
}private java.math.BigDecimal getErsteKuenftigeVorauszahlungFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getErsteKuenftigeVorauszahlungFromAggregierteBerechnung(source.getAggregierteBerechnung()
);
}private java.math.BigDecimal getErsteKuenftigeVorauszahlungFromAggregierteBerechnung(de.kommone.webservice.wee.Berechnungsergebnis source){
if(source == null) return null;return source.getErsteKuenftigeVorauszahlung();
}private java.math.BigDecimal getZweiteKuenftigeVorauszahlungFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getZweiteKuenftigeVorauszahlungFromAggregierteBerechnung(source.getAggregierteBerechnung()
);
}private java.math.BigDecimal getZweiteKuenftigeVorauszahlungFromAggregierteBerechnung(de.kommone.webservice.wee.Berechnungsergebnis source){
if(source == null) return null;return source.getZweiteKuenftigeVorauszahlung();
}private java.lang.String getAbsenderBankVerbindungBankinstitutFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getAbsenderBankVerbindungBankinstitutFromAbsender(source.getAbsender()
);
}private java.lang.String getAbsenderBankVerbindungBankinstitutFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return getAbsenderBankVerbindungBankinstitutFromBankverbindung(source.getBankverbindung()
);
}private java.lang.String getAbsenderBankVerbindungBankinstitutFromBankverbindung(de.kommone.webservice.wee.Bankverbindung source){
if(source == null) return null;return source.getBankinstitut();
}private java.math.BigDecimal getEntgeltM3FromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getEntgeltM3FromAggregierteBerechnung(source.getAggregierteBerechnung()
);
}private java.math.BigDecimal getEntgeltM3FromAggregierteBerechnung(de.kommone.webservice.wee.Berechnungsergebnis source){
if(source == null) return null;return source.getEntgeltM3();
}private java.lang.Boolean getSEPAerteiltFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return source.isSepAerteilt();
}private java.lang.Boolean getSEPAzuvorerklaertFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return source.isSepAzuvorerklaert();
}private java.lang.String getSbwUserIdFromWeeErklaerung(de.kommone.webservice.wee.WeeErklaerung source){
if(source == null) return null;return getSbwUserIdFromAbsender(source.getAbsender()
);
}private java.lang.String getSbwUserIdFromAbsender(de.kommone.webservice.wee.WeeEntgeltpflichtiger source){
if(source == null) return null;return source.getSbwUserId();
}}
