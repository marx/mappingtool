# MappingTool / KM-Connect-Tools
Das MappingTool ist das meistgenutzte Werkzeug, das in KM-Connect zum Einsatz gebracht wird, um eingehende Anfragen auf XOEV-Standards zu mappen. Dies ist ein fachliches Mapping, das durch ein JavaFX-Werkzeug unterstützt wird. Dieses JavaFX-Werkzeug wird zum visuellen Zuordnen (= "Mapping", hat nichts mit Kartografie zu tun) von Datenfeldern auf Elemente eines XML- Schemas genutzt. Zugeordnet werden statische Schnittstellenobjekte, die Teil einer WSDL sind, auf sich regelmäßig verändernde Schemaobjekte. Die Entwicklung des MappingTool ist so weit abgeschlossen und es wird zum aktuellen Zeitpunkt nicht aktiv weiterentwickelt (FeatureFreeze), kann aber natürlich gerne (geforked) weiterentwickelt oder auf die Anforderungen anderer Stakeholder angepasst oder erweitert werden. 

## Weitere Werkzeuge
Neben dem km-connect-tools-maptool umfasst dieses Projekt weitere Tools, die für die Entwicklung von KM-Connect wichtig sind bzw. diese massiv vereinfachen können. 

### km-connect-tools-xsd-fetcher

Ein Tool (es gibt hier aktuell nur eine Main- Methode) zum Download von xsd- Dateien, die innerhalb einer (oder mehrerer) xsd(s) referenziert werden. Die Dateien werden rekursiv heruntergeladen und lokal gespeichert und werden dann in einem catalog-file referenziert.

### km-connect-tools-xta1-client

Ein Tool zum Ansprechen eines XTA1- Webservice (= WS- Client), das Nachrichten abholen kann, die der WS bereitstellt.

### km-connect-tools-xta-ws

Ein Mock- Webservice, der die XTA- Funktionalität mockt. Dieser WS lässt sich aktuell auf einem Glassfish-Application-Server (Version 4.2.1 funktioniert) deployn. Das Deployment auf einem Tomcat schlägt leider fehl, da verschiedene xsd- Dateien denselben Target-Namespace haben. Damit kommt die Tomcat- interne JAX-WS-Implementierung nicht klar.
