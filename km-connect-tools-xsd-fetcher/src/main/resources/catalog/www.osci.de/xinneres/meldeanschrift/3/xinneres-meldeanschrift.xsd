<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xima="http://www.osci.de/xinneres/meldeanschrift/3" xmlns:xicgvz="http://www.osci.de/xinneres/codes/gemeindeverzeichnis/2" xmlns:xoev-lc="http://xoev.de/latinchars/1_1/datatypes" targetNamespace="http://www.osci.de/xinneres/meldeanschrift/3" version="3" elementFormDefault="unqualified" attributeFormDefault="unqualified">
  <xs:annotation>
    <xs:appinfo>
      <standard>
        <nameLang>XInneres-Basismodul</nameLang>
        <nameKurz>XInneres-Basismodul</nameKurz>
        <nameTechnisch>xinneres.basismodul</nameTechnisch>
        <kennung>urn:xoev-de:kosit:standard:xinneres.basismodul</kennung>
        <beschreibung>Im Auftrag des AK I hat die PG Standard bisher unter anderem die fachlichen Vorgaben und die Struktur der Meldeanschrift für die Bereiche Ausländer-, Melde- und Personenstandswesen harmonisiert. Der vorliegende Standard XInneres bildet auf technischer Ebene eine Klammer vor den Standards der Innenverwaltung. An dieser Stelle können gemeinsam genutzte Komponenten definiert werden. In der ersten Version von XInneres definiert der Standard die Datentypen für die Darstellung der Meldeanschrift.</beschreibung>
      </standard>
      <versionStandard>
        <version>6</version>
        <versionXOEVHandbuch>2.1</versionXOEVHandbuch>
        <versionXGenerator>2.6.1</versionXGenerator>
        <versionModellierungswerkzeug>18.0 SP6</versionModellierungswerkzeug>
        <nameModellierungswerkzeug>MagicDraw</nameModellierungswerkzeug>
      </versionStandard>
    </xs:appinfo>
  </xs:annotation>
  <xs:import schemaLocation="http://www.osci.de/xinneres/codes/gemeindeverzeichnis/2/xinneres-codes-gemeindeverzeichnis.xsd" namespace="http://www.osci.de/xinneres/codes/gemeindeverzeichnis/2" />
  <xs:import schemaLocation="http://xoev.de/latinchars/1_1/datatypes/latinchars.xsd" namespace="http://xoev.de/latinchars/1_1/datatypes" />
  <xs:complexType name="Meldeanschrift">
    <xs:annotation>
      <xs:appinfo>
        <title>Datentyp für eine Meldeanschrift</title>
      </xs:appinfo>
      <xs:documentation>Dieser Datentyp repräsentiert die gemeinsamen fachlichen Vorgaben der drei Standardisierungsbereiche Meldewesen, Ausländerwesen und Personenstandswesen für eine inländische Meldeanschrift auf der Grundlage des DSMeld.
Hinweis zu Hausnummernbereichen: Der DSMeld kennt keine Hausnummernbereiche. In diesen Fällen ist nur das erste Element des Hausnummernbereichs im Feld hausnummer einzutragen. Das zweite Element des Hausnummernbereichs kann in diesem Datentyp nicht übermittelt werden.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="gemeindeschluessel" minOccurs="0" type="xicgvz:Code.GemeindeVZ.AmtlicherGemeindeschluessel">
        <xs:annotation>
          <xs:appinfo>
            <implementationHint>
              <para>
                Solange die Aktualität der verwendeten Schlüsseltabelle nicht gegeben ist, wird vollständig auf die Plausibilisierung der
                <emphasis>listVersionID</emphasis>
                verzichtet.
              </para>
            </implementationHint>
          </xs:appinfo>
          <xs:documentation>Es ist der vom Statistischen Bundesamt herausgegebene bundeseinheitliche Gemeindeschlüssel der Gemeinde anzugeben, in der die Wohnung liegt.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="hausnummer" minOccurs="0" type="xima:Meldeanschrift.Hausnummer">
        <xs:annotation>
          <xs:documentation>Es sind nur die Ziffern einer Hausnummer anzugeben.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="hausnummerBuchstabeZusatzziffer" minOccurs="0" type="xima:Meldeanschrift.HausnummerBuchstabeZusatzziffer">
        <xs:annotation>
          <xs:documentation>Es sind Buchstaben oder Zusatzziffern zur Hausnummer anzugeben.
Beispiel: 124 A, 109.5</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="postleitzahl" minOccurs="0" type="xima:Meldeanschrift.Postleitzahl">
        <xs:annotation>
          <xs:documentation>Es ist die Postleitzahl anzugeben.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="stockwerkswohnungsnummer" minOccurs="0" type="xima:Meldeanschrift.Stockwerkswohnungsnummer">
        <xs:annotation>
          <xs:documentation>Es können Stockwerks- oder Wohnungsnummern angegeben werden, soweit sie für die Adressierung erforderlich sind. Beispiele: 7OG, 13OG, P für Parterre, HP für Hochparterre, St für Souterrain oder (Wohnung) 115.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="strasse" minOccurs="0" type="xima:Meldeanschrift.Strasse">
        <xs:annotation>
          <xs:documentation>Es ist die Bezeichnung der Straße in der amtlichen Schreibweise anzugeben.
Bei Überschreitung einer Feldlänge von 55 Zeichen muss sinnvoll abgekürzt werden.
Ist keine Straßenbezeichnung - wohl aber eine Hausnummer - vorhanden, so ist die Zeichenkette Hausnummer anzugeben. Sind weder Straßenbezeichnung noch Hausnummer vorhanden, so ist die Zeichenkette ohne Hausnummer anzugeben.
Zusätze, die nicht der Straßenbezeichnung dienen, sind nicht zulässig. Soweit Angaben wie z. B. Weg A 2 und 12 oder Weg B zur Adressierung benötigt werden, sind diese im Element zusatzangaben zu übermitteln.
Siehe DSMeld-Blatt 1205.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="teilnummerDerHausnummer" minOccurs="0" type="xima:Meldeanschrift.TeilnummerDerHausnummer">
        <xs:annotation>
          <xs:documentation>Es sind Teilnummern zur Hausnummer anzugeben.
Beispiel: 16 1/7</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="wohnort" minOccurs="0" type="xima:Meldeanschrift.Wohnort">
        <xs:annotation>
          <xs:documentation>Es ist die postalische Wohnortsbezeichnung anzugeben.

Bei Überschreitung einer Länge von 25 Zeichen darf sinnvoll abgekürzt werden.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="wohnortFruehererGemeindename" minOccurs="0" type="xima:Meldeanschrift.WohnortFruehererGemeindename">
        <xs:annotation>
          <xs:documentation>Es ist der frühere Gemeindename anzugeben, der als Stadt- bzw. Ortsteilname dem jetzigen Gemeindenamen hinzugefügt werden kann.Der frühere Gemeindename (jetziger Ortsteil- oder Stadtteilname) ist bei Adressierungen unterhalb des Namens (oberhalb der Straßenbezeichnung) anzugeben.Beispiel: Frau               Rita Scholl               Zuffenhausen              Am Stadtpark 12              70123 Stuttgart

Bei Überschreitung einer Länge von 25 Zeichen darf sinnvoll abgekürzt werden.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="wohnungsinhaber" minOccurs="0" type="xima:Meldeanschrift.Wohnungsinhaber">
        <xs:annotation>
          <xs:documentation>In diesem Element ist der Hauptmieter oder Eigentümer der Wohnung anzugeben, soweit dies für die Adressierung erforderlich ist. 

Bei Überschreitung einer Länge von 26 Zeichen darf sinnvoll abgekürzt werden.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="zusatzangaben" minOccurs="0" type="xima:Meldeanschrift.Zusatzangaben">
        <xs:annotation>
          <xs:documentation>Es sind Zusatzangaben zur Anschrift anzugeben. Beispiele: Hinterhaus, Gartenhaus.

Bei Überschreitung einer Länge von 21 Zeichen darf sinnvoll abgekürzt werden.</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="Meldeanschrift.Hausnummer">
    <xs:annotation>
      <xs:documentation>Dies ist der Typ für das Element 'hausnummer' im Typ Meldeanschrift.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xoev-lc:String.Latin">
      <xs:maxLength value="4" />
      <xs:pattern value="[0-9 ]*" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="Meldeanschrift.HausnummerBuchstabeZusatzziffer">
    <xs:annotation>
      <xs:documentation>Dies ist der Typ für das Element 'buchstabeZusatzziffer' im Typ Meldeanschrift.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xoev-lc:String.Latin">
      <xs:maxLength value="2" />
      <xs:pattern value="[\p{L}0-9. ]*" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="Meldeanschrift.Postleitzahl">
    <xs:annotation>
      <xs:documentation>Dies ist der Typ für das Element 'postleitzahl' im Typ Meldeanschrift.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xoev-lc:String.Latin">
      <xs:maxLength value="5" />
      <xs:pattern value="[0-9 ]*" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="Meldeanschrift.Stockwerkswohnungsnummer">
    <xs:annotation>
      <xs:documentation>Dies ist der Typ für das Element stockwerkswohnungsnummer' im Typ Meldeanschrift.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xoev-lc:String.Latin">
      <xs:maxLength value="4" />
      <xs:pattern value="[\p{L}0-9 .]*" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="Meldeanschrift.Strasse">
    <xs:annotation>
      <xs:documentation>Dies ist der Typ für das Element 'strasse' im Typ Meldeanschrift.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xoev-lc:String.Latin" />
  </xs:simpleType>
  <xs:simpleType name="Meldeanschrift.TeilnummerDerHausnummer">
    <xs:annotation>
      <xs:documentation>Dies ist der Typ für das Element 'teilnummerDerHausnummer' im Typ Meldeanschrift.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xoev-lc:String.Latin" />
  </xs:simpleType>
  <xs:simpleType name="Meldeanschrift.Wohnort">
    <xs:annotation>
      <xs:documentation>Dies ist der Typ für das Element 'wohnort' im Typ Meldeanschrift.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xoev-lc:String.Latin" />
  </xs:simpleType>
  <xs:simpleType name="Meldeanschrift.WohnortFruehererGemeindename">
    <xs:annotation>
      <xs:documentation>Dies ist der Typ für das Element 'fruehererGemeindename' im Typ Meldeanschrift.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xoev-lc:String.Latin" />
  </xs:simpleType>
  <xs:simpleType name="Meldeanschrift.Wohnungsinhaber">
    <xs:restriction base="xoev-lc:String.Latin" />
  </xs:simpleType>
  <xs:simpleType name="Meldeanschrift.Zusatzangaben">
    <xs:annotation>
      <xs:documentation>Dies ist der Typ für das Element 'zusatzangaben' im Typ Meldeanschrift.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xoev-lc:String.Latin" />
  </xs:simpleType>
</xs:schema>

