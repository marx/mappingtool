package de.kommone.webservice;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class Main extends Application {

    private XMLParseHandler handler = new XMLParseHandler();

    private List<String> visitedFiles = new ArrayList<>();

    private Map<String, String> catalogMapping = new HashMap<>();

    private List<String> messages = new ArrayList<>();
    private List<String> errors = new ArrayList<>();

    public XMLParseHandler getHandler() {
        return handler;
    }

    public void setHandler(XMLParseHandler handler) {
        this.handler = handler;
    }

    private Stage myStage;

    @FXML
    public Menu newMessageMenu;

    @FXML
    private TextField url;

    @FXML
    private TextArea textArea;

    @FXML
    private Label chosenPath;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @FXML
    public void initialize() {
        textArea.setWrapText(true);
    }

    @FXML
    private void loadXSDS() {
        String absolutePath = chosenPath.getText();
        if (absolutePath == null || absolutePath.isEmpty()) {
            // TODO : Fehlermeldung ausgeben
        }
        String relativePath = url.getText();
        Main main = fetch(absolutePath, relativePath);
        textArea.setText("");
        String text = "Meldungen: \n";
        int counter = 1;
        for (String message : main.getMessages()) {
            text += counter++ + ". " + message + "\n";
        }

        text += "Fehler: \n";
        counter = 1;
        for (String error : main.getErrors()) {
            text += counter++ + ". " + error + "\n";
        }
        text += "Inhalt vom Catalog: \n";
        text += main.printCatalog();
        textArea.setText(text);
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    @FXML
    private void choseDirectory() {

        DirectoryChooser directory = new DirectoryChooser();
        File file = directory.showDialog(myStage);
        if (file == null) {
            return;
        }
        chosenPath.setText(file.getAbsolutePath());
    }

    @Override
    public void start(Stage stage) throws IOException, ClassNotFoundException, URISyntaxException {

        final BorderPane application = (BorderPane) new FXMLLoader(
                getClass().getClassLoader().getResource("XSDFetcher.fxml")).load();

        final Scene scene = new Scene(application, 1550, 900);
        scene.setFill(Color.LIGHTGRAY);

        myStage = stage;
        myStage.setScene(scene);
        myStage.setTitle("XSD Fetcher");
        myStage.show();
    }

    private Main fetch(String path, String relativePath) {
        Main main = new Main();
        messages.clear();
        errors.clear();
        main.fetchXSDs(path + "/", relativePath + "/");
        return main;
    }

    public void fetchXSDs(String rootPath, String outputRelativeToRoot) {

        Path path = Paths.get(rootPath);
        List<Path> paths = new ArrayList<>();
        try {
            paths = listFiles(path);
        } catch (IOException e) {
            errors.add(e.getMessage());
        }
        paths.forEach(x -> {
            fetchXSDs(rootPath, x.getFileName().toString(), outputRelativeToRoot, true);
        });

        saveToFile(rootPath + "catalog.cat", printCatalog());

    }

    // list all files from this path
    private List<Path> listFiles(Path path) throws IOException {

        List<Path> result;
        try (Stream<Path> walk = Files.walk(path)) {
            result = walk.filter(Files::isRegularFile).collect(Collectors.toList());
        }
        return result;

    }

    public void fetchXSDs(String rootPath, String relativeToRoot, String outputRelativeToRoot, boolean isFirst) {

        // zunächst die alten Dateien löschen
        handler.initHandler();
        String filePath = isFirst ? rootPath + relativeToRoot : rootPath + outputRelativeToRoot + relativeToRoot;
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            if (filePath.contains("catalog.xml") || filePath.contains("catalog.cat")) {
                return;
            }
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(new File(filePath), handler);
        } catch (Exception e) {
            errors.add("Lesen von " + filePath + " fehlgeschlagen: " + e.getMessage());
            return;
        }

        // 1. Schritt : Dateien holen

        // 1. 1 : URLs holen
        List<String> urls = getHandler().getUrls();

        List<String> newFilesToVisit = new ArrayList<>();

        // 1. 2 : Strings über HTTP-Connection holen und ...
        HttpHelper helper = new HttpHelper();
        for (String url : urls) {

            // get the fileContent first
            String fileContent = null;
            try {
                fileContent = helper.getStringFromURL(url);
            } catch (IllegalStateException e) {
                errors.add(url + " konnte nicht gelesen werden");
                continue;
            }

            // derive the fileName
            String fileName = url.replace("http://", "");
            fileName = fileName.replace("https://", "");
            String catalogEntryKey = fileName.substring(0, fileName.indexOf('/'));
            String catalogEntryValue = catalogEntryKey;
            catalogEntryKey = outputRelativeToRoot + catalogEntryKey;
            if (!catalogMapping.containsKey(catalogEntryKey)) {
                if (url.startsWith("http")) {
                    catalogEntryValue = "http://" + catalogEntryValue;
                    catalogMapping.put(catalogEntryKey, catalogEntryValue);
                } else if (url.startsWith("https://")) {
                    catalogEntryValue = "https://" + catalogEntryValue;
                    catalogMapping.put(catalogEntryKey, catalogEntryKey);
                }
            }
            String path = rootPath + outputRelativeToRoot + fileName;

            if (visitedFiles.contains(url)) {
                continue;
            } else {
                visitedFiles.add(url);
                newFilesToVisit.add(fileName);
            }

            messages.add("Download von " + url);

            // 1. 3 : Dateien speichern
            saveToFile(path, fileContent);
        }

        // 2. Schritt : für jede gespeicherte Datei die ersten Schritte
        // wiederholen
        for (String newFile : newFilesToVisit) {
            fetchXSDs(rootPath, newFile, outputRelativeToRoot, false);
        }

    }

    private void saveToFile(String path, String fileContent) {
        final File parent = new File(path.substring(0, path.lastIndexOf('/') + 1));
        if (parent.exists()) {
            // nichts zu tun
        } else if (!parent.mkdirs()) {
            errors.add("Could not create parent directories ");
        }
        final File savedFile = new File(path);
        try (FileWriter writer = new FileWriter(savedFile)) {
            writer.write(fileContent);
            writer.flush();
        } catch (IOException e) {
            errors.add("Datei " + path + " konnte nicht gespeichert werden " + e.getMessage());
        }
    }

    public String printCatalog() {
        String catalog = "";
        Set<String> set = catalogMapping.keySet();
        for (String key : set) {
            catalog += "REWRITE_SYSTEM " + "\"" + catalogMapping.get(key) + "\" " + "\"" + key + "\"\n";
        }
        return catalog;
    }

}
