package de.kommone.webservice;

import java.util.*;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLParseHandler extends DefaultHandler {

    private List<String> urls = new ArrayList<>();

    public void initHandler() {
        urls.clear();
    }

    private boolean read = false;
    private boolean readKey = true;
    private String currentKey = null;
    private String currentValue = null;

    private Map<String, String> keyValue = new HashMap<>();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.contains("import")) {
            String value = attributes.getValue("schemaLocation");
            urls.add(value);
        }
        read = qName != null && qName.equals("SimpleValue");
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(qName.equals("SimpleValue")) {
            read = false;
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        if(read) {
            String readString = new String(ch, start, length);
            if(readKey) {
                readKey = false;
                currentKey = readString;
            } else {
                readKey = true;
                currentValue = readString;
                keyValue.put(currentKey, currentValue);
            }

        }
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public Map<String, String> getMap() {
        return this.keyValue;
    }

}
