package de.kommone.webservice;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ListenproviderHelper {

    public static void main(String[] args) {
        XMLParseHandler handler = new XMLParseHandler();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(new File("C:\\Projekte\\listen-provider-service\\src\\main\\resources\\urn_xoev-de_xbau_bw_codeliste_erleichterung_gegenstand\\urn_xoev-de_xbau_bw_codeliste_erleichterung_gegenstand.xml"), handler);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        Map<String, String> xml = handler.getMap();
        System.out.println(printXMLFromMap(xml));
    }

    private static String printXMLFromMap(Map<String, String> xml) {
        StringBuilder builder = new StringBuilder();
        builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<CodeList>");
        Set<String> set = xml.keySet();
        List<String> sortedKeys = set.stream().sorted().collect(Collectors.toList());
        for(String entry : sortedKeys) {
            builder.append("<Row>\n" +
                    "    <code>");
            builder.append(entry);
            builder.append("</code>\n" +
                    "    <beschreibung>");
            builder.append(xml.get(entry));
            builder.append("</beschreibung>\n" +
                    "  </Row>");
        }
        builder.append("</CodeList>");
        return builder.toString();
    }
}
