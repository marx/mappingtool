package de.kommone.webservice;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class HttpHelper {

    public String getStringFromURL(String url) {
        try {
            int proxyPort = 3128;
            InetSocketAddress proxyInet = new InetSocketAddress("squid.dzbw.de", proxyPort);
            Proxy proxy = new Proxy(Proxy.Type.HTTP, proxyInet);

            // Http(s)- Connection je nach Konfiguration
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection(proxy);
            connection.setRequestMethod("GET");

            // Input und Output möglich
            connection.setDoInput(true);
            connection.setDoOutput(true);
            byte[] buffer = new byte[4096];
            InputStream in = connection.getInputStream();
            return readResponse(StandardCharsets.UTF_8.toString(), buffer, in);
        } catch (ProtocolException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    /**
     * Liest aus dem Error- Stream der Connection
     *
     * @param connection
     * @param charset
     * @return
     * @throws IOException
     */
    public static String readResponseFromErrorStream(HttpURLConnection connection, String charset) throws IOException {

        byte[] buffer = new byte[4096];
        InputStream in = connection.getErrorStream();
        return in != null ? readResponse(charset, buffer, in) : new String();
    }

    /**
     * Liest einen Input- Stream aus. Dieser kann dabei als Error- oder
     * InputStream übergeben werden
     *
     * @param charset
     * @param buffer
     * @param in
     * @return
     * @throws IOException
     * @throws UnsupportedEncodingException
     */
    private static String readResponse(String charset, byte[] buffer, InputStream in) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int n = 0;
        if (in == null) {
            throw new IllegalStateException("Inputstream ist null");
        }
        while ((n = in.read(buffer)) != -1) {
            out.write(buffer, 0, n);
        }
        in.close();
        return new String(out.toByteArray(), charset);
    }

}
